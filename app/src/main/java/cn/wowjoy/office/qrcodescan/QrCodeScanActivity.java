package cn.wowjoy.office.qrcodescan;

import android.os.Build;
import android.os.Bundle;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.databinding.ActivityQrCodeScanBinding;



public class QrCodeScanActivity extends NewBaseActivity<ActivityQrCodeScanBinding,QrCodeScanViewModel> implements QRCodeView.Delegate {


    @Override
    protected void init(Bundle savedInstanceState) {

        binding.setViewmodel(viewModel);

        setSupportActionBar(binding.toolbar);
        binding.qrCodeScanner.setDelegate(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.qrCodeScanner.startSpot();
    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.qrCodeScanner.stopCamera();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        binding.toolbar.setTitle("");
    }

    @Override
    protected Class<QrCodeScanViewModel> getViewModel() {
        return QrCodeScanViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_qr_code_scan;
    }



    @Override
    public void onScanQRCodeSuccess(String result) {
        binding.toolbar.setNavigationIcon(R.mipmap.return_gray);
        binding.toolbarTitle.setTextColor(ContextCompat.getColor(this,R.color.homepageServerText1));
        binding.rootview.setBackgroundColor(ContextCompat.getColor(this,R.color.white));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            binding.toolbar.setBackground(ContextCompat.getDrawable(this,R.drawable.toolbar_bottom_line));
        else
            binding.toolbar.setBackgroundColor(ContextCompat.getColor(this,R.color.white));
        binding.qrCodeScanner.setVisibility(View.GONE);
        binding.result.setVisibility(View.VISIBLE);
        binding.qrCodeScanner.stopCamera();
        binding.result.setText(result);
        Toast.makeText(this,result,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onScanQRCodeOpenCameraError() {
        Toast.makeText(this,"扫码失败",Toast.LENGTH_SHORT).show();
    }
}
