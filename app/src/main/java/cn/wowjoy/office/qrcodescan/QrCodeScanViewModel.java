package cn.wowjoy.office.qrcodescan;

import android.support.annotation.NonNull;

import javax.inject.Inject;


import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Sherily on 2017/9/8.
 */

public class QrCodeScanViewModel extends NewBaseViewModel {
    @Inject
    public QrCodeScanViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
