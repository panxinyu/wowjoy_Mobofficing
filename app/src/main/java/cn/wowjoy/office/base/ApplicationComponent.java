package cn.wowjoy.office.base;

import javax.inject.Singleton;

import cn.wowjoy.office.data.local.GreenDaoModule;
import cn.wowjoy.office.data.local.entity.DaoSession;
import cn.wowjoy.office.data.local.entity.UserInfoDao;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.LoginService;
import cn.wowjoy.office.data.remote.RestModule;
import cn.wowjoy.office.data.remote.UpdateService;
import cn.wowjoy.office.utils.PreferenceManager;
import dagger.Component;

/**
 * Created by Sherily on 2017/8/23.
 * Description: application component, only create in {@link MainApplication}
 */
@Singleton
@Component(modules = {ApplicationMoudle.class, RestModule.class, GreenDaoModule.class})
public interface ApplicationComponent {
//    void inject(BaseActivity activity);

    //向子组件曝露注入的全局对象
    LoginService getLoginService();
    ApiService getApiService();
    UpdateService getUpdateService();
    PreferenceManager getPreferenceManager();

    DaoSession daosession();
    UserInfoDao userInfoDao();


}
