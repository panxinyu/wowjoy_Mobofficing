package cn.wowjoy.office.base;

import android.os.Process;
import android.os.StrictMode;

import com.zxy.tiny.Tiny;

import cn.wowjoy.office.data.local.GreenDaoModule;
import cn.wowjoy.office.data.remote.RestModule;
import cn.wowjoy.office.utils.PreferenceManager;

/**
 * Created by Sherily on 2017/8/23.
 */

public class MainApplication extends BaseApplication {

    private static ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDaggerApplicationComponent();
//        instance = this;
        PreferenceManager.init(this);
        //Android7.0的照片问题
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Tiny.getInstance().init(this);
        new Thread() {
            @Override
            public void run() {
                super.run();
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
//                Fresco.initialize(instance, ImagePipelineConfigFactory.getOkHttpImagePipelineConfig(instance));

            }
        }.start();

    }
    private void initDaggerApplicationComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationMoudle(new ApplicationMoudle(this))
                .greenDaoModule(new GreenDaoModule(this))
                .restModule(new RestModule())
                .build();
    }

    public ApplicationComponent getmApplicationComponent() {
        return mApplicationComponent;
    }

    public static void setmApplicationComponent(ApplicationComponent mApplicationComponent) {
        MainApplication.mApplicationComponent = mApplicationComponent;
    }
}
