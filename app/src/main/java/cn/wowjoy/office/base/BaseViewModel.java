package cn.wowjoy.office.base;

import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Sherily on 2017/8/23.
 * Description: viewmodel with base function
 */

public abstract class BaseViewModel extends BaseObservable{

    public ObservableBoolean isRefreshing = new ObservableBoolean(false);
    public ObservableBoolean isLoading = new ObservableBoolean(true);
    public ObservableBoolean isEmpty = new ObservableBoolean(false);
    public ObservableBoolean isError = new ObservableBoolean(false);

    public RecyclerView.LayoutManager layoutManager = null;

    public void refreshComplete() {
        isRefreshing.set(true);
        isRefreshing.set(false);
    }

    public void setState(boolean isLoading, boolean isEmpty, boolean isError) {
        this.isLoading.set(isLoading);
        this.isEmpty.set(isEmpty);
        this.isError.set(isError);
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public ObservableBoolean getIsLoading() {
        return isLoading;
    }

    public ObservableBoolean getIsEmpty() {
        return isEmpty;
    }

    public ObservableBoolean getIsError() {
        return isError;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

}
