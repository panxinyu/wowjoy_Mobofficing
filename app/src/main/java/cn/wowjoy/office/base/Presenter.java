package cn.wowjoy.office.base;

import com.github.jdsjlzx.recyclerview.LRecyclerView;

import io.reactivex.disposables.Disposable;

/**
 * Created by Sherily on 2017/8/23.
 * Description:
 */

public interface Presenter {
    void onCreate();

    //解除依赖
    void onDestroy();

    void loadData(boolean ref);

    void addDisposable(Disposable disposable);

    void onFailure(Throwable e);
}
