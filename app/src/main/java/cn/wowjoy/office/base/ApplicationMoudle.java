package cn.wowjoy.office.base;

import android.content.Context;

import javax.inject.Singleton;

import cn.wowjoy.office.utils.PreferenceManager;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Sherily on 2017/8/23.
 */
@Module
public class ApplicationMoudle {

    private final Context mContext;

    public ApplicationMoudle(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    @Singleton
    Context provideContext(){
        return mContext;
    }

    @Provides
    @Singleton
    PreferenceManager providePreferenceManager(){
        PreferenceManager.init(mContext);
        return PreferenceManager.getInstance();
    }
}
