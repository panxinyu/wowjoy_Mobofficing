package cn.wowjoy.office.base;

import android.app.Activity;
import android.content.Context;

import com.github.jdsjlzx.recyclerview.LRecyclerView;

import cn.wowjoy.office.utils.LogUtils;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Sherily on 2017/8/23.
 * Description: presenter with base function
 */

public abstract class BasePresenter<VM extends BaseViewModel> implements Presenter {

    private CompositeDisposable mCompositeDisposable;
    //在每个子类中通过dagger注入的方式绑定viewmodel。


    public abstract void onCreatePresenter();

    /**
     * activity的presenter直接在生成对象时候调onCreate,Fragment建议不要在初始化时调用
     */
    @Override
    public void onCreate() {
        this.mCompositeDisposable = new CompositeDisposable();
        onCreatePresenter();
    }

    @Override
    public void onDestroy() {
        if (null != mCompositeDisposable) {
            this.mCompositeDisposable.clear();
            this.mCompositeDisposable = null;
        }
    }

    @Override
    public void loadData(boolean ref) {
    }

    @Override
    public void addDisposable(Disposable disposable) {
        if (null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }

    @Override
    public void onFailure(Throwable e) {
        if (null != e && null != e.getMessage())
            LogUtils.e(e.getMessage());
    }

    protected abstract VM getViewModel();
}
