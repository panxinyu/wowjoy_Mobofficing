package cn.wowjoy.office.base;

/**
 * Created by Sherily on 2017/11/9.
 * Description:
 */

public interface HashComponent<T> {
    T getComponent();
}
