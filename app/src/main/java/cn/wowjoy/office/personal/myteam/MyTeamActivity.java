package cn.wowjoy.office.personal.myteam;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.github.jdsjlzx.interfaces.OnItemClickListener;

import javax.inject.Inject;

import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.databinding.ActivityMyTeamBinding;


public class MyTeamActivity extends NewBaseActivity<ActivityMyTeamBinding,MyTeamViewModel> {



    @Override
    protected void onResume() {
        super.onResume();
        viewModel.myAppinfos();
        viewModel.wladapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                if (null !=  mMyTeamPresenter.getViewModel().datas){
//                    Intent mIntent = new Intent(MyTeamActivity.this, PatrolTaskActivity.class);
//                    mIntent.putExtra("model",  mMyTeamPresenter.getViewModel().datas.get(position));
//                    startActivity(mIntent);
//                }
            }
        });
    }
    @Override
    protected void init(Bundle savedInstanceState) {



        binding.setViewModel(viewModel);

        binding.titleMyteam.titleTextTv.setText("我的团队");
        binding.titleMyteam.titleBackLl.setVisibility(View.VISIBLE);
        binding.titleMyteam.titleBackTv.setText("");
        binding.titleMyteam.titleBackLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        binding.rvMyteam.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected Class<MyTeamViewModel> getViewModel() {
        return MyTeamViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_team;
    }

}
