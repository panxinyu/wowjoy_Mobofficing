package cn.wowjoy.office.personal.exchange;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.databinding.ActivityMyComPanyBinding;


public class MyCompanyActivity extends NewBaseActivity<ActivityMyComPanyBinding,MyCompanyViewModel> {




    @Override
    protected void init(Bundle savedInstanceState) {
//        StatusBarCompat.StatusBarLightMode(this);

        binding.setViewModel(viewModel);

        binding.titleMyinfo.titleTextTv.setText("我的公司");
        binding.titleMyinfo.titleBackLl.setVisibility(View.VISIBLE);
        binding.titleMyinfo.titleBackTv.setText("");
        binding.titleMyinfo.titleBackLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected Class<MyCompanyViewModel> getViewModel() {
        return MyCompanyViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_com_pany;
    }


}
