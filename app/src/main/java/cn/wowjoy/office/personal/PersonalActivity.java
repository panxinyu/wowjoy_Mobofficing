package cn.wowjoy.office.personal;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.StaffListResponse;
import cn.wowjoy.office.data.response.StaffResponse;
import cn.wowjoy.office.databinding.ActivityPersonalBinding;

import cn.wowjoy.office.personal.about.AboutActivity;
import cn.wowjoy.office.personal.alter.AlterActivity;
import cn.wowjoy.office.personal.myinfo.MyInfoActivity;


public class PersonalActivity extends NewBaseActivity<ActivityPersonalBinding,PersonalViewModel> implements View.OnClickListener {

    private StaffResponse staffResponse;
    private MDialog waitDialog;


    @Override
    protected void init(Bundle savedInstanceState) {


        binding.setViewModel(viewModel);

        binding.imgvBack.setOnClickListener(this);
        binding.rlAbout.setOnClickListener(this);
        binding.rlAlter.setOnClickListener(this);

        binding.workStatePersonal.setOnClickListener(this);
        viewModel.getStaffInfo().observe(this, new Observer<LiveDataWrapper<StaffListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StaffListResponse> staffListResponseLiveDataWrapper) {
                switch (staffListResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(PersonalActivity.this);
                        break;
                    case ERROR:
                        waitDialog.dismiss();
                        handleException(staffListResponseLiveDataWrapper.error,false);
                        break;
                    case SUCCESS:
                        waitDialog.dismiss();
                        setModel(staffListResponseLiveDataWrapper.data.getStaffResponses().get(0));
                        break;

                }
            }
        });
    }

    public void setModel(StaffResponse response){
        this.staffResponse = response;
        binding.setModel(response);
    }

    @Override
    protected Class<PersonalViewModel> getViewModel() {
        return PersonalViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_personal;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_about:
                startActivity(new Intent(PersonalActivity.this, AboutActivity.class));
                break;
            case R.id.rl_alter:
                //修改密码
                startActivity(new Intent(PersonalActivity.this, AlterActivity.class));
                break;
            case R.id.work_state_personal:
                //个人状态信息
                MyInfoActivity.openActivity(this,staffResponse);
//                startActivityForResult(new Intent(PersonalActivity.this,MyInfoActivity.class),101);
                break;
            case R.id.imgv_back:
                finish();
                break;
        }
    }
}
