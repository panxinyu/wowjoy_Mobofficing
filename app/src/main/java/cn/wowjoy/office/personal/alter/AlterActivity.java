package cn.wowjoy.office.personal.alter;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.databinding.ActivityAlterBinding;

public class AlterActivity extends NewBaseActivity<ActivityAlterBinding,AlterViewModel> implements View.OnClickListener {


    @Override
    protected void init(Bundle savedInstanceState) {

        binding.setViewModel(viewModel);

        binding.alterTitle.titleTextTv.setText("修改密码");
        binding.alterTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.alterTitle.titleBackTv.setText("");
        binding.alterTitle.titleBackLl.setOnClickListener(this);
        binding.alterTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        binding.alterTitle.titleMenuConfirm.setText("完成");
        binding.alterTitle.titleMenuConfirm.setTextColor(getResources().getColor(R.color.white));
        binding.alterTitle.titleMenuConfirm.setOnClickListener(this);
    }

    @Override
    protected Class<AlterViewModel> getViewModel() {
        return AlterViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_alter;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.title_back_ll:
                finish();
                break;
            case R.id.title_menu_confirm:
               //提交新密码进行保存

                break;
        }
    }
}
