package cn.wowjoy.office.personal.about;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.MyToast;
import cn.wowjoy.office.common.widget.UpdateDialog;
import cn.wowjoy.office.databinding.ActivityAboutBinding;

public class AboutActivity extends NewBaseActivity<ActivityAboutBinding,AboutViewModel> implements View.OnClickListener {


    private UpdateDialog updateDialog;
    private void showDialog(){
        if (null == updateDialog)
            updateDialog = new UpdateDialog.Builder()
                .setCancel("下次再说", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        updateDialog.dismissAllowingStateLoss();
                    }
                })
                .setConfirm("立即更新", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new MyToast(AboutActivity.this).showinfo("进入下载进程");
                        updateDialog.dismissAllowingStateLoss();
                    }
                })
                .create();
        updateDialog.show(getSupportFragmentManager(),null);


    }
    @Override
    protected void init(Bundle savedInstanceState) {


        binding.setViewModel(viewModel);

        binding.titleMyteam.titleTextTv.setText("关于百草园");
        binding.titleMyteam.titleBackLl.setVisibility(View.VISIBLE);
        binding.titleMyteam.titleBackTv.setText("");
        binding.titleMyteam.titleBackLl.setOnClickListener(this);
        binding.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    @Override
    protected Class<AboutViewModel> getViewModel() {
        return AboutViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.title_back_ll:
                finish();
                break;
        }
    }
}
