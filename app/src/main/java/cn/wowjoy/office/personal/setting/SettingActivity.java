package cn.wowjoy.office.personal.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.base.BaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.databinding.ActivitySettingBinding;

import cn.wowjoy.office.loginnew.LoginActivity;
import cn.wowjoy.office.personal.about.AboutActivity;

public class SettingActivity extends NewBaseActivity<ActivitySettingBinding,SettingViewModel> implements View.OnClickListener {



    @Override
    protected void init(Bundle savedInstanceState) {

        binding.setViewModel(viewModel);

        binding.titleMyteam.titleTextTv.setText("设置");
        binding.titleMyteam.titleBackLl.setVisibility(View.VISIBLE);
        binding.titleMyteam.titleBackTv.setText("");
        binding.titleMyteam.titleBackLl.setOnClickListener(this);

        binding.aboutSetting.setOnClickListener(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected Class<SettingViewModel> getViewModel() {
        return SettingViewModel.class;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.about_setting:
             startActivity(new Intent(SettingActivity.this, AboutActivity.class));
                break;
            case R.id.bt_exit_setting:
                startActivity(new Intent(SettingActivity.this, LoginActivity.class));
                break;
            case R.id.title_back_ll:
                finish();
                break;
        }
    }
}
