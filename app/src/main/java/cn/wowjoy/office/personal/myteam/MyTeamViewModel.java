package cn.wowjoy.office.personal.myteam;

import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.response.MyTeamInfo;
import cn.wowjoy.office.databinding.ItemRvMyteamBinding;

/**
 * Created by Administrator on 2017/11/7.
 */

public class MyTeamViewModel extends NewBaseViewModel {

    @Inject
    ApiService apiService;
    public ArrayList<MyTeamInfo> datas;

    public BGABindingRecyclerViewAdapter<MyTeamInfo, ItemRvMyteamBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_myteam);
    public LRecyclerViewAdapter wladapter = new LRecyclerViewAdapter(wlinnerAdapter);

    @Inject
    public MyTeamViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
    private List<MyTeamInfo> createTest(){
        List<MyTeamInfo> appInfos = new ArrayList<>();
        appInfos.add(new MyTeamInfo("精神科"));
        appInfos.add(new MyTeamInfo("A科"));
        appInfos.add(new MyTeamInfo("B科"));
        appInfos.add(new MyTeamInfo("C科"));
        appInfos.add(new MyTeamInfo("D科"));
        return appInfos;
    }
    public void myAppinfos(){
//        Log.e("'PXY'", "myAppinfos: "+ mMyTeamViewModel.toString());
        setWData(createTest());
    }
    public void setWData(List<MyTeamInfo> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        wlinnerAdapter.setData(datas);
        wladapter.removeFooterView();
        wladapter.removeHeaderView();
        wladapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateViewModel() {

    }
}
