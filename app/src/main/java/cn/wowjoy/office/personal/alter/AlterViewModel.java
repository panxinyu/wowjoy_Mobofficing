package cn.wowjoy.office.personal.alter;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2017/11/14.
 */

public class AlterViewModel extends NewBaseViewModel{

    @Inject
    public AlterViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
