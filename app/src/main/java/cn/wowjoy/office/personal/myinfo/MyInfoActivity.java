package cn.wowjoy.office.personal.myinfo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.StaffResponse;
import cn.wowjoy.office.databinding.ActivityMyInfoBinding;


public class MyInfoActivity extends NewBaseActivity<ActivityMyInfoBinding,MyInfoViewModel> {

    public static void openActivity(Context context, StaffResponse staffResponse){
        Intent intent = new Intent(context,MyInfoActivity.class);
        intent.putExtra(Constants.PERSONAL_INFO,staffResponse);
        context.startActivity(intent);

    }
    @Override
    protected void init(Bundle savedInstanceState) {


        binding.setViewModel(viewModel);

        StaffResponse staffResponse = (StaffResponse) getIntent().getSerializableExtra(Constants.PERSONAL_INFO);
        binding.setModel(staffResponse);
        binding.titleMyinfo.titleTextTv.setText("个人详情");
        binding.titleMyinfo.titleBackLl.setVisibility(View.VISIBLE);
        binding.titleMyinfo.titleBackTv.setText("");
        binding.titleMyinfo.titleBackLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected Class<MyInfoViewModel> getViewModel() {
        return MyInfoViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_info;
    }


}
