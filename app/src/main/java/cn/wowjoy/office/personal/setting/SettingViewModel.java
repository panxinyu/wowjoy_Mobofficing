package cn.wowjoy.office.personal.setting;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2017/11/7.
 */

public class SettingViewModel extends NewBaseViewModel {
    @Override
    public void onCreateViewModel() {

    }

    @Inject
    public SettingViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
}
