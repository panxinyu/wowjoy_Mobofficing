package cn.wowjoy.office.personal.myinfo;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2017/10/20.
 */

public class MyInfoViewModel extends NewBaseViewModel {
    @Inject
    public MyInfoViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
