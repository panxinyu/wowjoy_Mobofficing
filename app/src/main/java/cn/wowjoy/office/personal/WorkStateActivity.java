package cn.wowjoy.office.personal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;

public class WorkStateActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mTitleBackLl;
    private ImageView mTitleBackIv;
    private TextView mTitleBackTv;
    private TextView mTitleTextTv;
    private TextView mTitleMenuConfirm;
    private RelativeLayout mRlWorking;
    private RelativeLayout mRlTrip;
    private RelativeLayout mRlHoliday;
    private RelativeLayout mRlBusiness;
    private RelativeLayout mRlSick;
    private RelativeLayout mRlRest;
    private TextView mStateNowTextView;
    private RelativeLayout mRlState;
    private ImageView mImgvState;
    private TextView mTvStateNow;


    private List<RelativeLayout> mLayouts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_state);
        initView();
        initData();
    }


    private void initView() {
        mTitleBackLl = (LinearLayout) findViewById(R.id.title_back_ll);
        mTitleBackIv = (ImageView) findViewById(R.id.title_back_iv);
        mTitleBackTv = (TextView) findViewById(R.id.title_back_tv);
        mTitleTextTv = (TextView) findViewById(R.id.title_text_tv);
        mTitleMenuConfirm = (TextView) findViewById(R.id.title_menu_confirm);
        mRlWorking = (RelativeLayout) findViewById(R.id.rl_working);
        mRlTrip = (RelativeLayout) findViewById(R.id.rl_trip);
        mRlHoliday = (RelativeLayout) findViewById(R.id.rl_holiday);
        mRlBusiness = (RelativeLayout) findViewById(R.id.rl_business);
        mRlSick = (RelativeLayout) findViewById(R.id.rl_sick);
        mRlRest = (RelativeLayout) findViewById(R.id.rl_rest);
        mStateNowTextView = findViewById(R.id.state_hint);

        mRlState = (RelativeLayout) findViewById(R.id.rl_state);
        mImgvState = (ImageView) findViewById(R.id.imgv_state);
        mTvStateNow = (TextView) findViewById(R.id.tv_state_now);

        mLayouts = new ArrayList<>();
        mLayouts.add(mRlWorking);
        mLayouts.add(mRlTrip);
        mLayouts.add(mRlHoliday);
        mLayouts.add(mRlBusiness);
        mLayouts.add(mRlSick);
        mLayouts.add(mRlRest);

        for (int i = 0; i < mLayouts.size(); i++) {
            mLayouts.get(i).setOnClickListener(this);
        }
    }

    private void initData() {

        mTitleTextTv.setText("添加工作状态");
        mTitleBackLl.setVisibility(View.VISIBLE);
        mTitleBackTv.setText("");
        mTitleBackLl.setOnClickListener(this);

        mTitleMenuConfirm.setVisibility(View.VISIBLE);
        mTitleMenuConfirm.setText("保存");
        mTitleMenuConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_working:
//                 mStateNowTextView.setText("工作中");
                mStateNowTextView.setVisibility(View.GONE);
                mRlState.setVisibility(View.VISIBLE);
                mImgvState.setBackground(getDrawable(R.mipmap.work));
                mTvStateNow.setText("工作中");
                break;
            case R.id.rl_trip:
                mStateNowTextView.setVisibility(View.GONE);
                mRlState.setVisibility(View.VISIBLE);
                mImgvState.setBackground(getDrawable(R.mipmap.travel));
                mTvStateNow.setText("出差中");
                break;
            case R.id.rl_holiday:
                mStateNowTextView.setVisibility(View.GONE);
                mRlState.setVisibility(View.VISIBLE);
                mImgvState.setBackground(getDrawable(R.mipmap.holiday));
                mTvStateNow.setText("休假中");
                break;
            case R.id.rl_business:
                mStateNowTextView.setVisibility(View.GONE);
                mRlState.setVisibility(View.VISIBLE);
                mImgvState.setBackground(getDrawable(R.mipmap.business));
                mTvStateNow.setText("会议中");
                break;
            case R.id.rl_sick:
                mStateNowTextView.setVisibility(View.GONE);
                mRlState.setVisibility(View.VISIBLE);
                mImgvState.setBackground(getDrawable(R.mipmap.sick));
                mTvStateNow.setText("生病中");
                break;
            case R.id.rl_rest:
                mStateNowTextView.setVisibility(View.GONE);
                mRlState.setVisibility(View.VISIBLE);
                mImgvState.setBackground(getDrawable(R.mipmap.rest));
                mTvStateNow.setText("休息中");
                break;
            case R.id.title_back_ll:
                finish();
                break;
            case R.id.title_menu_confirm:
                //保存当前的签名到后台，然后返回设置的值到前面的页面

                break;
        }
    }
}
