package cn.wowjoy.office.materialmanage.addReport;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.RecordAdapter;
import cn.wowjoy.office.common.adapter.StockRecordAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.ResultEmpty;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.AddCountReportBySheetListRequest;
import cn.wowjoy.office.data.response.GoodsDetailResponse;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.data.response.StaffRecordResponse;
import cn.wowjoy.office.data.response.StockRecordResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Sherily on 2018/1/15.
 * Description:
 */

public class AddStockViewModel extends NewBaseViewModel {

    @Inject
    ApiService apiService;
    StockRecordAdapter adapter = new StockRecordAdapter();
    MediatorLiveData<LiveDataWrapper<StockRecordResponse>> result = new MediatorLiveData<>();
    MediatorLiveData<RecordResponse> edit = new MediatorLiveData<>();
    MediatorLiveData<RecordResponse> jumpTo = new MediatorLiveData<>();
    MediatorLiveData<RecordResponse> delete = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<ResultEmpty>> save = new MediatorLiveData<>();

    private String barCode;
    private String headCode;
    private String goodsCode;

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    @Inject
    public AddStockViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setmEventHandler(this);
    }

    @Override
    public void onCreateViewModel() {

    }

    public void addStock(String stock,String code){

    }

    public void save(){
        save.setValue(LiveDataWrapper.loading(null));
//        AddCountReportBySheetListRequest request = new AddCountReportBySheetListRequest();
//        request.setRequests(adapter.getSaveList());
        Disposable disposable = apiService.addDetailInfos(adapter.getSaveList())
                .flatMap(new ResultDataParse<ResultEmpty>())
                .compose(new RxSchedulerTransformer<ResultEmpty>())
                .subscribe(new Consumer<ResultEmpty>() {
                               @Override
                               public void accept(ResultEmpty resultEmpty) throws Exception {
                                   save.setValue(LiveDataWrapper.success(resultEmpty));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                save.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }

    public void jumpTo(RecordResponse recordResponse,int position){
        recordResponse.setEditIemPosition(position);
        jumpTo.setValue(recordResponse);
    }

    public void delete(RecordResponse recordResponse,int position){
        recordResponse.setEditIemPosition(position);
        delete.setValue(recordResponse);
    }

    public void editItemStock(RecordResponse recordResponse,int position){
        recordResponse.setNotifyPosition(position);
        edit.setValue(recordResponse);
    }
    private StockRecordResponse mock(){
        StockRecordResponse stockRecordResponse = new StockRecordResponse();
        GoodsDetailResponse response = new GoodsDetailResponse();
        response.setBarCode("25548826588");
        response.setName("医用干式激光胶片（柯尼卡）");
        response.setPrice("16");
        response.setSpecifications("SD - X 35 * 43（ 14 * 17）125片 / 盒， 4盒 / 片， 500片 / 盒， 600片");
        response.setUnitView("片");
        stockRecordResponse.setGoods(response);
        stockRecordResponse.setCount("4.00");
        stockRecordResponse.setMoney("64.00");
        List<RecordResponse> list = new ArrayList<>();
        RecordResponse recordResponse = new RecordResponse();
        recordResponse.setBatch("1785963");
        recordResponse.setIndate("2017/11/23");
        recordResponse.setStockCount("550.00");
        list.add(recordResponse);
        RecordResponse recordResponse1 = new RecordResponse();
        recordResponse1.setBatch("1785966");
        recordResponse1.setIndate("2017/11/25");
        recordResponse1.setStockCount("600.00");
        recordResponse1.setRealStockCount("120.00");
        List<StaffRecordResponse> list1 = new ArrayList<>();
        StaffRecordResponse response1 = new StaffRecordResponse();
        response1.setStaffName("吴春花");
        response1.setStaffNumber("17001");
        response1.setTime("2017/12/25");
        response1.setStockCount("30");
        list1.add(response1);
        StaffRecordResponse response2 = new StaffRecordResponse();
        response2.setStaffName("张意义");
        response2.setStaffNumber("17006");
        response2.setTime("2017/12/28");
        response2.setStockCount("50");
        list1.add(response2);
        recordResponse1.setStaffRecordResponses(list1);
        list.add(recordResponse1);
        stockRecordResponse.setRecordResponses(list);
        return stockRecordResponse;
    }
    public void getRecord(){
        result.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCountReportInfoByCodePDA(headCode,null,null,goodsCode,barCode,null,null,null)
                .flatMap(new ResultDataParse<StockRecordResponse>())
                .compose(new RxSchedulerTransformer<StockRecordResponse>())
                .subscribe(new Consumer<StockRecordResponse>() {
                               @Override
                               public void accept(StockRecordResponse recordResponse) throws Exception {
                                   result.setValue(LiveDataWrapper.success(recordResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                result.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);

    }

    public void getDetail(){
        result.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getGoodsDetailInfo(null,barCode)
                .flatMap(new ResultDataParse<GoodsDetailResponse>())
                .compose(new RxSchedulerTransformer<GoodsDetailResponse>())
                .subscribe(new Consumer<GoodsDetailResponse>() {
                               @Override
                               public void accept(GoodsDetailResponse response) throws Exception {
                                   StockRecordResponse stockRecordResponse = new StockRecordResponse();
                                   stockRecordResponse.setHeadCode(headCode);
                                   stockRecordResponse.setGoods(response);
                                   result.setValue(LiveDataWrapper.success(stockRecordResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                   result.setValue(LiveDataWrapper.error(throwable,null));

                            }
                        });
        addDisposable(disposable);
    }
}
