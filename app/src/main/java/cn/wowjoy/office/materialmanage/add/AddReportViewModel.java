package cn.wowjoy.office.materialmanage.add;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.AddReportRecordAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.ResultEmpty;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.GetCountReportBySheetRequest;
import cn.wowjoy.office.data.response.InspectionTotalItemInfo;
import cn.wowjoy.office.data.response.InventoryResponse;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.data.response.StockRecordResponse;
import cn.wowjoy.office.databinding.ItemRvAddReportBinding;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/1/12.
 */

public class AddReportViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;
    private String headCode;
    private String keyWord;
    private String status;
    private int startIndex = 0;
    private int pageSize = 10;




    @Inject
    public AddReportViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setmEventObject(this);
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
        adapter.setKeyWord(keyWord);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void onCreateViewModel() {

    }

    @Override
    public void loadData(boolean ref) {
        super.loadData(ref);
        getCountReportInfoByCodePDA(ref);

    }

    MediatorLiveData<LiveDataWrapper<String>> reportHeadCode = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<StockRecordResponse>> recordList = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<Map<String,String>>> isGoodsChecked = new MediatorLiveData<>();
    MediatorLiveData<RecordResponse> jumpTo = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<ResultEmpty>> commit = new MediatorLiveData<>();
    public AddReportRecordAdapter adapter = new AddReportRecordAdapter();
    public LRecyclerViewAdapter lAdapter = new LRecyclerViewAdapter(adapter);

    public void setData(boolean isRefresh, List<RecordResponse> data) {
        if (isRefresh) {
            adapter.refresh(data);
        } else {
            if (null != data)
                adapter.loadMore(data);
        }
        lAdapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty() )){
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview,null,false);
            lAdapter.addFooterView(view);

        }
        lAdapter.notifyDataSetChanged();
        refreshComplete();
    }

    public void jumpTo(RecordResponse recordResponse){
        jumpTo.setValue(recordResponse);
    }

    public void getCountReportBySheet(String sheetHeadCode){
        reportHeadCode.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCountReportBySheet(new GetCountReportBySheetRequest(sheetHeadCode))
                .flatMap(new ResultDataParse<String>())
                .compose(new RxSchedulerTransformer<String>())
                .subscribe(new Consumer<String>() {
                               @Override
                               public void accept(String s) throws Exception {
                                   reportHeadCode.setValue(LiveDataWrapper.success(s));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                reportHeadCode.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }

    public void getCountReportInfoByCodePDA(boolean isRefresh){
        if (isRefresh){
            startIndex = 0;
        } else {
            ++startIndex;
        }
        recordList.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCountReportInfoByCodePDA(headCode,status,null,null,null,keyWord,startIndex,pageSize)
                .flatMap(new ResultDataParse<StockRecordResponse>())
                .compose(new RxSchedulerTransformer<StockRecordResponse>())
                .subscribe(new Consumer<StockRecordResponse>() {
                               @Override
                               public void accept(StockRecordResponse recordResponse) throws Exception {
                                   recordList.setValue(LiveDataWrapper.success(recordResponse));
                                   if (recordResponse == null){
                                       setData(isRefresh,null);
                                   } else {
                                       setData(isRefresh,recordResponse.getRecordResponses());
                                   }

                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                recordList.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }

    public void isGoodsChecked(String goodsCode,String barCode){
        isGoodsChecked.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.isGoodsChecked(headCode,goodsCode,barCode)
                .flatMap(new ResultDataParse<Map<String,String>>())
                .compose(new RxSchedulerTransformer<Map<String,String>>())
                .subscribe(new Consumer<Map<String,String>>() {
                               @Override
                               public void accept(Map<String,String> s) throws Exception {
                                   isGoodsChecked.setValue(LiveDataWrapper.success(s));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                isGoodsChecked.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);

    }

    public void commit(){
        commit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.confirmFinish(headCode)
                .flatMap(new ResultDataParse<ResultEmpty>())
                .compose(new RxSchedulerTransformer<ResultEmpty>())
                .subscribe(new Consumer<ResultEmpty>() {
                               @Override
                               public void accept(ResultEmpty s) throws Exception {
                                   commit.setValue(LiveDataWrapper.success(s));
                                   ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                commit.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }


}
