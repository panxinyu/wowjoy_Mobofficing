package cn.wowjoy.office.materialmanage.inventoryReport;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.InventoryReportAdapter;
import cn.wowjoy.office.common.adapter.StaffAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.ResultEmpty;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.CheckerResponse;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.data.response.StockRecordResponse;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Sherily on 2018/1/29.
 * Description:
 */

public class InventoryReportViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;

    private String key;
    private String headCode;
    private String status;
    private String storageCode;
    private String checkDate;
    private int startIndex = 0;
    private int pageSize = 10;


    StaffAdapter adapter = new StaffAdapter();
    LRecyclerViewAdapter lAdapter = new LRecyclerViewAdapter(adapter);
    InventoryReportAdapter inventoryReportAdapter = new InventoryReportAdapter();
    LRecyclerViewAdapter lRecyclerViewAdapter = new LRecyclerViewAdapter(inventoryReportAdapter);

    MediatorLiveData<RecordResponse> goToDetail = new MediatorLiveData<>();
    MediatorLiveData<CheckerResponse> addStall = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<List<CheckerResponse>>> staffResult = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<StockRecordResponse>> reportResult = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<StockRecordResponse>> basicResult = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<String>> isReadySubmit = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<String>> pdaSubmit = new MediatorLiveData<>();

    private List<CheckerResponse> mockStaff(){
        List<CheckerResponse> responses = new ArrayList<>();
        responses.add(new CheckerResponse("李一一","89707","医用仓库管理员"));
        responses.add(new CheckerResponse("李四","89708","耗材仓库管理员"));
        responses.add(new CheckerResponse("张全蛋他爸","89709","IT仓库管理员"));
        responses.add(new CheckerResponse("黄晓明","89710","医用仓库总管"));
        responses.add(new CheckerResponse("杨树林","89712","护士长"));
        responses.add(new CheckerResponse("李院士","89711","院长"));
        responses.add(new CheckerResponse("王阳明","89713","妇产科主任"));
        responses.add(new CheckerResponse("彭于晏","89714","外科主治医师"));
        responses.add(new CheckerResponse("吴磊","89715","内科主治医师"));
        responses.add(new CheckerResponse("苏志燮","89716","外科主任"));
        responses.add(new CheckerResponse("金宇彬","89717","内科主任"));
        responses.add(new CheckerResponse("宋仲基","89727","骨伤科专家"));
        responses.add(new CheckerResponse("权志龙","89737","整形美容科专家"));
        return responses;

    }

    private StockRecordResponse mockBasic(){
        StockRecordResponse recordResponse = new StockRecordResponse();
        recordResponse.setCheckFailMsg("这是审核失败的原因，鬼知道为什么失败啊，要死了,什么时候才能一夜暴富呢。。。。。");
        recordResponse.setStorageName("医用材料仓库");
        recordResponse.setChechDate("2017-12-12");
        recordResponse.setProfit("135");
        recordResponse.setLoss("-500");
        recordResponse.setTotal("-365");
        return recordResponse;
    }

    @Override
    public void loadData(boolean ref) {
        super.loadData(ref);
        getCountReportInfoByCodePDA(ref);
    }

    public void setKey(String key) {
        this.key = key;
        adapter.setKeyWord(key);
    }


    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Inject
    public InventoryReportViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setmEventHandler(this);
        inventoryReportAdapter.setmEventObject(this);
    }


    @Override
    public void onCreateViewModel() {

    }

    public void getBasic(){
        basicResult.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCountReportByHeadCode(headCode)
                .flatMap(new ResultDataParse<StockRecordResponse>())
                .compose(new RxSchedulerTransformer<StockRecordResponse>())
                .subscribe(new Consumer<StockRecordResponse>() {
                               @Override
                               public void accept(StockRecordResponse recordResponse) throws Exception {
                                   basicResult.setValue(LiveDataWrapper.success(recordResponse));
                                   storageCode = recordResponse.getStorageCode();
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                basicResult.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);

    }
    public void setData(boolean isRefresh, List<RecordResponse> data) {

        if (isRefresh) {
            inventoryReportAdapter.refresh(data);
        } else {
            if (data != null)
                inventoryReportAdapter.loadMore(data);
        }
        lRecyclerViewAdapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty() )){
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview,null,false);
            lRecyclerViewAdapter.addFooterView(view);

        }
        lRecyclerViewAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void getCountReportInfoByCodePDA(boolean isRefresh){
        if (isRefresh){
            startIndex = 0;
        } else {
            ++startIndex;
        }
        reportResult.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCountReportInfoByCodePDA(headCode,null,status,null,null,null,startIndex,pageSize)
                .flatMap(new ResultDataParse<StockRecordResponse>())
                .compose(new RxSchedulerTransformer<StockRecordResponse>())
                .subscribe(new Consumer<StockRecordResponse>() {
                               @Override
                               public void accept(StockRecordResponse recordResponse) throws Exception {
                                   reportResult.setValue(LiveDataWrapper.success(recordResponse));
                                   if (recordResponse == null){
                                       setData(isRefresh,null);
                                   } else {
                                       setData(isRefresh,recordResponse.getRecordResponses());
                                   }
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                reportResult.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }
    public void goToDetail(RecordResponse recordResponse){
        goToDetail.setValue(recordResponse);
    }

    public void getStaffList(){
        staffResult.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getStaffInfoByFuzzy(key)
                .flatMap(new ResultDataParse<List<CheckerResponse>>())
                .compose(new RxSchedulerTransformer<List<CheckerResponse>>())
                .subscribe(new Consumer<List<CheckerResponse>>() {
                               @Override
                               public void accept(List<CheckerResponse> checkerResponses) throws Exception {
                                   adapter.setDatas(checkerResponses);
                                   staffResult.setValue(LiveDataWrapper.success(checkerResponses));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                staffResult.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);

    }
    public void addStaff(CheckerResponse checkerResponse){
       addStall.setValue(checkerResponse);
    }

    public void isReadySubmit(){
        isReadySubmit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.isReadySubmit(headCode)
                .flatMap(new ResultDataParse<String>())
                .compose(new RxSchedulerTransformer<String>())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        isReadySubmit.setValue(LiveDataWrapper.success(s));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        isReadySubmit.setValue(LiveDataWrapper.error(throwable,null));

                    }
                });
        addDisposable(disposable);
    }

    public void pdaSubmit(List<CheckerResponse> list){
        pdaSubmit.setValue(LiveDataWrapper.loading(null));
        StockRecordResponse request = new StockRecordResponse();
        request.setHeadCode(headCode);
        request.setStorageCode(storageCode);
        request.setChechDate(checkDate);
        request.setCheckerResponseList(list);
        Disposable disposable = apiService.pdaSubmit(request)
                .flatMap(new ResultDataParse<String>())
                .compose(new RxSchedulerTransformer<String>())
                .subscribe(new Consumer<String>() {
                               @Override
                               public void accept(String resultEmpty) throws Exception {
                                   pdaSubmit.setValue(LiveDataWrapper.success(resultEmpty));
                                   ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                pdaSubmit.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }


}
