package cn.wowjoy.office.materialmanage.inventoryRecord;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.InventoryRecordAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.ResultEmpty;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.InventoryRecordResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Sherily on 2018/1/12.
 * Description:
 */

public class InventoryRecordViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;

    public InventoryRecordAdapter adapter = new InventoryRecordAdapter();
    public LRecyclerViewAdapter lAdapter = new LRecyclerViewAdapter(adapter);
    MediatorLiveData<LiveDataWrapper<List<InventoryRecordResponse>>> records = new MediatorLiveData<>();
    @Inject
    public InventoryRecordViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setmEventHandle(this);
    }

    @Override
    public void onCreateViewModel() {

    }

    private List<InventoryRecordResponse> mockRecord(){
        List<InventoryRecordResponse> responses = new ArrayList<>();
        responses.add(new InventoryRecordResponse("2017-10-12 12:00","IT信息库—全部"));
        responses.add(new InventoryRecordResponse("2017-10-22 12:00","IT信息库—全部"));
        return responses;
    }

    public void getRecord(String storageCode){
//        List<InventoryRecordResponse> responses = mockRecord();
        records.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getInVentoryRecord(storageCode)
                .flatMap(new ResultDataParse<List<InventoryRecordResponse> >())
                .compose(new RxSchedulerTransformer<List<InventoryRecordResponse> >())
                .subscribe(new Consumer<List<InventoryRecordResponse> >() {
                               @Override
                               public void accept(List<InventoryRecordResponse>  recordResponses) throws Exception {
                                   records.setValue(LiveDataWrapper.success(recordResponses));
                                   adapter.setDatas(recordResponses);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                records.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }

    public void handle(InventoryRecordResponse response){
        //跳转至添加盘点报告页面
//        Toast.makeText(getApplication(), "跳转至添加盘点报告页面", Toast.LENGTH_SHORT).show();
//        ARouter.getInstance().build("/materialmanage/addReport/addStock").navigation();
        ARouter.getInstance().build("/materialmanage/add/addreport")
                .withBoolean("isSheetToReport",true)
                .withString("sheetHeadCode",response.getHeadCode())
                .withString("reportHeadCode",null)
                .navigation();
    }
}
