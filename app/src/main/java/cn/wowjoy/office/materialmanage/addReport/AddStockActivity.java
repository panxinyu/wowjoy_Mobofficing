package cn.wowjoy.office.materialmanage.addReport;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.common.widget.keyborad.CustomBaseKeyboard;
import cn.wowjoy.office.common.widget.keyborad.CustomKeyboardManager;
import cn.wowjoy.office.databinding.ActivityAddStockBinding;
import cn.wowjoy.office.utils.ToastUtil;

@Route(path = "/materialmanage/addReport/addStock")
public class AddStockActivity extends NewBaseActivity<ActivityAddStockBinding,AddStockViewModel> implements View.OnClickListener, CustomBaseKeyboard.OnTextChangeListener {
    @Autowired
    String stock;

    @Override
    protected Class<AddStockViewModel> getViewModel() {
        return AddStockViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_stock;
    }
    DecimalFormat df = new DecimalFormat();
    String pattern = "0.00";
    private  CustomKeyboardManager customKeyboardManager;
    private CustomBaseKeyboard priceKeyboard;
    @Override
    protected void init(Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        binding.setViewmodel(viewModel);
        initView();
    }

    private void initView(){
        binding.cancel.setOnClickListener(this);
        binding.complete.setOnClickListener(this);
//        binding.addStock.addTextChangedListener(textWatcher);
        binding.addStock.setOnClickListener(this);
        customKeyboardManager = new CustomKeyboardManager(this);
        priceKeyboard = new CustomBaseKeyboard(this, R.xml.stock_price_num_keyboard) {
            @Override
            public boolean handleSpecialKey(EditText etCurrent, int primaryCode) {
                return false;
            }
        };
        priceKeyboard.setOnTextChangeListener(this);
        customKeyboardManager.setShowUnderView(binding.rl);
        customKeyboardManager.attachTo(binding.addStock, priceKeyboard);
        binding.addStock.setText(stock);
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // 限制最多能输入9位整数
            if (s.toString().contains(".")) {
                if (s.toString().indexOf(".") > 14) {
                    s = s.toString().subSequence(0,14) + s.toString().substring(s.toString().indexOf("."));
                    binding.addStock.setText(s);
//                    binding.addStock.setSelection(s.length());
                }
            }else {
                if (s.toString().length() > 14){
                    s = s.toString().subSequence(0,14);
                    binding.addStock.setText(s);
//                    binding.addStock.setSelection(14);
                }
            }
            // 判断小数点后只能输入两位
            if (s.toString().contains(".")) {
                if (s.length() - 1 - s.toString().indexOf(".") > 2) {
                    s = s.toString().subSequence(0,
                            s.toString().indexOf(".") + 3);
                    binding.addStock.setText(s);
//                    binding.addStock.setSelection(s.length());
                }
            }
            //如果第一个数字为0，第二个不为点，就不允许输入
            if (s.toString().startsWith("0") && s.toString().trim().length() > 1) {
                if (!s.toString().substring(1, 2).equals(".")) {
                    binding.addStock.setText(s.subSequence(0, 1));
//                    binding.addStock.setSelection(1);
                    return;
                }
            }

//            如果"."在起始位置,则起始位置自动补0
            if (s.toString().trim().substring(0).equals(".")) {
                s = "0" + s ;
                binding.addStock.setText(s);
//                binding.addStock.setSelection(2);
            }

            if (TextUtils.isEmpty(s)){
                binding.limit.setSelected(false);
                binding.limit.setText("(0/16)");
            } else if (s.toString().contains(".")){
                if (s.toString().trim().length() >= 17){
                    binding.limit.setSelected(true);
                    binding.limit.setText("(16/16)");
                } else {
                    binding.limit.setSelected(false);
                    binding.limit.setText("("+ (s.toString().trim().length() - 1) +"/16)");
                }
            } else {
                binding.limit.setSelected(false);
                binding.limit.setText("("+ (s.toString().trim().length()) +"/16)");
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
//            // 这里可以知道你已经输入的字数，大家可以自己根据需求来自定义文本控件实时的显示已经输入的字符个数
            int after_length = s.length();// 输入内容后编辑框所有内容的总长度




        }
    };


    private void complete(){
        setResult(RESULT_OK);
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel:
                onBackPressed();
                break;
            case R.id.complete:
//                viewModel.addStock("","");
                Intent intent = new Intent();
                intent.putExtra("count",binding.addStock.getText().toString().trim());
                setResult(RESULT_OK,intent);
                finish();
                break;
            case R.id.add_stock:
                customKeyboardManager.focus();
                break;
        }
    }

    @Override
    public void onChange(StringBuilder sb) {
        if (TextUtils.isEmpty(sb.toString().trim())){
            binding.limit.setSelected(false);
            binding.limit.setText("(0/16)");
        } else if (sb.toString().contains(".")){
            if (sb.toString().contains("-")){
                if (sb.toString().trim().length() >= 18){
                    binding.limit.setSelected(true);
                    binding.limit.setText("(16/16)");
                } else {
                    binding.limit.setSelected(false);
                    binding.limit.setText("("+ (sb.toString().trim().length() - 2) +"/16)");
                }
            } else {
                if (sb.toString().trim().length() >= 17){
                    binding.limit.setSelected(true);
                    binding.limit.setText("(16/16)");
                } else {
                    binding.limit.setSelected(false);
                    binding.limit.setText("("+ (sb.toString().trim().length() - 1) +"/16)");
                }
            }

        } else if (sb.toString().contains("-")){
            if (sb.toString().trim().length() >= 17){
                binding.limit.setSelected(true);
                binding.limit.setText("(16/16)");
            } else {
                binding.limit.setSelected(false);
                binding.limit.setText("("+ (sb.toString().trim().length() - 1) +"/16)");
            }
        } else {
            binding.limit.setSelected(false);
            binding.limit.setText("("+ (sb.toString().trim().length()) +"/16)");
        }
    }
}
