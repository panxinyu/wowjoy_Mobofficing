package cn.wowjoy.office.materialmanage.add;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;



import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorActivityAdapter;

import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.remote.ResultEmpty;
import cn.wowjoy.office.databinding.ActivityAddReportBinding;


@Route(path = "/materialmanage/add/addreport")
public class AddReportActivity extends NewBaseActivity<ActivityAddReportBinding, AddReportViewModel> implements View.OnClickListener {
    private static final int REQUEST_CODE1 = 1001;
    private static final int REQUEST_CODE2 = 1002;
    private static final int REQUEST_CODE3 = 1003;
    @Autowired
    boolean isSheetToReport;//从盘点记录查看盘点报告
    @Autowired
    String sheetHeadCode;
    @Autowired
    String reportHeadCode;


    private String key;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorActivityAdapter mTabIndicatorActivityAdapter;
    private AllFragment allFragment;
    private EnteredFragment enteredFragment;
    private NotEnteredFragment notEnteredFragment;
    private MDialog waitDialog;

    private void clearDialog(){
        if (waitDialog != null ){
            waitDialog.dismiss();
            waitDialog = null;
        }
    }


    @Override
    protected Class<AddReportViewModel> getViewModel() {
        return AddReportViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_report;
    }


    @Override
    protected void init(Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        initView();
        iniSearch();
        initData();
        initObserver();
        initVp();
        if (isSheetToReport){
            viewModel.getCountReportBySheet(sheetHeadCode);
        } else {
            viewModel.setHeadCode(reportHeadCode);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getCountReportInfoByCodePDA(true);
    }

    @Override
    protected void brodcast(Context context, String msg) {
        super.brodcast(context, msg);
    }

    private void initView(){
        binding.materialTitle.titleTextTv.setText("添加盘点报告");
        binding.materialTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.materialTitle.titleBackTv.setText("");
        binding.materialTitle.titleBackLl.setOnClickListener(this);

        binding.materialTitle.titleMenuConfirm.setText("提交汇总");
        binding.materialTitle.titleMenuConfirm.setTextColor(getResources().getColor(R.color.white));
        binding.materialTitle.titleMenuConfirm.setOnClickListener(this);
        binding.materialTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
    }
    private void initVp(){
        binding.tabIndicator.setScrollBar(new ColorBar(getApplicationContext(), getResources().getColor(R.color.view_line1), 4));//设置滚动条的颜色，及高度
        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this, R.color.appText14), ContextCompat.getColor(this, R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator, binding.vp);
        mTabIndicatorActivityAdapter = new TabIndicatorActivityAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter.setData(titles, fragments);
        indicatorViewPager.setAdapter(mTabIndicatorActivityAdapter);
        binding.vp.setCanScroll(true);
        binding.vp.setOffscreenPageLimit(3);

    }

    private void iniSearch(){
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();
                key = binding.searchEditText.getText().toString().trim();
                setKey();
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(binding.searchEditText.getText().toString())){
                    binding.delete.setVisibility(View.VISIBLE);

                } else {
                    binding.delete.setVisibility(View.GONE);
                    key = null;
                }

            }
        });
        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchEditText.setText("");
                binding.delete.setVisibility(View.GONE);
                key = null;
                setKey();
            }
        });
    }


    private void setKey(){
        allFragment.setKey(key);
        enteredFragment.setKey(key);
        notEnteredFragment.setKey(key);
    }
    private void initData() {
        titles = new ArrayList<>();
        titles.add("全部");
        titles.add("未录入");
        titles.add("已录入");
        fragments = new ArrayList<>();
        allFragment = AllFragment.newInstance(reportHeadCode, null);
        notEnteredFragment = NotEnteredFragment.newInstance(reportHeadCode, "n");
        enteredFragment = EnteredFragment.newInstance(reportHeadCode, "y");

        fragments.add(allFragment);
        fragments.add(notEnteredFragment);
        fragments.add(enteredFragment);
    }


    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.errorContent;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        if (isSheetToReport){
            viewModel.getCountReportBySheet(sheetHeadCode);
        }
    }

    private void initObserver(){
        viewModel.reportHeadCode.observe(this, new Observer<LiveDataWrapper<String>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<String> stringLiveDataWrapper) {
                switch (stringLiveDataWrapper.status){
                    case LOADING:
                        break;
                    case SUCCESS:
                        viewModel.setHeadCode(stringLiveDataWrapper.data);
                        allFragment.setHeadCode(stringLiveDataWrapper.data);
                        enteredFragment.setHeadCode(stringLiveDataWrapper.data);
                        notEnteredFragment.setHeadCode(stringLiveDataWrapper.data);
                        break;
                    case ERROR:
                        handleException(stringLiveDataWrapper.error,true);
                        break;
                }
            }
        });
        viewModel.commit.observe(this, new Observer<LiveDataWrapper<ResultEmpty>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<ResultEmpty> stringLiveDataWrapper) {
                switch (stringLiveDataWrapper.status){
                    case LOADING:
                        clearDialog();
                        waitDialog = CreateDialog.waitingDialog(AddReportActivity.this);
                        break;
                    case SUCCESS:
                        clearDialog();

                        break;
                    case ERROR:
                        clearDialog();
                        handleException(stringLiveDataWrapper.error,false);
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CODE1:
                allFragment.onActivityResult(requestCode,resultCode,data);
                break;
            case REQUEST_CODE2:
                enteredFragment.onActivityResult(requestCode,resultCode,data);
                break;
            case REQUEST_CODE3:
                notEnteredFragment.onActivityResult(requestCode,resultCode,data);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                //back
                onBackPressed();
                break;
            case R.id.title_menu_confirm:
                //提交
                viewModel.commit();
                break;
        }
    }
}
