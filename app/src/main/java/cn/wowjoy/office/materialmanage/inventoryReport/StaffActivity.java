package cn.wowjoy.office.materialmanage.inventoryReport;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;

import org.w3c.dom.ProcessingInstruction;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.CheckerResponse;
import cn.wowjoy.office.databinding.ActivityStaffBinding;
import cn.wowjoy.office.materialmanage.inventory.MaterialInventoryActivity;

@Route(path = "/materialmanage/inventoryReport/staff")
public class StaffActivity extends NewBaseActivity<ActivityStaffBinding,InventoryReportViewModel> implements View.OnClickListener {



    private String key;
    private MDialog waitDialog;

    private void clearDialog(){
        if (waitDialog != null ){
            waitDialog.dismiss();
            waitDialog = null;
        }
    }


    @Override
    protected Class<InventoryReportViewModel> getViewModel() {
        return InventoryReportViewModel.class;
    }
    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        iniSearch();
        initRV();
        initObserver();

    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.errorContent;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getStaffList();
    }

    private void initObserver(){
        viewModel.staffResult.observe(this, new Observer<LiveDataWrapper<List<CheckerResponse>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<CheckerResponse>> listLiveDataWrapper) {
                switch (listLiveDataWrapper.status){
                    case LOADING:
//                        clearDialog();
//                        waitDialog = CreateDialog.waitingDialog(StaffActivity.this);
                        break;
                    case SUCCESS:
//                        clearDialog();
                        break;
                    case ERROR:
//                        clearDialog();
                        handleException(listLiveDataWrapper.error,true);
                        break;
                }
            }
        });

        viewModel.addStall.observe(this, new Observer<CheckerResponse>() {
            @Override
            public void onChanged(@Nullable CheckerResponse checkerResponse) {
                Intent intent = new Intent();
                intent.putExtra("checker",checkerResponse);
                setResult(RESULT_OK,intent);
                StaffActivity.this.finish();
            }
        });
    }

    private void initRV(){
        binding.emptyView.emptyContent.setText("请输入工号/姓名进行搜索");
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(viewModel.lAdapter);
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setLoadMoreEnabled(false);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this,SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
    }

    private void iniSearch(){
        binding.back.setOnClickListener(this);
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();
                key = binding.searchEditText.getText().toString().trim();
                //请求接口
                viewModel.setKey(key);
                viewModel.getStaffList();
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(binding.searchEditText.getText().toString())){
                    binding.delete.setVisibility(View.VISIBLE);

                } else {
                    binding.delete.setVisibility(View.GONE);
                    key = null;
                }

            }
        });
        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchEditText.setText("");
                binding.delete.setVisibility(View.GONE);
                key = null;
                //清空搜索
                viewModel.setKey(key);
                viewModel.getStaffList();
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_staff;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
        }
    }
}
