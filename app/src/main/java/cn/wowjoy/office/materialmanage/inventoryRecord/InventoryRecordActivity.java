package cn.wowjoy.office.materialmanage.inventoryRecord;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InventoryRecordResponse;
import cn.wowjoy.office.databinding.ActivityInventoryRecordBinding;
import cn.wowjoy.office.utils.TextUtil;

@Route(path = "/materialmanage/inventoryRecord/inventoryRecord")
public class InventoryRecordActivity extends NewBaseActivity<ActivityInventoryRecordBinding,InventoryRecordViewModel> implements View.OnClickListener {

    @Autowired
    String storageCode;
    private MDialog waitDialog;
    @Override
    protected Class<InventoryRecordViewModel> getViewModel() {
        return InventoryRecordViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_inventory_record;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        binding.setViewmodel(viewModel);
        initView();
        initObserver();
        if (!TextUtils.isEmpty(storageCode))
            viewModel.getRecord(storageCode);
    }

    private void initView(){
        binding.back.setOnClickListener(this);
        binding.emptyView.emptyContent.setText("目前无待生成报告的记录，请前往盘点记录点击查看~~");
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(viewModel.lAdapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this,SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setLoadMoreEnabled(false);
        binding.recyclerView.setPullRefreshEnabled(false);
    }

    private void initObserver(){
        viewModel.records.observe(this, new Observer<LiveDataWrapper<List<InventoryRecordResponse>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<InventoryRecordResponse>> listLiveDataWrapper) {
                switch (listLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(InventoryRecordActivity.this);
                        break;
                    case SUCCESS:
                        waitDialog.dismiss();

                        break;
                    case ERROR:
                        waitDialog.dismiss();
                        handleException(listLiveDataWrapper.error,false);
                        break;
                }
            }
        });
    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.content;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getRecord(storageCode);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
        }
    }
}
