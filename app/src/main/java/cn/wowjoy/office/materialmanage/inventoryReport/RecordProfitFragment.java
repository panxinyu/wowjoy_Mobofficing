package cn.wowjoy.office.materialmanage.inventoryReport;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.ViewGroup;

import com.alibaba.android.arouter.launcher.ARouter;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.data.response.StockRecordResponse;
import cn.wowjoy.office.databinding.FragmentRecordBinding;

/**
 * A simple {@link NewBaseFragment} subclass.
 * Use the {@link RecordProfitFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecordProfitFragment extends NewBaseFragment<FragmentRecordBinding,InventoryReportViewModel> {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final int REQUEST_CODE1 = 1011;
    private static final int REQUEST_CODE2 = 1012;
    private static final int REQUEST_CODE3 = 1013;
    private int reqestcode;
    private boolean isResultBack;

    // TODO: Rename and change types of parameters
    private String headCode;
    private String status;
    private boolean isCanNotEdit;
    private String goodsCode;

    private MDialog waitDialog;

    private void clearDialog(){
        if (waitDialog != null ){
            waitDialog.dismiss();
            waitDialog = null;
        }
    }

    public RecordProfitFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RecordProfitFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecordProfitFragment newInstance(String param1, String param2,boolean param3) {
        RecordProfitFragment fragment = new RecordProfitFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putBoolean(ARG_PARAM3,param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected Class<InventoryReportViewModel> getViewModel() {
        return InventoryReportViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_record;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            headCode = getArguments().getString(ARG_PARAM1);
            status = getArguments().getString(ARG_PARAM2);
            isCanNotEdit = getArguments().getBoolean(ARG_PARAM3);
        }
        viewModel.setStatus(status);
        viewModel.setHeadCode(headCode);
        if (null == status){
            reqestcode = REQUEST_CODE1;
        } else if (TextUtils.equals("y",status)){
            reqestcode = REQUEST_CODE2;
        } else if (TextUtils.equals("n",status)){
            reqestcode = REQUEST_CODE3;
        }
    }


    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        initRV();
        initObsever();
        viewModel.getCountReportInfoByCodePDA(true);

    }

    private void initRV(){
        binding.setViewmodel(viewModel);
        viewModel.setHeadCode(headCode);
        viewModel.setStatus(status);
        binding.emptyView.emptyContent.setText("没有相关盘点报告");
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerView.setAdapter(viewModel.lRecyclerViewAdapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(getActivity(),SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
    }

    private void initObsever(){
        viewModel.reportResult.observe(this, new Observer<LiveDataWrapper<StockRecordResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StockRecordResponse> stockRecordResponseLiveDataWrapper) {
                switch (stockRecordResponseLiveDataWrapper.status){
                    case LOADING:
                        clearDialog();
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        clearDialog();
                        break;
                    case ERROR:
                        clearDialog();
                        handleException(stockRecordResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });

        viewModel.goToDetail.observe(this, new Observer<RecordResponse>() {
            @Override
            public void onChanged(@Nullable RecordResponse recordResponse) {
                goToReportDetail(null,recordResponse.getHeadCode(),recordResponse.getGoodsCode());
            }
        });

    }

    private void goToReportDetail(String barCode,String reportHeadCode,String goodsCode){
        ARouter.getInstance().build("/materialmanage/addReport/stockRecord")
                .withString("barCode",barCode)
                .withString("goodsCode",goodsCode)
                .withString("headCode",reportHeadCode)
                .withBoolean("isCanNotEdit", isCanNotEdit)
                .navigation(getActivity(),REQUEST_CODE2);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        isResultBack = true;
        switch (requestCode){
            case REQUEST_CODE1:
                if (null != data && data.getBooleanExtra("isSave",false)){
                    viewModel.getCountReportInfoByCodePDA(true);
                }
                break;
        }
    }

    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        if (!TextUtils.isEmpty(headCode) && !isResultBack){
            viewModel.getCountReportInfoByCodePDA(true);
        }
        if (isResultBack){
            isResultBack = false;
        }

    }


    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.errorContent;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getCountReportInfoByCodePDA(true);
    }

}
