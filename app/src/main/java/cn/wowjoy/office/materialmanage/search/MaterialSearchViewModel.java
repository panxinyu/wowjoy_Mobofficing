package cn.wowjoy.office.materialmanage.search;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.MaterialListAdapter;
import cn.wowjoy.office.common.adapter.MaterialSearchAdapter;
import cn.wowjoy.office.common.adapter.TagBaseAdapter;
import cn.wowjoy.office.data.response.MaterialSearchResponse;

/**
 * Created by Sherily on 2017/12/28.
 * Description:
 */

public class MaterialSearchViewModel extends NewBaseViewModel {


//    public MaterialSearchAdapter searchAdapter = new MaterialSearchAdapter();
//    public LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(searchAdapter);
//    private MediatorLiveData<MaterialSearchResponse> itemclick = new MediatorLiveData<>();
//    private MediatorLiveData<List<MaterialSearchResponse>> searchList = new MediatorLiveData<>();
//    private List<String> mockdata(){
//        List<String> tags = new ArrayList<>();
//        tags.add("00256554");
//        tags.add("pda");
//        tags.add("注射器");
//        tags.add("口罩");
//        tags.add("棉签");
//        tags.add("酒精");
//        tags.add("镊子");
//        tags.add("牙垫");
//        tags.add("1号绷带");
//        return tags;
//    }
//
//    private List<MaterialSearchResponse> mocklist(){
//        List<MaterialSearchResponse> responses = new ArrayList<>();
//        responses.add(new MaterialSearchResponse("张三","规格型号：男/190/25","库存：2000"));
//        responses.add(new MaterialSearchResponse("王五","规格型号：男/170/38","库存：10000"));
//        responses.add(new MaterialSearchResponse("韩梅梅","规格型号：女/175/23","库存：20"));
//        responses.add(new MaterialSearchResponse("小红","规格型号：男/160/29","库存：438"));
//        responses.add(new MaterialSearchResponse("小明","规格型号：男/165/41","库存：50000"));
//        return responses;
//    }
    @Inject
    public MaterialSearchViewModel(@NonNull NewMainApplication application) {
        super(application);
//        searchAdapter.setmEventHandle(this);
    }
//
    @Override
    public void onCreateViewModel() {

    }
//
//    public MediatorLiveData<MaterialSearchResponse> getItemclick() {
//        return itemclick;
//    }
//
//    public MediatorLiveData<List<MaterialSearchResponse>> getSearchList() {
//        return searchList;
//    }
//
//    public void search(String key){
//        searchAdapter.setKeyWord(key);
//        searchAdapter.setData(mocklist());
//        searchList.setValue(mocklist());
//    }
//
//    public LiveData<List<String>> getHistory(){
//        MediatorLiveData<List<String>> listLiveData = new MediatorLiveData<>();
//        listLiveData.setValue(mockdata());
//        return listLiveData;
//    }
//
//    public void handle(MaterialSearchResponse searchResponse){
//        itemclick.setValue(searchResponse);
//
//    }
}
