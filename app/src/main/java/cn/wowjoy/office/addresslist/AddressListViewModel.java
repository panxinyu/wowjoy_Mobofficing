package cn.wowjoy.office.addresslist;

import android.support.annotation.NonNull;
import android.util.Log;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.bingoogolapple.androidcommon.adapter.BGABindingViewHolder;
import cn.wowjoy.office.R;
import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.widget.MyToast;
import cn.wowjoy.office.data.response.AddressListInfo;
import cn.wowjoy.office.databinding.ItemRvAddresslistBinding;

/**
 * Created by Administrator on 2017/10/17.
 */

public class AddressListViewModel extends NewBaseViewModel {


    @Inject
    public AddressListViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    public BGABindingRecyclerViewAdapter<AddressListInfo, ItemRvAddresslistBinding> winnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_addresslist);
    public LRecyclerViewAdapter wadapter = new LRecyclerViewAdapter(winnerAdapter);
    public List<AddressListInfo> wdatas;

   public void setAddressListData(List<AddressListInfo> mData){
       if(null == wdatas){
           wdatas = new ArrayList<>();
       }
       wdatas.clear();
       wdatas.addAll(mData);
       winnerAdapter.setData(wdatas);
       wadapter.removeFooterView();
       wadapter.removeHeaderView();
       wadapter.notifyDataSetChanged();
   }

    @Override
    public void onCreateViewModel() {
        queryAddressListData();
    }

        public void onClickAddressListItem(BGABindingViewHolder holder, AddressListInfo model) {
        //TODO:跳转传递数据
        new MyToast(getApplication().getApplicationContext()).showinfo("我点到了:"+model.getName());
        Log.e("PXY", "onClickAddressListItem: "+model.getName() );
//        Intent mIntent = new Intent(getActivity(), StaticPutMedicineDetailActivity.class);
//        mIntent.putExtra("DJJLID", model.getDJJLID());
//        activity.startActivity(mIntent);
    }

    //查询 通讯录所有的team
    public void queryAddressListData(){
        List<AddressListInfo> m =new ArrayList<>();
        m.add(new AddressListInfo("信息部"));
        m.add(new AddressListInfo("UI部"));
        m.add(new AddressListInfo("H5部"));
        m.add(new AddressListInfo("城管部"));
        m.add(new AddressListInfo("11部"));
        m.add(new AddressListInfo("12部"));
        m.add(new AddressListInfo("13部"));
        setAddressListData(m);
//        Log.e("TAG", "queryAddressListData: "+ m.size() );
    }

}
