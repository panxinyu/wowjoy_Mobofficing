package cn.wowjoy.office.addresslist;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;




import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.databinding.FragmentAddressListBinding;
import cn.wowjoy.office.materialinspection.xunjian.MaterialActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddressListFragment extends NewBaseFragment<FragmentAddressListBinding,AddressListViewModel> {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private static final int REQUEST_CODE_CAMERA = 1;
    private ItemTouchHelper itemTouchHelper;

    public AddressListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddressListFragment newInstance(String param1, String param2) {
        AddressListFragment fragment = new AddressListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {

        binding.setViewModel(viewModel);

        binding.titleAddresslist.titleTextTv.setText("通讯录");
        binding.titleAddresslist.titleBackLl.setVisibility(View.VISIBLE);
        binding.titleAddresslist.titleBackTv.setText("");
        binding.lrAddresslist.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.lrAddresslist.setAdapter(viewModel.wadapter);
        binding.lrAddresslist.setPullRefreshEnabled(false);
       viewModel.wadapter.setOnItemClickListener((view, position) -> {
//            new MyToast(getContext()).showinfo("我点到了:"+getViewModel().wdatas.get(position).getName());
//            Log.e("PXY", "onClickAddressListItem: "+ getViewModel().wdatas.get(position).getName() );
            startActivity(new Intent(getContext(), MaterialActivity.class));
        });
    }

    @Override
    protected Class<AddressListViewModel> getViewModel() {
        return AddressListViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_address_list;
    }





}
