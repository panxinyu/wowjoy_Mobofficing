package cn.wowjoy.office.pm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Description:
 */
@Dao
public interface PmListDetailDao {
    @Query("SELECT * FROM detail_list")
    LiveData<List<PmReportListDetailEntity>> loadList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveEntity(PmReportListDetailEntity userInfo);

    @Query("SELECT * FROM detail_list")
    List<PmReportListDetailEntity> getAll();


    /**
     *   查询是否存在该明细
     * @param qrCode
     * @param userName
     * @return
     */
    @Query("SELECT * FROM detail_list WHERE qrCode=:qrCode and userName=:userName")
    List<PmReportListDetailEntity> queryExist(String qrCode ,String userName);



    /**
     *  更新数据
     * @param jsonEntity
     * @param qrCode
     * @param userName
     * @return
     */
    @Query("UPDATE detail_list SET jsonEntity = :jsonEntity WHERE qrCode=:qrCode and userName=:userName")
    void updateExist(String jsonEntity ,String qrCode,String userName);

    /**
     * 删除所有数据
     */
    @Query("DELETE FROM detail_list")
    void deleteAll();

    @Query("DELETE FROM detail_list where qrCode=:qrCode and userName=:userName")
    void deleteByQRCode(String qrCode,String userName);
}
