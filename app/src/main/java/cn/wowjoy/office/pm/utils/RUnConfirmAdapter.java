package cn.wowjoy.office.pm.utils;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.databinding.ItemPmReportAllRvKeyBinding;
import cn.wowjoy.office.pm.db.PmReportListEntity;
import cn.wowjoy.office.pm.view.viewodel.PmReportViewModel;

/**
 * Created by Administrator on 2018/4/12.
 */

public class RUnConfirmAdapter extends RecyclerView.Adapter<RUnConfirmAdapter.ItemViewHolder>{

    private Context context;
    private LayoutInflater inflater;
    private Object mEventListener;

    private List<PmReportListEntity> totalDates;

    private List<PmReportListEntity> selectList = new ArrayList<>();  //用户勾选 生成出库单的数据

    public List<PmReportListEntity> getSelectList() {
        return selectList;
    }

    public void setDates(List<PmReportListEntity> models) {
        this.totalDates = models;
        notifyDataSetChanged();
    }
    public void addMoreDate(List<PmReportListEntity> searchResponse){
        if(null != this.totalDates){
            this.totalDates.addAll(searchResponse);
            notifyDataSetChanged();
            Log.e("PXY", "addMoreDate: "+totalDates.size() );
        }
    }
    public void removeDate(String id){
        if(null == totalDates || totalDates.size()<1){
           return;
        }
        Iterator<PmReportListEntity> iterator = totalDates.iterator();
        while (iterator.hasNext()){
            PmReportListEntity entity = iterator.next();
            if(entity.getId().equals(id)){
                iterator.remove();
            }
        }
        notifyDataSetChanged();
    }
    //取消 后  取消所有的勾选
    public void clearSelect(){
        //取消选中状态
        selectList.clear();
        for(PmReportListEntity detail : totalDates){
               if(detail.isSelect()){
                   detail.setSelect(false);
               }
        }
        notifyDataSetChanged();
    }
    public void setClickble(boolean clickble){

    }

    public void setEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    public RUnConfirmAdapter() {
    }


    public void showAll(boolean state,boolean isSelect){
        selectList.clear();
        if(null != totalDates && totalDates.size()>0){
            for(PmReportListEntity a : totalDates){
                a.setShow(state);
                a.setSelect(isSelect);
            }
        }
         notifyDataSetChanged();
    }
    public void selectAll(PmReportListEntity bean){
        selectList.clear();
        if(null != totalDates && totalDates.size()>0){
            for(PmReportListEntity a : totalDates){
                a.setShow(true);
                if(bean.getCardDeptName().equals(a.getCardDeptName())){
                    a.setSelect(true);
                    if(!selectList.contains(bean)){
                        selectList.add(bean);
                    }
                }else{
                    a.setSelect(false);
                }
            }
        }
        notifyDataSetChanged();
    }
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_pm_report_all_rv_key,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Log.e("PXY", "select  position: "+position+ "  getAdapterPosition :"+holder.getAdapterPosition());
   //     holder.setIsRecyclable(false);
        holder.bind(mEventListener,totalDates.get(position),position);
        PmReportListEntity response = totalDates.get(position);
        if(response.isShow()){
            holder.binding.itemLl.setSwipeEnable(false);
        }else{
            holder.binding.itemLl.setSwipeEnable(true);
        }
        holder.binding.cbSelect.setChecked(response.isSelect());
        holder.binding.llRight.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                holder.binding.cbSelect.setChecked(true);
                showAll(true,false);
//                selectAll(response);
                ((PmReportViewModel) mEventListener).showSelectNum(selectList.size());
                ((PmReportViewModel) mEventListener).showBottom(true);
                return false;
            }
        });
        holder.binding.llRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PXY", "setOnClickListener: ");
                if(!response.isShow()){
                    ((PmReportViewModel) mEventListener).clickItem(response);
                }else{
                    if(!holder.binding.cbSelect.isChecked()){
                        if(!selectList.contains(response)){
                            selectList.add(response);
                        }
                        response.setSelect(true);
//                        holder.binding.cbSelect.setChecked(true);
                    }else{
                        if(selectList.contains(response)){
                            selectList.remove(response);
                        }
                        response.setSelect(false);
//                        holder.binding.cbSelect.setChecked(false);
                    }
                    holder. binding.cbSelect.setChecked(response.isSelect());
                    notifyItemChanged(position);
                    ((PmReportViewModel) mEventListener).showSelectNum(selectList.size());
                }
            }
        });
        holder.binding.cbSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PXY", "onClick: "+  holder.binding.cbSelect.isChecked()
                        +"    position: "+position+"   response: "+response.toString());
                if( holder.binding.cbSelect.isChecked()){
                    if(!selectList.contains(response)){
                        selectList.add(response);
                    }
                    response.setSelect(true);
                }else{
                    if(selectList.contains(response)){
                        selectList.remove(response);
                    }
                    response.setSelect(false);
                }
                holder. binding.cbSelect.setChecked(response.isSelect());
                notifyItemChanged(position);
                ((PmReportViewModel) mEventListener).showSelectNum(selectList.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == totalDates ? 0 : totalDates.size();
    }
      class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemPmReportAllRvKeyBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemPmReportAllRvKeyBinding) binding;
        }

        public void bind(Object mEventHandler,PmReportListEntity response,int position){
            binding.setItemEventHandler((PmReportViewModel) mEventHandler);
            binding.setModel(response);
        }
    }
}
