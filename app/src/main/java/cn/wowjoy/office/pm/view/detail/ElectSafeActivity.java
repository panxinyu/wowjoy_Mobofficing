package cn.wowjoy.office.pm.view.detail;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.common.widget.CommonDialog;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityElectSafeBinding;
import cn.wowjoy.office.pm.data.CardResponse;
import cn.wowjoy.office.pm.data.DictDataResponse;
import cn.wowjoy.office.pm.data.SelectBean;
import cn.wowjoy.office.pm.data.request.PmReportDtl;
import cn.wowjoy.office.pm.view.PMBaseActivity;
import cn.wowjoy.office.pm.view.viewodel.ElectSafeViewModel;
import cn.wowjoy.office.utils.MyTextWatcher;
import cn.wowjoy.office.utils.PickerViewUtils;
import cn.wowjoy.office.utils.ToastUtil;

import static cn.wowjoy.office.pm.view.PmReportDetailActivity.ELECT_SAFE;

public class ElectSafeActivity extends PMBaseActivity<ActivityElectSafeBinding, ElectSafeViewModel> implements View.OnClickListener {
    private OptionsPickerView typeOptionsPickerView;
    private int typePosition;
    private String checkType;
    private String[] checkTypeStrs = new String[]{"验收", "预防性检测", "稳定性检测"};
    private OptionsPickerView roomOptionsPickerView;
    private int roomPosition;
    private String checkOrgannization;

    //电源部分集合
    List<SelectBean> mFunCheckBeans;
    List<SelectBean> mTypeBeans;
    List<SelectBean> mTypes;
    String[] mTitles = new String[]{"保护接地阻抗(mΩ)", "绝缘抗租(电源-地)(MΩ)", "对地漏电流(正常状态)(μA)", "外壳漏电流(正常状态)(μA)"
            , "外壳漏电流(断开状态)(μA)", "病人漏电流(交流)(正常状态)(μA)", "病人漏电流(交流)(正常状态)(μA).", "病人漏电流(交流)(断开地线)(μA)"
            , "病人漏电流(交流)(断开地线)(μA).", "病人辅助漏电流(交流)(正常状态)(μA)", "病人辅助漏电流(交流)(正常状态)(μA).", "病人辅助漏电流(交流)(断开地线)(μA)"
            , "病人辅助漏电流(交流)(断开地线)(μA)."};
    String[] value = new String[]{"≤200", "≥10", "≤500", "≤100", "≤500", "≤100(B﹠BF)", "<10(CF)", "≤500(B﹠BF)",
            "≤50(CF)", "≤100(BF)", "<10(CF)", "≤500(BF)", "<50(CF)"};
    String[] value_Number = new String[]{"<=200", ">=10", "<=500", "<=100", "<=500", "<=100", "<<10", "<=500", "<=50",
            "<=100", "<<10", "<=500", "<<50"};
    //温度
    private String temperature;
    //湿度
    private String humidity;
    //备注
    private String remarks;
    private MDialog waitDialog;
    private Map<String, String> mMapEdit;
    private Map<String, Integer> mMapType;
    private int state;
    private String cardId;
    private CardResponse cardDetail;
    private PmReportDtl submit;
    private CommonDialog checkFailedDialog;
    private final String[] strs = {"B", "BF", "CF"};

    public static void launch(Activity context, int state, String cardId, CardResponse detail, PmReportDtl edit) {
        Intent i = new Intent(context, ElectSafeActivity.class);
        i.putExtra("cardId", cardId);
        i.putExtra("cardDetail", detail);
        i.putExtra("pmReportDtl", edit);
        if (null != edit && TextUtils.isEmpty(edit.getCheckResult())) {
            state = 1;
        }
        i.putExtra(Constants.PM_REPORT, state);
        context.startActivityForResult(i, ELECT_SAFE);
    }

    @Override
    protected Class<ElectSafeViewModel> getViewModel() {
        return ElectSafeViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_elect_safe;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        state = getIntent().getIntExtra(Constants.PM_REPORT, 0);
        cardId = getIntent().getStringExtra("cardId");
        cardDetail = (CardResponse) getIntent().getSerializableExtra("cardDetail");
        submit = (PmReportDtl) getIntent().getSerializableExtra("pmReportDtl");
        initView();
        initData();
        response();
        observe();
    }

    private void observe() {
        viewModel.typeData.observe(ElectSafeActivity.this, new Observer<LiveDataWrapper<DictDataResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<DictDataResponse> dictDataResponseLiveDataWrapper) {
                switch (dictDataResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(ElectSafeActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(ElectSafeActivity.this, waitDialog);
                        }
                        typeList.clear();
                        if (null != dictDataResponseLiveDataWrapper.data && dictDataResponseLiveDataWrapper.data.getList().size() > 0) {
                            for (DictDataResponse.DictData dictData : dictDataResponseLiveDataWrapper.data.getList()) {
                                typeList.add(dictData.getDictDataName());
                            }
                            typeOptionsPickerView = PickerViewUtils.getOptionPickerView("", typeList, PickerViewUtils.getCheckTypePosition(PickerViewUtils.checkType), ElectSafeActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckType, typeList, 1));
                            typeOptionsPickerView.show();
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(ElectSafeActivity.this, waitDialog);
                        }
                        handleException(dictDataResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
        viewModel.roomData.observe(ElectSafeActivity.this, new Observer<LiveDataWrapper<DictDataResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<DictDataResponse> dictDataResponseLiveDataWrapper) {
                switch (dictDataResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(ElectSafeActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(ElectSafeActivity.this, waitDialog);
                        }
                        roomList.clear();
                        if (null != dictDataResponseLiveDataWrapper.data && dictDataResponseLiveDataWrapper.data.getList().size() > 0) {
                            for (DictDataResponse.DictData dictData : dictDataResponseLiveDataWrapper.data.getList()) {
                                roomList.add(dictData.getDictDataName());
                            }
                            roomOptionsPickerView = PickerViewUtils.getOptionPickerView("", roomList, PickerViewUtils.getCheckOrganizationPosition(PickerViewUtils.checkOrganization), ElectSafeActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckWhere, roomList, 2));
                            roomOptionsPickerView.show();
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(ElectSafeActivity.this, waitDialog);
                        }
                        handleException(dictDataResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
    }

    private void response() {
    }

    private void initView() {
        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit, binding.remark, 50, ElectSafeActivity.this));

        binding.lookCheckTitle.titleTextTv.setText("电安全检测");
        binding.lookCheckTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.lookCheckTitle.titleBackTv.setText("");
        binding.lookCheckTitle.titleBackLl.setOnClickListener(this);
        binding.lookCheckTitle.titleMenuConfirm.setText("保存");
        if (state != 3) {
            binding.lookCheckTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        }
        binding.lookCheckTitle.titleMenuConfirm.setOnClickListener(this);
        binding.include4.title.setText("电源部分");
        binding.include4.tvState.setText("待填写");

        binding.rlSelect1.setOnClickListener(this);
        binding.rlSelect2.setOnClickListener(this);
        binding.rlSelect3.setOnClickListener(this);
        binding.rlSelect4.setOnClickListener(this);
        binding.rlSelect5.setOnClickListener(this);
        binding.rlSelect6.setOnClickListener(this);
        binding.include3.rlType.setOnClickListener(this);
        binding.include3.rlRoom.setOnClickListener(this);

        binding.include3.tvTemp.addTextChangedListener(editListener(binding.include3.tvTemp));
        binding.include3.tvEmp.addTextChangedListener(editListener(binding.include3.tvEmp));
        if (null != cardDetail) {
            binding.setModel(cardDetail);
        }
        switch (state) {
            case Constants.ADD_REPORT:
                binding.rlRemark.setVisibility(View.VISIBLE);
                break;
            case Constants.EDIT_REPORT:
                // 温度
                if (!TextUtils.isEmpty(submit.getTemperature())) {
                    binding.include3.tvTemp.setText(submit.getTemperature());
                    binding.include3.tvTemp.setSelection(submit.getTemperature().length());
                }
                //湿度
                if (!TextUtils.isEmpty(submit.getHumidity())) {
                    binding.include3.tvEmp.setText(submit.getHumidity());
                    binding.include3.tvEmp.setSelection(submit.getHumidity().length());
                }
                if (!TextUtils.isEmpty(submit.getCheckType())) {
                    binding.include3.tvCheckType.setText(PickerViewUtils.getCheckType(submit.getCheckType()));
                }
                if (!TextUtils.isEmpty(submit.getCheckOrganization())) {
                    binding.include3.tvCheckWhere.setText(PickerViewUtils.getCheckOrganization(submit.getCheckOrganization()));
                }
                //结果
                if (!TextUtils.isEmpty(submit.getCheckResult())) {
                    binding.tvResult.setText(submit.getCheckResult());
                    binding.resultSelect5.setText(submit.getCheckResult());
                    binding.include4.tvState.setText(submit.getCheckResult());
                    binding.checkResultElect.setText(submit.getCheckResult());
                }
                //备注
                binding.rlRemark.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(submit.getRemarks())) {
                    binding.remark.setText(submit.getRemarks());
                    binding.remark.setSelection(submit.getRemarks().length());
                }
                break;
            case Constants.LOOK_REPORT:
                binding.look3.setModel(submit);
                if (!TextUtils.isEmpty(submit.getCheckType())) {
                    binding.look3.tvCheckType.setText(PickerViewUtils.getCheckType(submit.getCheckType()));
                }
                if (!TextUtils.isEmpty(submit.getCheckOrganization())) {
                    binding.look3.tvCheckWhere.setText(PickerViewUtils.getCheckOrganization(submit.getCheckOrganization()));
                }
                //结果
                if (!TextUtils.isEmpty(submit.getCheckResult())) {
                    binding.tvResult.setText(submit.getCheckResult());
                    binding.resultSelect5.setText(submit.getCheckResult());
                    binding.include4.tvState.setText(submit.getCheckResult());
                    binding.checkResultElect.setText(submit.getCheckResult());
                }
                //备注
                if (!TextUtils.isEmpty(submit.getRemarks())) {
                    binding.remarkSee.setText(submit.getRemarks());
                    binding.remarkSee.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    String[] simulator = new String[]{"名称", "制造厂家", "规格型号", "出厂编码"};
    String[] simulatorVal = new String[]{"电气安全检测仪", "美国FLUKE", "ESA615", "3953003/3892675"};

    private void initData() {
        mFunCheckBeans = new ArrayList<>();
        mTypeBeans = new ArrayList<>();
        mTypes = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            SelectBean selectBean = new SelectBean();
            selectBean.setPrjType("性能模拟器");
            selectBean.setPriTypeCode("eleSimulator");
            selectBean.setPrjNo(i + 1 + "");
            selectBean.setEleSimulatorPj(simulator[i]);
            selectBean.setEleSimulatorPjVal(simulatorVal[i]);
            mTypes.add(selectBean);
        }
        mMapEdit = new HashMap<>();
        mMapType = new HashMap<>();
        if (state == 1) {
            for (int i = 0; i < mTitles.length; i++) {
                SelectBean selectBean = new SelectBean();
                selectBean.setPrjNo(i + 1 + "");
                selectBean.setPrjType("电源部分");
                selectBean.setElePowerPj(mTitles[i]);
//            selectBean.setFunCheckPrjVal();
                selectBean.setElePowerPjPerval(value[i]);
                selectBean.setValue(value_Number[i]);
                selectBean.setPriTypeCode("elePower");
                mFunCheckBeans.add(selectBean);
            }
            SelectBean selectBean = new SelectBean();
            selectBean.setPrjType("检测设备类型");
            selectBean.setEleCheckType("设备类型");
            selectBean.setPriTypeCode("eleCheckType");
            mTypeBeans.add(selectBean);
        } else if (state == 2 || state == 3) {
            for (SelectBean selectBean : submit.getPrjList()) {
                if (selectBean.getPrjType().equals("电源部分")) {
                    for (int i = 0; i < mTitles.length; i++) {
                        if (selectBean.getElePowerPj().equals(mTitles[i])) {
                            selectBean.setValue(value_Number[i]);
                        }
                    }
                    mFunCheckBeans.add(selectBean);
                    mMapEdit.put(selectBean.getElePowerPj(), selectBean.getElePowerPjResult().equals("合格") ? "合格" : "不合格");
                } else if (selectBean.getPrjType().equals("检测设备类型")) {
                    mTypeBeans.add(selectBean);
                    mMapType.put(selectBean.getPrjType(), selectBean.getEleCheckTypeVal().equals("B") ? 0 : selectBean.getEleCheckTypeVal().equals("BF") ? 1 : 2);
                }
            }
        }
        // 进行排序
//        Collections.sort(mFunCheckBeans, c);
        addView(mFunCheckBeans);
        addViewType(mTypeBeans);

    }

    private void addViewType(List<SelectBean> mList) {
        for (SelectBean bean : mList) {
            if (state == 3) {
                binding.include6.tvSee.setVisibility(View.VISIBLE);
                binding.include6.llPtf.setVisibility(View.GONE);
                TextView mTextView = binding.include6.tvSee;
                if (!TextUtils.isEmpty(bean.getEleCheckTypeVal())) {
                    mTextView.setText("设备检测类型:  " + bean.getEleCheckTypeVal());
                    binding.include6.tvState.setText(bean.getEleCheckTypeVal());
                    binding.resultSelect6.setText(bean.getEleCheckTypeVal());
                }
            } else {
                binding.include6.tvSee.setVisibility(View.GONE);
                binding.include6.llPtf.setVisibility(View.VISIBLE);
                TagFlowLayout tagFlowLayout = binding.include6.psResTF;
                if (!TextUtils.isEmpty(bean.getEleCheckTypeVal())) {
                    binding.include6.tvState.setText(bean.getEleCheckTypeVal());
                    binding.resultSelect6.setText(bean.getEleCheckTypeVal());
                }
                TagAdapter<String> tagAdapter = new TagAdapter<String>(Arrays.asList("B", "BF", "CF")) {
                    @Override
                    public View getView(FlowLayout parent, int position, String s) {
                        TextView tv = (TextView) LayoutInflater.from(ElectSafeActivity.this).inflate(R.layout.item_tag_full, tagFlowLayout, false);
                        tv.setText(s);
                        return tv;
                    }
                };
                tagFlowLayout.setAdapter(tagAdapter);
                if (!TextUtils.isEmpty(bean.getEleCheckTypeVal())) {
                    tagAdapter.setSelectedList(bean.getSelectPositionType(bean.getEleCheckTypeVal()));
                    //判断结果
                    binding.include6.tvState.setText(bean.getEleCheckTypeVal());
                    binding.resultSelect6.setText(bean.getEleCheckTypeVal());
                }
                tagFlowLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                    @Override
                    public boolean onTagClick(View view, int position, FlowLayout parent) {
                        bean.setEleCheckTypeVal(strs[position]);
                        if (((TagFlowLayout) parent).getSelectedList().size() == 0) {
                            mMapType.remove(bean.getPrjType());
                        } else {
                            mMapType.put(bean.getPrjType(), position);
                        }
                        String result = mMapType.size() != 1 ? "待选择" : strs[mMapType.get("检测设备类型")];
                        binding.include6.tvState.setText(result);
                        binding.resultSelect6.setText(result);
                        return true;
                    }
                });
            }
        }
    }

    private void checkResult(Map<String, String> mMapEdit, List<SelectBean> mList) {
        String result = mMapEdit.size() != mTitles.length ? "待填写" : notApply(mList) == 0 ? "合格" : notApply(mList) == 1? "不合格":"不适用";
        binding.include4.tvState.setText(result);
        binding.checkResultElect.setText(result);
        if(result.equals("不适用")){
            binding.tvResult.setText("不合格");
            binding.resultSelect5.setText("不合格");
            return;
        }
        binding.tvResult.setText(result.equals("待填写")?"待生成":result);
        binding.resultSelect5.setText(result.equals("待填写")?"待生成":result);
    }

    private void addView(List<SelectBean> mList) {
        if(state != 1){
            switch (notApply(mList)){
                case RESULT_OK:
                    binding.include4.tvState.setText("合格");
                    binding.checkResultElect.setText("合格");
                    break;
                case RESULT_FAIL:
                    binding.include4.tvState.setText("不合格");
                    binding.checkResultElect.setText("不合格");
                    break;
                case RESULT_NOTSUIT:
                    binding.include4.tvState.setText("不适用");
                    binding.checkResultElect.setText("不适用");
                    break;
            }
        }
        for (SelectBean bean : mList) {
            if (state == 3) {
                RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(ElectSafeActivity.this).inflate(R.layout.iten_look_see, null);
                TextView mTextView = mRl.findViewById(R.id.tv_see);
                if (!TextUtils.isEmpty(bean.getElePowerPj()) && !TextUtils.isEmpty(bean.getElePowerPjVal())) {
                    if(!bean.getElePowerPjVal().endsWith(".")){
                        mTextView.setText(bean.getElePowerPj() + ":  " + bean.getElePowerPjResult() + "— " + bean.getElePowerPjVal());
                    }else{
                        mTextView.setText(bean.getElePowerPj() + ":  " + bean.getElePowerPjResult() + "— " + bean.getElePowerPjVal().substring(0,bean.getElePowerPjVal().length()-1));
                    }
                }
                binding.include4.llTotal.addView(mRl);
            } else {
                RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(ElectSafeActivity.this).inflate(R.layout.item_include_elect_safe, null);
                TextView title = mRl.findViewById(R.id.title);
                if (!TextUtils.isEmpty(bean.getElePowerPj())) {
                    title.setText(bean.getElePowerPj());
                }
                TextView refValue = mRl.findViewById(R.id.ref_value);
                if (!TextUtils.isEmpty(bean.getElePowerPjPerval())) {
                    refValue.setText("参考值: " + bean.getElePowerPjPerval());
                }
                TextView state = mRl.findViewById(R.id.tv_state);
                if (!TextUtils.isEmpty(bean.getElePowerPjResult())) {
                    state.setText(bean.getElePowerPjResult());
                    if (bean.getElePowerPjResult().equals("合格")) {
                        state.setBackground(getResources().getDrawable(R.drawable.corners_bg_75ca5c));
                        state.setVisibility(View.VISIBLE);
                    } else if (bean.getElePowerPjResult().equals("不合格")) {
                        state.setBackground(getResources().getDrawable(R.drawable.corners_bg_fb6769));
                        state.setVisibility(View.VISIBLE);
                    } else if (bean.getElePowerPjResult().equals("不适用")) {
                        state.setBackground(getResources().getDrawable(R.drawable.corners_bg_ccccc));
                        state.setVisibility(View.VISIBLE);
                    }
                }
                EditText edit = mRl.findViewById(R.id.edit);
                if (!TextUtils.isEmpty(bean.getElePowerPjVal())) {
                    edit.setText(bean.getElePowerPjVal().contains("-") ? "-" : bean.getElePowerPjVal().endsWith(".")?bean.getElePowerPjVal().substring(0,bean.getElePowerPjVal().length()-1):bean.getElePowerPjVal());
                    edit.setSelection(edit.getText().toString().length());
                }
                edit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                        public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String s = editable.toString();
                        String real;
                        //如果起始位置为0,且第二位跟的不是".",则无法后续输入
                        if (s.startsWith("0")
                                && s.trim().length() > 1) {
                            if (!s.substring(1, 2).equals(".")) {
                                edit.setText(s.subSequence(0, 1));
                                edit.setSelection(1);
                            }
                        }
                        //判断是否已经输入过.了
                        if (edit.getText().toString().contains(".")) {
                            if (edit.getText().toString().indexOf(".", edit.getText().toString().indexOf(".") + 1) > 0) {
                                ToastUtil.toastWarning(ElectSafeActivity.this, "已经输入\".\"不能重复输入", -1);
                                edit.setText(edit.getText().toString().substring(0, edit.getText().toString().length() - 1));
                                edit.setSelection(edit.getText().toString().length());
                            }
                        }
                        //删除“.”后面超过2位后的数据
                        if (s.contains(".")) {
                            if (s.length() - 1 - s.indexOf(".") > digits) {
                                s = s.substring(0,
                                        s.indexOf(".") + digits + 1);
                                edit.setText(s);
                                edit.setSelection(s.length()); //光标移到最后
                            }
                        }
                        //如果"."在起始位置,则起始位置自动补0
                        if (s.substring(0).equals(".")) {
                            s = "0" + s;
                            edit.setText(s);
                            edit.setSelection(2);
                        }

                        if(s.equals(".")){
                            s = "0";
                        }
                        bean.setElePowerPjVal(s);
                        if (TextUtils.isEmpty(s)) {
                            state.setVisibility(View.GONE);
                            if (mMapEdit.containsKey(bean.getElePowerPj())) {
                                mMapEdit.remove(bean.getElePowerPj());
                            }
                            checkResult(mMapEdit, mList);
                            return;
                        }
                        if (s.contains("-")) {
                            state.setText("不适用");
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_ccccc));
                            state.setVisibility(View.VISIBLE);
                            bean.setElePowerPjResult("不适用");
                            mMapEdit.put(bean.getElePowerPj(), "不适用");
                            bean.setElePowerPjVal("-");
                            checkResult(mMapEdit, mList);
                            return;
                        }
                        mMapEdit.put(bean.getElePowerPj(), s);
                        if (setState(bean.getValue(), s)) {
                            state.setText("合格");
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_75ca5c));
                            state.setVisibility(View.VISIBLE);
                            bean.setElePowerPjResult("合格");
                        } else {
                            state.setText("不合格");
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_fb6769));
                            state.setVisibility(View.VISIBLE);
                            bean.setElePowerPjResult("不合格");
                        }
                        checkResult(mMapEdit, mList);
                    }
                });
                binding.include4.llTotal.addView(mRl);
            }
        }
    }

    /**
     * 0 合格  1  不合格  2  不适用
     *
     * @param mList
     * @return
     */
    private int notApply(List<SelectBean> mList) {
        int number = 0;
        for (SelectBean selectBean : mList) {
            if (selectBean.getElePowerPjResult().equals("不适用")) {
                number++;
                continue;
            }
            if (selectBean.getElePowerPjResult().equals("不合格")) {
                return RESULT_FAIL;
            }
        }
        if(number == mList.size()){
            return RESULT_NOTSUIT;
        }
        return RESULT_OK;
    }
    private boolean isPass(List<SelectBean> mList) {
        int number = 0;
        for (SelectBean selectBean : mList) {
            if (selectBean.getElePowerPjResult().equals("不合格")) {
                return false;
            }
        }
        return true;
    }

    private boolean setState(String value, String real) {
        if (!TextUtils.isEmpty(value) && !TextUtils.isEmpty(real)) {
            if(real.endsWith(".")){
                if(real.equals(".")){
                    real = "0";
                }else{
                    real = real.substring(0,real.length()-1);
                }
            }
            float v = Float.parseFloat(value.substring(2));
            float r = Float.parseFloat(real);

            if (value.substring(1, 2).equals("=")) {
                if (value.substring(0, 1).equals(">")) {
                    return r >= v;
                } else if (value.substring(0, 1).equals("<")) {
                    return r <= v;
                }
            } else {
                if (value.substring(0, 1).equals(">")) {
                    return r > v;
                } else if (value.substring(0, 1).equals("<")) {
                    return r < v;
                }
            }
        }
        return false;
    }
    LinearLayout linearLayout_include3;
    LinearLayout include1;
    private void showAndHide(int type) {
        if(state == 3){
            linearLayout_include3 = binding.look3.llTotal;
        }else{
            linearLayout_include3 = binding.include3.llTotal;
        }
        if(null == cardDetail){
            include1 = binding.rlOffLine;
        }else{
            include1 = binding.card;
        }
        switch (type) {
            case 1:
                binding.include2.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.llTotal.setVisibility(View.GONE);
                binding.include5.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (include1.getVisibility() == View.VISIBLE) {
                    include1.setVisibility(View.GONE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                } else {
                    include1.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.GONE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                include1.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.llTotal.setVisibility(View.GONE);
                binding.include5.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (binding.include2.getVisibility() == View.VISIBLE) {
                    binding.include2.setVisibility(View.GONE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);

                } else {
                    binding.include2.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.GONE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                }
                break;
            case 3:
                include1.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                binding.include4.llTotal.setVisibility(View.GONE);
                binding.include5.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (linearLayout_include3.getVisibility() == View.VISIBLE) {
                    linearLayout_include3.setVisibility(View.GONE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                } else {
                    linearLayout_include3.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.GONE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                include1.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                binding.include5.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (binding.include4.llTotal.getVisibility() == View.VISIBLE) {
                    binding.include4.llTotal.setVisibility(View.GONE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                } else {
                    binding.include4.llTotal.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.GONE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                }
                break;
            case 5:
                include1.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.llTotal.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (binding.include5.getVisibility() == View.VISIBLE) {
                    binding.include5.setVisibility(View.GONE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                } else {
                    binding.include5.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.GONE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                }
                break;
            case 6:
                include1.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.llTotal.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                binding.include5.setVisibility(View.GONE);

                //自己是否显示，若显示则隐藏
                if (binding.include6.llTotal.getVisibility() == View.VISIBLE) {
                    binding.include6.llTotal.setVisibility(View.GONE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                } else {
                    binding.include6.llTotal.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.GONE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_back_ll:
                onBackPressed();
                break;
            case R.id.rl_select_1:
                showAndHide(1);
                break;
            case R.id.rl_select_2:
                showAndHide(2);
                break;
            case R.id.rl_select_3:
                showAndHide(3);
                break;
            case R.id.rl_select_4:
                showAndHide(4);
                break;
            case R.id.rl_select_6:
                showAndHide(6);
                break;
            case R.id.rl_select_5:
                showAndHide(5);
//                checkResult(mMapEdit, mFunCheckBeans);
                break;
            case R.id.title_menu_confirm:
                //save
                if (TextUtils.isEmpty(binding.include3.tvCheckType.getText().toString())
                        || TextUtils.isEmpty(binding.include3.tvCheckWhere.getText().toString())
                        || TextUtils.isEmpty(binding.include3.tvTemp.getText().toString())
                        || TextUtils.isEmpty(binding.include3.tvEmp.getText().toString())
                        ) {
                    showCheckFailedDialog("基础信息模块有信息尚未填完,请填完保存");
                    return;
                }
                if (mMapType.size() != 1 || binding.include6.tvState.getText().equals("待选择")) {
                    showCheckFailedDialog("检测设备类型模块尚未选择,请选完保存");
                    return;
                }
                if (mMapEdit.size() != mFunCheckBeans.size() || binding.checkResultElect.getText().equals("待填写")) {
                    showCheckFailedDialog("电源部分模块有信息尚未填完,请选完保存");
                    return;
                }

                if (null == submit)
                    submit = new PmReportDtl();
                submit.setCheckModel("默认模板");
                submit.setCheckProject("85-2");
                submit.setCheckProjectName("电安全检测");
                submit.setCheckType(PickerViewUtils.checkType);
                submit.setCheckOrganization(PickerViewUtils.checkOrganization);
                submit.setTemperature(binding.include3.tvTemp.getText().toString().trim());
                submit.setHumidity(binding.include3.tvEmp.getText().toString().trim());
                submit.setCheckResult(binding.tvResult.getText().toString());
                if (!TextUtils.isEmpty(binding.remark.getText().toString().trim())) {
                    submit.setRemarks(binding.remark.getText().toString().trim());
                }
                List<SelectBean> mAll = new ArrayList<>();
                mAll.addAll(mFunCheckBeans);
                mAll.addAll(mTypeBeans);
                mAll.addAll(mTypes);
                submit.setPrjList(mAll);
                Intent intent = new Intent();
                intent.putExtra("ElecCheckResult", submit);
                setResult(Constants.RESULT_OK, intent);
                ToastUtil.toastImage(getApplication().getApplicationContext(), "保存成功", -1);
                finish();
                break;
            case R.id.rl_type:
                //save
//                viewModel.getDictData("86");
                typeOptionsPickerView = PickerViewUtils.getOptionPickerView("", typeList, PickerViewUtils.getCheckTypePosition(binding.include3.tvCheckType.getText().toString()), ElectSafeActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckType, typeList, 1));
                typeOptionsPickerView.show();

                break;
            case R.id.rl_room:
                //save
//                viewModel.getDictData("87");
                roomOptionsPickerView = PickerViewUtils.getOptionPickerView("", roomList, PickerViewUtils.getCheckOrganizationPosition(binding.include3.tvCheckWhere.getText().toString()), ElectSafeActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckWhere, roomList, 2));
                roomOptionsPickerView.show();
                break;
        }
    }



    @Override
    public void onBackPressed() {
        if(state != 3){
            noticeSave();
        }else {
            super.onBackPressed();
        }
    }
}
