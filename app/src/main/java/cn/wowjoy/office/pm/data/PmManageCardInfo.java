package cn.wowjoy.office.pm.data;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import cn.wowjoy.office.pm.db.PmReportListEntity;

public class PmManageCardInfo implements Serializable {
    private String id;
    private String cardNo;
    private String cardName;
    private String departmentName;
    private String produceNo;
    private String manufacturerName;
    private String companyName;
    private String assetsSpec;
    private String pmFunModel;
    private String pmFunModelName;
    private String qrCode;
    private String storeAddress;
    private String storekeeperName;
    private String cyclePm;
    private String lastPmDate;
    private String checkResultName;
    private String nextPmDate;
    private String supplierName;
    private String startDate;
    private List<PmReportListEntity> pmList;

    public String getStartDate() {
        return startDate;
    }
    public String getStartDateC() {
        if(TextUtils.isEmpty(startDate)){
            return "无";
        }
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getSupplierName() {
        return supplierName;
    }
    public String getSupplierNameC() {
        if(TextUtils.isEmpty(supplierName)){
            return "无";
        }
        return supplierName;
    }
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardName() {
        return cardName;
    }
    public String getCardNameC() {
        if(TextUtils.isEmpty(cardName)){
            return "无";
        }
        return cardName;
    }
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getDepartmentName() {
        return departmentName;
    }
    public String getDepartmentNameC() {
        if(TextUtils.isEmpty(departmentName)){
            return "无";
        }
        return departmentName;
    }
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getProduceNo() {
        return produceNo;
    }
    public String getProduceNoC() {
        if(TextUtils.isEmpty(produceNo)){
            return "无";
        }
        return produceNo;
    }
    public void setProduceNo(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }
    public String getManufacturerNameC() {
        if(TextUtils.isEmpty(manufacturerName)){
            return "无";
        }
        return manufacturerName;
    }
    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
    public String getCompanyNameC() {
        if(TextUtils.isEmpty(companyName)){
            return "无";
        }
        return companyName;
    }
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAssetsSpecC() {
        if(TextUtils.isEmpty(assetsSpec)){
            return "无";
        }
        return assetsSpec;
    }
    public String getAssetsSpec() {
        return assetsSpec;
    }

    public void setAssetsSpec(String assetsSpec) {
        this.assetsSpec = assetsSpec;
    }

    public String getPmFunModel() {
        return pmFunModel;
    }

    public void setPmFunModel(String pmFunModel) {
        this.pmFunModel = pmFunModel;
    }

    public String getPmFunModelName() {
        return pmFunModelName;
    }
    public String getPmFunModelNameC() {
        if(TextUtils.isEmpty(pmFunModelName)){
            return "无";
        }
        return pmFunModelName;
    }
    public void setPmFunModelName(String pmFunModelName) {
        this.pmFunModelName = pmFunModelName;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getStoreAddress() {
        return storeAddress;
    }
    public String getStoreAddressC() {
        if(TextUtils.isEmpty(storeAddress)){
            return "无";
        }
        return storeAddress;
    }
    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStorekeeperNameC() {
        if(TextUtils.isEmpty(storekeeperName)){
            return "无";
        }
        return storekeeperName;
    }
    public String getStorekeeperName() {
        return storekeeperName;
    }

    public void setStorekeeperName(String storekeeperName) {
        this.storekeeperName = storekeeperName;
    }

    public String getCyclePm() {
        return cyclePm;
    }
    public String getCyclePmC() {
        if(TextUtils.isEmpty(cyclePm)){
            return "无";
        }
        return cyclePm+"（月）";
    }
    public void setCyclePm(String cyclePm) {
        this.cyclePm = cyclePm;
    }

    public String getLastPmDate() {
        return lastPmDate;
    }
    public String getLastPmDateC() {
        if(TextUtils.isEmpty(lastPmDate)){
            return "无";
        }
        return lastPmDate;
    }
    public void setLastPmDate(String lastPmDate) {
        this.lastPmDate = lastPmDate;
    }

    public String getCheckResultName() {
        return checkResultName;
    }
    public String getCheckResultNameC() {
        if(TextUtils.isEmpty(checkResultName)){
            return "无";
        }
        return checkResultName;
    }
    public void setCheckResultName(String checkResultName) {
        this.checkResultName = checkResultName;
    }

    public String getNextPmDate() {
        return nextPmDate;
    }
    public String getNextPmDateC() {
        if(TextUtils.isEmpty(nextPmDate)){
            return "无";
        }
        return nextPmDate;
    }

    public void setNextPmDate(String nextPmDate) {
        this.nextPmDate = nextPmDate;
    }

    public List<PmReportListEntity> getPmList() {
        return pmList;
    }

    public void setPmList(List<PmReportListEntity> pmList) {
        this.pmList = pmList;
    }
}
