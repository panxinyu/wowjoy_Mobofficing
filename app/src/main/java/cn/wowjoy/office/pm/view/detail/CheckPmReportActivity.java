package cn.wowjoy.office.pm.view.detail;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityCheckPmReportBinding;
import cn.wowjoy.office.pm.data.PmManageCardInfo;
import cn.wowjoy.office.pm.data.PmReportDetailResponse;
import cn.wowjoy.office.pm.view.PMBaseActivity;
import cn.wowjoy.office.pm.view.PmReportDetailActivity;
import cn.wowjoy.office.pm.view.viewodel.CheckPmReportViewModel;
import cn.wowjoy.office.utils.ToastUtil;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class CheckPmReportActivity extends PMBaseActivity<ActivityCheckPmReportBinding, CheckPmReportViewModel> implements View.OnClickListener {
    private PmManageCardInfo info;

    public static void launch(Activity activity, PmManageCardInfo info) {
        Intent intent = new Intent(activity, CheckPmReportActivity.class);
        intent.putExtra("info", info);
        activity.startActivity(intent);
    }


    @Override
    protected Class<CheckPmReportViewModel> getViewModel() {
        return CheckPmReportViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_pm_report;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        info = (PmManageCardInfo) getIntent().getSerializableExtra("info");
        binding.setViewModel(viewModel);
        binding.setModel(info);
        initView();
        initObserve();
    }

    private void initObserve() {
        //网络请求得数据
        viewModel.detail.observe(this, new Observer<LiveDataWrapper<PmReportDetailResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<PmReportDetailResponse> pmReportDetailResponseLiveDataWrapper) {
                switch (pmReportDetailResponseLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(CheckPmReportActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(CheckPmReportActivity.this);
                        if (null != pmReportDetailResponseLiveDataWrapper.data) {
                            PmReportDetailActivity.launch(CheckPmReportActivity.this, Constants.LOOK_REPORT, pmReportDetailResponseLiveDataWrapper.data.getPmReportVO().getQrCode(), pmReportDetailResponseLiveDataWrapper.data, false);
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(CheckPmReportActivity.this);
                        handleException(pmReportDetailResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });
    }

    private void initView() {
        binding.lookCheckTitle.titleTextTv.setText("查看设备详情");
        binding.lookCheckTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.lookCheckTitle.titleBackTv.setText("");
        binding.lookCheckTitle.titleBackLl.setOnClickListener(this);
        binding.lookCheckTitle.titleMenuConfirm.setText("生成报告");
        binding.lookCheckTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        binding.lookCheckTitle.titleMenuConfirm.setOnClickListener(this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(CheckPmReportActivity.this));
        binding.recyclerView.setAdapter(viewModel.mAdapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(CheckPmReportActivity.this, SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("暂无PM记录");
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
//        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               
//            }
//        });
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setLoadMoreEnabled(false);
        if (null != info.getPmList()) {
            viewModel.setDate(info.getPmList());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                onBackPressed();
                break;
            case R.id.title_menu_confirm:
                ToastUtil.toastWarning(CheckPmReportActivity.this, "功能暂不开放", -1);
                break;
        }
    }
}
