package cn.wowjoy.office.pm.data;

import java.io.Serializable;
import java.util.List;

public class DictDataResponse implements Serializable {
    private List<DictData> list;

    public List<DictData> getList() {
        return list;
    }

    public void setList(List<DictData> list) {
        this.list = list;
    }

    public class  DictData{
        private String dictTypeValue;
        private String dictDataValue;
        private String dictTypeName;
        private String dictId;
        private String dictDataName;

        public String getDictTypeValue() {
            return dictTypeValue;
        }

        public void setDictTypeValue(String dictTypeValue) {
            this.dictTypeValue = dictTypeValue;
        }

        public String getDictDataValue() {
            return dictDataValue;
        }

        public void setDictDataValue(String dictDataValue) {
            this.dictDataValue = dictDataValue;
        }

        public String getDictTypeName() {
            return dictTypeName;
        }

        public void setDictTypeName(String dictTypeName) {
            this.dictTypeName = dictTypeName;
        }

        public String getDictId() {
            return dictId;
        }

        public void setDictId(String dictId) {
            this.dictId = dictId;
        }

        public String getDictDataName() {
            return dictDataName;
        }

        public void setDictDataName(String dictDataName) {
            this.dictDataName = dictDataName;
        }
    }
}
