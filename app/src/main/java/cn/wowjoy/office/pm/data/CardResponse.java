package cn.wowjoy.office.pm.data;

import java.io.Serializable;

public class CardResponse implements Serializable{
    private String id;
    private String cardNo;
    private String cardName;
    private String departmentName;
    private String produceNo;
    private String manufacturerName; //生产产商
    private String companyName;
    private String assetsSpec;
    private String pmFunModel;  //性能模板
    private String pmFunModelName; //性能模板名称
    private String qrCode; //  卡片的二维码信息

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getProduceNo() {
        return produceNo;
    }

    public void setProduceNo(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAssetsSpec() {
        return assetsSpec;
    }

    public void setAssetsSpec(String assetsSpec) {
        this.assetsSpec = assetsSpec;
    }

    public String getPmFunModel() {
        return pmFunModel;
    }

    public void setPmFunModel(String pmFunModel) {
        this.pmFunModel = pmFunModel;
    }

    public String getPmFunModelName() {
        return pmFunModelName;
    }

    public void setPmFunModelName(String pmFunModelName) {
        this.pmFunModelName = pmFunModelName;
    }
}
