package cn.wowjoy.office.pm.data.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.pm.data.SelectBean;

public  class PmReportDtl implements Serializable {
        private String   id;
        private String   isDeleted;
        private String   billId;
        private String   checkProject;
        private String   checkModel;
        private String   checkType;
        private String   checkOrganization;
        private String   temperature;
        private String   humidity;
        private String   checkResult;
        private String   remarks;
        private List<SelectBean> prjList;
        private String checkProjectName;
        private String checkModelName;

    public String getCheckModelName() {
        return checkModelName;
    }

    public void setCheckModelName(String checkModelName) {
        this.checkModelName = checkModelName;
    }

    public String getCheckProjectName() {
        return checkProjectName;
    }

    public void setCheckProjectName(String checkProjectName) {
        this.checkProjectName = checkProjectName;
    }

    public String getHumidityC(){
       if(null == humidity){
           return "暂无";
       }
       return "";
   }
   public  void addSelectBean(List<SelectBean> mlists){
       if(null == prjList)
           prjList=new ArrayList<>();
       if(null != mlists){
           prjList.addAll(mlists);
       }

   }
    public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(String isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getBillId() {
            return billId;
        }

        public void setBillId(String billId) {
            this.billId = billId;
        }

        public String getCheckProject() {
            return checkProject;
        }

        public void setCheckProject(String checkProject) {
            this.checkProject = checkProject;
        }

        public String getCheckModel() {
            return checkModel;
        }

        public void setCheckModel(String checkModel) {
            this.checkModel = checkModel;
        }

        public String getCheckType() {
            return checkType;
        }

        public void setCheckType(String checkType) {
            this.checkType = checkType;
        }

        public String getCheckOrganization() {
            return checkOrganization;
        }

        public void setCheckOrganization(String checkOrganization) {
            this.checkOrganization = checkOrganization;
        }

        public String getTemperature() {
            return temperature;
        }
        public String getTemperatureC() {
        if(temperature.endsWith(".")){
            temperature = temperature.substring(0,temperature.indexOf("."));
        }
            return temperature+" ℃";
        }
        public void setTemperature(String temperature) {
            this.temperature = temperature;
        }

        public String getHumidity() {
            return humidity;
        }
        public String getHumidityD() {
            if(humidity.endsWith(".")){
                humidity = humidity.substring(0,humidity.indexOf("."));
            }
           return humidity+" %";
        }
        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }

        public String getCheckResult() {
            return checkResult;
        }

        public void setCheckResult(String checkResult) {
            this.checkResult = checkResult;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public List<SelectBean> getPrjList() {
            return prjList;
        }

        public void setPrjList(List<SelectBean> prjList) {
            this.prjList = prjList;
        }
    }
