package cn.wowjoy.office.pm.data;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

import cn.wowjoy.office.pm.db.PmReportListEntity;

public class PmReportIndexResponse implements Serializable{

    private int count;
    private List<PmReportListEntity> list;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<PmReportListEntity> getList() {
        return list;
    }

    public void setList(List<PmReportListEntity> list) {
        this.list = list;
    }

    public class PmReportIndexDetail implements Serializable{
        private String id;
        private String billNo;
        private String cardName;
        private String cardDeptName;
        private String pmDate;
        private String billStatusName;  //单据状态
        private String checkResult;
        private String checkResultName; //检测结果

        private String keyWord;  //搜索关键字

        private boolean isShow = false;  //展示 勾选框  长按一个条目就要显示所有
        private boolean isSelect = false; //是否被勾选
        private String remarks;//离线备注

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getCardDeptName() {
            return cardDeptName;
        }

        public void setCardDeptName(String cardDeptName) {
            this.cardDeptName = cardDeptName;
        }

        public boolean isShow() {
            return isShow;
        }

        public void setShow(boolean show) {
            isShow = show;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }

        public String getKeyWord() {
            return keyWord;
        }

        public void setKeyWord(String keyWord) {
            this.keyWord = keyWord;
        }

        public String getCheckResult() {
            return checkResult;
        }

        public void setCheckResult(String checkResult) {
            this.checkResult = checkResult;
        }

        public boolean getStatus() {
            return !TextUtils.isEmpty(checkResultName) && checkResultName.equals("正常");
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBillNo() {
            return billNo;
        }

        public void setBillNo(String billNo) {
            this.billNo = billNo;
        }

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }


        public String getPmDate() {
            return pmDate;
        }

        public void setPmDate(String pmDate) {
            this.pmDate = pmDate;
        }

        public String getBillStatusName() {
            return billStatusName;
        }

        public void setBillStatusName(String billStatusName) {
            this.billStatusName = billStatusName;
        }

        public String getCheckResultName() {
            return checkResultName;
        }

        public void setCheckResultName(String CheckResultName) {
            this.checkResultName = CheckResultName;
        }
    }



}
