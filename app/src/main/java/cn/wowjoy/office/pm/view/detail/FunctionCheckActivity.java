package cn.wowjoy.office.pm.view.detail;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityFuntionCheckBinding;
import cn.wowjoy.office.pm.data.CardResponse;
import cn.wowjoy.office.pm.data.DictDataResponse;
import cn.wowjoy.office.pm.data.SelectBean;
import cn.wowjoy.office.pm.data.request.PmReportDtl;
import cn.wowjoy.office.pm.view.PMBaseActivity;
import cn.wowjoy.office.pm.view.viewodel.FunctionCheckViewModel;
import cn.wowjoy.office.utils.MyTextWatcher;
import cn.wowjoy.office.utils.PickerViewUtils;
import cn.wowjoy.office.utils.ToastUtil;

import static cn.wowjoy.office.pm.view.PmReportDetailActivity.FUNCTION;

public class FunctionCheckActivity extends PMBaseActivity<ActivityFuntionCheckBinding, FunctionCheckViewModel> implements View.OnClickListener {
    private MDialog waitDialog;
    private final String[] strs = {"合格", "不合格", "不适用"};
    String[] mTitles = new String[]{"报警限检查", "声光报警", "静音检查"};
    int TYPE_CHECK = 1;
    int TYPE_MEASURE = 2;
    private OptionsPickerView typeOptionsPickerView;
    private int typePosition;
    private String checkType;
    private String[] checkTypeStrs = new String[]{"验收", "预防性检测", "稳定性检测"};
    private OptionsPickerView roomOptionsPickerView;
    private int roomPosition;
    private String checkOrgannization;

    //温度
    private String temperature;
    //湿度
    private String humidity;
    //备注
    private String remarks;
    private List<SelectBean> mMeasureList;
    private Map<String, Integer> mMapMeasure;

    //测量检查
    private String[] checkValue = new String[]{"40(38.8~41.2)", "60(58.2~61.8)",
            "130(126.1~133.9)", "60/30(40)", "120/80(93)", "200/150(167)", "88", "99", "94"};
    private String[] checkTitles = new String[]{"心电脉搏1(次/min)", "心电脉搏2(次/min)", "心电脉搏3(次/min)",
            "无创血压(mmHg)-低血压模式", "无创血压(mmHg)-正常血压模式", "无创血压(mmHg)-高血压模式", "血氧饱和度1(%)",
            "血氧饱和度2(%)", "血氧饱和度3(%)"};
    private String[] errVals = new String[]{"±3％", "±3％", "±3％", "±10mmHg", "±10mmHg", "±10mmHg", "±2", "±2", "±2"};
    private List<SelectBean> mCheckList;
    private Map<String, String> mMapCheck;

    private int state;
    private String cardId;
    private CardResponse cardDetail;
    private PmReportDtl submit;
    private String checkModel;

    private SelectBean lookBean;//性能外观实体


    public static void launch(Activity context, int state, String cardId, CardResponse detail, PmReportDtl edit, String checkModel) {
        Intent i = new Intent(context, FunctionCheckActivity.class);
        if(null != edit && TextUtils.isEmpty(edit.getCheckResult())){
            state = 1;
        }
        i.putExtra(Constants.PM_REPORT, state);
        i.putExtra("cardId", cardId);
        i.putExtra("cardDetail", detail);
        i.putExtra("pmReportDtl", edit);
        i.putExtra("checkModel", checkModel);
        context.startActivityForResult(i, FUNCTION);
    }

    @Override
    protected Class<FunctionCheckViewModel> getViewModel() {
        return FunctionCheckViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_funtion_check;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        state = getIntent().getIntExtra(Constants.PM_REPORT, 0);
        cardId = getIntent().getStringExtra("cardId");
        cardDetail = (CardResponse) getIntent().getSerializableExtra("cardDetail");
        submit = (PmReportDtl) getIntent().getSerializableExtra("pmReportDtl");
        checkModel = getIntent().getStringExtra("checkModel");
        initView();
        initData();
        response();
        observe();
    }

    private void observe() {
        viewModel.typeData.observe(FunctionCheckActivity.this, new Observer<LiveDataWrapper<DictDataResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<DictDataResponse> dictDataResponseLiveDataWrapper) {
                switch (dictDataResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(FunctionCheckActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(FunctionCheckActivity.this, waitDialog);
                        }
                        typeList.clear();
                        if (null != dictDataResponseLiveDataWrapper.data && dictDataResponseLiveDataWrapper.data.getList().size() > 0) {
                            for (DictDataResponse.DictData dictData : dictDataResponseLiveDataWrapper.data.getList()) {
                                typeList.add(dictData.getDictDataName());
                            }
                            typeOptionsPickerView = PickerViewUtils.getOptionPickerView("", typeList, PickerViewUtils.getCheckTypePosition(PickerViewUtils.checkType), FunctionCheckActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckType, typeList, 1));
                            typeOptionsPickerView.show();
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(FunctionCheckActivity.this, waitDialog);
                        }
                        handleException(dictDataResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
        viewModel.roomData.observe(FunctionCheckActivity.this, new Observer<LiveDataWrapper<DictDataResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<DictDataResponse> dictDataResponseLiveDataWrapper) {
                switch (dictDataResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(FunctionCheckActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(FunctionCheckActivity.this, waitDialog);
                        }
                        roomList.clear();
                        if (null != dictDataResponseLiveDataWrapper.data && dictDataResponseLiveDataWrapper.data.getList().size() > 0) {
                            for (DictDataResponse.DictData dictData : dictDataResponseLiveDataWrapper.data.getList()) {
                                roomList.add(dictData.getDictDataName());
                            }
                            roomOptionsPickerView = PickerViewUtils.getOptionPickerView("", roomList, PickerViewUtils.getCheckOrganizationPosition(PickerViewUtils.checkOrganization), FunctionCheckActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckWhere, roomList, 2));
                            roomOptionsPickerView.show();
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(FunctionCheckActivity.this, waitDialog);
                        }
                        handleException(dictDataResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
    }

    private void response() {
    }

    private void initView() {
        binding.include3.tvTemp.addTextChangedListener(editListener(binding.include3.tvTemp));
        binding.include3.tvEmp.addTextChangedListener(editListener(binding.include3.tvEmp));
        binding.remarkReason.addTextChangedListener(new MyTextWatcher(binding.tvLimitReason, binding.remarkReason, 50, FunctionCheckActivity.this));
        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit, binding.remark, 50, FunctionCheckActivity.this));

        binding.lookCheckTitle.titleTextTv.setText("性能检测");
        binding.lookCheckTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.lookCheckTitle.titleBackTv.setText("");
        binding.lookCheckTitle.titleBackLl.setOnClickListener(this);
        binding.lookCheckTitle.titleMenuConfirm.setText("保存");
        if (state != 3) {
            binding.lookCheckTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        }
        binding.lookCheckTitle.titleMenuConfirm.setOnClickListener(this);

        binding.rlSelect1.setOnClickListener(this);
        binding.rlSelect2.setOnClickListener(this);
        binding.rlSelect3.setOnClickListener(this);
        binding.rlSelect4.setOnClickListener(this);
        binding.rlSelect5.setOnClickListener(this);
        binding.rlSelect6.setOnClickListener(this);
        binding.rlSelect7.setOnClickListener(this);
        binding.include3.rlType.setOnClickListener(this);
        binding.include3.rlRoom.setOnClickListener(this);
        binding.include5.tvState.setText("待填写");
        binding.include5.title.setText("测量检查");
        binding.include6.tvState.setText("待选择");
        binding.include6.title.setText("其他检测");

        binding.brTrue.setOnClickListener(this);
        binding.brFalse.setOnClickListener(this);
        binding.remarkReason.addTextChangedListener(new MyTextWatcher(binding.tvLimitReason, binding.remarkReason, 50, FunctionCheckActivity.this));
        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit, binding.remark, 50, FunctionCheckActivity.this));
        if (null != cardDetail) {
            binding.setModel(cardDetail);
        }
        switch (state) {
            case Constants.ADD_REPORT:
                binding.brTrue.setSelected(false);
                binding.brFalse.setSelected(false);
                binding.rlRemark.setVisibility(View.VISIBLE);
                break;
            case Constants.EDIT_REPORT:
                // 温度
                if (!TextUtils.isEmpty(submit.getTemperature())) {
                    binding.include3.tvTemp.setText(submit.getTemperature());
                    binding.include3.tvTemp.setSelection(submit.getTemperature().length());
                }
                //湿度
                if (!TextUtils.isEmpty(submit.getHumidity())) {
                    binding.include3.tvEmp.setText(submit.getHumidity());
                    binding.include3.tvEmp.setSelection(submit.getHumidity().length());
                }
                if (!TextUtils.isEmpty(submit.getCheckType())) {
                    binding.include3.tvCheckType.setText(PickerViewUtils.getCheckType(submit.getCheckType()));
                }
                if (!TextUtils.isEmpty(submit.getCheckOrganization())) {
                    binding.include3.tvCheckWhere.setText(PickerViewUtils.getCheckOrganization(submit.getCheckOrganization()));
                }
                //结果
                if (!TextUtils.isEmpty(submit.getCheckResult())) {
                    binding.tvResult.setText(submit.getCheckResult());
                    binding.resultSelect7.setText(submit.getCheckResult());
                }
                //备注
                binding.rlRemark.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(submit.getRemarks())) {
                    binding.remark.setText(submit.getRemarks());
                    binding.remark.setSelection(submit.getRemarks().length());
                }
                break;
            case Constants.LOOK_REPORT:
                binding.look3.setModel(submit);
                if (!TextUtils.isEmpty(submit.getCheckType())) {
                    binding.look3.tvCheckType.setText(PickerViewUtils.getCheckType(submit.getCheckType()));
                }
                if(!TextUtils.isEmpty(submit.getCheckOrganization())){
                    binding.look3.tvCheckWhere.setText(PickerViewUtils.getCheckOrganization(submit.getCheckOrganization()));
                }
                //结果
                if(!TextUtils.isEmpty(submit.getCheckResult())){
                    binding.tvResult.setText(submit.getCheckResult());
                    binding.resultSelect7.setText(submit.getCheckResult());
                }
                //备注
                if (!TextUtils.isEmpty(submit.getRemarks())) {
                    binding.remarkSee.setText(submit.getRemarks());
                    binding.remarkSee.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private int size = 0;
    private int size2 = 1;
    private List<SelectBean> mTypes;
    String[] simulator =new String[]{"名称","制造厂家","规格型号","出厂编码"};
    String[] simulatorVal =new String[]{"生命体征模拟仪","美国PLUKE","Prosim8","3906021"};
    private void initData() {
        mMeasureList = new ArrayList<>();
        mCheckList = new ArrayList<>();
        mMapMeasure = new HashMap<>();
        mMapCheck = new HashMap<>();
        mTypes = new ArrayList<>();
        for (int i=0;i<4;i++){
            SelectBean selectBean =new SelectBean();
            selectBean.setPrjType("性能模拟器");
            selectBean.setPrjNo(i+1+"");
            selectBean.setPriTypeCode("funSimulator");
            selectBean.setFunSimulatorPj(simulator[i]);
            selectBean.setFunSimulatorPjVal(simulatorVal[i]);
            mTypes.add(selectBean);
        }
        if (state == 1) {
            for (String title : checkTitles) {
                SelectBean selectBean = new SelectBean("测量检查", size + 1 + "");
                selectBean.setFunCheckPrj(title);
                selectBean.setFunCheckPrjPerval(checkValue[size]);
                selectBean.setFunCheckPrjErrval(errVals[size]);
                selectBean.setPriTypeCode("funCheck");
                if (title.contains("无创血压")) {
                    selectBean.setMoreCompare(true);
                }
                mCheckList.add(selectBean);
                size++;
            }
            for (String mTitle : mTitles) {
                SelectBean selectBean = new SelectBean("其他检测", size2 + "");
                selectBean.setFunOtherPrj(mTitle);
                selectBean.setPriTypeCode("funOther");
                mMeasureList.add(selectBean);
                size2++;
            }
            //性能外观检测
            lookBean = new SelectBean();
            lookBean.setPrjNo(1 + "");
            lookBean.setPrjType("性能外观功能");
            lookBean.setPriTypeCode("funApp");
            lookBean.setFunAppPj(getString(R.string.funAppPj));
        } else if (state == 2|| state == 3) {
            for (SelectBean selectBean : submit.getPrjList()) {
                switch (selectBean.getPrjType()) {
                    case "测量检查":
                        if (selectBean.getFunCheckPrj().contains("无创血压")) {
                            selectBean.setMoreCompare(true);
                        }
                        mCheckList.add(selectBean);
                        mMapCheck.put(selectBean.getFunCheckPrj(), selectBean.getFunCheckPrjResult().equals("合格") ? "合格" : "不合格");
                        break;
                    case "其他检测":
                        mMeasureList.add(selectBean);
                        mMapMeasure.put(selectBean.getFunOtherPrj(), selectBean.getFunOtherPrjVal().equals("合格") ? 0 : selectBean.getFunOtherPrjVal().equals("不合格") ? 1 : 2);
                        break;
                    case "性能外观功能":
                        lookBean = selectBean;
                        //设置性能外观的备注 选中状态按钮
                        if(state == 3){
                            binding.llSelectInclude4.setVisibility(View.GONE);
                            if (!TextUtils.isEmpty(lookBean.getFunAppPjVal())) {
                                binding.resultLook.setText(lookBean.getFunAppPjVal());
                                binding.resultSelect4.setText(lookBean.getFunAppPjVal());
                                if(lookBean.getFunAppPjVal().equals("不合格")){
                                    binding.seeReason.setVisibility(View.VISIBLE);
                                }
                                if (!TextUtils.isEmpty(lookBean.getFunAppPjRem())) {
                                    binding.seeReason.setText("不合格原因: "+lookBean.getFunAppPjRem());
                                }
                            }
                        }else{
                            if (!TextUtils.isEmpty(lookBean.getFunAppPjVal())) {
                                setButtonSelect(lookBean.getFunAppPjVal());
                                binding.resultLook.setText(lookBean.getFunAppPjVal());
                                binding.resultSelect4.setText(lookBean.getFunAppPjVal());
                            }
                            if (!TextUtils.isEmpty(lookBean.getFunAppPjRem())) {
                                binding.remarkReason.setText(lookBean.getFunAppPjRem());
                                binding.remarkReason.setSelection(lookBean.getFunAppPjRem().length());
                            }
                        }

                        break;
                }
            }
        }
        // 进行排序
//        Collections.sort(mMeasureList, c);
//        Collections.sort(mCheckList, c);
        addViewOther(mMeasureList);
        addView(mCheckList);
    }

    public void setButtonSelect(String state) {
        if (state.equals("合格")) {
            binding.brTrue.setSelected(true);
            binding.brFalse.setSelected(false);
            binding.rlReason.setVisibility(View.GONE);
        } else if (state.equals("不合格")) {
            binding.brTrue.setSelected(false);
            binding.brFalse.setSelected(true);
            binding.rlReason.setVisibility(View.VISIBLE);
        }
    }

    private void addViewOther(List<SelectBean> mList) {
        if(state != 1){
            switch (notApply(mList, TYPE_MEASURE)){
                case RESULT_OK:
                    binding.include6.tvState.setText("合格");
                    binding.resultSelect6.setText("合格");
                    break;
                case RESULT_FAIL:
                    binding.include6.tvState.setText("不合格");
                    binding.resultSelect6.setText("不合格");
                    break;
                case RESULT_NOTSUIT:
                    binding.include6.tvState.setText("不适用");
                    binding.resultSelect6.setText("不适用");
                    break;
            }
        }
        for (SelectBean bean : mList) {
            if (state == 3) {
                RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(FunctionCheckActivity.this).inflate(R.layout.iten_look_see, null);
                TextView mTextView = mRl.findViewById(R.id.tv_see);
                    if (!TextUtils.isEmpty(bean.getFunOtherPrj()) && !TextUtils.isEmpty(bean.getFunOtherPrjVal())) {
                        mTextView.setText(bean.getFunOtherPrj() + ":  " + bean.getFunOtherPrjVal());
                    }
                binding.include6.llTotal.addView(mRl);
            }else {
                LinearLayout mRl = (LinearLayout) LayoutInflater.from(FunctionCheckActivity.this).inflate(R.layout.item_lookcheck, null);
                TextView mTextView = mRl.findViewById(R.id.title);
                if (!TextUtils.isEmpty(bean.getFunOtherPrj())) {
                    mTextView.setText(bean.getFunOtherPrj());
                }

                TagFlowLayout tagFlowLayout = (mRl.findViewById(R.id.psResTF));
                TagAdapter<String> tagAdapter = new TagAdapter<String>(Arrays.asList("合格", "不合格", "不适用")) {
                    @Override
                    public View getView(FlowLayout parent, int position, String s) {
                        TextView tv = (TextView) LayoutInflater.from(FunctionCheckActivity.this).inflate(R.layout.item_tag_full, tagFlowLayout, false);
                        tv.setText(s);
                        return tv;
                    }
                };
                tagFlowLayout.setAdapter(tagAdapter);
                if (!TextUtils.isEmpty(bean.getFunOtherPrjVal())) {
                    tagAdapter.setSelectedList(bean.getSelectPosition(bean.getFunOtherPrjVal()));
                }
                tagFlowLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                    @Override
                    public boolean onTagClick(View view, int position, FlowLayout parent) {
                        bean.setFunOtherPrjVal(strs[position]);
//                    bean.setFunOtherPrjValByPosition(position);
                        if (((TagFlowLayout) parent).getSelectedList().size() == 0) {
                            mMapMeasure.remove(bean.getFunOtherPrj());
                        } else {
                            mMapMeasure.put(bean.getFunOtherPrj(), position);
                        }
                        String result = mMapMeasure.size() != mTitles.length ? "待选择" : notApply(mList, TYPE_MEASURE) == 0 ? "合格" :notApply(mList, TYPE_MEASURE) == 1? "不合格":"不适用";
                        binding.include6.tvState.setText(result);
                        binding.resultSelect6.setText(result);
                        checkResult();
                        return true;
                    }
                });
                binding.include6.llTotal.addView(mRl);
            }
        }
    }
    private void addView(List<SelectBean> mList) {
        if(state != 1){
            switch (notApply(mList, TYPE_CHECK)){
                case RESULT_OK:
                    binding.include5.tvState.setText("合格");
                    binding.resultSelect5.setText("合格");
                    break;
                case RESULT_FAIL:
                    binding.include5.tvState.setText("不合格");
                    binding.resultSelect5.setText("不合格");
                    break;
                case RESULT_NOTSUIT:
                    binding.include5.tvState.setText("不适用");
                    binding.resultSelect5.setText("不适用");
                    break;
            }
        }
        for (SelectBean bean : mList) {
            if(state == 3){
                RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(FunctionCheckActivity.this).inflate(R.layout.iten_look_see, null);
                TextView mTextView = mRl.findViewById(R.id.tv_see);
                if (!TextUtils.isEmpty(bean.getFunCheckPrj()) && !TextUtils.isEmpty(bean.getFunCheckPrjVal())) {
                    String value ="";  //   0./90.(90)
                    if(bean.getFunCheckPrjVal().contains("/")){
                        value = bean.getFunCheckPrjVal();
                        String r1 = value.substring(0,bean.getFunCheckPrjVal().indexOf("/"));
                        String r2 = value.substring(bean.getFunCheckPrjVal().indexOf("/")+1,bean.getFunCheckPrjVal().indexOf("("));
                        String r3 = value.substring(bean.getFunCheckPrjVal().indexOf("(")+1,bean.getFunCheckPrjVal().indexOf(")"));
                        //多个编辑框
                        if (r1.equals(".")){
                            r1 = "0";
                        }
                        if (r2.equals(".")){
                            r2 = "0";
                        }
                        if (r3.equals(".")){
                            r3 = "0";
                        }
                         if(r1.endsWith(".")){
                             r1 = r1.substring(r1.length()-1);
                         }
                        if(r2.endsWith(".")){
                            r2 = r2.substring(r2.length()-1);
                        }
                        if(r3.endsWith(".")){
                            r3 = r3.substring(r3.length()-1);
                        }
                        value = r1+"/"+r2+"("+r3+")";
                    }else{
                        if(bean.getFunCheckPrjVal().endsWith(".")){
                            value = bean.getFunCheckPrjVal().substring(0,bean.getFunCheckPrjVal().length()-1);
                        }else{
                            value =bean.getFunCheckPrjVal();
                        }
                    }
                    mTextView.setText(bean.getFunCheckPrj()+":  "+bean.getFunCheckPrjResult()+"— "+value);
                }
                binding.include5.llTotal.addView(mRl);

            }else{
                if (bean.isMoreCompare()) {
                    RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(FunctionCheckActivity.this).inflate(R.layout.item_include_function, null);
                    TextView title = mRl.findViewById(R.id.title);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrj())) {
                        title.setText(bean.getFunCheckPrj());
                    }
                    TextView refValue = mRl.findViewById(R.id.ref_value);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrjPerval())) {
                        refValue.setText("设定值: " + bean.getFunCheckPrjPerval());
                    }
                    TextView state = mRl.findViewById(R.id.tv_state);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrjResult())) {
                        state.setText(bean.getFunCheckPrjResult());
                        if (bean.getFunCheckPrjResult().equals("合格")) {
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_75ca5c));
                        } else if (bean.getFunCheckPrjResult().equals("不合格")) {
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_fb6769));
                        } else if (bean.getFunCheckPrjResult().equals("不适用")) {
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_ccccc));
                        }
                        state.setVisibility(View.VISIBLE);
                    }
                    //123/90(60)
                    EditText editHigh = mRl.findViewById(R.id.edit_high);
                    EditText editLow = mRl.findViewById(R.id.edit_low);
                    EditText editNormal = mRl.findViewById(R.id.edit_normal);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrjVal()) && !TextUtils.isEmpty(bean.getFunCheckPrjPerval())) {
                        String highV = bean.getFunCheckPrjPerval().substring(0, bean.getFunCheckPrjPerval().indexOf("/"));

                        String lowV = bean.getFunCheckPrjPerval().substring(bean.getFunCheckPrjPerval().indexOf("/") + 1, bean.getFunCheckPrjPerval().indexOf("("));

                        String normalV = bean.getFunCheckPrjPerval().substring(bean.getFunCheckPrjPerval().indexOf("(") + 1, bean.getFunCheckPrjPerval().indexOf(")"));
                        if(bean.getFunCheckPrjVal().contains("-")){
                            editHigh.setText("-");
                            editHigh.setSelection(editHigh.getText().length());
                            editLow.setText("-");
                            editLow.setSelection(editHigh.getText().length());
                            editNormal.setText("-");
                            editNormal.setSelection(editHigh.getText().length());
                        }else{
                            String highR = bean.getFunCheckPrjVal().substring(0, bean.getFunCheckPrjVal().indexOf("/"));
                            String lowR = bean.getFunCheckPrjVal().substring(bean.getFunCheckPrjVal().indexOf("/") + 1, bean.getFunCheckPrjVal().indexOf("("));
                            String normalR = bean.getFunCheckPrjVal().substring(bean.getFunCheckPrjVal().indexOf("(") + 1, bean.getFunCheckPrjVal().indexOf(")"));
                            if(highR.endsWith(".")){
                                highR = highR.substring(0,highR.indexOf("."));
                            }
                            if(lowR.endsWith(".")){
                                lowR = lowR.substring(0,lowR.indexOf("."));
                            }
                            if(normalR.endsWith(".")){
                                normalR = normalR.substring(0,normalR.indexOf("."));
                            }
                            editHigh.setText(highR);
                            editHigh.setSelection(highR.length());

                            editLow.setText(lowR);
                            editLow.setSelection(lowR.length());

                            editNormal.setText(normalR);
                            editNormal.setSelection(normalR.length());
                            bean.setHigh(setState(highV, highR, bean.getFunCheckPrj()));
                            bean.setHighValue(highR);

                            bean.setLow(setState(lowV, lowR, bean.getFunCheckPrj()));
                            bean.setLowValue(lowR);

                            bean.setNormal(setState(normalV, normalR, bean.getFunCheckPrj()));
                            bean.setNormalValue(normalR);
                        }

                    }
                    editHigh.addTextChangedListener(editListenerMore(1, bean, state, mList,editHigh));
                    editLow.addTextChangedListener(editListenerMore(2, bean, state, mList,editLow));
                    editNormal.addTextChangedListener(editListenerMore(3, bean, state, mList,editNormal));
                    binding.include5.llTotal.addView(mRl);
                } else {
                    RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(FunctionCheckActivity.this).inflate(R.layout.item_include_elect_safe, null);
                    TextView title = mRl.findViewById(R.id.title);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrj())) {
                        title.setText(bean.getFunCheckPrj());
                    }
                    TextView refValue = mRl.findViewById(R.id.ref_value);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrjPerval())) {
                        // 如果小数点结尾就去掉小数点
                        if (bean.getFunCheckPrj().contains("心电脉搏")) {
                            String value = bean.getFunCheckPrjPerval().substring(0, bean.getFunCheckPrjPerval().indexOf("("));
                            if(value.endsWith(".")){
                                refValue.setText("设定值: " + value.substring(0,value.length()-1) );
                            }else{
                                refValue.setText("设定值: " + value);
                            }
                        } else {
                            if(bean.getFunCheckPrjPerval().endsWith(".")){
                                refValue.setText("设定值: " + bean.getFunCheckPrjPerval().substring(0,bean.getFunCheckPrjPerval().length()-1));
                            }else{
                                refValue.setText("设定值: " + bean.getFunCheckPrjPerval());
                            }
                        }
                    }
                    TextView state = mRl.findViewById(R.id.tv_state);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrjResult())) {
                        state.setText(bean.getFunCheckPrjResult());
                        if (bean.getFunCheckPrjResult().equals("合格")) {
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_75ca5c));
                        } else if (bean.getFunCheckPrjResult().equals("不合格")) {
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_fb6769));
                        } else if (bean.getFunCheckPrjResult().equals("不适用")) {
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_ccccc));
                        }
                        state.setVisibility(View.VISIBLE);
                    }
                    EditText edit = mRl.findViewById(R.id.edit);
                    if (!TextUtils.isEmpty(bean.getFunCheckPrjVal())) {
                        edit.setText(bean.getFunCheckPrjVal().contains("-")?"-":bean.getFunCheckPrjVal().endsWith(".")?bean.getFunCheckPrjVal().substring(0,bean.getFunCheckPrjVal().indexOf(".")) : bean.getFunCheckPrjVal());
                        edit.setSelection(edit.getText().length());
                    }
                    edit.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            String s = editable.toString();
                            if (s.toString().startsWith("0")
                                    && s.toString().trim().length() > 1) {
                                if (!s.toString().substring(1, 2).equals(".")) {
                                    edit.setText(s.subSequence(0, 1));
                                    edit.setSelection(1);
                                }
                            }

                            //判断是否已经输入过.了
                            if (edit.getText().toString().contains(".")) {
                                if (edit.getText().toString().indexOf(".", edit.getText().toString().indexOf(".") + 1) > 0) {
                                    ToastUtil.toastWarning(FunctionCheckActivity.this,"已经输入\".\"不能重复输入",-1);
                                    edit.setText(edit.getText().toString().substring(0, edit.getText().toString().length() - 1));
                                    edit.setSelection(edit.getText().toString().length());
                                }
                            }


                            //删除“.”后面超过2位后的数据
                            if (s.toString().contains(".")) {
                                if (s.length() - 1 - s.toString().indexOf(".") > digits) {
                                    s = s.toString().substring(0,
                                            s.toString().indexOf(".") + digits+1);
                                    edit.setText(s);
                                    edit.setSelection(s.length()); //光标移到最后
                                }
                            }
                            //如果"."在起始位置,则起始位置自动补0
                            if (s.toString().trim().substring(0).equals(".")) {
                                s = "0" + s;
                                edit.setText(s);
                                edit.setSelection(2);
                            }
                            if(s.equals(".")){
                                s = "0";
                            }
                            String real;
                            bean.setFunCheckPrjVal(s);
                            if (TextUtils.isEmpty(s)) {
                                state.setVisibility(View.GONE);
                                if (mMapCheck.containsKey(bean.getFunCheckPrj())) {
                                    mMapCheck.remove(bean.getFunCheckPrj());
                                }
                                checkEditResult(mList);
                                checkResult();
                                return;
                            }
                            if (s.contains("-")) {
                                state.setText("不适用");
//                                edit.setText("-");
                                bean.setFunCheckPrjVal("-");
                                state.setBackground(getResources().getDrawable(R.drawable.corners_bg_ccccc));
                                state.setVisibility(View.VISIBLE);
                                bean.setFunCheckPrjResult("不适用");
                                mMapCheck.put(bean.getFunCheckPrj(), "不适用");
                                checkEditResult(mList);
                                checkResult();
                                return;
                            }
                            mMapCheck.put(bean.getFunCheckPrj(), s);
                            //这里有个小bug  只比较整数，看测试的了
                            String value;
                            if (bean.getFunCheckPrjPerval().contains("(")) {
                                value = bean.getFunCheckPrjPerval().substring(0, bean.getFunCheckPrjPerval().indexOf("("));
                            } else {
                                value = bean.getFunCheckPrjPerval();
                            }
                                if (setState(value, s, bean.getFunCheckPrj())) {
                                    state.setText("合格");
                                    state.setBackground(getResources().getDrawable(R.drawable.corners_bg_75ca5c));
                                    bean.setFunCheckPrjResult("合格");
                                } else {
                                    state.setText("不合格");
                                    state.setBackground(getResources().getDrawable(R.drawable.corners_bg_fb6769));
                                    bean.setFunCheckPrjResult("不合格");
                                }
                                state.setVisibility(View.VISIBLE);
                               checkEditResult(mList);
                               checkResult();
                        }
                    });
                    binding.include5.llTotal.addView(mRl);
                }
            }
        }
    }
   private void checkEditResult(List<SelectBean> mList){
       String result = mMapCheck.size() != checkTitles.length ? "待填写" :notApply(mList, TYPE_CHECK) == 0 ? "合格" :notApply(mList, TYPE_CHECK) == 1? "不合格":"不适用";
//       if(result.equals("不适用")){
//           binding.include5.tvState.setText("不合格");
//           binding.resultSelect5.setText("不合格");
//           return;
//       }
       binding.include5.tvState.setText(result);
       binding.resultSelect5.setText(result);
   }
    private void checkResult() {
        String result = "";
        if (!binding.resultLook.getText().toString().equals("待选择") && !binding.include5.tvState.getText().toString().equals("待填写") &&
                !binding.include6.tvState.getText().toString().equals("待选择")
                ) {
            if(binding.include5.tvState.getText().toString().equals("不适用") && !binding.include6.tvState.getText().toString().equals("不适用")){
                result = binding.include6.tvState.getText().toString().equals("合格") &&
                       binding.resultLook.getText().toString().equals("合格") ? "合格" : "不合格";
            }else if(!binding.include5.tvState.getText().toString().equals("不适用") && binding.include6.tvState.getText().toString().equals("不适用")){
                result = binding.include5.tvState.getText().toString().equals("合格") &&
                        binding.resultLook.getText().toString().equals("合格") ? "合格" : "不合格";
            }else if(binding.include5.tvState.getText().toString().equals("不适用") && binding.include6.tvState.getText().toString().equals("不适用")){
                result = binding.resultLook.getText().toString();
            }else{
                if (!binding.resultLook.getText().toString().equals("合格") || !binding.include5.tvState.getText().toString().equals("合格") ||
                        !binding.include6.tvState.getText().toString().equals("合格")) {
                    result ="不合格";
                }else{
                    result ="合格";
                }
            }
        } else {
            result = "待生成";
        }
        binding.tvResult.setText(result);
        binding.resultSelect7.setText(result);
    }

    public TextWatcher editListenerMore(int type, SelectBean bean, TextView state, List<SelectBean> mList
                                         , EditText edit) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        edit.setText(s.subSequence(0, 1));
                        edit.setSelection(1);
                    }
                }

                //判断是否已经输入过.了
                if (edit.getText().toString().contains(".")) {
                    if (edit.getText().toString().indexOf(".", edit.getText().toString().indexOf(".") + 1) > 0) {
                        ToastUtil.toastWarning(FunctionCheckActivity.this,"已经输入\".\"不能重复输入",-1);
                        edit.setText(edit.getText().toString().substring(0, edit.getText().toString().length() - 1));
                        edit.setSelection(edit.getText().toString().length());
                    }
                }
                //删除“.”后面超过2位后的数据
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > digits) {
                        s = s.toString().substring(0,
                                s.toString().indexOf(".") + digits+1);
                        edit.setText(s);
                        edit.setSelection(s.length()); //光标移到最后
                    }
                }
                //如果"."在起始位置,则起始位置自动补0
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    edit.setText(s);
                    edit.setSelection(2);
                }

                if(s.equals(".")){
                    s ="0";
                }
                String real;
                if (TextUtils.isEmpty(s)) {
                    state.setVisibility(View.GONE);
                    if (mMapCheck.containsKey(bean.getFunCheckPrj())) {
                        mMapCheck.remove(bean.getFunCheckPrj());
                    }
                    switch (type) {
                        case 1:
                            bean.setHigh(false);
                            bean.setHighValue("");
                            break;
                        case 2:
                            bean.setLow(false);
                            bean.setLowValue("");
                            break;
                        case 3:
                            bean.setNormal(false);
                            bean.setNormalValue("");
                            break;
                    }
                    checkEditResult(mList);
                    checkResult();
                    return;
                }
                if (s.contains("-")) {
//                    editText.setText("-");
                    state.setText("不适用");
                    state.setBackground(getResources().getDrawable(R.drawable.corners_bg_ccccc));
                    state.setVisibility(View.VISIBLE);
                    bean.setFunCheckPrjResult("不适用");
                    bean.setFunCheckPrjVal("-");
                    mMapCheck.put(bean.getFunCheckPrj(), "不适用");
                    checkEditResult(mList);
                    checkResult();
                    return;
                }
                //这里有个小bug  只比较整数，看测试的了
                String value = "";  //比较值
                    switch (type) {
                        case 1:
                            value = bean.getFunCheckPrjPerval().substring(0, bean.getFunCheckPrjPerval().indexOf("/"));
                            bean.setHigh(setState(value, s, bean.getFunCheckPrj()));
                            bean.setHighValue(s);
                            break;
                        case 2:
                            value = bean.getFunCheckPrjPerval().substring(bean.getFunCheckPrjPerval().indexOf("/") + 1, bean.getFunCheckPrjPerval().indexOf("("));
                            bean.setLow(setState(value, s, bean.getFunCheckPrj()));
                            bean.setLowValue(s);
                            break;
                        case 3:
                            value = bean.getFunCheckPrjPerval().substring(bean.getFunCheckPrjPerval().indexOf("(") + 1, bean.getFunCheckPrjPerval().indexOf(")"));
                            bean.setNormal(setState(value, s, bean.getFunCheckPrj()));
                            bean.setNormalValue(s);
                            break;
                    }
                    if (bean.isEditOver()) {
                        //是否都填值
                        if (bean.isHigh() && bean.isLow() && bean.isNormal()) {
                            //都填写完成 并且合格
                            bean.setFunCheckPrjResult("合格");
                            state.setText("合格");
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_75ca5c));
                            mMapCheck.put(bean.getFunCheckPrj(),"合格");
                        } else {
                            bean.setFunCheckPrjResult("不合格");
                            state.setText("不合格");
                            state.setBackground(getResources().getDrawable(R.drawable.corners_bg_fb6769));
                            mMapCheck.put(bean.getFunCheckPrj(),"不合格");
                        }
                        bean.setFunCheckPrjVal(bean.getHighValue() + "/" + bean.getLowValue() + "(" + bean.getNormalValue() + ")");
                        state.setVisibility(View.VISIBLE);
                        checkEditResult(mList);
                    }
                checkResult();
            }
        };
    }
    /**
     *   0 合格  1  不合格  2  不适用
     * @param mList
     * @param type
     * @return
     */
    private int notApply(List<SelectBean> mList, int type) {
        int number = 0;
        if (type == TYPE_MEASURE) {
            for (SelectBean selectBean : mList) {
                if (selectBean.getFunOtherPrjVal().equals("不适用")) {
                    number++;
                    continue;
                }
                if (selectBean.getFunOtherPrjVal().equals("不合格")) {
                    return RESULT_FAIL;
                }
            }
        } else {
            for (SelectBean selectBean : mList) {
                if (selectBean.getFunCheckPrjResult().equals("不适用")) {
                    number++;
                    continue;
                }
                if (selectBean.getFunCheckPrjResult().equals("不合格")) {
                    return RESULT_FAIL;
                }
            }
        }
        if(number == mList.size()){
            return RESULT_NOTSUIT;
        }
        return RESULT_OK;
    }
    private boolean isPass(List<SelectBean> mList, int type) {
            for (SelectBean selectBean : mList) {
                if (selectBean.getFunCheckPrjResult().equals("不合格")) {
                    return false;
                }
            }
        return true;
    }

    /**
     *
     * @param value  比较值
     * @param real   实际值
     * @param title  标题
     * @return
     */
    private boolean setState(String value, String real, String title) {
        float cha = 0;
        if(real.endsWith(".")){
            if(real.equals(".")){
                real = "0";
            }else{
                real = real.substring(0,real.length()-1);
            }
        }
        if (title.contains("心电")) {
            //3%   40（38.8~41.2）	60（58.2~61.8）	130（126.1~133.9）
            float v = Float.parseFloat(value);
            float r = Float.parseFloat(real);
            return r>=(float) (v - v* 0.03) && r<=(float) (v + v*0.03);
        } else if (title.contains("血压")) {
            cha = 10;
        } else if(title.contains("血氧")){
            cha = 2;
        }
        if (!TextUtils.isEmpty(value) && !TextUtils.isEmpty(real)) {
            float v = Float.parseFloat(value);
            float r = Float.parseFloat(real);
            return r <= v + cha && r >= v - cha;
        }
        return false;
    }

    LinearLayout linearLayout_include3;
    LinearLayout include1;
    private void showAndHide(int type) {
        if(state == 3){
            linearLayout_include3 = binding.look3.llTotal;
        }else{
            linearLayout_include3 = binding.include3.llTotal;
        }
        if(null == cardDetail){
            include1 = binding.rlOffLine;
        }else{
            include1 = binding.card;
        }
        switch (type) {
            case 1:
                binding.include2.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.setVisibility(View.GONE);
                binding.include5.llTotal.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                binding.include7.setVisibility(View.GONE);

                //自己是否显示，若显示则隐藏
                if (include1.getVisibility() == View.VISIBLE) {
                    include1.setVisibility(View.GONE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                } else {
                    include1.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.GONE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                include1.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.setVisibility(View.GONE);
                binding.include5.llTotal.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                binding.include7.setVisibility(View.GONE);

                //自己是否显示，若显示则隐藏
                if (binding.include2.getVisibility() == View.VISIBLE) {
                    binding.include2.setVisibility(View.GONE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);

                } else {
                    binding.include2.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.GONE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                }
                break;
            case 3:
                include1.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                binding.include4.setVisibility(View.GONE);
                binding.include5.llTotal.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                binding.include7.setVisibility(View.GONE);

                //自己是否显示，若显示则隐藏
                if (linearLayout_include3.getVisibility() == View.VISIBLE) {
                    linearLayout_include3.setVisibility(View.GONE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                } else {
                    linearLayout_include3.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.GONE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                include1.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include5.llTotal.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                binding.include7.setVisibility(View.GONE);

                //自己是否显示，若显示则隐藏
                if (binding.include4.getVisibility() == View.VISIBLE) {
                    binding.include4.setVisibility(View.GONE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                } else {
                    binding.include4.setVisibility(View.VISIBLE);
                    binding.rlSelect4.setVisibility(View.GONE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                }
                break;
            case 5:
                include1.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                binding.include7.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (binding.include5.llTotal.getVisibility() == View.VISIBLE) {
                    binding.include5.llTotal.setVisibility(View.GONE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                } else {
                    binding.include5.llTotal.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.GONE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                }
                break;
            case 6:
                include1.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.setVisibility(View.GONE);
                binding.include5.llTotal.setVisibility(View.GONE);
                binding.include7.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (binding.include6.llTotal.getVisibility() == View.VISIBLE) {
                    binding.include6.llTotal.setVisibility(View.GONE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                } else {
                    binding.include6.llTotal.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.GONE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                }
                break;
            case 7:
                include1.setVisibility(View.GONE);
                binding.include2.setVisibility(View.GONE);
                linearLayout_include3.setVisibility(View.GONE);
                binding.include4.setVisibility(View.GONE);
                binding.include5.llTotal.setVisibility(View.GONE);
                binding.include6.llTotal.setVisibility(View.GONE);
                //自己是否显示，若显示则隐藏
                if (binding.include7.getVisibility() == View.VISIBLE) {
                    binding.include7.setVisibility(View.GONE);
                    binding.rlSelect7.setVisibility(View.VISIBLE);
                } else {
                    binding.include7.setVisibility(View.VISIBLE);
                    binding.rlSelect7.setVisibility(View.GONE);
                    binding.rlSelect4.setVisibility(View.VISIBLE);
                    binding.rlSelect3.setVisibility(View.VISIBLE);
                    binding.rlSelect1.setVisibility(View.VISIBLE);
                    binding.rlSelect2.setVisibility(View.VISIBLE);
                    binding.rlSelect5.setVisibility(View.VISIBLE);
                    binding.rlSelect6.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_back_ll:
                onBackPressed();
                break;
            case R.id.rl_select_1:
                showAndHide(1);
                break;
            case R.id.rl_select_2:
                showAndHide(2);
                break;
            case R.id.rl_select_3:
                showAndHide(3);
                break;
            case R.id.rl_select_4:
                showAndHide(4);
                break;
            case R.id.rl_select_5:
                showAndHide(5);
                break;
            case R.id.rl_select_6:
                showAndHide(6);
                break;
            case R.id.rl_select_7:
                showAndHide(7);
                break;
            case R.id.title_menu_confirm:
                //save
                submitFunction();
                break;
            case R.id.rl_type:
                //save
//                viewModel.getDictData("86");
                typeOptionsPickerView = PickerViewUtils.getOptionPickerView("", typeList, PickerViewUtils.getCheckTypePosition(binding.include3.tvCheckType.getText().toString()), FunctionCheckActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckType, typeList, 1));
                typeOptionsPickerView.show();

                break;
            case R.id.rl_room:
                //save
//                viewModel.getDictData("87");
                roomOptionsPickerView = PickerViewUtils.getOptionPickerView("", roomList, PickerViewUtils.getCheckOrganizationPosition(binding.include3.tvCheckWhere.getText().toString()), FunctionCheckActivity.this, PickerViewUtils.getListener(binding.include3.tvCheckWhere, roomList, 2));
                roomOptionsPickerView.show();
                break;

            case R.id.br_true:
                if (binding.brTrue.isSelected()) {
                    binding.brTrue.setSelected(false);
                    binding.brFalse.setSelected(true);
                    binding.brTrue.setTextColor(getResources().getColor(R.color.C_333333));
                    binding.brFalse.setTextColor(getResources().getColor(R.color.white));
                    binding.rlReason.setVisibility(View.VISIBLE);
                    binding.resultSelect4.setText("不合格");
                    binding.resultLook.setText("不合格");
                    lookBean.setFunAppPjVal("不合格");
                    lookBean.setFunAppPjValCode("2");
                } else {
                    binding.brTrue.setSelected(true);
                    binding.brFalse.setSelected(false);
                    binding.brFalse.setTextColor(getResources().getColor(R.color.C_333333));
                    binding.brTrue.setTextColor(getResources().getColor(R.color.white));
                    binding.rlReason.setVisibility(View.GONE);
                    binding.resultSelect4.setText("合格");
                    binding.resultLook.setText("合格");
                    lookBean.setFunAppPjVal("合格");
                    lookBean.setFunAppPjValCode("1");
                }
                checkResult();
                break;
            case R.id.br_false:
                if (!binding.brFalse.isSelected()) {
                    binding.brTrue.setSelected(false);
                    binding.brFalse.setSelected(true);
                    binding.brTrue.setTextColor(getResources().getColor(R.color.C_333333));
                    binding.brFalse.setTextColor(getResources().getColor(R.color.white));
                    binding.rlReason.setVisibility(View.VISIBLE);
                    binding.resultSelect4.setText("不合格");
                    binding.resultLook.setText("不合格");
                    lookBean.setFunAppPjVal("不合格");
                    lookBean.setFunAppPjValCode("2");
                } else {
                    binding.brTrue.setSelected(true);
                    binding.brFalse.setSelected(false);
                    binding.brFalse.setTextColor(getResources().getColor(R.color.C_333333));
                    binding.brTrue.setTextColor(getResources().getColor(R.color.white));
                    binding.rlReason.setVisibility(View.GONE);
                    binding.resultSelect4.setText("合格");
                    binding.resultLook.setText("合格");
                    lookBean.setFunAppPjVal("合格");
                    lookBean.setFunAppPjValCode("1");
                }
                checkResult();
                break;
        }
    }

    private void submitFunction() {
        if (TextUtils.isEmpty(binding.include3.tvCheckType.getText().toString())
                || TextUtils.isEmpty(binding.include3.tvCheckWhere.getText().toString())
                || TextUtils.isEmpty(binding.include3.tvTemp.getText().toString())
                || TextUtils.isEmpty(binding.include3.tvEmp.getText().toString())
                ) {
            showCheckFailedDialog("基础信息模块有信息尚未填完,请填完保存");
            return;
        }
        if (binding.resultLook.getText().equals("待选择")) {
            showCheckFailedDialog("外观功能模块尚未选择,请选完保存");
            return;
        }
        if (mMapCheck.size() != mCheckList.size() || binding.include5.tvState.getText().equals("待填写")) {
            showCheckFailedDialog("测量检查模块有信息待填写,请填完保存");
            return;
        }
        if (mMapMeasure.size() != mMeasureList.size() || binding.include6.tvState.getText().equals("待填写")) {
            showCheckFailedDialog("其他检测模块有信息待填写,请填完保存");
            return;
        }

        if (null == submit)
            submit = new PmReportDtl();
        submit.setCheckModel(checkModel);
        submit.setCheckModelName("监护仪模板");//暂时写死
        submit.setCheckProject("85-3");
        submit.setCheckProjectName("性能检测");
        submit.setCheckType(PickerViewUtils.checkType);
        submit.setCheckOrganization(PickerViewUtils.checkOrganization);
        submit.setTemperature(binding.include3.tvTemp.getText().toString().trim());
        submit.setHumidity(binding.include3.tvEmp.getText().toString().trim());
        submit.setCheckResult(binding.tvResult.getText().toString());
        if (!TextUtils.isEmpty(binding.remark.getText().toString().trim())) {
            submit.setRemarks(binding.remark.getText().toString().trim());
        }
        if (!TextUtils.isEmpty(binding.remarkReason.getText().toString().trim()) && lookBean.getFunAppPjVal().equals("不合格")) {
            lookBean.setFunAppPjRem(binding.remarkReason.getText().toString().trim());
        }
        List<SelectBean> mAll = new ArrayList<>();
        mAll.addAll(mMeasureList);
        mAll.addAll(mCheckList);
        mAll.addAll(mTypes);
        mAll.add(lookBean);
        submit.setPrjList(mAll);
        Intent intent = new Intent();
        intent.putExtra("FunctionCheckResult", submit);
        setResult(Constants.RESULT_OK, intent);
        ToastUtil.toastImage(getApplication().getApplicationContext(), "保存成功", -1);
        finish();
    }


    @Override
    public void onBackPressed() {
        if(state != 3){
            noticeSave();
        }else {
            super.onBackPressed();
        }
    }
}
