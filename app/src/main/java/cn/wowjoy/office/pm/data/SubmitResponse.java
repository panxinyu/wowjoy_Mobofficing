package cn.wowjoy.office.pm.data;

import java.io.Serializable;

public class SubmitResponse implements Serializable{
    private String errorMsg;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
