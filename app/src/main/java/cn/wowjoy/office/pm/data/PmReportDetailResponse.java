package cn.wowjoy.office.pm.data;

import java.io.Serializable;
import java.util.List;

import cn.wowjoy.office.pm.data.request.PmReportDtl;

public class PmReportDetailResponse implements Serializable {
    private PmReportVO pmReportVO;

    public PmReportVO getPmReportVO() {
        return pmReportVO;
    }

    public void setPmReportVO(PmReportVO pmReportVO) {
        this.pmReportVO = pmReportVO;
    }

    public static class PmReportVO implements Serializable{
        private String id;
        private String cardId;
        private String qrCode;
        private String cardNo;
        private String cardName;
        private String produceNo;
        private String assetsSpec;
        private String startDate;
        private String cardDeptName;
        private String companyName;
        private String cyclePm;
        private String lastPmDate;
        private String lastPmUserName;
        private String pmDate;
        private String pmUserID;
        private String pmUserName;
        private String checkResult;
        private String checkResultName;
        private String remarks;
        private String offLineRemark;
        private String billStatus;
        private String billStatusName;
        private List<PmReportDtl> pmReportDtlList;


        public String getOffLineRemark() {
            return offLineRemark;
        }

        public void setOffLineRemark(String offLineRemark) {
            this.offLineRemark = offLineRemark;
        }

        public String getQrCode() {
            return qrCode;
        }

        public void setQrCode(String qrCode) {
            this.qrCode = qrCode;
        }

        public String getBillStatus() {
            return billStatus;
        }

        public void setBillStatus(String billStatus) {
            this.billStatus = billStatus;
        }

        public String getBillStatusName() {
            return billStatusName;
        }

        public void setBillStatusName(String billStatusName) {
            this.billStatusName = billStatusName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getCardNo() {
            if(null == cardNo){
                return "无";
            }
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public String getCardName() {
            if(null == cardName){
                return "无";
            }
            return cardName;

        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public String getProduceNo() {
            if(null == produceNo){
                return "无";
            }
            return produceNo;
        }

        public void setProduceNo(String produceNo) {
            this.produceNo = produceNo;
        }

        public String getAssetsSpec() {
            return assetsSpec;
        }

        public void setAssetsSpec(String assetsSpec) {
            this.assetsSpec = assetsSpec;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getCardDeptName() {
            if(null == cardDeptName){
                return "无";
            }
            return cardDeptName;
        }

        public void setCardDeptName(String cardDeptName) {
            this.cardDeptName = cardDeptName;
        }

        public String getCompanyName() {
            if(null == companyName){
                return "无";
            }
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCyclePm() {
            return cyclePm;
        }

        public void setCyclePm(String cyclePm) {
            this.cyclePm = cyclePm;
        }

        public String getLastPmDate() {
            return lastPmDate;
        }

        public void setLastPmDate(String lastPmDate) {
            this.lastPmDate = lastPmDate;
        }

        public String getLastPmUserName() {
            return lastPmUserName;
        }

        public void setLastPmUserName(String lastPmUserName) {
            this.lastPmUserName = lastPmUserName;
        }

        public String getPmDate() {
            return pmDate;
        }

        public void setPmDate(String pmDate) {
            this.pmDate = pmDate;
        }

        public String getPmUserID() {
            return pmUserID;
        }

        public void setPmUserID(String pmUserID) {
            this.pmUserID = pmUserID;
        }

        public String getPmUserName() {
            return pmUserName;
        }

        public void setPmUserName(String pmUserName) {
            this.pmUserName = pmUserName;
        }

        public String getCheckResult() {
            return checkResult;
        }

        public void setCheckResult(String checkResult) {
            this.checkResult = checkResult;
        }

        public String getCheckResultName() {
            return checkResultName;
        }

        public void setCheckResultName(String checkResultName) {
            this.checkResultName = checkResultName;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public List<PmReportDtl> getPmReportDtlList() {
            return pmReportDtlList;
        }

        public void setPmReportDtlList(List<PmReportDtl> pmReportDtlList) {
            this.pmReportDtlList = pmReportDtlList;
        }

        public class PmReportDtlVO implements Serializable{
            private String id;
            private String billId;
            private String checkProject;
            private String checkProjectName;
            private String checkModel;
            private String checkModelName;
            private String checkResult;
            private String remarks;
            private  List<SelectBean> mSelectBeans;

            public List<SelectBean> getSelectBeans() {
                return mSelectBeans;
            }

            public void setSelectBeans(List<SelectBean> selectBeans) {
                mSelectBeans = selectBeans;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }


            public String getCheckProject() {
                return checkProject;
            }

            public void setCheckProject(String checkProject) {
                this.checkProject = checkProject;
            }

            public String getBillId() {
                return billId;
            }

            public void setBillId(String billId) {
                this.billId = billId;
            }

            public String getCheckProjectName() {
                return checkProjectName;
            }

            public void setCheckProjectName(String checkProjectName) {
                this.checkProjectName = checkProjectName;
            }

            public String getCheckModel() {
                return checkModel;
            }

            public void setCheckModel(String checkModel) {
                this.checkModel = checkModel;
            }

            public String getCheckModelName() {
                return checkModelName;
            }

            public void setCheckModelName(String checkModelName) {
                this.checkModelName = checkModelName;
            }

            public String getCheckResult() {
                return checkResult;
            }

            public void setCheckResult(String checkResult) {
                this.checkResult = checkResult;
            }

            public String getRemarks() {
                return remarks;
            }

            public void setRemarks(String remarks) {
                this.remarks = remarks;
            }
        }
    }

}
