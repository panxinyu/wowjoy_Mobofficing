package cn.wowjoy.office.pm.view.viewodel;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.bingoogolapple.androidcommon.adapter.BGABindingViewHolder;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.databinding.ItemPmManageCheckRvBinding;
import cn.wowjoy.office.pm.data.PmReportDetailResponse;
import cn.wowjoy.office.pm.db.PmReportListEntity;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class CheckPmReportViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;
    @Inject
    public CheckPmReportViewModel(@NonNull NewMainApplication application) {
        super(application);
        linnerAdapter.setItemEventHandler(this);
    }

    @Override
    public void onCreateViewModel() {

    }
    //ALL的Task
    public ArrayList<PmReportListEntity> date;
    public BGABindingRecyclerViewAdapter<PmReportListEntity, ItemPmManageCheckRvBinding> linnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_pm_manage_check_rv);
    public LRecyclerViewAdapter mAdapter = new LRecyclerViewAdapter(linnerAdapter);

    public void setDate(List<PmReportListEntity> infos){
        if (null == date)
            date = new ArrayList<>();
        date.clear();
        date.addAll(infos);
        linnerAdapter.setData(date);
        mAdapter.removeFooterView();
        mAdapter.removeHeaderView();
        mAdapter.notifyDataSetChanged();
    }
    //点击查看详情
    public void onItemClick(BGABindingViewHolder holder, PmReportListEntity model) {
            getPmReport(model.getId());
    }
    public MediatorLiveData<LiveDataWrapper<PmReportDetailResponse>> detail = new MediatorLiveData<>();
    //点击查看详情
    public void getPmReport(String id) {
        detail.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getPmReport(id)
                .flatMap(new ResultDataParse<PmReportDetailResponse>())
                .compose(new RxSchedulerTransformer<PmReportDetailResponse>())
                .subscribe(new Consumer<PmReportDetailResponse>() {
                    @Override
                    public void accept(PmReportDetailResponse taskListResponse) throws Exception {
                        detail.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        detail.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }
}
