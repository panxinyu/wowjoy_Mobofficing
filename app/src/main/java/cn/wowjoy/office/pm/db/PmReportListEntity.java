package cn.wowjoy.office.pm.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "report_list")
public class PmReportListEntity implements Serializable{
    @PrimaryKey(autoGenerate = true)
    @SerializedName("_id")
    private long _id;
    private String id;
    private String qrCode;
    private String remarks;
    private String userName;
    private String pmDate;          //时间
    private String billStatusName;  //单据状态
    private String checkResultName; //检测结果
    private String cardName;
    private String cardDeptName;
//    private String test;
    private boolean isLocal = false; //是否是本地数据
    private long updateTime;  //根据时间进行排序  desc
    @Ignore
    private String pmUserName;
    @Ignore
    private String billNo;
    @Ignore
    private String checkResult;
    @Ignore
    private String keyWord;  //搜索关键字
    @Ignore
    private boolean isShow = false;  //展示 勾选框  长按一个条目就要显示所有
    @Ignore
    private boolean isSelect = false; //是否被勾选

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getPmUserName() {
        return pmUserName;
    }

    public void setPmUserName(String pmUserName) {
        this.pmUserName = pmUserName;
    }

    public void setLocal(boolean local) {
        isLocal = local;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public String getBillNoC(){
        return !TextUtils.isEmpty(billNo)?billNo : !TextUtils.isEmpty(remarks) ? remarks : "";
    }
    public boolean getStatus() {
        return !TextUtils.isEmpty(checkResultName) && checkResultName.equals("正常");
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPmDate() {
        return pmDate;
    }

    public void setPmDate(String pmDate) {
        this.pmDate = pmDate;
    }

    public String getBillStatusName() {
        return billStatusName;
    }

    public void setBillStatusName(String billStatusName) {
        this.billStatusName = billStatusName;
    }

    public String getCheckResultName() {
        return checkResultName;
    }

    public void setCheckResultName(String checkResultName) {
        this.checkResultName = checkResultName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCardName() {
        return cardName;
    }
    public String getCardNameC() {
        if(TextUtils.isEmpty(cardName)){
            return "待完善";
        }
        return cardName;
    }
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardDeptName() {
        return cardDeptName;
    }
    public String getCardDeptNameC() {
        if(TextUtils.isEmpty(cardDeptName)){
            return "待完善";
        }
        return cardDeptName;
    }
    public void setCardDeptName(String cardDeptName) {
        this.cardDeptName = cardDeptName;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
