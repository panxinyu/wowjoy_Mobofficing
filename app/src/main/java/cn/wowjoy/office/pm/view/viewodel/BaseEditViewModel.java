package cn.wowjoy.office.pm.view.viewodel;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.pm.data.DictDataResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class BaseEditViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;


    @Inject
    public BaseEditViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
    public MediatorLiveData<LiveDataWrapper<DictDataResponse>> typeData = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<DictDataResponse>> roomData = new MediatorLiveData<>();
    //获取全部任务的List
    public void getDictData(String dictTypeValue){
        typeData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getDictData(dictTypeValue)
                .flatMap(new ResultDataParse<DictDataResponse>())
                .compose(new RxSchedulerTransformer<DictDataResponse>())
                .subscribe(new Consumer<DictDataResponse>() {
                    @Override
                    public void accept(DictDataResponse taskListResponse) throws Exception {
                        if(dictTypeValue.equals("86")){
                            typeData.setValue(LiveDataWrapper.success(taskListResponse));
                        }else{
                            roomData.setValue(LiveDataWrapper.success(taskListResponse));
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if(dictTypeValue.equals("86")) {
                            typeData.setValue(LiveDataWrapper.error(throwable, null));
                        }else{
                            roomData.setValue(LiveDataWrapper.error(throwable, null));
                        }
                    }
                });
        addDisposable(disposable);
    }

}
