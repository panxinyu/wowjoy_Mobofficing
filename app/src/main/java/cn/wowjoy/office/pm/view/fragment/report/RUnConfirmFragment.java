//package cn.wowjoy.office.pm.view.fragment.report;
//
//
//import android.app.Fragment;
//import android.arch.lifecycle.Observer;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.LinearLayoutManager;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.github.jdsjlzx.interfaces.OnItemClickListener;
//import com.github.jdsjlzx.interfaces.OnRefreshListener;
//
//import cn.wowjoy.office.R;
//import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
//import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
//import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
//import cn.wowjoy.office.common.widget.CreateDialog;
//import cn.wowjoy.office.common.widget.MDialog;
//import cn.wowjoy.office.databinding.FragmentRallBinding;
//import cn.wowjoy.office.pm.data.PmReportIndexResponse;
//import cn.wowjoy.office.pm.view.viewodel.PmReportViewModel;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class RUnConfirmFragment extends NewBaseFragment<FragmentRallBinding,PmReportViewModel>{
//
//    private MDialog waitDialog;
//    private String status;
//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @return A new instance of fragment StayOutStockFragment.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static RUnConfirmFragment newInstance(String status) {
//        RUnConfirmFragment fragment = new RUnConfirmFragment();
//        Bundle args = new Bundle();
//        args.putString("status", status);
//        fragment.setArguments(args);
//        return fragment;
//    }
//    public RUnConfirmFragment() {
//        // Required empty public constructor
//    }
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            status = getArguments().getString("status");
//        }
//        viewModel.setStatus(status);
//    }
//
//    @Override
//    protected Class<PmReportViewModel> getViewModel() {
//        return PmReportViewModel.class;
//    }
//
//    @Override
//    protected int getLayoutId() {
//        return R.layout.fragment_un_confirm;
//    }
//
//    @Override
//    protected void onCreateViewLazy(Bundle savedInstanceState) {
//        binding.setViewModel(viewModel);
//
//        binding.rvFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
//        binding.rvFragment.setAdapter(viewModel.nolinnerAdapter2);
//        binding.rvFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));
//        binding.emptyView.emptyContent.setText("暂无数据");
//        binding.rvFragment.setEmptyView(binding.emptyView.getRoot());
//        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewModel.getAllListInfo(true,"",status);
//            }
//        });
//        binding.rvFragment.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                //下拉刷新 请求服务器新数据
//                viewModel.getAllListInfo(true,"",status);
//            }
//        });
//        initObserve();
//    }
//    private void initObserve(){
//        viewModel.allData.observe(this, new Observer<LiveDataWrapper<PmReportIndexResponse>>() {
//            @Override
//            public void onChanged(@Nullable LiveDataWrapper<PmReportIndexResponse> inspectionTotalResponseLiveDataWrapper) {
//                switch (inspectionTotalResponseLiveDataWrapper.status){
//                    case LOADING:
//                        waitDialog = CreateDialog.waitingDialog(getActivity());
//                        break;
//                    case SUCCESS:
//                        if (null != waitDialog) {
//                            CreateDialog.dismiss(getActivity(), waitDialog);
//                        }
//                        if(null != inspectionTotalResponseLiveDataWrapper.data){
//                        }
//                        break;
//                    case ERROR:
//                        if (null != waitDialog) {
//                            CreateDialog.dismiss(getActivity(), waitDialog);
//                        }
//                        binding.rvFragment.refreshComplete(1);
//                        handleException(inspectionTotalResponseLiveDataWrapper.error,true);
//                        break;
//                }
//            }
//        });
//    }
//    @Override
//    protected void onResumeLazy() {
//        super.onResumeLazy();
////        mDonePresenter.myAppinfos();
//        binding.rvFragment.refresh();
//        viewModel.allAdapter.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
//            }
//        });
//    }
//
//    @Override
//    protected ViewGroup getErrorViewRoot() {
//        return binding.flError;
//    }
//
//    @Override
//    protected void refreshError() {
//        super.refreshError();
//        viewModel.getAllListInfo(true,"",status);
//    }
//
//}
