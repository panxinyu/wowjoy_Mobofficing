package cn.wowjoy.office.pm.view.viewodel;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.bingoogolapple.androidcommon.adapter.BGABindingViewHolder;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.databinding.ItemPmManagerRvKeyBinding;
import cn.wowjoy.office.databinding.ItemPmReportAllRvBinding;
import cn.wowjoy.office.homepage.MenuHelper;
import cn.wowjoy.office.pm.data.PmManageCardInfo;
import cn.wowjoy.office.pm.data.PmManageResponse;
import cn.wowjoy.office.pm.data.PmReportDetailResponse;
import cn.wowjoy.office.pm.data.PmReportIndexResponse;
import cn.wowjoy.office.pm.db.PmReportListEntity;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class PmSearchViewModel extends NewBaseViewModel {
    @Inject
    public PmSearchViewModel(@NonNull NewMainApplication application) {
        super(application);
        searchlinnerAdapter.setItemEventHandler(this);
        reportlinnerAdapter.setItemEventHandler(this);
    }

    @Override
    public void onCreateViewModel() {

    }

    @Inject
    ApiService apiService;
    //搜索的数据
    public MediatorLiveData<LiveDataWrapper<PmManageResponse>> searchData = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<PmReportIndexResponse>> reportData = new MediatorLiveData<>();
    //管理Task
    public ArrayList<PmManageResponse.Detail> search_list;
    public BGABindingRecyclerViewAdapter<PmManageResponse.Detail, ItemPmManagerRvKeyBinding> searchlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_pm_manager_rv_key);
    public LRecyclerViewAdapter searchAdapter = new LRecyclerViewAdapter(searchlinnerAdapter);

    //报告Task
    public ArrayList<PmReportListEntity> report_list;
    public BGABindingRecyclerViewAdapter<PmReportListEntity, ItemPmReportAllRvBinding> reportlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_pm_report_search_rv);
    public LRecyclerViewAdapter reportAdapter = new LRecyclerViewAdapter(reportlinnerAdapter);
    //获取全部任务的List
    public void getSearchListInfo(String fuzzySearchForPda) {
        searchData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getListPmCardData(fuzzySearchForPda, MenuHelper.getAuidS().get("zc_pm"))
                .flatMap(new ResultDataParse<PmManageResponse>())
                .compose(new RxSchedulerTransformer<PmManageResponse>())
                .subscribe(new Consumer<PmManageResponse>() {
                    @Override
                    public void accept(PmManageResponse taskListResponse) throws Exception {
                        searchData.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        searchData.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }
    public void setWData(List<PmManageResponse.Detail> data) {
        if (null == search_list)
            search_list = new ArrayList<>();
        search_list.clear();
        search_list.addAll(data);
        searchlinnerAdapter.setData(search_list);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }
    public void clearData(){
        if (null == search_list)
            search_list = new ArrayList<>();
        search_list.clear();
        searchlinnerAdapter.setData(search_list);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }

    //获取所有任务的List
    public void getReportListInfo(String fuzzySearchForPda){
        reportData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getlistPmReportData(fuzzySearchForPda, MenuHelper.getAuidS().get("zc_pm"))
                .flatMap(new ResultDataParse<PmReportIndexResponse>())
                .compose(new RxSchedulerTransformer<PmReportIndexResponse>())
                .subscribe(new Consumer<PmReportIndexResponse>() {
                    @Override
                    public void accept(PmReportIndexResponse taskListResponse) throws Exception {
                        reportData.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        reportData.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
    public void setReportData(List<PmReportListEntity> data) {
        if (null == report_list)
            report_list = new ArrayList<>();
        report_list.clear();
        report_list.addAll(data);
         reportlinnerAdapter.setData(report_list);
         reportAdapter.removeFooterView();
         reportAdapter.removeHeaderView();
         reportAdapter.notifyDataSetChanged();
    }
    public void clearReportData(){
        if (null == report_list)
            report_list = new ArrayList<>();
        report_list.clear();
        reportlinnerAdapter.setData(report_list);
        reportAdapter.removeFooterView();
        reportAdapter.removeHeaderView();
        reportAdapter.notifyDataSetChanged();
    }



    //点击查看详情          管理
    public void onManageItemClick(BGABindingViewHolder holder, PmManageResponse.Detail model) {
        getManagerCardInfo(model.getCardId());
    }
    public  MediatorLiveData<LiveDataWrapper<PmManageCardInfo>> detail = new MediatorLiveData<>();
    //点击查看详情         管理
    public void getManagerCardInfo(String id){
        detail.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getManagerCardInfo(id)
                .flatMap(new ResultDataParse<PmManageCardInfo>())
                .compose(new RxSchedulerTransformer<PmManageCardInfo>())
                .subscribe(new Consumer<PmManageCardInfo>() {
                    @Override
                    public void accept(PmManageCardInfo taskListResponse) throws Exception {
                        detail.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        detail.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }



    //点击查看详情
    public void onReportItemClick(BGABindingViewHolder holder, PmReportListEntity model) {
        if(!model.isLocal()){
            getPmReport(model.getId()+"");
        }else{
//            onLocalItemClick(holder,model);
        }
    }
    //报告点击
    public MediatorLiveData<LiveDataWrapper<PmReportDetailResponse>> detailReport = new MediatorLiveData<>();
    //点击查看详情
    public void getPmReport(String id) {
        detailReport.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getPmReport(id)
                .flatMap(new ResultDataParse<PmReportDetailResponse>())
                .compose(new RxSchedulerTransformer<PmReportDetailResponse>())
                .subscribe(new Consumer<PmReportDetailResponse>() {
                    @Override
                    public void accept(PmReportDetailResponse taskListResponse) throws Exception {
                        detailReport.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        detailReport.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }

}