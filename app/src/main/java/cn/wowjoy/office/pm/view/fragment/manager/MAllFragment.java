package cn.wowjoy.office.pm.view.fragment.manager;


import android.app.Fragment;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.interfaces.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.event.RedPointEvent;
import cn.wowjoy.office.databinding.FragmentMallBinding;
import cn.wowjoy.office.pm.data.PmManageCardInfo;
import cn.wowjoy.office.pm.data.PmManageResponse;
import cn.wowjoy.office.pm.view.detail.CheckPmReportActivity;
import cn.wowjoy.office.pm.view.viewodel.PmManagerViewModel;
import cn.wowjoy.office.utils.dialog.DialogUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class MAllFragment extends NewBaseFragment<FragmentMallBinding,PmManagerViewModel> {


    private MDialog waitDialog;
    private String status;
    public MAllFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StayOutStockFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MAllFragment newInstance(String status) {
        MAllFragment fragment = new MAllFragment();
        Bundle args = new Bundle();
        args.putString("status", status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            status = getArguments().getString("status");
        }
        viewModel.setStatus(status);
    }

    @Override
    protected Class<PmManagerViewModel> getViewModel() {
        return PmManagerViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mall;
    }

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        binding.rvFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        switch (status) {
            case "": //全部   待完善+未确认
                binding.rvFragment.setAdapter(viewModel.allAdapter);
                status ="";
                break;
            case "82-1":
                binding.rvFragment.setAdapter(viewModel.soonAdapter);
                break;
            case "82-2":
                binding.rvFragment.setAdapter(viewModel.outAdapter);
                break;
            case "82-4":
                binding.rvFragment.setAdapter(viewModel.wrongAdapter);
                break;
        }
        binding.rvFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("暂无数据");
        binding.rvFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getAllListInfo(true,"",status);
            }
        });
        binding.rvFragment.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                //下拉刷新 请求服务器新数据
                viewModel.getAllListInfo(true,"",status);
            }
        });
        initObserve();
    }
    private void initObserve(){
        viewModel.detail.observe(this, new Observer<LiveDataWrapper<PmManageCardInfo>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<PmManageCardInfo> pmManageCardInfoLiveDataWrapper) {
                switch (pmManageCardInfoLiveDataWrapper.status){
                    case LOADING:
                        DialogUtils.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(getActivity());
                        CheckPmReportActivity.launch(getActivity(),pmManageCardInfoLiveDataWrapper.data);
                        break;
                    case ERROR:
                        DialogUtils.dismiss(getActivity());
                        handleException(pmManageCardInfoLiveDataWrapper.error,false);
                        break;
                }
            }
        });
        viewModel.allData.observe(this, new Observer<LiveDataWrapper<PmManageResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<PmManageResponse> inspectionTotalResponseLiveDataWrapper) {
                switch (inspectionTotalResponseLiveDataWrapper.status){
                    case LOADING:
                        DialogUtils.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(getActivity());
                        if(null != inspectionTotalResponseLiveDataWrapper.data){
                            EventBus.getDefault().post(new RedPointEvent(inspectionTotalResponseLiveDataWrapper.data.getCount(),status));
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(getActivity());
                        binding.rvFragment.refreshComplete(1);
                        handleExceptionPM(inspectionTotalResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RedPointEvent event) {

    }
    @Override
    protected void onStartLazy() {
        super.onStartLazy();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroyViewLazy() {
        super.onDestroyViewLazy();
        EventBus.getDefault().unregister(this);
    }
    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        binding.rvFragment.refresh();
    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getAllListInfo(true,"",status);
    }
}
