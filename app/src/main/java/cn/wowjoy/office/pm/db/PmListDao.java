package cn.wowjoy.office.pm.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by pxy on 2017/12/20.
 * Description:
 */
@Dao
public interface PmListDao {
    @Query("SELECT * FROM report_list")
    LiveData<List<PmReportListEntity>> loadList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveEntity(PmReportListEntity userInfo);

    @Query("SELECT * FROM report_list where userName=:userName order by updateTime desc limit (:start-1)*:pageSize,:pageSize")
    List<PmReportListEntity> getDataByUser(String userName,int start,int pageSize);
    @Query("SELECT * FROM report_list limit (:start-1)*:pageSize,:pageSize")
    List<PmReportListEntity> getDataAll(int start,int pageSize);


    @Query("SELECT * FROM report_list where qrCode=:qrCode and userName=:userName")
    List<PmReportListEntity> searchData(String qrCode,String userName);
    /**
     *  更新数据
     * @param qrCode
     * @param userName
     * @return
     */
    @Query("UPDATE report_list SET remarks = :remarks , pmDate=:time , updateTime =:updateTime ,cardName =:cardName , cardDeptName = :cardDeptName WHERE qrCode=:qrCode and userName=:userName")
    void updateExist(String remarks ,String time,long updateTime,String cardName,String cardDeptName,String qrCode,String userName);
    @Query("UPDATE report_list SET remarks = :remarks WHERE qrCode=:qrCode and userName=:userName")
    void updateExist(String remarks ,String qrCode,String userName);

    @Query("DELETE FROM report_list where qrCode=:qrCode and userName=:userName")
    void deleteByQRCode(String qrCode,String userName);
    @Query("DELETE FROM report_list")
    void deleteAll();
}
