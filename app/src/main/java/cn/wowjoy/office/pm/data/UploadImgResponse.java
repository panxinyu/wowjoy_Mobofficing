package cn.wowjoy.office.pm.data;

import java.io.Serializable;

public class UploadImgResponse implements Serializable {
    private String fileId;
    private String thumbnailId;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(String thumbnailId) {
        this.thumbnailId = thumbnailId;
    }
}
