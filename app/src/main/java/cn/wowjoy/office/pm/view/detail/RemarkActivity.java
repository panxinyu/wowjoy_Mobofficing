package cn.wowjoy.office.pm.view.detail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityRemarkBinding;
import cn.wowjoy.office.pm.view.PMBaseActivity;
import cn.wowjoy.office.pm.view.viewodel.RemarkViewModel;
import cn.wowjoy.office.utils.MyTextWatcher;
import cn.wowjoy.office.utils.ToastUtil;

public class RemarkActivity extends PMBaseActivity<ActivityRemarkBinding,RemarkViewModel> implements View.OnClickListener {
    private static int REMARK = 101;

    private String remark;
    @Override
    protected Class<RemarkViewModel> getViewModel() {
        return RemarkViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_remark;
    }
    public static void launch(Activity context, String remark) {
        Intent i = new Intent(context, RemarkActivity.class);
        i.putExtra("remark", remark);
        context.startActivityForResult(i, REMARK);
    }
    @Override
    protected void init(Bundle savedInstanceState) {
         remark = getIntent().getStringExtra("remark");
        initView();
        initData();
    }

    private void initData() {
        if(!TextUtils.isEmpty(remark)){
            binding.remark.setText(remark);
            binding.remark.setSelection(binding.remark.getText().length());
        }
    }

    private void initView() {
        binding.remarkTitle.titleTextTv.setText("备注");
        binding.remarkTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.remarkTitle.titleBackTv.setText("取消");
        binding.remarkTitle.titleBackIv.setVisibility(View.GONE);
        binding.remarkTitle.titleBackLl.setOnClickListener(this);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) binding.remarkTitle.titleBackLl.getLayoutParams();
        layoutParams.leftMargin = 24;
        binding.remarkTitle.titleBackLl.setLayoutParams(layoutParams);
        binding.remarkTitle.titleMenuConfirm.setText("确认");
        binding.remarkTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        binding.remarkTitle.titleMenuConfirm.setOnClickListener(this);

        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit,binding.remark,50,RemarkActivity.this));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.title_back_ll:
                onBackPressed();
                break;
            case R.id.title_menu_confirm:
                if(TextUtils.isEmpty(binding.remark.getText().toString())){
                    ToastUtil.toastWarning(RemarkActivity.this,"请输入备注后再确认",-1);
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("remark",binding.remark.getText().toString());
                setResult(Constants.RESULT_OK,intent);
                finish();
                break;
        }
    }
}
