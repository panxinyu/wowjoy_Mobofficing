package cn.wowjoy.office.pm.view.viewodel;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

import java.util.Map;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.pm.data.SubmitResponse;
import cn.wowjoy.office.pm.data.UploadImgResponse;
import cn.wowjoy.office.utils.ImageFileParse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.RequestBody;

public class SignNameViewModel extends NewBaseViewModel {


    @Inject
    public SignNameViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
    @Inject
    ApiService apiService;
    public GlideUrl getGlideUrlHeaderUrl(String glideUrlHeader, String mToken) {
        GlideUrl glideUrl = null;
        if (!TextUtils.isEmpty(glideUrlHeader)) {
            glideUrl = new GlideUrl(glideUrlHeader, new LazyHeaders.Builder()
                    .addHeader("Content-Type", "application/json;charset=UTF-8")
                    .addHeader("Authorization", mToken)
                    .build());
        }
        return glideUrl;
    }
    private Map<String, RequestBody> initSaveInfo( String filePath){
        Map<String, RequestBody> bodyMap = new ArrayMap<>();
//        Gson gson = new Gson();
//        RequestBody bodyJson = RequestBody.create(MEDIA_TYPE_JSON, gson.toJson(request));
//        bodyMap.put("inspectRecordDtlVO", bodyJson);
        bodyMap = ImageFileParse.typeTurnNoJson(filePath,bodyMap,"file");
        return bodyMap;

    }
    public MediatorLiveData<LiveDataWrapper<UploadImgResponse>> upload = new MediatorLiveData<>();
    public void uploadSignPic(String FilePaths){
        upload.setValue(LiveDataWrapper.loading(null));
        Map<String, RequestBody> initSaveInfo = initSaveInfo(FilePaths);
        Disposable disposable = apiService.uploadSignName(initSaveInfo)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((UploadImgResponse taskListResponse) -> {
                    upload.setValue(LiveDataWrapper.success(taskListResponse));
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        upload.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
    public MediatorLiveData<LiveDataWrapper<SubmitResponse>> sign = new MediatorLiveData<>();
    public void sighPmReports(String ids, String currentUserId, String thumbnailId, String pictureId){
        sign.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.sighPmReports(  ids,  currentUserId,  thumbnailId,  pictureId)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((SubmitResponse taskListResponse) -> {
                    sign.setValue(LiveDataWrapper.success(taskListResponse));
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        sign.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
}
