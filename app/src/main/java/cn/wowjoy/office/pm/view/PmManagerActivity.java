package cn.wowjoy.office.pm.view;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.badgeview.BGABadgeTextView;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorRedPointAdapter;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.event.RedPointEvent;
import cn.wowjoy.office.databinding.ActivityPmManagerBinding;
import cn.wowjoy.office.pm.data.PmReportDetailResponse;
import cn.wowjoy.office.pm.db.PmReportListDetailEntity;
import cn.wowjoy.office.pm.view.fragment.manager.MAllFragment;
import cn.wowjoy.office.pm.view.viewodel.PmManagerViewModel;
import cn.wowjoy.office.utils.MD5Util;
import cn.wowjoy.office.utils.ToastUtil;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class PmManagerActivity extends PMBaseActivity<ActivityPmManagerBinding, PmManagerViewModel> implements View.OnClickListener {

    private List<Integer> icons;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorRedPointAdapter mTabIndicatorActivityAdapter;

    @Override
    protected Class<PmManagerViewModel> getViewModel() {
        return PmManagerViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pm_manager;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        binding.materialTitle.titleTextTv.setText("PM管理");
        binding.materialTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.materialTitle.titleBackTv.setText("");
        binding.materialTitle.titleBackLl.setOnClickListener(this);
        binding.materialTitle.imgvSearch.setVisibility(View.VISIBLE);
        binding.materialTitle.imgvSearch.setOnClickListener(this);
        initData();
        binding.tabIndicator.setScrollBar(new ColorBar(getApplicationContext(), getResources().getColor(R.color.view_line1), 4));//设置滚动条的颜色，及高度
        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this, R.color.appText14), ContextCompat.getColor(this, R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator, binding.vp);
        mTabIndicatorActivityAdapter = new TabIndicatorRedPointAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter.setData(titles, fragments);
        indicatorViewPager.setAdapter(mTabIndicatorActivityAdapter);
        binding.vp.setCanScroll(true);
        binding.btReport.setOnClickListener(this);


        viewModel.scan.observe(this, new Observer<LiveDataWrapper<List<PmReportListDetailEntity>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<PmReportListDetailEntity>> listLiveDataWrapper) {
                switch (listLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(PmManagerActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(PmManagerActivity.this);
                        if (null != listLiveDataWrapper.data && listLiveDataWrapper.data.size() > 0) {
                            PmReportDetailResponse pmReportDetailResponse = new Gson().fromJson(listLiveDataWrapper.data.get(0).getJsonEntity(), PmReportDetailResponse.class);
                            PmReportDetailActivity.launch(PmManagerActivity.this, Constants.EDIT_REPORT, qrCode, pmReportDetailResponse, false);
                        } else {
                            PmReportDetailActivity.launch(PmManagerActivity.this, Constants.ADD_REPORT, qrCode, null, false);
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(PmManagerActivity.this);
                        break;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RedPointEvent event) {
        Log.e("PXY", "onMessageEvent: " + event.toString());
        int count = event.getCount();
        int position = 0;
        switch (event.getStatus()) {
//            case "":
//                ((BGABadgeTextView) binding.tabIndicator.getItemView(position)).hiddenBadge();
//                break;
            case "82-1":
                position = 1;
                break;
            case "82-2":
                position = 2;
                break;
            case "82-4":
                position = 3;
                break;
        }
        if (count > 0) {
            if (position == 0) {
                ((BGABadgeTextView) binding.tabIndicator.getItemView(position)).hiddenBadge();
                return;
            }
            ((BGABadgeTextView) binding.tabIndicator.getItemView(position)).showCirclePointBadge();
        } else {
            ((BGABadgeTextView) binding.tabIndicator.getItemView(position)).hiddenBadge();
        }
    }
    //没网时 红点要去掉
    @Override
    public void onChangeListener(int status) {
//        super.onChangeListener(status);
        if (status == 404) {
            // 离线状态
            for(int i = 0;i<4;i++){
                    ((BGABadgeTextView) binding.tabIndicator.getItemView(i)).hiddenBadge();
            }
        }
    }
    private void initData() {
        titles = new ArrayList<>();
        titles.add("全部");
        titles.add("即将超期");
        titles.add("已超期");
        titles.add("故障中");
        fragments = new ArrayList<>();
        fragments.add(MAllFragment.newInstance(""));
        fragments.add(MAllFragment.newInstance("82-1"));
        fragments.add(MAllFragment.newInstance("82-2"));
        fragments.add(MAllFragment.newInstance("82-4"));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                finish();
                break;
            case R.id.imgv_search:
                Intent intent = new Intent(PmManagerActivity.this, PmSearchActivity.class);
                intent.putExtra(Constants.PM_SEARCH, Constants.MANAGER_SEARCH);
                startActivity(intent);
                break;
            case R.id.bt_report:
                startActivity(new Intent(PmManagerActivity.this, PmReportActivity.class));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        openBroadCast();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeBroadCast();
    }

    private String qrCode;

    @Override
    protected void brodcast(Context context, String msg) {
        super.brodcast(context, msg);
        if (!MD5Util.IsUUID(msg)) {
            ToastUtil.toastWarning(context, getString(R.string.notBelong), -1);
            return;
        }
        qrCode = msg;
        //先去查找列表中是否已存在（包括本地和联网的）  网络状况？
//        if(NetWorkUtils.isMobileConnected(context)){
//            //查找本地数据库和远程服务端是否有该条
//
//        }else{
//            //查找本地数据库是否存在该条           没有则视为新增
//
//        }
        viewModel.queryByQrCode(qrCode, viewModel.userInfo.getStaffName());
    }


}
