package cn.wowjoy.office.pm.view;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import java.util.List;

import cn.wowjoy.office.BuildConfig;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.common.widget.CommonDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivitySignNameBinding;
import cn.wowjoy.office.homepage.MenuHelper;
import cn.wowjoy.office.pm.data.SubmitResponse;
import cn.wowjoy.office.pm.data.UploadImgResponse;
import cn.wowjoy.office.pm.db.PmReportListEntity;
import cn.wowjoy.office.pm.view.viewodel.SignNameViewModel;
import cn.wowjoy.office.utils.ImageUtil;
import cn.wowjoy.office.utils.ToastUtil;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class SignNameActivity extends PMBaseActivity<ActivitySignNameBinding,SignNameViewModel> implements View.OnClickListener {
    private List<PmReportListEntity> selectList;
    private String ids;
    private StringBuilder mStringBuilder = new StringBuilder();
    private String filePath = BuildConfig.PICTURE_PATH;

    @Override
    protected Class<SignNameViewModel> getViewModel() {
        return SignNameViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_name;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
         binding.setViewModel(viewModel);
         binding.signTitle.titleTextTv.setText("批量签名");
        binding.signTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.signTitle.titleBackTv.setText("清空");
        binding.signTitle.titleBackTv.setPadding(20,0,0,0);
        binding.signTitle.titleBackTv.setOnClickListener(this);
        binding.signTitle.titleBackIv.setOnClickListener(this);
        binding.signTitle.titleMenuConfirm.setText("保存");
        binding.signTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        binding.signTitle.titleMenuConfirm.setOnClickListener(this);

        selectList = (List<PmReportListEntity>) getIntent().getSerializableExtra("select");
        if(null != selectList && selectList.size()>0){
            for(PmReportListEntity detail : selectList){
                if(selectList.indexOf(detail) == selectList.size()-1){
                    mStringBuilder.append(detail.getId());
                }else{
                    mStringBuilder.append(detail.getId()).append(",");
                }
            }
        }
        initObserve();
    }

    private void initObserve() {
        viewModel.sign.observe(this, new Observer<LiveDataWrapper<SubmitResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<SubmitResponse> stringLiveDataWrapper) {
                switch (stringLiveDataWrapper.status){
                    case LOADING:
                        DialogUtils.waitingDialog(SignNameActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(SignNameActivity.this);
                        ToastUtil.toastImage(SignNameActivity.this,"签名成功",-1);
                        //删除签名文件
                        ImageUtil.deleteImg(SignNameActivity.this,binding.signature.getBitmapPath());
                        finish();
                        break;
                    case ERROR:
                        DialogUtils.dismiss(SignNameActivity.this);
                        handleException(stringLiveDataWrapper.error,false);
                        finish();
                        break;
                }
            }
        });
        viewModel.upload.observe(this, new Observer<LiveDataWrapper<UploadImgResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<UploadImgResponse> uploadImgResponseLiveDataWrapper) {
                switch (uploadImgResponseLiveDataWrapper.status){
                    case LOADING:
                        DialogUtils.waitingDialog(SignNameActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(SignNameActivity.this);
                        if(null != uploadImgResponseLiveDataWrapper.data){
                            viewModel.sighPmReports(mStringBuilder.toString(), MenuHelper.getAuidS().get("zc_pm"),uploadImgResponseLiveDataWrapper.data.getThumbnailId(),uploadImgResponseLiveDataWrapper.data.getFileId());
                        }else{
                            ToastUtil.toastWarning(SignNameActivity.this,"签名失败",-1);
                            finish();
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(SignNameActivity.this);
                        handleException(uploadImgResponseLiveDataWrapper.error,false);
                        finish();
                        break;
                }
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.title_menu_confirm:
                if(!binding.signature.canSub()){
                    ToastUtil.toastWarning(SignNameActivity.this,"请签名后再保存!",-1);
                    return;
                }

                if(!TextUtils.isEmpty(binding.signature.savePhoto(Constants.BASE_PATH_PIC))){
                    viewModel.uploadSignPic(binding.signature.getBitmapPath());
                }
                break;
            case R.id.title_back_iv:
              onBackPressed();
                break;
            case R.id.title_back_tv:
                noticeClear();
                break;
        }
    }
    private CommonDialog noticeClearDialog;

    public void noticeClear() {
        noticeClearDialog = new CommonDialog.Builder()
                .setTitle("提示")
                .setContent("是否要清空签名？")
                .setCancelBtn("否", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noticeClearDialog.dismiss();
                    }
                })
                .setConfirmBtn("是", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        binding.signature.clear();
                        noticeClearDialog.dismiss();
                    }
                })
                .create();
        noticeClearDialog.show(getSupportFragmentManager(), null);
    }
}
