package cn.wowjoy.office.pm.data;

import java.io.Serializable;
import java.util.List;

public class PmManageResponse implements Serializable{
    private int count;

    private List<Detail> list;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Detail> getList() {
        return list;
    }

    public void setList(List<Detail> list) {
        this.list = list;
    }

   public class Detail implements Serializable{
        private String cardId;    //设备Id

        private String cardName;   //设备名称

        private String assetsSpec;  //规格型号

        private String departmentName;// 所在科室

        private String nextPmDate;  //下次PM日期

       private String keyWord;  //搜索关键字

       public String getKeyWord() {
           return keyWord;
       }

       public void setKeyWord(String keyWord) {
           this.keyWord = keyWord;
       }

       public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public String getAssetsSpec() {
            return assetsSpec;
        }

        public void setAssetsSpec(String assetsSpec) {
            this.assetsSpec = assetsSpec;
        }

        public String getDepartmentName() {
            return departmentName;
        }

        public void setDepartmentName(String departmentName) {
            this.departmentName = departmentName;
        }

        public String getNextPmDate() {
            return nextPmDate;
        }

        public void setNextPmDate(String nextPmDate) {
            this.nextPmDate = nextPmDate;
        }
    }
}
