package cn.wowjoy.office.pm.data;

import java.io.Serializable;
import java.util.List;

import cn.wowjoy.office.pm.data.request.PmReportDtl;

public class PmProjectResponse implements Serializable {
    private PrjInfo prjInfo;

    public PrjInfo getPrjInfo() {
        return prjInfo;
    }

    public void setPrjInfo(PrjInfo prjInfo) {
        this.prjInfo = prjInfo;
    }

    public class PrjInfo{
        //外观
        private List<SelectBean> appMaiten; //维护与保养
        private List<SelectBean> appStatus; //外观

        //电安全
        private List<SelectBean> eleSimulator; //电安全模拟器
        private List<SelectBean> elePower;  //电源部分
        private List<SelectBean> eleCheckType;//其他检测部分


        //性能
        private List<SelectBean> funSimulator; //性能模拟器
        private List<SelectBean> funApp;   //外观功能
        private List<SelectBean> funCheck; //测量检查
        private List<SelectBean> funOther; //其他检测
        private PmReportDtl basicInfo;

        public List<SelectBean> getEleCheckType() {
            return eleCheckType;
        }

        public void setEleCheckType(List<SelectBean> eleCheckType) {
            this.eleCheckType = eleCheckType;
        }

        public List<SelectBean> getEleSimulator() {
            return eleSimulator;
        }

        public void setEleSimulator(List<SelectBean> eleSimulator) {
            this.eleSimulator = eleSimulator;
        }

        public List<SelectBean> getElePower() {
            return elePower;
        }

        public void setElePower(List<SelectBean> elePower) {
            this.elePower = elePower;
        }

        public List<SelectBean> getFunSimulator() {
            return funSimulator;
        }

        public void setFunSimulator(List<SelectBean> funSimulator) {
            this.funSimulator = funSimulator;
        }

        public List<SelectBean> getFunApp() {
            return funApp;
        }

        public void setFunApp(List<SelectBean> funApp) {
            this.funApp = funApp;
        }

        public List<SelectBean> getFunCheck() {
            return funCheck;
        }

        public void setFunCheck(List<SelectBean> funCheck) {
            this.funCheck = funCheck;
        }

        public List<SelectBean> getFunOther() {
            return funOther;
        }

        public void setFunOther(List<SelectBean> funOther) {
            this.funOther = funOther;
        }

        public List<SelectBean> getAppMaiten() {
            return appMaiten;
        }

        public void setAppMaiten(List<SelectBean> appMaiten) {
            this.appMaiten = appMaiten;
        }

        public List<SelectBean> getAppStatus() {
            return appStatus;
        }

        public void setAppStatus(List<SelectBean> appStatus) {
            this.appStatus = appStatus;
        }

        public PmReportDtl getBasicInfo() {
            return basicInfo;
        }

        public void setBasicInfo(PmReportDtl basicInfo) {
            this.basicInfo = basicInfo;
        }
    }
}
