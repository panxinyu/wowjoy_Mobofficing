package cn.wowjoy.office.pm.view.detail;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.common.widget.CommonDialog;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityLookCheckBinding;
import cn.wowjoy.office.pm.data.CardResponse;
import cn.wowjoy.office.pm.data.DictDataResponse;
import cn.wowjoy.office.pm.data.SelectBean;
import cn.wowjoy.office.pm.data.request.PmReportDtl;
import cn.wowjoy.office.pm.view.PMBaseActivity;
import cn.wowjoy.office.pm.view.viewodel.LookCheckViewModel;
import cn.wowjoy.office.utils.MyTextWatcher;
import cn.wowjoy.office.utils.PickerViewUtils;
import cn.wowjoy.office.utils.ToastUtil;

import static cn.wowjoy.office.pm.view.PmReportDetailActivity.LOOKCHECK;

public class LookCheckActivity extends PMBaseActivity<ActivityLookCheckBinding, LookCheckViewModel> implements View.OnClickListener {
    private List<SelectBean> mLookList;
    String[] mTitles = new String[]{"设备外观清洁", "主机、底座及其余组件无外观损坏", "控制开关正常","日间显示亮度足够", "电源线、电池(主机)、电缆配件完整", "有正确清晰的序列号、标签和警示"};
    private List<SelectBean> mCheckList;
    String[] check = new String[]{"除尘", "检查各参数正确", "报警功能正常", "附件及易损零件检查", "过滤网、通风口清洁", "机械组件紧固"};

    private MDialog waitDialog;
    private final String[] strs = {"合格", "不合格", "不适用"};
//    /**
//     * 检测类型列表
//     */
//    private List<String> typeList = new ArrayList<>();
    private OptionsPickerView typeOptionsPickerView;
    private int typePosition;
    private String checkType;
    private String[] checkTypeStrs = new String[]{"验收", "预防性检测", "稳定性检测"};
//    /**
//     * 检测机构列表
//     */
//    private List<String> roomList = new ArrayList<>();
    private OptionsPickerView roomOptionsPickerView;
    private int roomPosition;
    private String checkOrgannization;

    private Map<String, Integer> mMapLook;
    private Map<String, Integer> mMapCheck;

    //温度
    private String temperature;
    //湿度
    private String humidity;
    //备注
    private String remarks;

    //状态
    private int state;
    private String cardId;
    private CardResponse cardDetail;
    private CommonDialog checkFailedDialog;

    private PmReportDtl submit;


    @Override
    protected Class<LookCheckViewModel> getViewModel() {
        return LookCheckViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_look_check;
    }

    public static void launch(Activity context, int state, String cardId, CardResponse detail, PmReportDtl edit) {
        Intent i = new Intent(context, LookCheckActivity.class);
        if(null != edit && TextUtils.isEmpty(edit.getCheckResult())){
            state = 1;
        }
        i.putExtra(Constants.PM_REPORT, state);
        i.putExtra("cardId", cardId);
        i.putExtra("cardDetail", detail);
        i.putExtra("pmReportDtl", edit);
        context.startActivityForResult(i, LOOKCHECK);
    }

    @Override
    protected void init(Bundle savedInstanceState) {

        state = getIntent().getIntExtra(Constants.PM_REPORT, 0);
        cardId = getIntent().getStringExtra("cardId");
        cardDetail = (CardResponse) getIntent().getSerializableExtra("cardDetail");
        submit = (PmReportDtl) getIntent().getSerializableExtra("pmReportDtl");
        initView();
        initData();
        response();
        observe();
    }

    private void observe() {
        viewModel.typeData.observe(LookCheckActivity.this, new Observer<LiveDataWrapper<DictDataResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<DictDataResponse> dictDataResponseLiveDataWrapper) {
                switch (dictDataResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(LookCheckActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(LookCheckActivity.this, waitDialog);
                        }
                        typeList.clear();
                        if (null != dictDataResponseLiveDataWrapper.data && dictDataResponseLiveDataWrapper.data.getList().size() > 0) {
                            for (DictDataResponse.DictData dictData : dictDataResponseLiveDataWrapper.data.getList()) {
                                typeList.add(dictData.getDictDataName());
                            }
                            typeOptionsPickerView = PickerViewUtils.getOptionPickerView("", typeList, PickerViewUtils.getCheckTypePosition(PickerViewUtils.checkType), LookCheckActivity.this, PickerViewUtils.getListener(binding.include2.tvCheckType, typeList, 1));
                            typeOptionsPickerView.show();
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(LookCheckActivity.this, waitDialog);
                        }
                        handleException(dictDataResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
        viewModel.roomData.observe(LookCheckActivity.this, new Observer<LiveDataWrapper<DictDataResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<DictDataResponse> dictDataResponseLiveDataWrapper) {
                switch (dictDataResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(LookCheckActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(LookCheckActivity.this, waitDialog);
                        }
                        roomList.clear();
                        if (null != dictDataResponseLiveDataWrapper.data && dictDataResponseLiveDataWrapper.data.getList().size() > 0) {
                            for (DictDataResponse.DictData dictData : dictDataResponseLiveDataWrapper.data.getList()) {
                                roomList.add(dictData.getDictDataName());
                            }
                            roomOptionsPickerView = PickerViewUtils.getOptionPickerView("", roomList, PickerViewUtils.getCheckOrganizationPosition(PickerViewUtils.checkOrganization), LookCheckActivity.this, PickerViewUtils.getListener(binding.include2.tvCheckWhere, roomList, 2));
                            roomOptionsPickerView.show();
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(LookCheckActivity.this, waitDialog);
                        }
                        handleException(dictDataResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
    }

    private void response() {
    }

    private void initView() {
        binding.include2.tvTemp.addTextChangedListener(editListener(binding.include2.tvTemp));
        binding.include2.tvEmp.addTextChangedListener(editListener(binding.include2.tvEmp));
        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit, binding.remark, 50, LookCheckActivity.this));

        binding.lookCheckTitle.titleTextTv.setText("外观检测");
        binding.lookCheckTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.lookCheckTitle.titleBackTv.setText("");
        binding.lookCheckTitle.titleBackLl.setOnClickListener(this);
        binding.lookCheckTitle.titleMenuConfirm.setText("保存");
        if (state != 3) {
            binding.lookCheckTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        }
        binding.lookCheckTitle.titleMenuConfirm.setOnClickListener(this);

        binding.rlSelect1.setOnClickListener(this);
        binding.rlSelect2.setOnClickListener(this);
        binding.rlSelect3.setOnClickListener(this);
        binding.rlSelect4.setOnClickListener(this);
        binding.rlSelect5.setOnClickListener(this);
        binding.include2.rlType.setOnClickListener(this);
        binding.include2.rlRoom.setOnClickListener(this);
        if (null != cardDetail) {
            binding.setModel(cardDetail);
        }
        switch (state) {
            case Constants.ADD_REPORT:
                binding.rlRemark.setVisibility(View.VISIBLE);
                break;
            case Constants.EDIT_REPORT:
                // 温度
                if (!TextUtils.isEmpty(submit.getTemperature())) {
                    binding.include2.tvTemp.setText(submit.getTemperature());
                    binding.include2.tvTemp.setSelection(submit.getTemperature().length());
                }
                //湿度
                if (!TextUtils.isEmpty(submit.getHumidity())) {
                    binding.include2.tvEmp.setText(submit.getHumidity());
                    binding.include2.tvEmp.setSelection(submit.getHumidity().length());
                }
                if (!TextUtils.isEmpty(submit.getCheckType())) {
                    binding.include2.tvCheckType.setText(PickerViewUtils.getCheckType(submit.getCheckType()));
                }
                if(!TextUtils.isEmpty(submit.getCheckOrganization())){
                    binding.include2.tvCheckWhere.setText(PickerViewUtils.getCheckOrganization(submit.getCheckOrganization()));
                }
                //结果
                if(!TextUtils.isEmpty(submit.getCheckResult())){
                    binding.tvResult.setText(submit.getCheckResult());
                    binding.resultSelect5.setText(submit.getCheckResult());
                }
                //备注
                binding.rlRemark.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(submit.getRemarks())) {
                    binding.remark.setText(submit.getRemarks());
                    binding.remark.setSelection(submit.getRemarks().length());
                }
                break;
            case Constants.LOOK_REPORT:
                binding.look2.setModel(submit);
                if (!TextUtils.isEmpty(submit.getCheckType())) {
                    binding.look2.tvCheckType.setText(PickerViewUtils.getCheckType(submit.getCheckType()));
                }
                if(!TextUtils.isEmpty(submit.getCheckOrganization())){
                    binding.look2.tvCheckWhere.setText(PickerViewUtils.getCheckOrganization(submit.getCheckOrganization()));
                }
                //结果
                if(!TextUtils.isEmpty(submit.getCheckResult())){
                    binding.tvResult.setText(submit.getCheckResult());
                    binding.resultSelect5.setText(submit.getCheckResult());
                }
                //备注
                if (!TextUtils.isEmpty(submit.getRemarks())) {
                    binding.remarkSee.setText(submit.getRemarks());
                    binding.remarkSee.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private int size = 1;
    private int size2 = 1;

    private void initData() {
        mMapLook = new HashMap<>();
        mMapCheck = new HashMap<>();
        mLookList = new ArrayList<>();
        mCheckList = new ArrayList<>();
        if (state == 1) {
            for (String mTitle : mTitles) {
                SelectBean selectBean = new SelectBean("外观状态检测", size + "");
                selectBean.setAppStatusPj(mTitle);
                selectBean.setPriTypeCode("appStatus");
                mLookList.add(selectBean);
                size++;
            }
            for (String mTitle : check) {
                SelectBean selectBean = new SelectBean("维护与保养", size2 + "");
                selectBean.setAppMaitenPj(mTitle);
                selectBean.setPriTypeCode("appMaiten");
                mCheckList.add(selectBean);
                size2++;
            }
        } else if (state == 2 || state == 3) {
            for (SelectBean selectBean : submit.getPrjList()) {
                if (selectBean.getPrjType().equals("外观状态检测")) {
                    mLookList.add(selectBean);
                    mMapLook.put(selectBean.getAppStatusPj(),selectBean.getAppStatusPjVal().equals("合格")?0:selectBean.getAppStatusPjVal().equals("不合格")?1:2);
                } else if (selectBean.getPrjType().equals("维护与保养")) {
                    mCheckList.add(selectBean);
                    mMapCheck.put(selectBean.getAppMaitenPj(),selectBean.getAppMaitenPjVal().equals("合格")?0:selectBean.getAppMaitenPjVal().equals("不合格")?1:2);
                }
            }
        }


        // 进行排序
//        Collections.sort(mLookList, c);
//        Collections.sort(mCheckList, c);
        addView(mLookList, 3);
        addView(mCheckList, 4);
    }

    private void addView(List<SelectBean> mList, int type) {
        if(state != 1){
            switch (notApply(mList, type)){
                case RESULT_OK:
                    if (type == 3){
                        binding.include3.tvState.setText("合格");
                        binding.resultSelect3.setText("合格");
                    }else{
                        binding.include4.tvState.setText("合格");
                        binding.resultSelect4.setText("合格");
                    }
                    break;
                case RESULT_FAIL:
                    if (type == 3){
                        binding.include3.tvState.setText("不合格");
                        binding.resultSelect3.setText("不合格");
                    }else{
                        binding.include4.tvState.setText("不合格");
                        binding.resultSelect4.setText("不合格");
                    }
                    break;
                case RESULT_NOTSUIT:
                    if (type == 3){
                        binding.include3.tvState.setText("不适用");
                        binding.resultSelect3.setText("不适用");
                    }else{
                        binding.include4.tvState.setText("不适用");
                        binding.resultSelect4.setText("不适用");
                    }
                    break;
            }
        }
        for (SelectBean bean : mList) {
            if(state == 3){
                RelativeLayout mRl = (RelativeLayout) LayoutInflater.from(LookCheckActivity.this).inflate(R.layout.iten_look_see, null);
                TextView mTextView = mRl.findViewById(R.id.tv_see);
                if (type == 3) {
                    if (!TextUtils.isEmpty(bean.getAppStatusPj()) && !TextUtils.isEmpty(bean.getAppStatusPjVal())) {
                        mTextView.setText(bean.getAppStatusPj()+":  "+bean.getAppStatusPjVal());
                    }
                } else {
                    if (!TextUtils.isEmpty(bean.getAppMaitenPj()) && !TextUtils.isEmpty(bean.getAppMaitenPjVal())) {
                        mTextView.setText(bean.getAppMaitenPj()+":  "+bean.getAppMaitenPjVal());
                    }
                }
                if (type == 3) {
                    binding.include3.llTotal.addView(mRl);
                } else {
                    binding.include4.llTotal.addView(mRl);
                }
            }else {
                LinearLayout mRl = (LinearLayout) LayoutInflater.from(LookCheckActivity.this).inflate(R.layout.item_lookcheck, null);
                TextView mTextView = mRl.findViewById(R.id.title);
                TagFlowLayout tagFlowLayout = (mRl.findViewById(R.id.psResTF));
                TagAdapter<String> tagAdapter = new TagAdapter<String>(Arrays.asList("合格", "不合格", "不适用")) {
                    @Override
                    public View getView(FlowLayout parent, int position, String s) {
                        TextView tv = (TextView) LayoutInflater.from(LookCheckActivity.this).inflate(R.layout.item_tag_full, tagFlowLayout, false);
                        tv.setText(s);
                        return tv;
                    }
                };
                tagFlowLayout.setAdapter(tagAdapter);
                if (type == 3) {
                    if (!TextUtils.isEmpty(bean.getAppStatusPj())) {
                        mTextView.setText(bean.getAppStatusPj());
                    }
                    if (!TextUtils.isEmpty(bean.getAppStatusPjVal())) {
                        tagAdapter.setSelectedList(bean.getSelectPosition(bean.getAppStatusPjVal()));
                    }
                } else {
                    if (!TextUtils.isEmpty(bean.getAppMaitenPj())) {
                        mTextView.setText(bean.getAppMaitenPj());
                    }
                    if (!TextUtils.isEmpty(bean.getAppMaitenPjVal())) {
                        tagAdapter.setSelectedList(bean.getSelectPosition(bean.getAppMaitenPjVal()));
                    }
                }
                tagFlowLayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                    @Override
                    public boolean onTagClick(View view, int position, FlowLayout parent) {
                        if (type == 3) {
//                        bean.setAppStatusPjValByPosition(position);
                            bean.setAppStatusPjVal(strs[position]);
                            if (((TagFlowLayout) parent).getSelectedList().size() == 0) {
                                mMapLook.remove(bean.getAppStatusPj());
                            } else {
                                mMapLook.put(bean.getAppStatusPj(), position);
                            }
                            //先判断是否全部都是不适用
                            String result = mMapLook.size() != mTitles.length ? "待选择" : notApply(mList, type) == 0 ? "合格" : notApply(mList, type) == 1? "不合格":"不适用";
                            binding.include3.tvState.setText(result);
                            binding.resultSelect3.setText(result);
                        } else {
                            bean.setAppMaitenPjVal(strs[position]);
//                        bean.setAppMaitenPjValByPosition(position);
                            if (((TagFlowLayout) parent).getSelectedList().size() == 0) {
                                mMapCheck.remove(bean.getAppMaitenPj());
                            } else {
                                mMapCheck.put(bean.getAppMaitenPj(), position);
                            }
                            String result = mMapCheck.size() != check.length ? "待选择" : notApply(mList, type) == 0 ? "合格" : notApply(mList, type) == 1? "不合格":"不适用";
                            binding.include4.tvState.setText(result);
                            binding.resultSelect4.setText(result);
                        }
                        checkResult();
                        return true;
                    }
                });
                if (type == 3) {
                    binding.include3.llTotal.addView(mRl);
                } else {
                    binding.include4.llTotal.addView(mRl);
                }
            }


        }
    }

    /**
     *   0 合格  1  不合格  2  不适用
     * @param mList
     * @param type
     * @return
     */
    private int notApply(List<SelectBean> mList, int type) {
        int number = 0;
        if (type == 3) {
            for (SelectBean selectBean : mList) {
                if (selectBean.getAppStatusPjVal().equals("不适用")) {
                    number++;
                    continue;
                }
                if (selectBean.getAppStatusPjVal().equals("不合格")) {
                    return RESULT_FAIL;
                }
            }
        } else {
            for (SelectBean selectBean : mList) {
                if (selectBean.getAppMaitenPjVal().equals("不适用")) {
                    number++;
                    continue;
                }
                if (selectBean.getAppMaitenPjVal().equals("不合格")) {
                    return RESULT_FAIL;
                }
            }
        }
        if(number == mList.size()){
            return RESULT_NOTSUIT;
        }
        return RESULT_OK;
    }
    LinearLayout linearLayout;
    LinearLayout include1;
    private void showAndHide(int type) {
         if(state == 3){
             linearLayout = binding.look2.llTotal;
         }else{
             linearLayout = binding.include2.llTotal;
         }
        if(null == cardDetail){
            include1 = binding.rlOffLine; 
        }else{
            include1 = binding.card;
        }
            switch (type) {
                case 1:
//                    binding.include2.llTotal.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.GONE);
                    binding.include3.llTotal.setVisibility(View.GONE);
                    binding.include4.llTotal.setVisibility(View.GONE);
                    binding.include5.setVisibility(View.GONE);

                    //自己是否显示，若显示则隐藏
                    if (include1.getVisibility() == View.VISIBLE) {
                        include1.setVisibility(View.GONE);
                        binding.rlSelect1.setVisibility(View.VISIBLE);
                    } else {
                        include1.setVisibility(View.VISIBLE);
                        binding.rlSelect1.setVisibility(View.GONE);
                        binding.rlSelect2.setVisibility(View.VISIBLE);
                        binding.rlSelect3.setVisibility(View.VISIBLE);
                        binding.rlSelect4.setVisibility(View.VISIBLE);
                        binding.rlSelect5.setVisibility(View.VISIBLE);
                    }
                    break;
                case 2:
                    include1.setVisibility(View.GONE);
                    binding.include3.llTotal.setVisibility(View.GONE);
                    binding.include4.llTotal.setVisibility(View.GONE);
                    binding.include5.setVisibility(View.GONE);

                    //自己是否显示，若显示则隐藏
                    if (linearLayout.getVisibility() == View.VISIBLE) {
                        linearLayout.setVisibility(View.GONE);
                        binding.rlSelect2.setVisibility(View.VISIBLE);

                    } else {
                        linearLayout.setVisibility(View.VISIBLE);
                        binding.rlSelect1.setVisibility(View.VISIBLE);
                        binding.rlSelect2.setVisibility(View.GONE);
                        binding.rlSelect3.setVisibility(View.VISIBLE);
                        binding.rlSelect4.setVisibility(View.VISIBLE);
                        binding.rlSelect5.setVisibility(View.VISIBLE);
                    }
                    break;
                case 3:
                    include1.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.GONE);
                    binding.include4.llTotal.setVisibility(View.GONE);
                    binding.include5.setVisibility(View.GONE);

                    //自己是否显示，若显示则隐藏
                    if (binding.include3.llTotal.getVisibility() == View.VISIBLE) {
                        binding.include3.llTotal.setVisibility(View.GONE);
                        binding.rlSelect3.setVisibility(View.VISIBLE);
                    } else {
                        binding.include3.llTotal.setVisibility(View.VISIBLE);
                        binding.rlSelect3.setVisibility(View.GONE);
                        binding.rlSelect1.setVisibility(View.VISIBLE);
                        binding.rlSelect2.setVisibility(View.VISIBLE);
                        binding.rlSelect4.setVisibility(View.VISIBLE);
                        binding.rlSelect5.setVisibility(View.VISIBLE);
                    }
                    break;
                case 4:
                    include1.setVisibility(View.GONE);
                    binding.include3.llTotal.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.GONE);
                    binding.include5.setVisibility(View.GONE);

                    //自己是否显示，若显示则隐藏
                    if (binding.include4.llTotal.getVisibility() == View.VISIBLE) {
                        binding.include4.llTotal.setVisibility(View.GONE);
                        binding.rlSelect4.setVisibility(View.VISIBLE);
                    } else {
                        binding.include4.llTotal.setVisibility(View.VISIBLE);
                        binding.rlSelect4.setVisibility(View.GONE);
                        binding.rlSelect3.setVisibility(View.VISIBLE);
                        binding.rlSelect1.setVisibility(View.VISIBLE);
                        binding.rlSelect2.setVisibility(View.VISIBLE);
                        binding.rlSelect5.setVisibility(View.VISIBLE);
                    }
                    break;
                case 5:
                    include1.setVisibility(View.GONE);
                    binding.include3.llTotal.setVisibility(View.GONE);
                    binding.include4.llTotal.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.GONE);

                    //自己是否显示，若显示则隐藏
                    if (binding.include5.getVisibility() == View.VISIBLE) {
                        binding.include5.setVisibility(View.GONE);
                        binding.rlSelect5.setVisibility(View.VISIBLE);
                    } else {
                        binding.include5.setVisibility(View.VISIBLE);
                        binding.rlSelect5.setVisibility(View.GONE);
                        binding.rlSelect4.setVisibility(View.VISIBLE);
                        binding.rlSelect3.setVisibility(View.VISIBLE);
                        binding.rlSelect1.setVisibility(View.VISIBLE);
                        binding.rlSelect2.setVisibility(View.VISIBLE);
                    }
                    break;
            }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_back_ll:
                onBackPressed();
                break;
            case R.id.rl_select_1:
                showAndHide(1);
                break;
            case R.id.rl_select_2:
                showAndHide(2);
                break;
            case R.id.rl_select_3:
                showAndHide(3);
                break;
            case R.id.rl_select_4:
                showAndHide(4);
                break;
            case R.id.rl_select_5:
                showAndHide(5);
                checkResult();
                break;
            case R.id.title_menu_confirm:
                //save
                if (TextUtils.isEmpty(binding.include2.tvCheckType.getText().toString())
                        || TextUtils.isEmpty(binding.include2.tvCheckWhere.getText().toString())
                        || TextUtils.isEmpty(binding.include2.tvTemp.getText().toString())
                        || TextUtils.isEmpty(binding.include2.tvEmp.getText().toString())
                        ) {
                    showCheckFailedDialog("基础信息模块有信息尚未填完,请填完保存");
                    return;
                }
                if (mMapLook.size() != mLookList.size() || binding.include3.tvState.getText().equals("待选择")) {
                    showCheckFailedDialog("外观状态检测模块有信息尚未选完,请选完保存");
                    return;
                }
                if (mMapCheck.size() != mCheckList.size() || binding.include4.tvState.getText().equals("待选择")) {
                    showCheckFailedDialog("维护与保养模块有信息尚未选完,请选完保存");
                    return;
                }
                if (null == submit)
                    submit = new PmReportDtl();
                submit.setCheckModel("默认模板");
                submit.setCheckProject("85-1");
                submit.setCheckProjectName("外观检测");
                submit.setCheckType(PickerViewUtils.checkType);
                submit.setCheckOrganization(PickerViewUtils.checkOrganization);
                submit.setTemperature(binding.include2.tvTemp.getText().toString().trim());
                submit.setHumidity(binding.include2.tvEmp.getText().toString().trim());
                submit.setCheckResult(binding.tvResult.getText().toString());
//                submit.setAppStatusPjResult(binding.include3.tvState.getText().toString());
//                submit.setAppMaitenPjResult(binding.include4.tvState.getText().toString());
                if (!TextUtils.isEmpty(binding.remark.getText().toString().trim())) {
                    submit.setRemarks(binding.remark.getText().toString().trim());
                }
                List<SelectBean> mAll = new ArrayList<>();
                mAll.addAll(mLookList);
                mAll.addAll(mCheckList);
                submit.setPrjList(mAll);
                Intent intent = new Intent();
                intent.putExtra("LookCheckResult", submit);
                setResult(Constants.RESULT_OK, intent);
                ToastUtil.toastImage(getApplication().getApplicationContext(), "保存成功", -1);
                finish();
                break;
            case R.id.rl_type:
                //save
//                viewModel.getDictData("86");
                typeOptionsPickerView = PickerViewUtils.getOptionPickerView("", typeList, PickerViewUtils.getCheckTypePosition(binding.include2.tvCheckType.getText().toString()), LookCheckActivity.this, PickerViewUtils.getListener(binding.include2.tvCheckType, typeList, 1));
                typeOptionsPickerView.show();

                break;
            case R.id.rl_room:
                //save
//                viewModel.getDictData("87");
                roomOptionsPickerView = PickerViewUtils.getOptionPickerView("", roomList, PickerViewUtils.getCheckOrganizationPosition(binding.include2.tvCheckWhere.getText().toString()), LookCheckActivity.this, PickerViewUtils.getListener(binding.include2.tvCheckWhere, roomList, 2));
                roomOptionsPickerView.show();
                break;
        }
    }
    private void checkResult(){
        if (binding.include3.tvState.getText().toString().equals("合格") && binding.include4.tvState.getText().toString().equals("合格")) {
            binding.tvResult.setText("合格");
            binding.resultSelect5.setText("合格");
        } else if (binding.include3.tvState.getText().toString().equals("不适用") && binding.include4.tvState.getText().toString().equals("不适用")
                ) {
            binding.tvResult.setText("不合格");
            binding.resultSelect5.setText("不合格");
        }else if(binding.include3.tvState.getText().toString().equals("不合格") || binding.include4.tvState.getText().toString().equals("不合格")){
            binding.tvResult.setText("不合格");
            binding.resultSelect5.setText("不合格");
        } else if(binding.include3.tvState.getText().toString().equals("合格") && binding.include4.tvState.getText().toString().equals("不适用")){
            binding.tvResult.setText("合格");
            binding.resultSelect5.setText("合格");
        }else if(binding.include3.tvState.getText().toString().equals("不适用") && binding.include4.tvState.getText().toString().equals("合格")){
            binding.tvResult.setText("合格");
            binding.resultSelect5.setText("合格");
        }else {
            binding.tvResult.setText("待生成");
            binding.resultSelect5.setText("待生成");
        }
    }

    @Override
    public void onBackPressed() {
        if(state != 3){
            noticeSave();
        }else {
            super.onBackPressed();
        }

    }
}
