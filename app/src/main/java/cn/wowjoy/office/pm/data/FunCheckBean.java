package cn.wowjoy.office.pm.data;

import java.io.Serializable;

public class FunCheckBean implements Serializable {
     private String prjType;            //大类型
     private String prjNo;              //顺序
     private  String funCheckPrj;        //标题
     private String funCheckPrjVal;      //实际值
     private String funCheckPrjPerval;   //范围  60（53.2-61.9）
     private String funCheckPrjErrval;   //正负差
     private String funCheckPrjResult;   //是否合格

    private String value;  //预估值

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FunCheckBean() {
    }

    public String getPrjType() {
        return prjType;
    }

    public void setPrjType(String prjType) {
        this.prjType = prjType;
    }

    public String getPrjNo() {
        return prjNo;
    }

    public void setPrjNo(String prjNo) {
        this.prjNo = prjNo;
    }

    public String getFunCheckPrj() {
        return funCheckPrj;
    }

    public void setFunCheckPrj(String funCheckPrj) {
        this.funCheckPrj = funCheckPrj;
    }

    public String getFunCheckPrjVal() {
        return funCheckPrjVal;
    }

    public void setFunCheckPrjVal(String funCheckPrjVal) {
        this.funCheckPrjVal = funCheckPrjVal;
    }

    public String getFunCheckPrjPerval() {
        return funCheckPrjPerval;
    }

    public void setFunCheckPrjPerval(String funCheckPrjPerval) {
        this.funCheckPrjPerval = funCheckPrjPerval;
    }

    public String getFunCheckPrjErrval() {
        return funCheckPrjErrval;
    }

    public void setFunCheckPrjErrval(String funCheckPrjErrval) {
        this.funCheckPrjErrval = funCheckPrjErrval;
    }

    public String getFunCheckPrjResult() {
        return funCheckPrjResult;
    }

    public void setFunCheckPrjResult(String funCheckPrjResult) {
        this.funCheckPrjResult = funCheckPrjResult;
    }
}
