package cn.wowjoy.office.pm.view;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.board.NetBroadcastReceiver;
import cn.wowjoy.office.common.widget.CommonDialog;
import cn.wowjoy.office.pm.data.SelectBean;
import cn.wowjoy.office.utils.NetWorkUtils;
import cn.wowjoy.office.utils.ToastUtil;

public abstract class PMBaseActivity <DB extends ViewDataBinding, VM extends NewBaseViewModel> extends NewBaseActivity<DB, VM> implements NetBroadcastReceiver.NetChangeListener{
    private CommonDialog checkFailedDialog;
    /**
     * 检测类型列表
     */
    public List<String> typeList = new ArrayList<>();
    /**
     * 检测机构列表
     */
    public List<String> roomList = new ArrayList<>();

    //小数点位数
    public int digits = 2;


    // 选择结果
    public String result;
    public final int RESULT_OK = 0;
    public final int RESULT_FAIL = 1;
    public final int RESULT_NOTSUIT = 2;

    public static NetBroadcastReceiver.NetChangeListener listener;
    public static int netType;


    /**
     * 判断有无网络 。
     *
     * @return true 有网, false 没有网络.
     */
    public boolean isNetConnect() {
        if (netType == 200) {
            return true;
        } else if (netType == 201) {
            return true;
        } else if (netType == 404) {
            return false;
        }
        return false;
    }
    @Override
    public void onChangeListener(int status) {
        netType = status;
//        Log.e("PXY", "netType:" + status);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //状态栏
        typeList.add("验收");
        typeList.add("预防性检测");
        typeList.add("稳定性检测");

        roomList.add("医院");
        roomList.add("生产厂家");
        roomList.add("第三方服务机构");
        netType = NetWorkUtils.getNetWorkState(this);
        listener =this;
    }
    public TextWatcher editListener(EditText edit) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //如果起始位置为0,且第二位跟的不是".",则无法后续输入

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                if (s.toString().startsWith("0")
                        && s.toString().trim().length() > 1) {
                    if (!s.toString().substring(1, 2).equals(".")) {
                        edit.setText(s.subSequence(0, 1));
                        edit.setSelection(1);
                    }
                }
                //判断是否已经输入过.了
                if (edit.getText().toString().contains(".")) {
                    if (edit.getText().toString().indexOf(".", edit.getText().toString().indexOf(".") + 1) > 0) {
                        ToastUtil.toastWarning(PMBaseActivity.this, "已经输入\".\"不能重复输入", -1);
                        edit.setText(edit.getText().toString().substring(0, edit.getText().toString().length() - 1));
                        edit.setSelection(edit.getText().toString().length());
                    }
                }


                //删除“.”后面超过2位后的数据
                if (s.toString().contains(".")) {
                    if (s.length() - 1 - s.toString().indexOf(".") > digits) {
                        s = s.toString().substring(0,
                                s.toString().indexOf(".") + digits + 1);
                        edit.setText(s);
                        edit.setSelection(s.length()); //光标移到最后
                    }
                }
                //如果"."在起始位置,则起始位置自动补0
                if (s.toString().trim().substring(0).equals(".")) {
                    s = "0" + s;
                    edit.setText(s);
                    edit.setSelection(2);
                }
            }
        };
    }

    public Comparator<Map.Entry<String, String>> deviceComptor = new Comparator<Map.Entry<String, String>>() {
        //升序排序
        public int compare(Map.Entry<String, String> o1,
                           Map.Entry<String, String> o2) {
            int i = Integer.parseInt(o1.getKey().substring(0,1));
            int i2 = Integer.parseInt(o2.getKey().substring(0,1));
            if (i < i2) return -1;
            else return 1;
        }
    };
//    public Comparator deviceComptor = new Comparator<Integer>() {
//        @Override
//        public int compare(Integer i, Integer i2) {
//                if (i < i2) return -1;
//                else return 1;
//        }
//    };
    public Comparator c = new Comparator<SelectBean>() {
        @Override
        public int compare(SelectBean o1, SelectBean o2) {
            if(!TextUtils.isEmpty(o1.getPrjNo()) && !TextUtils.isEmpty(o2.getPrjNo())){
                int i = Integer.parseInt(o1.getPrjNo());
                int i2 = Integer.parseInt(o2.getPrjNo());
                if (i < i2) return -1;
                else return 1;
            }
            return 0;
        }
    };
    public void showCheckFailedDialog(String hint) {
        checkFailedDialog = new CommonDialog.Builder()
                .setTitle("保存失败")
                .setContentColor(R.color.appText)
                .setContent(hint)
                .showCancel(false)
                .setConfirmBtn("我知道了", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkFailedDialog.dismissAllowingStateLoss();
                    }
                })
                .create();
        checkFailedDialog.show(getSupportFragmentManager(), "");
    }

    private CommonDialog noticeSaveDialog;

    public void noticeSave() {
        noticeSaveDialog = new CommonDialog.Builder()
                .setTitle("提醒")
                .setContent("您还未保存，是否退出")
                .setCancelBtn("否", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noticeSaveDialog.dismiss();
                    }
                })
                .setConfirmBtn("是", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noticeSaveDialog.dismiss();
                        finish();
                    }
                })
                .create();
        noticeSaveDialog.show(getSupportFragmentManager(), null);
    }

}
