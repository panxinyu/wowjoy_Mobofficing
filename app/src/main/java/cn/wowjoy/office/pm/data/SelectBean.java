package cn.wowjoy.office.pm.data;

import android.text.TextUtils;

import java.io.Serializable;

public class SelectBean implements Serializable {
    private String id;        //明细ID
    private String prjType;      //大类型
    private String appStatusPj; //名称
    private String appStatusPjVal; //是否合格
    private String prjNo;//顺序

    //edit框的类型
    private String funCheckPrj;        //标题
    private String funCheckPrjVal;      //实际值
    private String funCheckPrjPerval;   //范围  60（53.2-61.9）
    private String funCheckPrjErrval;   //正负差
    private String funCheckPrjResult;   //是否合格
    private boolean isMoreCompare = false;


    private String funAppPj;
    private String funAppPjVal;
    private String funAppPjRem;
    private String funAppPjValCode;

    private String value;  //预估值


    private String appMaitenPj;
    private String appMaitenPjVal;
    private String eleCheckType;
    private  String eleCheckTypeVal;
    private String eleSimulatorPj;
    private String eleSimulatorPjVal;
    private String elePowerPj;
    private String elePowerPjVal;
    private String elePowerPjPerval;
    private String elePowerPjResult;
    private String funSimulatorPj;
    private String funSimulatorPjVal;
    private String funOtherPrj;
    private String funOtherPrjVal;
    //比较三个值
    private  boolean high;
    private boolean low;
    private boolean normal;
    private String highValue;
    private String lowValue;
    private String normalValue;


    //英文代号
    private String prjTypeCode;

    public String getFunAppPjValCode() {
        return funAppPjValCode;
    }

    public void setFunAppPjValCode(String funAppPjValCode) {
        this.funAppPjValCode = funAppPjValCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrjTypeCode() {
        return prjTypeCode;
    }

    public void setPrjTypeCode(String prjTypeCode) {
        this.prjTypeCode = prjTypeCode;
    }

    public String getEleCheckType() {
        return eleCheckType;
    }

    public void setEleCheckType(String eleCheckType) {
        this.eleCheckType = eleCheckType;
    }

    public String getPriTypeCode() {
        return prjTypeCode;
    }

    public void setPriTypeCode(String priTypeCode) {
        this.prjTypeCode = priTypeCode;
    }

    public  boolean isEditOver(){
        return !TextUtils.isEmpty(highValue) && !TextUtils.isEmpty(lowValue) && !TextUtils.isEmpty(normalValue);
    }
    public String getHighValue() {
        return highValue;
    }

    public void setHighValue(String highValue) {
        this.highValue = highValue;
    }

    public String getLowValue() {
        return lowValue;
    }

    public void setLowValue(String lowValue) {
        this.lowValue = lowValue;
    }

    public String getNormalValue() {
        return normalValue;
    }

    public void setNormalValue(String normalValue) {
        this.normalValue = normalValue;
    }

    public boolean isHigh() {
        return high;
    }

    public void setHigh(boolean high) {
        this.high = high;
    }

    public boolean isLow() {
        return low;
    }

    public void setLow(boolean low) {
        this.low = low;
    }

    public boolean isNormal() {
        return normal;
    }

    public void setNormal(boolean normal) {
        this.normal = normal;
    }

    public boolean isMoreCompare() {
        return isMoreCompare;
    }

    public void setMoreCompare(boolean moreCompare) {
        isMoreCompare = moreCompare;
    }

    public String getEleCheckTypeVal() {
        return eleCheckTypeVal;
    }

    public void setEleCheckTypeVal(String eleCheckTypeVal) {
        this.eleCheckTypeVal = eleCheckTypeVal;
    }

    public String getAppMaitenPj() {
        return appMaitenPj;
    }

    public void setAppMaitenPj(String appMaitenPj) {
        this.appMaitenPj = appMaitenPj;
    }

    public String getAppMaitenPjVal() {
        return appMaitenPjVal;
    }

    public void setAppMaitenPjVal(String appMaitenPjVal) {
        this.appMaitenPjVal = appMaitenPjVal;
    }

    public String getEleSimulatorPj() {
        return eleSimulatorPj;
    }

    public void setEleSimulatorPj(String eleSimulatorPj) {
        this.eleSimulatorPj = eleSimulatorPj;
    }

    public String getEleSimulatorPjVal() {
        return eleSimulatorPjVal;
    }

    public void setEleSimulatorPjVal(String eleSimulatorPjVal) {
        this.eleSimulatorPjVal = eleSimulatorPjVal;
    }

    public String getElePowerPj() {
        return elePowerPj;
    }

    public void setElePowerPj(String elePowerPj) {
        this.elePowerPj = elePowerPj;
    }

    public String getElePowerPjVal() {
        return elePowerPjVal;
    }

    public void setElePowerPjVal(String elePowerPjVal) {
        this.elePowerPjVal = elePowerPjVal;
    }

    public String getElePowerPjPerval() {
        return elePowerPjPerval;
    }

    public void setElePowerPjPerval(String elePowerPjPerval) {
        this.elePowerPjPerval = elePowerPjPerval;
    }

    public String getElePowerPjResult() {
        return elePowerPjResult;
    }

    public void setElePowerPjResult(String elePowerPjResult) {
        this.elePowerPjResult = elePowerPjResult;
    }

    public String getFunSimulatorPj() {
        return funSimulatorPj;
    }

    public void setFunSimulatorPj(String funSimulatorPj) {
        this.funSimulatorPj = funSimulatorPj;
    }

    public String getFunSimulatorPjVal() {
        return funSimulatorPjVal;
    }

    public void setFunSimulatorPjVal(String funSimulatorPjVal) {
        this.funSimulatorPjVal = funSimulatorPjVal;
    }

    public String getFunOtherPrj() {
        return funOtherPrj;
    }

    public void setFunOtherPrj(String funOtherPrj) {
        this.funOtherPrj = funOtherPrj;
    }

    public String getFunOtherPrjVal() {
        return funOtherPrjVal;
    }

    public void setFunOtherPrjVal(String funOtherPrjVal) {
        this.funOtherPrjVal = funOtherPrjVal;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public SelectBean() {
    }

    public SelectBean(String prjType, String prjNo) {
        this.prjType = prjType;
        this.prjNo = prjNo;
    }

    public SelectBean(String prjType, String appStatusPj, String appStatusPjVal) {
        this.prjType = prjType;
        this.appStatusPj = appStatusPj;
        this.appStatusPjVal = appStatusPjVal;
    }

    public String getPrjNo() {
        return prjNo;
    }

    public void setPrjNo(String prjNo) {
        this.prjNo = prjNo;
    }

    public String getPrjType() {
        return prjType;
    }

    public void setPrjType(String prjType) {
        this.prjType = prjType;
    }

    public String getAppStatusPj() {
        return appStatusPj;
    }

    public void setAppStatusPj(String appStatusPj) {
        this.appStatusPj = appStatusPj;
    }

    public String getAppStatusPjVal() {
        return appStatusPjVal;
    }

    public void setAppStatusPjVal(String appStatusPjVal) {
        this.appStatusPjVal = appStatusPjVal;
    }

//    private final String[] strs = {"合格", "不合格", "不适用"};
//    private final String[] strs2 = {"B", "BF", "CF"};
//
//    public void setAppStatusPjValByPosition(int position) {
//        this.appStatusPjVal = strs[position];
//    }
//    public void setFunOtherPrjValByPosition(int position) {
//        this.funOtherPrjVal = strs[position];
//    }
//    public void setAppMaitenPjValByPosition(int position) {
//        this.appMaitenPjVal = strs[position];
//    }
//    public void setEleTypePjValByPosition(int position) {
//        this.eleCheckTypeVal = strs2[position];
//    }
//    public String getEleTypePjValByPosition(int position) {
//        return strs2[position];
//    }

    public int getSelectPosition(String appStatusPjVal) {
        switch (appStatusPjVal) {
            case "合格":
                return 0;
            case "不合格":
                return 1;
            case "不适用":
                return 2;
        }
        return 0;
    }
    public int getSelectPositionType(String type) {
        switch (type) {
            case "B":
                return 0;
            case "BF":
                return 1;
            case "CF":
                return 2;
        }
        return 0;
    }

    public String getFunCheckPrj() {
        return funCheckPrj;
    }

    public void setFunCheckPrj(String funCheckPrj) {
        this.funCheckPrj = funCheckPrj;
    }

    public String getFunCheckPrjVal() {
        return funCheckPrjVal;
    }

    public void setFunCheckPrjVal(String funCheckPrjVal) {
        this.funCheckPrjVal = funCheckPrjVal;
    }

    public String getFunCheckPrjPerval() {
        return funCheckPrjPerval;
    }

    public void setFunCheckPrjPerval(String funCheckPrjPerval) {
        this.funCheckPrjPerval = funCheckPrjPerval;
    }

    public String getFunCheckPrjErrval() {
        return funCheckPrjErrval;
    }

    public void setFunCheckPrjErrval(String funCheckPrjErrval) {
        this.funCheckPrjErrval = funCheckPrjErrval;
    }

    public String getFunCheckPrjResult() {
        return funCheckPrjResult;
    }

    public void setFunCheckPrjResult(String funCheckPrjResult) {
        this.funCheckPrjResult = funCheckPrjResult;
    }

    public String getFunAppPj() {
        return funAppPj;
    }

    public void setFunAppPj(String funAppPj) {
        this.funAppPj = funAppPj;
    }

    public String getFunAppPjVal() {
        return funAppPjVal;
    }

    public void setFunAppPjVal(String funAppPjVal) {
        this.funAppPjVal = funAppPjVal;
    }

    public String getFunAppPjRem() {
        return funAppPjRem;
    }

    public void setFunAppPjRem(String funAppPjRem) {
        this.funAppPjRem = funAppPjRem;
    }

    @Override
    public String toString() {
        return "SelectBean{" +
                "appStatusPj='" + appStatusPj + '\'' +
                ", appStatusPjVal='" + appStatusPjVal + '\'' +
                '}';
    }
}
