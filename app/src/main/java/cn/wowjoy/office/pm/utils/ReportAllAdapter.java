package cn.wowjoy.office.pm.utils;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.databinding.ItemPmReportAllRvKeyBinding;
import cn.wowjoy.office.pm.db.PmReportListEntity;
import cn.wowjoy.office.pm.view.viewodel.PmReportViewModel;

/**
 * Created by Administrator on 2018/4/12.
 */

public class ReportAllAdapter extends RecyclerView.Adapter<ReportAllAdapter.ItemViewHolder>{

    private Context context;
    private LayoutInflater inflater;
    private Object mEventListener;

    private List<PmReportListEntity> netWorkDatas;

    public void setDates(List<PmReportListEntity> models) {
        this.netWorkDatas = models;
        notifyDataSetChanged();
    }
    public void addMoreDate(List<PmReportListEntity> searchResponse){
        if(null != this.netWorkDatas){
            this.netWorkDatas.addAll(searchResponse);
            notifyDataSetChanged();
        }

    }

    public void setEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    public ReportAllAdapter() {
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_pm_report_all_rv,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Log.e("PXY", "select  position: "+position+ "  getAdapterPosition :"+holder.getAdapterPosition());
   //     holder.setIsRecyclable(false);
        holder.bind(mEventListener,netWorkDatas.get(position),position);
        PmReportListEntity response = netWorkDatas.get(position);
        holder.binding.cbSelect.setChecked(response.isSelect());
        holder.binding.llRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PXY", "setOnClickListener: ");
                if(!response.isShow()){
                    ((PmReportViewModel) mEventListener).clickItem(response);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == netWorkDatas ? 0 : netWorkDatas.size();
    }
      class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemPmReportAllRvKeyBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemPmReportAllRvKeyBinding) binding;
        }

        public void bind(Object mEventHandler,PmReportListEntity response,int position){
            binding.setItemEventHandler((PmReportViewModel) mEventHandler);
            binding.setModel(response);
        }
    }
}
