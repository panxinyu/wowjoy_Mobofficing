package cn.wowjoy.office.pm.view.viewodel;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

public class FunctionCheckViewModel extends BaseEditViewModel {
    @Inject
    public FunctionCheckViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
}
