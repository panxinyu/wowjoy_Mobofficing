package cn.wowjoy.office.pm.data.request;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PmReportDetailSubmit  implements Serializable{
    private String  id;
    private String  cardId;
    private String  pmDate;
    private String  pmUserId;
    private String  checkResult;
    private String  remarks;
    private String  createDatetime;
    private String  createUserId;
    private String qrCodeOld; //  卡片的二维码信息   旧的
    private String qrCodeNew; //  卡片的二维码信息   替换的
    private List<PmReportDtl> pmReportDtlList = new ArrayList<>();
    public void addPmReportDtl(PmReportDtl dtl){
        if(null != dtl){
            Iterator<PmReportDtl> iterator =pmReportDtlList.iterator();
            while (iterator.hasNext()){
                PmReportDtl p = iterator.next();
                if (p.getCheckProjectName().equals(dtl.getCheckProjectName())){
                    iterator.remove();
                }
            }
            pmReportDtlList.add(dtl);
        }
    }
    public void removePmReportDtl(PmReportDtl dtl){
        if(null != dtl){
            Iterator<PmReportDtl> iterator =pmReportDtlList.iterator();
            while (iterator.hasNext()){
                PmReportDtl p = iterator.next();
                if (p.getCheckProjectName().equals(dtl.getCheckProjectName())){
                    iterator.remove();
                }
            }
        }
        PmReportDtl p = new PmReportDtl();
        if(null != dtl && !TextUtils.isEmpty(dtl.getId())){
            p.setId(dtl.getId());
        }
        p.setCheckModel("无");
        p.setCheckProjectName("性能检测");
        p.setCheckProject("85-3");
        pmReportDtlList.add(p);
    }
    public String getQrCodeOld() {
        return qrCodeOld;
    }

    public void setQrCodeOld(String qrCodeOld) {
        this.qrCodeOld = qrCodeOld;
    }

    public String getQrCodeNew() {
        return qrCodeNew;
    }

    public void setQrCodeNew(String qrCodeNew) {
        this.qrCodeNew = qrCodeNew;
    }

    public void setCheckModule(String checkProjectName, String checkModule){
        for (PmReportDtl p : pmReportDtlList) {
            if (p.getCheckProjectName().equals(checkProjectName)) {
                p.setCheckModel(checkModule);
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPmDate() {
        return pmDate;
    }

    public void setPmDate(String pmDate) {
        this.pmDate = pmDate;
    }

    public String getPmUserId() {
        return pmUserId;
    }

    public void setPmUserId(String pmUserId) {
        this.pmUserId = pmUserId;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public List<PmReportDtl> getPmReportDtlList() {
        return pmReportDtlList;
    }

    public void setPmReportDtlList(List<PmReportDtl> pmReportDtlList) {
        this.pmReportDtlList = pmReportDtlList;
    }


}
