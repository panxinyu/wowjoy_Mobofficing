package cn.wowjoy.office.applystock.receipts;

import android.arch.lifecycle.MediatorLiveData;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.alibaba.android.arouter.launcher.ARouter;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.ReceiptsDetailAdapter;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.ApplyHeadInfoResponse;
import cn.wowjoy.office.data.response.DetailVoInfo;
import cn.wowjoy.office.data.response.QRVoInfo;
import cn.wowjoy.office.data.response.StockStorageResponse;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ReceiptsDetailViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;
    private String headCode;
    private int type;
    private boolean isNeedCommit = false;


    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Inject
    public ReceiptsDetailViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setHandle(this);

    }

    ReceiptsDetailAdapter adapter = new ReceiptsDetailAdapter();
    LRecyclerViewAdapter lAdapter = new LRecyclerViewAdapter(adapter);
    MediatorLiveData<LiveDataWrapper<ApplyHeadInfoResponse>> response = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<Object>> create = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<StockStorageResponse>> permission = new MediatorLiveData<>();
    MediatorLiveData<LiveDataWrapper<QRVoInfo>> scanData = new MediatorLiveData<>();
    @Override
    public void onCreateViewModel() {

}

    public void setData(List<DetailVoInfo> data) {
        adapter.setDatas(data);
        lAdapter.removeFooterView();
        lAdapter.removeHeaderView();
        lAdapter.notifyDataSetChanged();
    }

    public void getDetail(){
        switch (type){
            case Constants.RECEIPTS_CODE_APPLY://申领
                getApplyHead();
                break;
            case Constants.RECEIPTS_CODE_APPLY_FOR://申请
                break;
            case Constants.RECEIPTS_CODE_ALLOT://调拨
                getAllocationHeadByHeadCodePda();

                break;
        }
    }

    public void commit(ApplyHeadInfoResponse request){
        switch (type){
            case Constants.RECEIPTS_CODE_APPLY:
                addOutStorageByApplyNew(request);
                break;
            case Constants.RECEIPTS_CODE_APPLY_FOR:
                break;
            case Constants.RECEIPTS_CODE_ALLOT:
                addOutStorageByAllocationQR(request);

                break;
        }
    }

    public void queryByScan(String qcCode) {
        scanData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getQRVoInfo(qcCode)
                    .flatMap(new ResultDataParse<QRVoInfo>())
                .compose(new RxSchedulerTransformer<QRVoInfo>())
                .subscribe(new Consumer<QRVoInfo>() {
                    @Override
                    public void accept(QRVoInfo qrVoInfo) throws Exception {
                        //获取的数据发送给Activity来使用
                        scanData.setValue(LiveDataWrapper.success(qrVoInfo));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        scanData.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }
    public void getApplyHead(){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getApplyHead(headCode)
                .flatMap(new ResultDataParse<ApplyHeadInfoResponse>())
                .compose(new RxSchedulerTransformer<ApplyHeadInfoResponse>())
                .subscribe(new Consumer<ApplyHeadInfoResponse>() {
                               @Override
                               public void accept(ApplyHeadInfoResponse applyHeadDetailListResponse) throws Exception {
                                   setData(applyHeadDetailListResponse.getDetailVoInfos());
                                   response.setValue(LiveDataWrapper.success(applyHeadDetailListResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                response.setValue(LiveDataWrapper.error(throwable,null));

                            }
                        });
        addDisposable(disposable);
    }

    public void getAllocationHeadByHeadCodePda(){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable =  apiService.getAllocationHeadByHeadCodePda(headCode,null,null)
                .flatMap(new ResultDataParse<ApplyHeadInfoResponse>())
                .compose(new RxSchedulerTransformer<ApplyHeadInfoResponse>())
                .subscribe(new Consumer<ApplyHeadInfoResponse>() {
                               @Override
                               public void accept(ApplyHeadInfoResponse applyHeadDetailListResponse) throws Exception {
                                   setData(applyHeadDetailListResponse.getAllocationDetailList());
                                   response.setValue(LiveDataWrapper.success(applyHeadDetailListResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                response.setValue(LiveDataWrapper.error(throwable,null));

                            }
                        });
        addDisposable(disposable);
    }

    public void addOutStorageByApplyNew(ApplyHeadInfoResponse request){
        create.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.addOutStorageByApplyNew(request)
                .flatMap(new ResultDataParse<Object>())
                .compose(new RxSchedulerTransformer<Object >())
                .subscribe(new Consumer<Object>() {
                               @Override
                               public void accept(Object  emptyResult) throws Exception {
                                   create.setValue(LiveDataWrapper.success(emptyResult));
                                   ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                create.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }

    public void addOutStorageByAllocationQR(ApplyHeadInfoResponse request){
        create.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.addOutStorageByAllocationQR(request)
                .flatMap(new ResultDataParse<ApplyHeadInfoResponse>())
                .compose(new RxSchedulerTransformer<ApplyHeadInfoResponse >())
                .subscribe(new Consumer<ApplyHeadInfoResponse>() {
                               @Override
                               public void accept(ApplyHeadInfoResponse  emptyResult) throws Exception {
                                   create.setValue(LiveDataWrapper.success(emptyResult));
                                   ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                create.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }

    public void isExamineAndConfirmOut(){
        permission.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.isExamineAndConfirmOut()
                .flatMap(new ResultDataParse<StockStorageResponse>())
                .compose(new RxSchedulerTransformer<StockStorageResponse>())
                .subscribe(new Consumer<StockStorageResponse>() {
                               @Override
                               public void accept(StockStorageResponse stockStorageResponse) throws Exception {
                                   permission.setValue(LiveDataWrapper.success(stockStorageResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                permission.setValue(LiveDataWrapper.error(throwable,null));

                            }
                        });
    }

    public void jumpDetail(DetailVoInfo detailVoInfo){
        if(detailVoInfo.isNeedQR()){
            Bundle bundle = new Bundle();
            bundle.putSerializable("detailVoInfo",detailVoInfo);
            bundle.putInt("type",type);
            ARouter.getInstance().build("/applystock/detail/receiptsDetailItem")
                    .withBundle("detailVoInfo",bundle)
                    .navigation();
        }else{
            Bundle bundle = new Bundle();
            bundle.putSerializable("detailVoInfo",detailVoInfo);
            ARouter.getInstance().build("/applystock/detail/applymaterialdetail")
                    .withBundle("detailVoInfo",bundle)
                    .navigation();
        }

    }
}
