package cn.wowjoy.office.applystock.detail;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.launcher.ARouter;
import com.facebook.stetho.inspector.jsonrpc.protocol.EmptyResult;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.ApplyHeadInfoResponse;
import cn.wowjoy.office.data.response.DetailVoInfo;
import cn.wowjoy.office.databinding.ApplyHeadDetailItemViewBinding;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ApplyHeadDetailViewModel extends NewBaseViewModel {

    @Inject
    ApiService apiService;

    private String headCode;

    private int type;
    private boolean isSwipe = false;


    public void setType(int type) {
        this.type = type;
    }


    BGABindingRecyclerViewAdapter<DetailVoInfo,ApplyHeadDetailItemViewBinding> innerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.apply_head_detail_item_view);
    LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(innerAdapter);

    MediatorLiveData<LiveDataWrapper<ApplyHeadInfoResponse>> response = new MediatorLiveData<>();
    MediatorLiveData<ApplyHeadInfoResponse> jump = new MediatorLiveData<>();


    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }


    public void setData(List<DetailVoInfo> data) {
        innerAdapter.setData(data);
        adapter.removeFooterView();
        adapter.removeHeaderView();
        adapter.notifyDataSetChanged();
    }


    @Inject
    public ApplyHeadDetailViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
    public void getDetail(){
        switch (type){
            case Constants.RECEIPTS_CODE_APPLY://申领
                getApplyHead();
                break;
            case Constants.RECEIPTS_CODE_APPLY_FOR://申请
                break;
            case Constants.RECEIPTS_CODE_ALLOT://调拨
                getAllocationHeadByHeadCodePda();

                break;
        }
    }

    public void getApplyHead(){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getApplyHead(headCode)
                .flatMap(new ResultDataParse<ApplyHeadInfoResponse>())
                .compose(new RxSchedulerTransformer<ApplyHeadInfoResponse>())
                .subscribe(new Consumer<ApplyHeadInfoResponse>() {
                               @Override
                               public void accept(ApplyHeadInfoResponse applyHeadDetailListResponse) throws Exception {
                                  setData(applyHeadDetailListResponse.getDetailVoInfos());
                                  response.setValue(LiveDataWrapper.success(applyHeadDetailListResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                response.setValue(LiveDataWrapper.error(throwable,null));

                            }
                        });
        addDisposable(disposable);
    }

    public void getAllocationHeadByHeadCodePda(){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable =  apiService.getAllocationHeadByHeadCodePda(headCode,null,null)
                .flatMap(new ResultDataParse<ApplyHeadInfoResponse>())
                .compose(new RxSchedulerTransformer<ApplyHeadInfoResponse>())
                .subscribe(new Consumer<ApplyHeadInfoResponse>() {
                               @Override
                               public void accept(ApplyHeadInfoResponse applyHeadDetailListResponse) throws Exception {
                                   setData(applyHeadDetailListResponse.getAllocationDetailList());
                                   response.setValue(LiveDataWrapper.success(applyHeadDetailListResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                response.setValue(LiveDataWrapper.error(throwable,null));

                            }
                        });
        addDisposable(disposable);
    }

    public void jumpDetail(ApplyHeadInfoResponse response){
        jump.setValue(response);

    }






}
