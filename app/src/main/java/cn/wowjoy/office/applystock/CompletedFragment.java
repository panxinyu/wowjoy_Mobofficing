package cn.wowjoy.office.applystock;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.ViewGroup;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.ApplyHeadListResponse;
import cn.wowjoy.office.databinding.FragmentCompletedBinding;

/**
 * A simple {@link NewBaseFragment} subclass.
 * Use the {@link CompletedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompletedFragment extends NewBaseFragment<FragmentCompletedBinding, ApplyStockViewModel> {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String STATUS = "status";
    private static final String STORAGECODE = "storagecode";
    private static final String TYPE = "type";
    private boolean isFirstGet = false;

    // TODO: Rename and change types of parameters
    private String status;
    private String storageCode;
    private int type;

    public CompletedFragment() {
        // Required empty public constructor
    }
    private MDialog waitDialog;

    private void clearDialog(){
        if (waitDialog != null ){
            waitDialog.dismiss();
            waitDialog = null;
        }
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param status Parameter 1.
     * @param type Parameter 2.
     * @return A new instance of fragment CompletedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CompletedFragment newInstance(String status, int type) {
        CompletedFragment fragment = new CompletedFragment();
        Bundle args = new Bundle();
        args.putString(STATUS, status);
        args.putInt(TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }
    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;

        viewModel.setStroageCode(storageCode);
    }
    @Override
    protected Class<ApplyStockViewModel> getViewModel() {
        return ApplyStockViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_completed;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            status = getArguments().getString(STATUS);
            type = getArguments().getInt(TYPE);
        }
        viewModel.setStatus(status);
        viewModel.setType(type);
        viewModel.setIsEnd("y");
        viewModel.setCanCreate(false);

    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        if ( !TextUtils.isEmpty(storageCode))
            viewModel.getList(true);

    }

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        initView();
        initObserve();
    }

    public void initView(){
        binding.emptyView.emptyContent.setText("没有已出库的申领单");
        binding.rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rv.setAdapter(viewModel.lAdapter);
        binding.rv.addItemDecoration(new SimpleItemDecoration(getActivity(),SimpleItemDecoration.VERTICAL_LIST));
        binding.rv.setEmptyView(binding.emptyView.getRoot());
        binding.rv.setLoadMoreEnabled(true);
    }

    public void initObserve(){
        viewModel.reponses.observe(this, new Observer<LiveDataWrapper<ApplyHeadListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<ApplyHeadListResponse> applyHeadListResponseLiveDataWrapper) {
                switch (applyHeadListResponseLiveDataWrapper.status){
                    case LOADING:
                        clearDialog();
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        clearDialog();
                        break;
                    case ERROR:
                        clearDialog();
                        handleException(applyHeadListResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });


    }

    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        if ( !TextUtils.isEmpty(storageCode)){
            isFirstGet = true;
            viewModel.getList(true);

        }
    }
}
