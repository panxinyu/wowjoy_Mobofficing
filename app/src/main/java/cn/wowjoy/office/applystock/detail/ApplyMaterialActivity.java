package cn.wowjoy.office.applystock.detail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.data.response.DetailVoInfo;
import cn.wowjoy.office.databinding.ActivityApplyMaterialBinding;

@Route(path = "/applystock/detail/applymaterialdetail")
public class ApplyMaterialActivity extends NewBaseActivity<ActivityApplyMaterialBinding,ApplyHeadDetailViewModel> {


    @Autowired
    Bundle detailVoInfo;



    @Override
    protected Class<ApplyHeadDetailViewModel> getViewModel() {
        return ApplyHeadDetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_apply_material;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.setModel((DetailVoInfo) detailVoInfo.getSerializable("detailVoInfo"));

    }


}
