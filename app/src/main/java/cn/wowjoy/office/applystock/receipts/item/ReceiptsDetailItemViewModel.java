package cn.wowjoy.office.applystock.receipts.item;

import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.response.QRVoInfo;
import cn.wowjoy.office.databinding.ItemLrReceiptsProduceDetailBinding;

/**
 * Created by Administrator on 2018/5/15.
 */

public class ReceiptsDetailItemViewModel extends NewBaseViewModel{
    public BGABindingRecyclerViewAdapter<QRVoInfo, ItemLrReceiptsProduceDetailBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_lr_receipts_produce_detail);
    public LRecyclerViewAdapter lrAdapter = new LRecyclerViewAdapter(wlinnerAdapter);

    @Inject
    public ReceiptsDetailItemViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
    public ArrayList<QRVoInfo> datas;
    public void setWData(List<QRVoInfo> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        wlinnerAdapter.setData(datas);
        lrAdapter.removeFooterView();
        lrAdapter.removeHeaderView();
        lrAdapter.notifyDataSetChanged();
    }
    @Override
    public void onCreateViewModel() {

    }
}
