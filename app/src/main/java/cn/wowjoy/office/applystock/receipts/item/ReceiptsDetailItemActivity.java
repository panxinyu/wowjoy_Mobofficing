package cn.wowjoy.office.applystock.receipts.item;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.github.jdsjlzx.interfaces.OnItemClickListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.DetailVoInfo;
import cn.wowjoy.office.databinding.ActivityReceiptsDetailItemBinding;
import cn.wowjoy.office.materialinspection.applyfor.produce.qrdetail.StockQrDetailActivity;

@Route(path = "/applystock/detail/receiptsDetailItem")
public class ReceiptsDetailItemActivity extends NewBaseActivity<ActivityReceiptsDetailItemBinding,ReceiptsDetailItemViewModel> {
    @Autowired
    Bundle detailVoInfo;

    private DetailVoInfo response;
    private int type;

    @Override
    protected Class<ReceiptsDetailItemViewModel> getViewModel() {
        return ReceiptsDetailItemViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_receipts_detail_item;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        ARouter.getInstance().inject(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.lrAdapter);

        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.emptyView.emptyContent.setText("空空的列表");

        viewModel.lrAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String qrCode = "";
                if(type == Constants.RECEIPTS_CODE_APPLY){
                     qrCode = response.getQrvolist().get(position).getQrCode();
                }else if(type == Constants.RECEIPTS_CODE_ALLOT){
                     qrCode = response.getqRList().get(position).getQrCode();
                }
                Intent intent = new Intent(ReceiptsDetailItemActivity.this, StockQrDetailActivity.class);
                intent.putExtra("qrCode",qrCode);
                startActivity(intent);

            }
        });


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        response = (DetailVoInfo) detailVoInfo.getSerializable("detailVoInfo");
        type = detailVoInfo.getInt("type");

        if("n".equalsIgnoreCase(response.getIsQR())){
            binding.rlKufang.setVisibility(View.GONE);
            binding.content.setVisibility(View.GONE);
        }else{
            binding.rlKufang.setVisibility(View.VISIBLE);
            binding.content.setVisibility(View.VISIBLE);
        }
        binding.setModel(response);
        if(type == Constants.RECEIPTS_CODE_APPLY){
            viewModel.setWData(response.getQrvolist());
            binding.tvTypeOutdetail.setText(response.getQrvolist().size()+"");
        }else if(type == Constants.RECEIPTS_CODE_ALLOT){
            viewModel.setWData(response.getqRList());
            binding.tvTypeOutdetail.setText(response.getqRList().size()+"");
        }


    }
}
