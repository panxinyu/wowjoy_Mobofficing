package cn.wowjoy.office.applystock.detail;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.facebook.stetho.inspector.jsonrpc.protocol.EmptyResult;

import cn.wowjoy.office.R;
import cn.wowjoy.office.applystock.ApplyStockActivity;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.remote.ResultException;
import cn.wowjoy.office.data.response.ApplyHeadInfoResponse;
import cn.wowjoy.office.databinding.ActivityApplyHeadDetailBinding;
import cn.wowjoy.office.materialinspection.check.detail.add.CategoryActivity;

@Route(path = "/applystock/detail/applyheaddetail")
public class ApplyHeadDetailActivity extends NewBaseActivity<ActivityApplyHeadDetailBinding,ApplyHeadDetailViewModel> implements View.OnClickListener {

    @Autowired
    int type;
    @Autowired
    String headCode;
    @Autowired
    boolean isCanCreate;
    private MDialog waitDialog;
    private void clearDialog(){
        if (waitDialog != null ){
            waitDialog.dismiss();
            waitDialog = null;
        }
    }

    private boolean isSwipe = false;

    @Override
    protected Class<ApplyHeadDetailViewModel> getViewModel() {
        return ApplyHeadDetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_apply_head_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ARouter.getInstance().inject(this);
        binding.setViewModel(viewModel);
        viewModel.setHeadCode(headCode);
        viewModel.setType(type);
        initView();
        initObsever();
        viewModel.getDetail();

    }

    private void initView(){
        if (isCanCreate)
            binding.create.setVisibility(View.VISIBLE);
        else
            binding.create.setVisibility(View.GONE);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(viewModel.adapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this,SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.back.setOnClickListener(this);

    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.content;

    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getDetail();
    }

    private void initObsever(){
        viewModel.response.observe(this, new Observer<LiveDataWrapper<ApplyHeadInfoResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<ApplyHeadInfoResponse> applyHeadInfoResponseLiveDataWrapper) {
                switch (applyHeadInfoResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(ApplyHeadDetailActivity.this);
                        break;
                    case SUCCESS:
                        clearDialog();
                        ApplyHeadInfoResponse applyHeadInfoResponse = applyHeadInfoResponseLiveDataWrapper.data;
                        switch (type){
                            case Constants.RECEIPTS_CODE_APPLY://申领
                               applyHeadInfoResponse.setIaAllot(false);

                                break;
                            case Constants.RECEIPTS_CODE_APPLY_FOR://申请
                                break;
                            case Constants.RECEIPTS_CODE_ALLOT://调拨
                                applyHeadInfoResponse.setIaAllot(true);
                                break;
                        }
                        applyHeadInfoResponse.setDone(!isCanCreate);
                        binding.setModel(applyHeadInfoResponse);
                        break;
                    case ERROR:
                        clearDialog();

                        handleException(applyHeadInfoResponseLiveDataWrapper.error,true);

                        break;
                }
            }
        });
        viewModel.jump.observe(this, new Observer<ApplyHeadInfoResponse>() {
            @Override
            public void onChanged(@Nullable ApplyHeadInfoResponse applyHeadInfoResponse) {
                if (null != applyHeadInfoResponse){
                    initIsSWipe();
                    //跳转逻辑
                    ARouter.getInstance().build("/applystock/receipts/receiptsdetail")
                            .withString("headCode",applyHeadInfoResponse.getHeadCode())
                            .withInt("type", type)
                            .withBoolean("isSwipe",isSwipe)
                            .navigation(ApplyHeadDetailActivity.this,1001);
                } else {
                    Toast.makeText(getApplication().getApplicationContext(), "当前没有可生成出库单的清单！", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1001:
                if (null != data && data.getBooleanExtra(Constants.IS_NEED_REFRESH,false)){
                    binding.create.setVisibility(View.GONE);
                    //                    viewModel.getDetail();
                    ApplyHeadDetailActivity.this.finish();

                }

                break;
        }
    }

    private void initIsSWipe(){
        switch (type){
            case Constants.RECEIPTS_CODE_APPLY://申领
                isSwipe = false;
                break;
            case Constants.RECEIPTS_CODE_APPLY_FOR://申请
                isSwipe = true;
                break;
            case Constants.RECEIPTS_CODE_ALLOT://调拨
                isSwipe = false;
                break;
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back:
                onBackPressed();
                break;
        }
    }
}
