package cn.wowjoy.office.materialinspection.check.detail;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.CheckTaskUpdateListRequest;
import cn.wowjoy.office.data.request.SubmitCheckTaskListDetailRequest;
import cn.wowjoy.office.data.response.CheckBroadResultResponse;
import cn.wowjoy.office.data.response.InventoryTaskListDtlResponse;
import cn.wowjoy.office.data.response.SubmitCheckTaskListDetailResponse;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/1/5.
 */

public class CheckDetailViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;

    @Override
    public void onCreateViewModel() {

    }
    @Inject
    public CheckDetailViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
    private MediatorLiveData<LiveDataWrapper<InventoryTaskListDtlResponse>> response = new MediatorLiveData<>();
    private  MediatorLiveData<LiveDataWrapper<CheckBroadResultResponse>> broadResult = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<CheckBroadResultResponse>> getBroadResult() {
        return broadResult;
    }
    public MediatorLiveData<LiveDataWrapper<InventoryTaskListDtlResponse>> getResponse() {
        return response;
    }

    public void getDetail(String taskId,String taskDtlId) {
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCheckTaskDtlByPda(taskId,taskDtlId)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((InventoryTaskListDtlResponse dtlResponse) -> {
                    response.setValue(LiveDataWrapper.success(dtlResponse));
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        response.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
    public MediatorLiveData<LiveDataWrapper<Boolean>> submitDetail(SubmitCheckTaskListDetailRequest request ,boolean isShowToast, String tostText){
        final MediatorLiveData<LiveDataWrapper<Boolean>> submit = new MediatorLiveData<>();
        submit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.saveCheckTaskDtl(request)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((SubmitCheckTaskListDetailResponse submitPDAResponse) -> {
                    //TODO: 设置界面上的详细数据 着重区分下！！！ 已完成和未完成显示的页面区别
                    if(isShowToast){
                        ToastUtil.toastImage(getApplication().getApplicationContext(),tostText,-1);
                    }
                    submit.setValue(LiveDataWrapper.success(true));
                }, (Throwable throwable) -> {
                    submit.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
        return submit;
    }
    public void updateByBroad(String qrCode, String taskId ){

        broadResult.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.updateListByBroadCast(new CheckTaskUpdateListRequest(qrCode,taskId,"1","1"))
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((CheckBroadResultResponse submitPDAResponse) -> {
//                    ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                    broadResult.setValue(LiveDataWrapper.success(submitPDAResponse));
                }, (Throwable throwable) ->{
                    broadResult.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
    }
}
