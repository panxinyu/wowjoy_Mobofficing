package cn.wowjoy.office.materialinspection.xunjian.manage.search;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.InspectionTotalItemInfo;
import cn.wowjoy.office.data.response.InspectionTotalResponse;
import cn.wowjoy.office.databinding.ItemRvDonetaskSearchBinding;
import cn.wowjoy.office.homepage.MenuHelper;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2017/11/15.
 */

public class DoneTaskSearchViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;

   private MediatorLiveData<LiveDataWrapper<InspectionTotalResponse>> searchData = new MediatorLiveData<>();

   @Inject
    public DoneTaskSearchViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    public MediatorLiveData<LiveDataWrapper<InspectionTotalResponse>> getSearchData() {
        return searchData;
    }

    public ArrayList<InspectionTotalItemInfo> datas;
    public BGABindingRecyclerViewAdapter<InspectionTotalItemInfo, ItemRvDonetaskSearchBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_donetask_search);
    public LRecyclerViewAdapter searchAdapter = new LRecyclerViewAdapter(wlinnerAdapter);

    @Override
    public void onCreateViewModel() {

    }

    public void setWData(List<InspectionTotalItemInfo> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        wlinnerAdapter.setData(datas);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }
    public void clearData(){
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        wlinnerAdapter.setData(datas);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }
    //    未完成   taskStatus = 91-2,91-3   并且  inspect_record_bill_status ! =92-1,92-2(可能为空)
//    已完成   taskStatus = 91-3，91-4  并且  inspect_record_bill_status ! =92-0
    public void judgeDone(List<InspectionTotalItemInfo>  itemInfos){
        for(InspectionTotalItemInfo itemInfo : itemInfos ){
                if("91-3".equals(itemInfo.getTaskStatus()) || "91-4".equals(itemInfo.getTaskStatus())){
                    if(null != itemInfo.getInspectRecordBillStatus() && !"92-0".equals(itemInfo.getInspectRecordBillStatus()) ){
                    itemInfo.setDone(true);
                }
            }
        }
    }

    //获取未完成任务的List
    public void getSearchListInfo(String billinfo){
        searchData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getTaskListVagueInfo(billinfo, MenuHelper.getAuidS().get("zc_inspect"))
                .flatMap(new ResultDataParse<InspectionTotalResponse>())
                .compose(new RxSchedulerTransformer<InspectionTotalResponse>())
                .subscribe(new Consumer<InspectionTotalResponse>() {
                    @Override
                    public void accept(InspectionTotalResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        searchData.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        searchData.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }

}
