package cn.wowjoy.office.materialinspection.check.manage.search;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InventoryTaskListResponse;
import cn.wowjoy.office.databinding.ActivityCheckSearchBinding;
import cn.wowjoy.office.materialinspection.check.task.CheckTaskListActivity;

public class CheckSearchActivity extends NewBaseActivity<ActivityCheckSearchBinding,CheckSearchViewModel> {
    private MDialog waitDialog;

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        setSupportActionBar(binding.toolbar);
        addKeyboard();
        initView();
        initListener();
        initObserve();
    }

    private void initObserve() {
        viewModel.getSearchData().observe(this, new Observer<LiveDataWrapper<InventoryTaskListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InventoryTaskListResponse> InventoryTaskListResponseLiveDataWrapper) {
                switch (InventoryTaskListResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(CheckSearchActivity.this);
                        break;
                    case SUCCESS:
                        waitDialog.dismiss();
                        if (null != InventoryTaskListResponseLiveDataWrapper.data) {
//                            viewModel.judgeDone(InventoryTaskListResponseLiveDataWrapper.data.getResultList());
                            viewModel.setWData(InventoryTaskListResponseLiveDataWrapper.data.getInventoryTotalItemInfoLists());
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(CheckSearchActivity.this, waitDialog);
                        }
                        handleException(InventoryTaskListResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });
    }

    private void initView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.searchAdapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setLoadMoreEnabled(false);
    }

    private void initListener() {
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();
                String key = binding.searchEditText.getText().toString().trim();
                // TODO：联网模糊查询
                if (!key.isEmpty()) {
                    //  搜索清空的时候
                    viewModel.getSearchListInfo(key.trim());
                    return true;
                }
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO：联网模糊查询
                if (s.toString().isEmpty()) {
                    //  搜索清空的时候
                    hideSoftInput();
                    viewModel.clearData();
                }
            }
        });
        viewModel.searchAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //TODO: 已完成与未完成的跳转
                if (null != viewModel.datas) {
                    Intent mIntent = new Intent(CheckSearchActivity.this, CheckTaskListActivity.class);
                    mIntent.putExtra("taskId", viewModel.datas.get(position).getId());
                    mIntent.putExtra("isDone", viewModel.datas.get(position).isDone());
                    mIntent.putExtra("billNo", viewModel.datas.get(position).getBillNo());
                    startActivity(mIntent);
                    finish();
                }
            }
        });
    }

    /**
     * 获取焦点，调起软键盘
     */
    private void addKeyboard() {
        binding.searchEditText.setFocusable(true);
        binding.searchEditText.setFocusableInTouchMode(true);
        binding.searchEditText.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected Class<CheckSearchViewModel> getViewModel() {
        return CheckSearchViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_search;
    }
}
