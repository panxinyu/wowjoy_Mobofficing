package cn.wowjoy.office.materialinspection.xunjian.task;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.remote.ResultException;
import cn.wowjoy.office.data.response.SingleTaskController;
import cn.wowjoy.office.databinding.ActivityEditAdviceBinding;
import cn.wowjoy.office.pm.view.PMBaseActivity;
import cn.wowjoy.office.utils.MyTextWatcher;
import cn.wowjoy.office.utils.ToastUtil;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class EditAdviceActivity extends PMBaseActivity<ActivityEditAdviceBinding, EditAdviceViewModel> implements View.OnClickListener {

    public static void lanuch(boolean isDone, SingleTaskController single, Activity activity) {
        Intent intent = new Intent(activity, EditAdviceActivity.class);
        intent.putExtra("single", single);
        intent.putExtra("isDone", isDone);
        activity.startActivityForResult(intent, Constants.EIDT_CODE);
    }

    private SingleTaskController single;
    private boolean isDone = false;

    @Override
    protected Class<EditAdviceViewModel> getViewModel() {
        return EditAdviceViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_advice;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        single = (SingleTaskController) getIntent().getSerializableExtra("single");
        isDone = getIntent().getBooleanExtra("isDone", false);

        binding.include.titleBackLl.setVisibility(View.VISIBLE);
        binding.include.titleBackTv.setText("");
        binding.include.titleBackLl.setOnClickListener(this);

        binding.include.titleMenuConfirm.setText("提交");
        binding.include.titleMenuConfirm.setTextColor(getResources().getColor(R.color.white));
        binding.include.titleMenuConfirm.setOnClickListener(this);

        binding.include.titleTextTv.setText(isDone ? "巡检总结查看页" : "巡检任务提交页");
        binding.include.titleMenuConfirm.setVisibility(isDone ? View.GONE : View.VISIBLE);
        binding.tvLimit.setVisibility(isDone ? View.GONE : View.VISIBLE);
        binding.tvLimit2.setVisibility(isDone ? View.GONE : View.VISIBLE);
        binding.tvLimit3.setVisibility(isDone ? View.GONE : View.VISIBLE);

        if (isDone) {
            if (null != single.getContentNote() && !TextUtils.isEmpty(single.getContentNote())) {
                binding.remark.setText(single.getContentNote());
            } else {
                binding.remark.setText("无");
            }

            if (null != single.getImprovement() && !TextUtils.isEmpty(single.getImprovement())) {
                binding.remark2.setText(single.getImprovement());
            } else {
                binding.remark2.setText("无");
            }

            if (null != single.getSummary() && !TextUtils.isEmpty(single.getSummary())) {
                binding.remark3.setText(single.getSummary());
            } else {
                binding.remark3.setText("无");
            }
            setCanNotEditNoClick(binding.remark);
            setCanNotEditNoClick(binding.remark2);
            setCanNotEditNoClick(binding.remark3);
        }


        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit, binding.remark, 100, EditAdviceActivity.this));
        binding.remark2.addTextChangedListener(new MyTextWatcher(binding.tvLimit2, binding.remark2, 100, EditAdviceActivity.this));
        binding.remark3.addTextChangedListener(new MyTextWatcher(binding.tvLimit3, binding.remark3, 100, EditAdviceActivity.this));


    }

    public void setCanNotEditNoClick(EditText view) {
        view.setFocusable(false);
        view.setFocusableInTouchMode(false);
        // 如果之前没设置过点击事件，该处可省略
        view.setOnClickListener(null);
    }

    public  String replaceBlank(String src) {
        String dest = "";
        if (src != null) {
            Pattern pattern = Pattern.compile("\t|\br|\r|\n|\\s*");
            Matcher matcher = pattern.matcher(src);
            dest = matcher.replaceAll("");
        }
        return dest;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_menu_confirm:
                if (TextUtils.isEmpty(binding.remark.getText().toString().trim())) {
                    binding.remark.setText("");
                    showCheckFailedDialog("巡检内容/问题信息尚未填写,请填完保存");
                    return;
                }
                if (TextUtils.isEmpty(binding.remark2.getText().toString().trim())) {
                    binding.remark2.setText("");
                    showCheckFailedDialog("改进措施信息尚未填写,请填完保存");
                    return;
                }
                if (TextUtils.isEmpty(binding.remark3.getText().toString().trim())) {
                    binding.remark3.setText("");
                    showCheckFailedDialog("巡检总结信息尚未填写,请填完保存");
                    return;
                }
                String r1 = replaceBlank(binding.remark.getText().toString().trim());
                String r2 = replaceBlank(binding.remark2.getText().toString().trim());
                String r3 = replaceBlank(binding.remark3.getText().toString().trim());
                viewModel.submitPDA(r1, r2, r3, single.getId(), single.getInspectRecordId(), single.getUseDepartmentId())
                        .observe(this, new Observer<LiveDataWrapper<Boolean>>() {
                            @Override
                            public void onChanged(@Nullable LiveDataWrapper<Boolean> booleanLiveDataWrapper) {
                                switch (booleanLiveDataWrapper.status) {
                                    case LOADING:
                                        DialogUtils.waitingDialog(EditAdviceActivity.this);
                                        break;
                                    case SUCCESS:
                                        DialogUtils.dismiss(EditAdviceActivity.this);
                                        ToastUtil.toastImage(getApplication().getApplicationContext(), "提交成功", -1);
                                        setResult(Constants.RESULT_OK);
                                        finish();
                                        break;
                                    case ERROR:
                                        DialogUtils.dismiss(EditAdviceActivity.this);
                                        Throwable throwable = booleanLiveDataWrapper.error;
                                        if (throwable instanceof ResultException && throwable.getMessage().equals("当前巡检任务尚未巡检完成,无法提交")) {
                                            CreateDialog.submitDialog(EditAdviceActivity.this, "提交失败", "当前巡检任务还有设备未巡检,请巡检完成后提交", 101, null, "确定");
                                        } else {
                                            handleException(throwable, false);
                                        }
                                        break;
                                }
                            }
                        });
                break;
            case R.id.title_back_ll:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}
