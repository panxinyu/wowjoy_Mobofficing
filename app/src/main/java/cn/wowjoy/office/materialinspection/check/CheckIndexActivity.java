package cn.wowjoy.office.materialinspection.check;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorActivityAdapter;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.databinding.ActivityCheckIndexBinding;
import cn.wowjoy.office.materialinspection.check.manage.done.CheckDoneFragment;
import cn.wowjoy.office.materialinspection.check.manage.nodone.CheckNoDoneFragment;
import cn.wowjoy.office.materialinspection.check.manage.search.CheckSearchActivity;

public class CheckIndexActivity extends NewBaseActivity<ActivityCheckIndexBinding,CheckIndexViewModel> implements View.OnClickListener {
    private List<Integer> icons;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorActivityAdapter mTabIndicatorActivityAdapter;


    @Override
    protected Class<CheckIndexViewModel> getViewModel() {
        return CheckIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_index;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.materialTitle.titleTextTv.setText("资产盘点");
        binding.materialTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.materialTitle.titleBackTv.setText("");
        binding.materialTitle.titleBackLl.setOnClickListener(this);
        binding.materialTitle.imgvSearch.setVisibility(View.VISIBLE);
        binding.materialTitle.imgvSearch.setOnClickListener(this);
        initData();
        binding.tabIndicator.setScrollBar(new ColorBar(getApplicationContext(), getResources().getColor(R.color.view_line1), 4));//设置滚动条的颜色，及高度
        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this, R.color.appText14), ContextCompat.getColor(this, R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator, binding.vp);
        mTabIndicatorActivityAdapter = new TabIndicatorActivityAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter.setData(titles, fragments);
        indicatorViewPager.setAdapter(mTabIndicatorActivityAdapter);
        binding.vp.setCanScroll(true);
        binding.vp.setOffscreenPageLimit(2);
    }
    private void initData() {
        titles = new ArrayList<>();
        titles.add("未完成");
        titles.add("已完成");
        fragments = new ArrayList<>();
        fragments.add(CheckNoDoneFragment.newInstance("", ""));
        fragments.add(CheckDoneFragment.newInstance("", ""));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                finish();
                break;
            case R.id.imgv_search:
                startActivity(new Intent(CheckIndexActivity.this, CheckSearchActivity.class));
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        openBroadCast();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeBroadCast();
    }
    @Override
    protected void brodcast(Context context, String msg) {
        super.brodcast(context, msg);
//        Log.e("pxy", "brodcast: "+ msg);
        CreateDialog.submitDialog(CheckIndexActivity.this, "扫码失败", "请进入盘点任务页面再扫描盘点!",101,null,"我知道了");
    }
}
