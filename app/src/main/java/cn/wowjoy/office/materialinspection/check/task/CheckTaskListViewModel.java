package cn.wowjoy.office.materialinspection.check.task;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.ItemInventoryRvDetailAdapter;
import cn.wowjoy.office.common.adapter.MenuAdapter;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.CheckTaskUpdateListRequest;
import cn.wowjoy.office.data.request.SubmitCheckTaskListDetailRequest;
import cn.wowjoy.office.data.response.CheckBroadResultResponse;
import cn.wowjoy.office.data.response.InventoryTaskDtlVO;
import cn.wowjoy.office.data.response.InventoryTaskDetailListResponse;
import cn.wowjoy.office.data.response.SingleTaskController;
import cn.wowjoy.office.data.response.SubmitCheckTaskListDetailResponse;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2017/12/28.
 */

public class CheckTaskListViewModel extends NewBaseViewModel {

    @Inject
    public CheckTaskListViewModel(@NonNull NewMainApplication application) {
        super(application);
        menuAdapter.setmEventListener(this);
        mItemRvDetailAdapter.setItemEventHandler(this);
    }

    @Override
    public void onCreateViewModel() {

    }
    private MediatorLiveData<LiveDataWrapper<InventoryTaskDetailListResponse>> response = new MediatorLiveData<>();
    private MediatorLiveData<Boolean> load = new MediatorLiveData<>();
    private MediatorLiveData<InventoryTaskDtlVO> jump = new MediatorLiveData<>();

    private  MediatorLiveData<LiveDataWrapper<CheckBroadResultResponse>> broadResult = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<CheckBroadResultResponse>> getBroadResult() {
        return broadResult;
    }

    MediatorLiveData<PopuModel>  meun = new MediatorLiveData<>();
    private String taskId;
    private  String filter;

    public MenuAdapter menuAdapter = new MenuAdapter();


    public MediatorLiveData<InventoryTaskDtlVO> getJump() {
        return jump;
    }

    public MediatorLiveData<Boolean> getLoad() {
        return load;
    }

    public MediatorLiveData<LiveDataWrapper<InventoryTaskDetailListResponse>> getResponse() {
        return response;
    }

    public void jump(InventoryTaskDtlVO InventoryTaskDtlVO){
        jump.setValue(InventoryTaskDtlVO);
    }

    @Override
    public void loadData(boolean ref) {
        super.loadData(ref);
        load.setValue(ref);
    }
    @Inject
    ApiService apiService;
    public SingleTaskController single;
    public ArrayList<InventoryTaskDtlVO> datas;
    public ItemInventoryRvDetailAdapter mItemRvDetailAdapter = new ItemInventoryRvDetailAdapter();
    public LRecyclerViewAdapter wladapter = new LRecyclerViewAdapter(mItemRvDetailAdapter);


    public void setWData(List<InventoryTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        mItemRvDetailAdapter.setData(datas);
        refreshComplete();
    }
    public void refreshData(boolean isRefresh ,List<InventoryTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        if (isRefresh) {
            mItemRvDetailAdapter.refresh(data);
        } else {
            mItemRvDetailAdapter.loadMore(data);
        }
        wladapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty() )){
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview,null,false);
            wladapter.addFooterView(view);
        }
        wladapter.notifyDataSetChanged();
        refreshComplete();
    }

    private final int pageSize = 10;
    private int pageIndex = 0;
    public void getInventoryTaskListControllerByPageSize(String taskId,String type,String billInfo, boolean isRefresh,String filter){
        this.taskId = taskId;
        this.filter = filter;
        if (isRefresh){
            pageIndex = 0;
        } else {
            ++pageIndex;
        }
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getListInventoryTaskDtlPdaByPageSize(taskId,type,billInfo,pageIndex,pageSize)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((InventoryTaskDetailListResponse taskListResponse) -> {
                    //TODO: 设置界面上的详细数据 着重区分下！！！ 已完成和未完成显示的页面区别
                    response.setValue(LiveDataWrapper.success(taskListResponse));
                    refreshData(isRefresh ,taskListResponse.getDetailList());
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        response.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
    public void updateByBroad(String qrCode, String taskId ){

        broadResult.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.updateListByBroadCast(new CheckTaskUpdateListRequest(qrCode,taskId,"1","1"))
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((CheckBroadResultResponse submitPDAResponse) -> {
//                    ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                    broadResult.setValue(LiveDataWrapper.success(submitPDAResponse));
                }, (Throwable throwable) ->{
                    broadResult.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
    }



    public MediatorLiveData<LiveDataWrapper<Boolean>> submitPDA(String taskId ){
        final MediatorLiveData<LiveDataWrapper<Boolean>> submit = new MediatorLiveData<>();
        submit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.submitCheckTaskListDetail(new SubmitCheckTaskListDetailRequest(taskId))
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((SubmitCheckTaskListDetailResponse submitPDAResponse) -> {
                    //TODO: 设置界面上的详细数据 着重区分下！！！ 已完成和未完成显示的页面区别
                    ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                    submit.setValue(LiveDataWrapper.success(true));
                }, (Throwable throwable) -> {
                    submit.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
        return submit;
    }


    public void sort(PopuModel popuModel){
        meun.setValue(popuModel);
    }
}
