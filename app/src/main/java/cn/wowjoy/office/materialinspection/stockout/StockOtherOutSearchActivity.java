package cn.wowjoy.office.materialinspection.stockout;

import android.os.Bundle;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.databinding.ActivityStockOtherOutSearchBinding;

public class StockOtherOutSearchActivity extends NewBaseActivity<ActivityStockOtherOutSearchBinding,StockOtherOutSearchViewModel> {

    @Override
    protected Class<StockOtherOutSearchViewModel> getViewModel() {
        return StockOtherOutSearchViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_other_out_search;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }


}
