package cn.wowjoy.office.materialinspection.stockout.otherout.detail;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2018/4/13.
 */

public class StockOutDetailViewModel extends NewBaseViewModel {

    @Inject
    public StockOutDetailViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
