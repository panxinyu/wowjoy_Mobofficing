package cn.wowjoy.office.materialinspection.check.manage.search;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.InventoryTaskListResponse;
import cn.wowjoy.office.data.response.InventoryTotalItemInfo;
import cn.wowjoy.office.databinding.ItemRvCheckSearchBinding;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2017/12/28.
 */

public class CheckSearchViewModel extends NewBaseViewModel{

    @Inject
    public CheckSearchViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
    @Override
    public void onCreateViewModel() {

    }
    @Inject
    ApiService apiService;
    private MediatorLiveData<LiveDataWrapper<InventoryTaskListResponse>> searchData = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<InventoryTaskListResponse>> getSearchData() {
        return searchData;
    }

    public ArrayList<InventoryTotalItemInfo> datas;
    public BGABindingRecyclerViewAdapter<InventoryTotalItemInfo, ItemRvCheckSearchBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_check_search);
    public LRecyclerViewAdapter searchAdapter = new LRecyclerViewAdapter(wlinnerAdapter);

    public void setWData(List<InventoryTotalItemInfo> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        wlinnerAdapter.setData(datas);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }
    public void clearData(){
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        wlinnerAdapter.setData(datas);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }

    //获取未完成任务的List
    public void getSearchListInfo(String billinfo){
        searchData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getInventoryListVagueInfo(billinfo)
                .flatMap(new ResultDataParse<InventoryTaskListResponse>())
                .compose(new RxSchedulerTransformer<InventoryTaskListResponse>())
                .subscribe(new Consumer<InventoryTaskListResponse>() {
                    @Override
                    public void accept(InventoryTaskListResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        searchData.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        searchData.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }

}
