package cn.wowjoy.office.materialinspection.Thread;

import android.app.Instrumentation;
import android.os.SystemClock;
import android.view.MotionEvent;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2018/4/27.
 */

public class ThreadManager {
    private ExecutorService mExecutorService;
    private int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private int KEEP_ALIVE_TIME = 2;
    private TimeUnit mTimeUnit = TimeUnit.SECONDS;
    BlockingDeque<Runnable> mRunnables = new LinkedBlockingDeque<Runnable>();

    public ThreadManager() {
        mExecutorService = new ThreadPoolExecutor(NUMBER_OF_CORES,NUMBER_OF_CORES*2,
                KEEP_ALIVE_TIME,mTimeUnit,mRunnables);
    }

    public void doFolid(int height){
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                Instrumentation inst = new Instrumentation();
                float x = 45.0f;
                float y = height;
                long dowTime = SystemClock.uptimeMillis();
                inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime,
                        MotionEvent.ACTION_DOWN, x, y,0));
                inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime,
                        MotionEvent.ACTION_MOVE, x, y - 50,0));
                inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+20,
                        MotionEvent.ACTION_MOVE, x, y - 100,0));
                inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+30,
                        MotionEvent.ACTION_MOVE, x, y - 150,0));
                inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+40,
                        MotionEvent.ACTION_MOVE, x, y - 200,0));
                inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+40,
                        MotionEvent.ACTION_UP, x, y - 250,0));

            }
        });
    }
}
