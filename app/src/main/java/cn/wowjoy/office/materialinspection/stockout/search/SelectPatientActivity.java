package cn.wowjoy.office.materialinspection.stockout.search;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.StockSelectResponse;
import cn.wowjoy.office.databinding.ActivitySelectPatientBinding;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class SelectPatientActivity extends NewBaseActivity<ActivitySelectPatientBinding,SelectPatientViewModel> implements View.OnClickListener {
    private MDialog waitDialog;
    String name = "";
    private String key;

    @Override
    protected Class<SelectPatientViewModel> getViewModel() {
        return SelectPatientViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_patient;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        name = getIntent().getStringExtra("name");

        binding.selectTitle.titleTextTv.setText("选择病患名称");
        binding.selectTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.selectTitle.titleBackTv.setText("");
        binding.selectTitle.titleBackLl.setOnClickListener(this);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(viewModel.adapter);
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setLoadMoreEnabled(false);
        binding.recyclerView.setPullRefreshEnabled(false);

        initListener();
        initObserve();


    }

    private void initListener() {
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();

                key = binding.searchEditText.getText().toString().trim();
                //       viewModel.getGoodsListInfo(isStop,key,true);
                viewModel.getPatientList(key);
//                materialListPresenter.getInfo();
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(binding.searchEditText.getText().toString())) {
                    binding.delete.setVisibility(View.VISIBLE);
                } else {
                    binding.delete.setVisibility(View.GONE);
                    key = null;
                }

            }
        });
        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchEditText.setText("");
                binding.delete.setVisibility(View.GONE);
                key = null;
            }
        });
    }

    private void initObserve() {
        viewModel.getSearch().observe(this, new Observer<LiveDataWrapper<List<StockSelectResponse>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<StockSelectResponse>> StockSelectResponseLiveDataWrapper) {
                switch (StockSelectResponseLiveDataWrapper.status) {
                    case LOADING:
                       DialogUtils.waitingDialog(SelectPatientActivity.this);
                        break;
                    case SUCCESS:
                            DialogUtils.dismiss(SelectPatientActivity.this);
                        if (null != StockSelectResponseLiveDataWrapper.data) {
                            List<StockSelectResponse> categoryNameList = StockSelectResponseLiveDataWrapper.data;
                            viewModel.mStockSelectAdapter.setPopuModels(categoryNameList, key);
                        }

                        break;
                    case ERROR:
                        DialogUtils.dismiss(SelectPatientActivity.this);
                        handleException(StockSelectResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
//        viewModel.getResponse().observe(this, new Observer<LiveDataWrapper<List<StockSelectResponse>>>() {
//            @Override
//            public void onChanged(@Nullable LiveDataWrapper<List<StockSelectResponse>> StockSelectResponseLiveDataWrapper) {
//                switch (StockSelectResponseLiveDataWrapper.status) {
//                    case LOADING:
//                        waitDialog = CreateDialog.waitingDialog(SelectPatientActivity.this);
//                        break;
//                    case SUCCESS:
//                        if (null != waitDialog) {
//                            CreateDialog.dismiss(SelectPatientActivity.this, waitDialog);
//                        }
//                        if (null != StockSelectResponseLiveDataWrapper.data) {
//                            List<StockSelectResponse> categoryNameList = StockSelectResponseLiveDataWrapper.data;
//                            viewModel.mStockSelectAdapter.setPopuModels(categoryNameList);
//                        }
//                        break;
//                    case ERROR:
//                        if (null != waitDialog) {
//                            CreateDialog.dismiss(SelectPatientActivity.this, waitDialog);
//                        }
//                        handleException(StockSelectResponseLiveDataWrapper.error, true);
//                        break;
//                }
//            }
//        });

        viewModel.meun.observe(this, new Observer<StockSelectResponse>() {
            @Override
            public void onChanged(@Nullable StockSelectResponse popuModel) {
                sort(popuModel);
            }
        });
    }


    private void sort(StockSelectResponse popuModel) {
        Intent intent = new Intent();
        intent.putExtra("name", popuModel.getPatientName());
        intent.putExtra("code", popuModel.getPatientIdC());
        setResult(Constants.RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getPatientList("");
    }

    @Override
    public void onBackPressed() {
        setResult(Constants.RESULT_FAIL);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                setResult(Constants.RESULT_FAIL);
                finish();
                break;
        }
    }


}
