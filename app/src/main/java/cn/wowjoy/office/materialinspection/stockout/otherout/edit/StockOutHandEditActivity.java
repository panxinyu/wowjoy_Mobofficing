package cn.wowjoy.office.materialinspection.stockout.otherout.edit;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.keyborad.CustomBaseKeyboard;
import cn.wowjoy.office.common.widget.keyborad.CustomKeyboardManager;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityStockOutHandEditBinding;
import cn.wowjoy.office.utils.ToastUtil;

public class StockOutHandEditActivity extends NewBaseActivity<ActivityStockOutHandEditBinding,StockOutHandEditViewModel> implements View.OnClickListener, CustomBaseKeyboard.OnTextChangeListener {
    private  CustomKeyboardManager customKeyboardManager;
    private CustomBaseKeyboard priceKeyboard;
    //实际库存
    private String amount;

    private String factOut;

    private String canOutNumber = "";

    private boolean isApply = false;

    @Override
    protected Class<StockOutHandEditViewModel> getViewModel() {
        return StockOutHandEditViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_out_hand_edit;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        amount = getIntent().getStringExtra("amount");
        factOut = getIntent().getStringExtra("factOut");
        canOutNumber = getIntent().getStringExtra("canOutNumber");
         isApply = getIntent().getBooleanExtra("isApply", false);
        initView();
    }

    private void initView(){
        //申请出库 编辑数量
//        if(isApply){
//
//        }



        binding.back.setOnClickListener(this);
        binding.complete.setOnClickListener(this);
//        binding.addStock.addTextChangedListener(textWatcher);
        binding.addStock.setOnClickListener(this);
        customKeyboardManager = new CustomKeyboardManager(this);
        priceKeyboard = new CustomBaseKeyboard(this, R.xml.stock_price_num_keyboard) {
            @Override
            public boolean handleSpecialKey(EditText etCurrent, int primaryCode) {
                return false;
            }
        };
        priceKeyboard.setOnTextChangeListener(this);
        customKeyboardManager.setShowUnderView(binding.rl);
        customKeyboardManager.attachTo(binding.addStock, priceKeyboard);
         binding.addStock.setText(factOut);
    }



    private void complete(){
        setResult(Constants.RESULT_OK);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
            case R.id.complete:
                if(TextUtils.isEmpty(binding.addStock.getText().toString().trim())){
                    ToastUtil.toastWarning(StockOutHandEditActivity.this, "请填写数量", -1);
                }

                float now = Float.parseFloat(binding.addStock.getText().toString().trim());
                float total = Float.parseFloat(amount);
                if(isApply && !"".equals(canOutNumber)){
                    float canOutNumberF = Float.parseFloat(canOutNumber);
                    if(now>canOutNumberF){
                        ToastUtil.toastWarning(StockOutHandEditActivity.this, "超出未出库数量,请核对后输入", -1);
                        return;
                    }
                }
                if(now > total){
                    if(!isApply){
                        ToastUtil.toastWarning(StockOutHandEditActivity.this, "该物资库存不足,请核对后输入", -1);
                    }else{
                        ToastUtil.toastWarning(StockOutHandEditActivity.this, "超过申请数量,请核对后输入", -1);
                    }
                    return;
                }

                Intent intent = new Intent();
                intent.putExtra("count",binding.addStock.getText().toString().trim());
                setResult(Constants.RESULT_OK,intent);
                finish();
                break;
            case R.id.add_stock:
                customKeyboardManager.focus();
                break;
        }
    }

    @Override                                 //
    public void onChange(StringBuilder sb) {  // 1  1.   1.2   1.22   1.222
        Log.e("PXY", "sb: "+ sb.toString().trim());
        if(!TextUtils.isEmpty(sb.toString().trim()) && sb.toString().contains(".")){
            if(sb.toString().substring(sb.toString().indexOf(".")+1,sb.toString().length()).length() > 2){
                       return;
            }
        }

        if (TextUtils.isEmpty(sb.toString().trim())){
            binding.limit.setSelected(false);
            binding.limit.setText("(0/16)");
        } else if (sb.toString().contains(".")){
            if (sb.toString().contains("-")){
                if (sb.toString().trim().length() >= 18){
                    binding.limit.setSelected(true);
                    binding.limit.setText("(16/16)");
                } else {
                    binding.limit.setSelected(false);
                    binding.limit.setText("("+ (sb.toString().trim().length() - 2) +"/16)");
                }
            } else {
                if (sb.toString().trim().length() >= 17){
                    binding.limit.setSelected(true);
                    binding.limit.setText("(16/16)");
                } else {
                    binding.limit.setSelected(false);
                    binding.limit.setText("("+ (sb.toString().trim().length() - 1) +"/16)");
                }
            }

        } else if (sb.toString().contains("-")){
            if (sb.toString().trim().length() >= 17){
                binding.limit.setSelected(true);
                binding.limit.setText("(16/16)");
            } else {
                binding.limit.setSelected(false);
                binding.limit.setText("("+ (sb.toString().trim().length() - 1) +"/16)");
            }
        } else {
            binding.limit.setSelected(false);
            binding.limit.setText("("+ (sb.toString().trim().length()) +"/16)");
        }
    }


}
