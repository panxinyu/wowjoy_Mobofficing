package cn.wowjoy.office.materialinspection.check.manage.done;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InventoryTaskListResponse;
import cn.wowjoy.office.databinding.FragmentCheckDoneBinding;
import cn.wowjoy.office.materialinspection.check.CheckIndexViewModel;
import cn.wowjoy.office.materialinspection.check.task.CheckTaskListActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckDoneFragment extends NewBaseFragment<FragmentCheckDoneBinding, CheckIndexViewModel> {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MDialog waitDialog;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public static CheckDoneFragment newInstance(String param1, String param2) {
        CheckDoneFragment checkDoneFragment = new CheckDoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        checkDoneFragment.setArguments(args);
        return checkDoneFragment;
    }

    @Override
    protected Class<CheckIndexViewModel> getViewModel() {
        return CheckIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_check_done;
    }


    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        binding.rvDoneFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvDoneFragment.setAdapter(viewModel.doneAdapter);
        binding.rvDoneFragment.addItemDecoration(new SimpleItemDecoration(getContext(), SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("没有完成的盘点任务");
        binding.rvDoneFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getDoneListInfo();
            }
        });
        binding.rvDoneFragment.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                //下拉刷新 请求服务器新数据
                viewModel.getDoneListInfo();
            }
        });
        initObserve();
    }

    private void initObserve() {
        viewModel.getDoneData().observe(this, new Observer<LiveDataWrapper<InventoryTaskListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InventoryTaskListResponse> InventoryTaskListResponseLiveDataWrapper) {
                switch (InventoryTaskListResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        if (null != InventoryTaskListResponseLiveDataWrapper.data) {
                            viewModel.setDoneData(InventoryTaskListResponseLiveDataWrapper.data.getInventoryTotalItemInfoLists());
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        binding.rvDoneFragment.refreshComplete(1);
                        handleException(InventoryTaskListResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });
    }
    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        binding.rvDoneFragment.refresh();
        viewModel.doneAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (null !=  viewModel.mDonedatas){
                    Intent intent = new Intent(getActivity(), CheckTaskListActivity.class);
                    intent.putExtra("taskId",viewModel.mDonedatas.get(position).getId());
                    intent.putExtra("isDone",true);
                    intent.putExtra("billNo",viewModel.mDonedatas.get(position).getBillNo());
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel. getDoneListInfo();
    }
}
