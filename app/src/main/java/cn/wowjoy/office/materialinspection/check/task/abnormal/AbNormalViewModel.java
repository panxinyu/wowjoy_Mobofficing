package cn.wowjoy.office.materialinspection.check.task.abnormal;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2018/1/24.
 */

public class AbNormalViewModel extends NewBaseViewModel {

    @Inject
    public AbNormalViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
