package cn.wowjoy.office.materialinspection.applyfor.produce.qrdetail;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.databinding.ActivityStockQrDetailBinding;

public class StockQrDetailActivity extends NewBaseActivity<ActivityStockQrDetailBinding,StockQrDetailViewModel> {

    private MDialog waitDialog;

    @Override
    protected Class<StockQrDetailViewModel> getViewModel() {
        return StockQrDetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_qr_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        initObserve();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String qrCode = getIntent().getStringExtra("qrCode");
        viewModel.getQrInfo(qrCode);
    }

    public void initObserve(){
        viewModel.getData().observe(this, new Observer<LiveDataWrapper<ApplyDemandHeadDetailItem>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<ApplyDemandHeadDetailItem> qrVoInfoLiveDataWrapper) {
                switch (qrVoInfoLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(StockQrDetailActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockQrDetailActivity.this, waitDialog);
                        }
                        if(null != qrVoInfoLiveDataWrapper.data){
                             binding.setModel(qrVoInfoLiveDataWrapper.data);
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockQrDetailActivity.this, waitDialog);
                        }
                        handleException(qrVoInfoLiveDataWrapper.error, false);
                        break;
                }
            }
        });

    }

}
