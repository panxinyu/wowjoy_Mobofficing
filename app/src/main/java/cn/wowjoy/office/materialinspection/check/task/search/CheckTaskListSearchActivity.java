package cn.wowjoy.office.materialinspection.check.task.search;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InventoryTaskDetailListResponse;
import cn.wowjoy.office.data.response.InventoryTaskDtlVO;
import cn.wowjoy.office.databinding.ActivityCheckTaskListSearchBinding;
import cn.wowjoy.office.materialinspection.check.detail.CheckDetailActivity;

public class CheckTaskListSearchActivity extends NewBaseActivity<ActivityCheckTaskListSearchBinding,CheckTaskListSearchViewModel> {

    private MDialog waitDialog;
    private String taskId;
    private boolean isDone;

    @Override
    protected Class<CheckTaskListSearchViewModel> getViewModel() {
        return CheckTaskListSearchViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_task_list_search;
    }

    @Override
    protected void onResume() {
        super.onResume();
        taskId = getIntent().getExtras().getString("taskId");
        isDone = getIntent().getExtras().getBoolean("isDone");
    }

    @Override
    protected void init(Bundle savedInstanceState) {
          binding.setViewModel(viewModel);

        setSupportActionBar(binding.toolbar);
         initView();
         initListener();
          initObserve();
    }

    private void initView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.searchAdapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
//        binding.recyclerView.setLoadMoreEnabled(false);
    }

    private void initListener() {
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();
                String key = binding.searchEditText.getText().toString().trim();
                // TODO：联网模糊查询
                if (!key.isEmpty()) {
                    //  搜索清空的时候
                    viewModel.getInventoryTaskListControllerByBillInfo(taskId,key.trim(),true);
                    return true;
                }
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO：联网模糊查询
                if (s.toString().isEmpty()) {
                    //  搜索清空的时候
                    viewModel.mItemRvDetailAdapter.clearDate();
                }
            }
        });
        viewModel.searchAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //TODO: 已完成与未完成的跳转
                if (null != viewModel.datas) {
                    Intent intent = new Intent(CheckTaskListSearchActivity.this, CheckDetailActivity.class);
                    intent.putExtra("taskId",taskId);
                    intent.putExtra("taskDtlId", viewModel.datas.get(position).getId());
                    intent.putExtra("data", viewModel.datas.get(position));
                    intent.putExtra("isDone", isDone);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    private void initObserve() {
        viewModel.getResponse().observe(this, new Observer<LiveDataWrapper<InventoryTaskDetailListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InventoryTaskDetailListResponse> InventoryTaskDetailListResponseLiveDataWrapper) {
                if (InventoryTaskDetailListResponseLiveDataWrapper != null) {
                    switch (InventoryTaskDetailListResponseLiveDataWrapper.status) {
                        case LOADING:
                            waitDialog = CreateDialog.waitingDialog(CheckTaskListSearchActivity.this);
                            break;
                        case SUCCESS:
                            if (null != waitDialog) {
                                CreateDialog.dismiss(CheckTaskListSearchActivity.this, waitDialog);
                            }
//                            updateUI(InventoryTaskDetailListResponseLiveDataWrapper.data, filter);
                            break;
                        case ERROR:
                            if (null != waitDialog) {
                                CreateDialog.dismiss(CheckTaskListSearchActivity.this, waitDialog);
                            }
                            handleException(InventoryTaskDetailListResponseLiveDataWrapper.error, true);
                            break;
                    }
                }
            }
        });
        viewModel.getLoad().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
//                setFilterText(filter.trim(), aBoolean.booleanValue());

                if("".equals(binding.searchEditText.getText().toString().trim())  ){
                    viewModel.getInventoryTaskListControllerByBillInfo(taskId,binding.searchEditText.getText().toString().trim(),aBoolean.booleanValue());
                }
            }
        });
//        viewModel.getJump().observe(this, new Observer<InventoryTaskDtlVO>() {
//            @Override
//            public void onChanged(@Nullable InventoryTaskDtlVO InventoryTaskDtlVO) {
//                jump(InventoryTaskDtlVO);
//            }
//        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    public void jump(InventoryTaskDtlVO inspectTaskDtlVO) {
        Intent intent = new Intent(this, CheckDetailActivity.class);
//        intent.putExtra("totalId", viewModel.single.getId());
        intent.putExtra("taskId",taskId);
        intent.putExtra("taskDtlId", inspectTaskDtlVO);
        intent.putExtra("isDone", isDone);
        startActivity(intent);
    }
}
