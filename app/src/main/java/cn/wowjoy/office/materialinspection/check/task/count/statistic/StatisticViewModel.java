package cn.wowjoy.office.materialinspection.check.task.count.statistic;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2018/1/8.
 */

public class StatisticViewModel extends NewBaseViewModel {

    @Inject
    public StatisticViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
