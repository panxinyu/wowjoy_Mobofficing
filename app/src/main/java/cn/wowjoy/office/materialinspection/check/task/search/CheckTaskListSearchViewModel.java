package cn.wowjoy.office.materialinspection.check.task.search;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.ItemInventoryRvDetailAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.InventoryTaskDtlVO;
import cn.wowjoy.office.data.response.InventoryTaskDetailListResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/1/24.
 */

public class CheckTaskListSearchViewModel extends NewBaseViewModel{
    @Inject
    public CheckTaskListSearchViewModel(@NonNull NewMainApplication application) {
        super(application);
//        mItemRvDetailAdapter.setItemEventHandler(this);
    }

    @Override
    public void onCreateViewModel() {

    }
    @Override
    public void loadData(boolean ref) {
        super.loadData(ref);
        load.setValue(ref);
    }
    private MediatorLiveData<LiveDataWrapper<InventoryTaskDetailListResponse>> response = new MediatorLiveData<>();
//    private MediatorLiveData<InventoryTaskDtlVO> jump = new MediatorLiveData<>();
    private MediatorLiveData<Boolean> load = new MediatorLiveData<>();

    public MediatorLiveData<Boolean> getLoad() {
        return load;
    }

    public MediatorLiveData<LiveDataWrapper<InventoryTaskDetailListResponse>> getResponse() {
        return response;
    }
//    public MediatorLiveData<InventoryTaskDtlVO> getJump() {
//        return jump;
//    }
//    public void jump(InventoryTaskDtlVO InventoryTaskDtlVO){
//        jump.setValue(InventoryTaskDtlVO);
//    }
    @Inject
    ApiService apiService;
    public ArrayList<InventoryTaskDtlVO> datas;
    public ItemInventoryRvDetailAdapter mItemRvDetailAdapter = new ItemInventoryRvDetailAdapter();
    public LRecyclerViewAdapter searchAdapter = new LRecyclerViewAdapter(mItemRvDetailAdapter);

    public void refreshData(List<InventoryTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);

       mItemRvDetailAdapter.refresh(data);
        searchAdapter.removeFooterView();
        searchAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void refreshData(boolean isRefresh ,List<InventoryTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        if (isRefresh) {
            mItemRvDetailAdapter.refresh(data);
        } else {
            mItemRvDetailAdapter.loadMore(data);
        }
        searchAdapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty() )){
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview,null,false);
            searchAdapter.addFooterView(view);
        }
        searchAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void setWData(List<InventoryTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
//        wlinnerAdapter.setData(datas);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }
    public void clearData(){
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
//        wlinnerAdapter.setData(datas);
        searchAdapter.removeFooterView();
        searchAdapter.removeHeaderView();
        searchAdapter.notifyDataSetChanged();
    }

    //获取未完成任务的List
    private final int pageSize = 10;
    private int pageIndex = 0;
    public void getInventoryTaskListControllerByBillInfo(String taskId,String billInfo,boolean isRefresh){
//        this.taskId = taskId;
//        this.filter = filter;
        if (isRefresh){
            pageIndex = 0;
        } else {
            ++pageIndex;
        }
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getListInventoryTaskDtlPdaByBillInfo(taskId,billInfo,pageIndex,pageSize)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((InventoryTaskDetailListResponse taskListResponse) -> {
                    //TODO: 设置界面上的详细数据 着重区分下！！！ 已完成和未完成显示的页面区别
                    response.setValue(LiveDataWrapper.success(taskListResponse));
                    refreshData(isRefresh,taskListResponse.getDetailList());
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        response.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
}
