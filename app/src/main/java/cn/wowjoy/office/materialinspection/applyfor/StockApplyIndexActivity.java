package cn.wowjoy.office.materialinspection.applyfor;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.badgeview.BGABadgeTextView;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorBadgetAdapter;
import cn.wowjoy.office.common.widget.CommonDialog;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.event.CountEvent;
import cn.wowjoy.office.data.event.StorageCodeEvent;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.remote.ResultException;
import cn.wowjoy.office.databinding.ActivityStockApplyIndexBinding;
import cn.wowjoy.office.materialinspection.applyfor.index.StockDoneFragment;
import cn.wowjoy.office.materialinspection.applyfor.index.StockNoDoneFragment;

public class StockApplyIndexActivity extends NewBaseActivity<ActivityStockApplyIndexBinding,StockApplyIndexViewModel> implements View.OnClickListener {

    private List<Integer> icons;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorBadgetAdapter mTabIndicatorActivityAdapter;
    private CommonDialog warningDialog;
    private MDialog waitDialog;

    private StockDoneFragment mDoneFragment;
    private StockNoDoneFragment mNoDoneFragment;


    @Override
    protected Class<StockApplyIndexViewModel> getViewModel() {
        return StockApplyIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_apply_index;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        viewModel.getStorageUserId();
        binding.materialTitle.titleTextTv.setText("申请出库");
        binding.materialTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.materialTitle.titleBackTv.setText("");
        binding.materialTitle.titleBackLl.setOnClickListener(this);
//        binding.materialTitle.imgvSearch.setVisibility(View.VISIBLE);
//        binding.materialTitle.imgvSearch.setOnClickListener(this);
        initData();
        binding.tabIndicator.setScrollBar(new ColorBar(getApplicationContext(), getResources().getColor(R.color.view_line1), 4));//设置滚动条的颜色，及高度
        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this, R.color.appText14), ContextCompat.getColor(this, R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator, binding.vp);
    //    mTabIndicatorActivityAdapter = new TabIndicatorActivityAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter = new TabIndicatorBadgetAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter.setData(titles, fragments);
        indicatorViewPager.setAdapter(mTabIndicatorActivityAdapter);
        binding.vp.setCanScroll(true);
        binding.vp.setOffscreenPageLimit(2);
        initObserve();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CountEvent event) {
        if (event.getCount() <= 0){
            ((BGABadgeTextView)binding.tabIndicator.getItemView(0)).hiddenBadge();
        } else if (event.getCount() <= 99){
            ((BGABadgeTextView)binding.tabIndicator.getItemView(0)).showTextBadge(String.valueOf(event.getCount()));
        } else {
            ((BGABadgeTextView)binding.tabIndicator.getItemView(0)).showTextBadge("99");
        }
    }
    private void initObserve() {
        viewModel.getUserId().observe(this, new Observer<LiveDataWrapper<List<PopuModel>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<PopuModel>> listLiveDataWrapper) {
                switch (listLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(StockApplyIndexActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockApplyIndexActivity.this, waitDialog);
                        }
                        mNoDoneFragment.setStorageCode(listLiveDataWrapper.data.get(0).getStorageCode());
                        mDoneFragment.setStorageCode(listLiveDataWrapper.data.get(0).getStorageCode());
                        EventBus.getDefault().post(new StorageCodeEvent(listLiveDataWrapper.data.get(0).getStorageCode()));
//                        RxBus.getInstance().post(202,listLiveDataWrapper.data.get(0).getStorageCode());
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockApplyIndexActivity.this, waitDialog);
                        }
                        if (listLiveDataWrapper.error instanceof ResultException) {
                            initDialog();
                        } else {
                            handleException(listLiveDataWrapper.error, false);
                        }
                        break;
                }
            }
        });
    }

    private void initDialog() {
        warningDialog = new CommonDialog.Builder()
                .showTitle(false)
                .setContent("获取权限失败，请确认是否有权限！")
                .showCancel(false)
                .setConfirmBtn("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        StockApplyIndexActivity.this.finish();
                    }
                })
                .create();
        warningDialog.setCancelable(false);
        warningDialog.show(getSupportFragmentManager(), "");
    }

    private void initData() {
        titles = new ArrayList<>();
        titles.add("待出库");
        titles.add("已完成");
        fragments = new ArrayList<>();

        mNoDoneFragment = StockNoDoneFragment.newInstance("", "");
        mDoneFragment = StockDoneFragment.newInstance("", "");
        fragments.add(mNoDoneFragment);
        fragments.add(mDoneFragment);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                finish();
                break;
//            case R.id.imgv_search:
//                startActivity(new Intent(CheckIndexActivity.this, CheckSearchActivity.class));
//                break;
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        openBroadCast();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeBroadCast();
    }

    @Override
    protected void brodcast(Context context, String msg) {
        super.brodcast(context, msg);
    }
//    private CodeCallBack mCodeCallBack;
//    public void
//    /*接口*/
//    public interface CodeCallBack {
//        /*定义一个获取信息的方法*/
//         void getResult(String code);
//    }
}
