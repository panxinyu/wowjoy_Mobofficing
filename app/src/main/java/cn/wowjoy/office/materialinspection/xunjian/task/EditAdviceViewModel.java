package cn.wowjoy.office.materialinspection.xunjian.task;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.SubmitPDARequest;
import cn.wowjoy.office.data.response.SubmitPDAResponse;
import cn.wowjoy.office.homepage.MenuHelper;
import io.reactivex.disposables.Disposable;

public class EditAdviceViewModel extends NewBaseViewModel{
    @Inject
    public EditAdviceViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Inject
     ApiService apiService;
    @Override
    public void onCreateViewModel() {

    }
    public MediatorLiveData<LiveDataWrapper<Boolean>> submitPDA(String contentNote,String improvement,String summary,  String id , String inspectRecordId , String useDepartmentId){
        final MediatorLiveData<LiveDataWrapper<Boolean>> submit = new MediatorLiveData<>();
        submit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.submitPDA(new SubmitPDARequest(id,inspectRecordId,useDepartmentId,contentNote,improvement,summary), MenuHelper.getAuidS().get("zc_inspect"))
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((SubmitPDAResponse submitPDAResponse) -> {
                    submit.setValue(LiveDataWrapper.success(true));
                }, (Throwable throwable) -> {
                    submit.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
        return submit;
    }
}
