package cn.wowjoy.office.materialinspection.check.detail.add;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.AutoEditItemAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.SubmitCheckTaskListDetailRequest;
import cn.wowjoy.office.data.response.AssetsCardQueryCondition;
import cn.wowjoy.office.data.response.InventoryAssetsCardQueryResponse;
import cn.wowjoy.office.data.response.SubmitCheckTaskListDetailResponse;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;

/**
 * Created by Administrator on 2018/1/5.
 */

public class CheckAddViewModel extends NewBaseViewModel {
    @Inject
    public CheckAddViewModel(@NonNull NewMainApplication application) {
        super(application);
        mAutoEditAdapter.setmEventListener(this);
    }

    @Override
    public void onCreateViewModel() {

    }
    @Inject
    ApiService apiService;

     MediatorLiveData<LiveDataWrapper<Boolean>> submit = new MediatorLiveData<>();
    private MediatorLiveData<LiveDataWrapper<InventoryAssetsCardQueryResponse>> query = new MediatorLiveData<>();
    private MediatorLiveData<AssetsCardQueryCondition> click = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<Boolean>> getSubmit() {
        return submit;
    }

    public MediatorLiveData<AssetsCardQueryCondition> getClick() {
        return click;
    }

    public MediatorLiveData<LiveDataWrapper<InventoryAssetsCardQueryResponse>> getQuery() {
        return query;
    }
    public AutoEditItemAdapter mAutoEditAdapter =new AutoEditItemAdapter(1);
   public List<AssetsCardQueryCondition> detailList ;

    /**
     *  copy模糊集合
     * @param list
     */
    public void setDetailList(List<AssetsCardQueryCondition> list) {
        if(null == detailList){
            detailList=new ArrayList<>();
        }
        detailList.clear();
        detailList.addAll(list);
    }

    public void getQueryList(String num){
        query.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCheckAssetsCardList(num)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((InventoryAssetsCardQueryResponse inventoryAssetsCardQueryResponse) -> {
                    query.setValue(LiveDataWrapper.success(inventoryAssetsCardQueryResponse));
                }, (Throwable throwable) -> {
                    query.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
    }

    /**
     *    提交 盘盈
     * @param request
     * @param isShowToast
     * @return
     */
    public void submitDetail(SubmitCheckTaskListDetailRequest request , boolean isShowToast){
        submit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.saveCheckTaskDtl(request)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((SubmitCheckTaskListDetailResponse submitPDAResponse) -> {
                    if(isShowToast){
                        ToastUtil.toastImage(getApplication().getApplicationContext(),"提交成功",-1);
                    }
                    submit.setValue(LiveDataWrapper.success(true));
                }, (Throwable throwable) -> {
                    submit.setValue(LiveDataWrapper.error(throwable,null));
                });
        addDisposable(disposable);
    }


}
