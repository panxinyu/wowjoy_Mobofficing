package cn.wowjoy.office.materialinspection.stockout;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.customview.MaskFramLayout;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.databinding.ActivityStockOutIndexBinding;

public class StockOutIndexActivity extends NewBaseActivity<ActivityStockOutIndexBinding, StockOutIndexViewModel> implements View.OnClickListener {
    private boolean isOpen_kind = false;

    private boolean isOpen_state = false;

    private int initHeight_kind;

    private int initHeight_state;

    @Override
    protected Class<StockOutIndexViewModel> getViewModel() {
        return StockOutIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_out_index;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        initView();
        initMenu();
        initRV();
        initObserve();

    }

    private void initObserve() {
        viewModel.getMenuAllKindLD().observe(this, new Observer<PopuModel>() {
            @Override
            public void onChanged(@Nullable PopuModel popuModel) {
                sort(popuModel,1);
            }
        });
        viewModel.getMenuAllStateLD().observe(this, new Observer<PopuModel>() {
            @Override
            public void onChanged(@Nullable PopuModel popuModel) {
                sort(popuModel,2);
            }
        });
    }

    private void initRV() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(viewModel.wladapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this,SimpleItemDecoration.VERTICAL_LIST));

        binding.emptyView.emptyContent.setText("没有已完成任务");
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                viewModel. getDoneApplyHeadList(true);
            }
        });



    }

    private void initView() {

        binding.back.setOnClickListener(this);

    }

    private void initMenu() {
        binding.menuAllKindFl.setBgAlapht(77);
        binding.menuAllKindRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        binding.menuAllKindRecyclerview.setAdapter(viewModel.allKindMenu_Adapter);
        binding.menuAllKindRecyclerview.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        viewModel.createMenu();
        binding.menuAllKindFl.setDimissListener(new MaskFramLayout.DimissListener() {
            @Override
            public void onDismiss() {
//                isOpen_kind = false;
                closeFolder(1);
                binding.menuAllKind.setSelected(isOpen_kind);
            }
        });

        binding.menuAllKind.setSelected(false);
        binding.menuAllKind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关闭隔壁的家伙
                closeFolder(2);
                binding.menuAllState.setSelected(isOpen_state);



                viewModel.allKindMenu_Adapter.setType(1);
                removeErrorview();
                if (isOpen_kind) {
                    closeFolder(1);

                } else {
                    openFolder(1);
                }
                binding.menuAllKind.setSelected(isOpen_kind);

            }
        });
        binding.menuAllStateFl.setBgAlapht(77);
        binding.menuAllStateRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        binding.menuAllStateRecyclerview.setAdapter(viewModel.allStateMenu_Adapter);
        binding.menuAllStateRecyclerview.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
//        viewModel.createMenu();
        binding.menuAllStateFl.setDimissListener(new MaskFramLayout.DimissListener() {
            @Override
            public void onDismiss() {
//                isOpen_state = false;
                closeFolder(2);
                binding.menuAllState.setSelected(isOpen_state);
            }
        });

        binding.menuAllState.setSelected(false);
        binding.menuAllState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //关闭隔壁的家伙
                closeFolder(1);
                binding.menuAllKind.setSelected(isOpen_kind);



                viewModel.allKindMenu_Adapter.setType(2);
                removeErrorview();
                if (isOpen_state) {
                    closeFolder(2);

                } else {
                    openFolder(2);
                }
                binding.menuAllState.setSelected(isOpen_state);

            }
        });
    }

    /**
     * 弹出文件夹列表
     */
    protected void openFolder(int type) {

        hideSoftInput();
        if (type == 1) {
            if (!isOpen_kind) {
                binding.menuAllKindFl.setVisibility(View.VISIBLE);
                if (binding.menuAllKindRecyclerview.getHeight() != 0) {
                    initHeight_kind = binding.menuAllKindRecyclerview.getHeight();
                }
                ObjectAnimator animator = ObjectAnimator.ofFloat(binding.menuAllKindRecyclerview, "translationY",
                        -initHeight_kind, 0).setDuration(300);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        binding.menuAllKindRecyclerview.setVisibility(View.VISIBLE);
                    }
                });
                animator.start();
                isOpen_kind = true;
            }
        } else if (type == 2) {
            if (!isOpen_state) {
                binding.menuAllStateFl.setVisibility(View.VISIBLE);
                if (binding.menuAllStateRecyclerview.getHeight() != 0) {
                    initHeight_state = binding.menuAllStateRecyclerview.getHeight();
                }
                ObjectAnimator animator = ObjectAnimator.ofFloat(binding.menuAllStateRecyclerview, "translationY",
                        -initHeight_state, 0).setDuration(300);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        binding.menuAllStateRecyclerview.setVisibility(View.VISIBLE);
                    }
                });
                animator.start();
                isOpen_state = true;
            }
        }

    }



    /**
     * 收起文件夹列表
     */
    protected void closeFolder(int type) {

        hideSoftInput();
        if(type == 1){
            if (isOpen_kind) {
                if (binding.menuAllKindFl.getHeight() != 0) {
                    initHeight_kind = binding.menuAllKindRecyclerview.getHeight();
                }
                ObjectAnimator animator = ObjectAnimator.ofFloat(binding.menuAllKindRecyclerview, "translationY",
                        0, -initHeight_kind).setDuration(300);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        binding.menuAllKindRecyclerview.setVisibility(View.GONE);
                        binding.menuAllKindFl.setVisibility(View.GONE);
                    }
                });
                animator.start();
                isOpen_kind = false;
            }
        }else if(type == 2){
            if (isOpen_state) {
                if (binding.menuAllStateFl.getHeight() != 0) {
                    initHeight_state = binding.menuAllStateRecyclerview.getHeight();
                }
                ObjectAnimator animator = ObjectAnimator.ofFloat(binding.menuAllStateRecyclerview, "translationY",
                        0, -initHeight_state).setDuration(300);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        binding.menuAllStateRecyclerview.setVisibility(View.GONE);
                        binding.menuAllStateFl.setVisibility(View.GONE);
                    }
                });
                animator.start();
                isOpen_state = false;
            }
        }

    }
    public void sort(PopuModel popuModel,int type) {
        closeFolder(type);
        if(type == 1){
            binding.menuAllKind.setText(popuModel.getName());
            isOpen_kind = false;
            binding.menuAllKind.setSelected(false);
            binding.menuAllKindFl.setVisibility(View.GONE);
            //TODO:查询 并改变RecycleView


        }else if(type == 2){
            binding.menuAllState.setText(popuModel.getName());
            isOpen_state = false;
            binding.menuAllState.setSelected(false);
            binding.menuAllStateFl.setVisibility(View.GONE);
            //查询 并改变RecycleView

        }

    }
    // 筛选菜单的文字
    public void setFilterText(String state, boolean ref,int type) {

    //    switchRequest(state, ref);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;


        }
    }
}
