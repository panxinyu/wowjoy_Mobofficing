package cn.wowjoy.office.materialinspection.check.task.count;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorActivityAdapter;
import cn.wowjoy.office.databinding.ActivityCheckCountBinding;
import cn.wowjoy.office.materialinspection.check.task.count.difference.DifferentFragment;
import cn.wowjoy.office.materialinspection.check.task.count.statistic.StatisticFragment;

public class CheckCountActivity extends NewBaseActivity<ActivityCheckCountBinding,CheckCountViewModel> implements View.OnClickListener {

    @Override
    protected Class<CheckCountViewModel> getViewModel() {
        return CheckCountViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_check_count;
    }

    private List<Integer> icons;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorActivityAdapter mTabIndicatorActivityAdapter;



    @Override
    protected void init(Bundle savedInstanceState) {

        binding.materialTitle.titleTextTv.setText("盘点统计");
        binding.materialTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.materialTitle.titleBackTv.setText("");
        binding.materialTitle.titleBackLl.setOnClickListener(this);
//        binding.materialTitle.imgvSearch.setVisibility(View.VISIBLE);
//        binding.materialTitle.imgvSearch.setOnClickListener(this);
        initData();
        binding.tabIndicator.setScrollBar(new ColorBar(getApplicationContext(), getResources().getColor(R.color.view_line1), 4));//设置滚动条的颜色，及高度
        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this, R.color.appText14), ContextCompat.getColor(this, R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator, binding.vp);
        mTabIndicatorActivityAdapter = new TabIndicatorActivityAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter.setData(titles, fragments);
        indicatorViewPager.setAdapter(mTabIndicatorActivityAdapter);
        binding.vp.setCanScroll(true);
        binding.vp.setOffscreenPageLimit(2);
    }

    private void initData() {
        titles = new ArrayList<>();
        titles.add("盘点统计");
        titles.add("盘点差异统计");
        icons = new ArrayList<>();
        icons.add(R.drawable.maintab_1_selector);
//        icons.add(R.drawable.contacts_selector);
//        icons.add(R.drawable.approval_selector);
        icons.add(R.drawable.mine_selector);
        fragments = new ArrayList<>();
        fragments.add(StatisticFragment.newInstance("", ""));
        fragments.add(DifferentFragment.newInstance("", ""));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                finish();
                break;
        }
    }
}
