package cn.wowjoy.office.materialinspection.xunjian;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorActivityAdapter;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.databinding.ActivityMaterialBinding;
import cn.wowjoy.office.materialinspection.xunjian.detail.DeviceDetailActivity;
import cn.wowjoy.office.materialinspection.xunjian.manage.done.DoneFragment;
import cn.wowjoy.office.materialinspection.xunjian.manage.nodone.NoDoneFragment;
import cn.wowjoy.office.materialinspection.xunjian.manage.search.DoneTaskSearchActivity;

public class MaterialActivity extends NewBaseActivity<ActivityMaterialBinding, MaterialViewModel> implements View.OnClickListener{

    private List<Integer> icons;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorActivityAdapter mTabIndicatorActivityAdapter;



    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        binding.materialTitle.titleTextTv.setText("巡检管理");
        binding.materialTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.materialTitle.titleBackTv.setText("");
        binding.materialTitle.titleBackLl.setOnClickListener(this);
        binding.materialTitle.imgvSearch.setVisibility(View.VISIBLE);
        binding.materialTitle.imgvSearch.setOnClickListener(this);
        initData();
        binding.tabIndicator.setScrollBar(new ColorBar(getApplicationContext(), getResources().getColor(R.color.view_line1), 4));//设置滚动条的颜色，及高度
        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this, R.color.appText14), ContextCompat.getColor(this, R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator, binding.vp);
        mTabIndicatorActivityAdapter = new TabIndicatorActivityAdapter(getSupportFragmentManager());
        mTabIndicatorActivityAdapter.setData(titles, fragments);
        indicatorViewPager.setAdapter(mTabIndicatorActivityAdapter);
        binding.vp.setCanScroll(true);
        binding.vp.setOffscreenPageLimit(2);
    }

    private void initData() {
        titles = new ArrayList<>();
        titles.add("未完成");
        titles.add("已完成");
        icons = new ArrayList<>();
        icons.add(R.drawable.maintab_1_selector);
//        icons.add(R.drawable.contacts_selector);
//        icons.add(R.drawable.approval_selector);
        icons.add(R.drawable.mine_selector);
        fragments = new ArrayList<>();
        fragments.add(NoDoneFragment.newInstance("", ""));
        fragments.add(DoneFragment.newInstance("", ""));

    }

    @Override
    protected void onResume() {
        super.onResume();
//        openBroadCast();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        closeBroadCast();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_material;
    }


    @Override
    protected Class<MaterialViewModel> getViewModel() {
        return MaterialViewModel.class;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
                finish();
                break;
            case R.id.imgv_search:
                startActivity(new Intent(MaterialActivity.this, DoneTaskSearchActivity.class));
                break;
        }
    }

    @Override
    protected void brodcast(Context context, String msg) {
        super.brodcast(context, msg);
        String key = msg;
        Intent intent = new Intent(this, DeviceDetailActivity.class);
        intent.putExtra("qrCode",key);
        intent.putExtra("isDone", false);
        intent.putExtra("isShow", false);
//        intent.putExtra("isFind", true);
        startActivityForResult(intent, Constants.RESULT_CODE);
    }
}
