package cn.wowjoy.office.materialinspection.check.manage.nodone;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InventoryTaskListResponse;
import cn.wowjoy.office.databinding.FragmentCheckNoDoneBinding;
import cn.wowjoy.office.materialinspection.check.CheckIndexViewModel;
import cn.wowjoy.office.materialinspection.check.task.CheckTaskListActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckNoDoneFragment extends NewBaseFragment<FragmentCheckNoDoneBinding,CheckIndexViewModel> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MDialog waitDialog ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public static CheckNoDoneFragment newInstance(String param1, String param2) {
        CheckNoDoneFragment checkNoDoneFragment = new CheckNoDoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        checkNoDoneFragment.setArguments(args);
        return checkNoDoneFragment;
    }
    @Override
    protected Class<CheckIndexViewModel> getViewModel() {
        return CheckIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_check_no_done;
    }



    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        binding.rvNodoneFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvNodoneFragment.setAdapter(viewModel.noDoneAdapter);
        binding.rvNodoneFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("没有未完成的盘点任务");
        binding.rvNodoneFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel. getNoDoneListInfo();
            }
        });
        binding.rvNodoneFragment.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                //下拉刷新 请求服务器新数据
                viewModel. getNoDoneListInfo();
            }
        });

        initObserve();
    }
    private void initObserve(){
        viewModel.getNodoneData().observe(this, new Observer<LiveDataWrapper<InventoryTaskListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InventoryTaskListResponse> inventoryTaskListResponseLiveDataWrapper) {
                switch (inventoryTaskListResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        if(null != inventoryTaskListResponseLiveDataWrapper.data){
                            viewModel. setNoDoneData(inventoryTaskListResponseLiveDataWrapper.data.getInventoryTotalItemInfoLists());
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        binding.rvNodoneFragment.refreshComplete(1);
                        handleException(inventoryTaskListResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });
    }
    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        binding.rvNodoneFragment.refresh();
        viewModel.noDoneAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (null !=  viewModel.mNoDonedatas){
                    Intent intent = new Intent(getActivity(), CheckTaskListActivity.class);
                    intent.putExtra("taskId",viewModel.mNoDonedatas.get(position).getId());
                    intent.putExtra("billNo",viewModel.mNoDonedatas.get(position).getBillNo());
                    intent.putExtra("isDone",false);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel. getNoDoneListInfo();
    }
}
