package cn.wowjoy.office.materialinspection.applyfor.produce.qrdetail;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/5/14.
 */

public class StockQrDetailViewModel extends NewBaseViewModel {

    @Inject
    ApiService apiService;
    @Inject
    public StockQrDetailViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
    private MediatorLiveData<LiveDataWrapper<ApplyDemandHeadDetailItem>> data = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<ApplyDemandHeadDetailItem>> getData() {
        return data;
    }
    //获取qr条目详细数据
    public void getQrInfo(String qrCode) {
        data.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getQRByQRCodePdaApply(qrCode)
                .flatMap(new ResultDataParse<ApplyDemandHeadDetailItem>())
                .compose(new RxSchedulerTransformer<ApplyDemandHeadDetailItem>())
                .subscribe(new Consumer<ApplyDemandHeadDetailItem>() {
                    @Override
                    public void accept(ApplyDemandHeadDetailItem taskListResponse) throws Exception {
                        data.setValue(LiveDataWrapper.success(taskListResponse));
//                    setNoDoneData(taskListResponse.getResultList());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        data.setValue(LiveDataWrapper.error(throwable, null));
//                        mNoDoneFragment.handleException(throwable,true);
//                        binding.rvNodoneFragment.refreshComplete(1);

                    }
                });
        addDisposable(disposable);
    }
}
