package cn.wowjoy.office.materialinspection.applyfor.index;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListResponse;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListSingle;
import cn.wowjoy.office.databinding.FragmentStockDoneBinding;
import cn.wowjoy.office.materialinspection.applyfor.StockApplyIndexViewModel;
import cn.wowjoy.office.materialinspection.applyfor.detail.StockApplyDetailActivity;
import cn.wowjoy.office.utils.ToastUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class StockDoneFragment extends NewBaseFragment<FragmentStockDoneBinding,StockApplyIndexViewModel> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MDialog waitDialog ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String storageCode;
    public StockDoneFragment() {
        // Required empty public constructor
    }


    @Override
    protected Class<StockApplyIndexViewModel> getViewModel() {
        return StockApplyIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_stock_done;
    }



    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
        viewModel.setStorageCode(storageCode);
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StockDoneFragment newInstance(String param1, String param2) {
        StockDoneFragment doneFragment = new StockDoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        doneFragment.setArguments(args);
        return doneFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        viewModel.setType(2);
    }

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        binding.rvDoneFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvDoneFragment.setAdapter(viewModel.doneAdapter);
        binding.rvDoneFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));

        binding.emptyView.emptyContent.setText("没有已完成任务");
        binding.rvDoneFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel. getDoneApplyHeadList(true);
            }
        });
//        binding.rvDoneFragment.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                //下拉刷新 请求服务器新数据
//                if ( !TextUtils.isEmpty(storageCode))
//                    viewModel.getDoneApplyHeadList(true);
//            }
//        });
        binding.rvDoneFragment.setLoadMoreEnabled(true);
        initObserve();
    }
    private void initObserve(){
        viewModel.jump.observe(this, new Observer<StockApplyDemandHeadListSingle>() {
            @Override
            public void onChanged(@Nullable StockApplyDemandHeadListSingle stockApplyDemandHeadListSingle) {
                Intent mIntent = new Intent(getActivity(), StockApplyDetailActivity.class);
                if(null != stockApplyDemandHeadListSingle.getHeadCode()){
                    mIntent.putExtra("headCode",stockApplyDemandHeadListSingle.getHeadCode());
                    mIntent.putExtra("isDone",true);
                    startActivity(mIntent);
                }else{
                    ToastUtil.toastWarning(getActivity(),"服务器的锅，联系管理员",-1);
                }

            }
        });
        viewModel.getDoneData().observe(this, new Observer<LiveDataWrapper<StockApplyDemandHeadListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StockApplyDemandHeadListResponse> StockApplyDemandHeadListResponseLiveDataWrapper) {
                switch (StockApplyDemandHeadListResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
//                        if(null != StockApplyDemandHeadListResponseLiveDataWrapper.data){
//                            viewModel.getDoneApplyHeadList();
//                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        binding.rvDoneFragment.refreshComplete(1);
                        handleException(StockApplyDemandHeadListResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });
    }
    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        if(!TextUtils.isEmpty(storageCode)){
            viewModel.getDoneApplyHeadList(true);
        }

    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
         viewModel.getDoneApplyHeadList(true);
    }
}
