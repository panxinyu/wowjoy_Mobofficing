package cn.wowjoy.office.materialinspection.xunjian.manage.done;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InspectionTotalResponse;
import cn.wowjoy.office.databinding.FragmentDoneBinding;
import cn.wowjoy.office.materialinspection.xunjian.MaterialViewModel;
import cn.wowjoy.office.materialinspection.xunjian.task.PatrolTaskActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoneFragment extends NewBaseFragment<FragmentDoneBinding,MaterialViewModel> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MDialog waitDialog ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoneFragment newInstance(String param1, String param2) {
        DoneFragment doneFragment = new DoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        doneFragment.setArguments(args);
        return doneFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        binding.rvDoneFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvDoneFragment.setAdapter(viewModel.doneAdapter);
        binding.rvDoneFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("没有完成的巡检任务");
        binding.rvDoneFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getDoneListInfo("y","","","");
            }
        });
        binding.rvDoneFragment.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                //下拉刷新 请求服务器新数据
                viewModel.getDoneListInfo("y","","","");
            }
        });
        initObserve();
    }

    private void initObserve(){
        viewModel.getDoneData().observe(this, new Observer<LiveDataWrapper<InspectionTotalResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InspectionTotalResponse> inspectionTotalResponseLiveDataWrapper) {
                switch (inspectionTotalResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        if(null != inspectionTotalResponseLiveDataWrapper.data){
                            viewModel. setDoneData(inspectionTotalResponseLiveDataWrapper.data.getResultList());
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        binding.rvDoneFragment.refreshComplete(1);
                        handleException(inspectionTotalResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });
    }

    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
//        mDonePresenter.myAppinfos();
        binding.rvDoneFragment.refresh();
        viewModel.doneAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (null !=  viewModel.mDonedatas){
                    Intent mIntent = new Intent(getActivity(), PatrolTaskActivity.class);
                    mIntent.putExtra("taskId",viewModel.mDonedatas.get(position).getId());
                    mIntent.putExtra("isDone",true);
                    startActivity(mIntent);
                }
            }
        });
    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getDoneListInfo("y","","","");
    }
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_done;
    }

    @Override
    protected Class<MaterialViewModel> getViewModel() {
        return MaterialViewModel.class;
    }


}
