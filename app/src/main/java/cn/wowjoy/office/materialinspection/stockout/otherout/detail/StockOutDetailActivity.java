package cn.wowjoy.office.materialinspection.stockout.otherout.detail;

import android.os.Bundle;
import android.view.View;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.databinding.ActivityStockOutDetailBinding;

public class StockOutDetailActivity extends NewBaseActivity<ActivityStockOutDetailBinding,StockOutDetailViewModel> {

    @Override
    protected Class<StockOutDetailViewModel> getViewModel() {
        return StockOutDetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_out_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
       binding.setViewModel(viewModel);
        binding.deviceTitle.titleTextTv.setText("查看物资详情");
        binding.deviceTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.deviceTitle.titleBackTv.setText("");
        binding.deviceTitle.titleBackLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
       binding.tvClose.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               finish();
           }
       });

    }

    @Override
    protected void onResume() {
        super.onResume();
        int type = getIntent().getIntExtra("type", 101);
        if(type == Constants.RECEIPTS_CODE_APPLY_FOR){
            ApplyDemandHeadDetailItem model = (ApplyDemandHeadDetailItem) getIntent().getSerializableExtra("model");
            binding.setModelApply(model);
            binding.setType(true);
        }else if(type == Constants.OTHER_CODE_APPLY){
            StockForOtherOutResponse model = (StockForOtherOutResponse) getIntent().getSerializableExtra("model");
            binding.setModelOut(model);
            binding.setType(false);
        }
    }
}
