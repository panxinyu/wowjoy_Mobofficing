package cn.wowjoy.office.materialinspection.check.detail.add;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.CategoryListAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.CheckCategoryListResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/1/30.
 */

public class CategoryViewModel extends NewBaseViewModel {
    @Inject
    public CategoryViewModel(@NonNull NewMainApplication application) {
        super(application);
        mCategoryListAdapter.setmEventListener(this);
    }
    @Inject
    ApiService apiService;

    MediatorLiveData<LiveDataWrapper<CheckCategoryListResponse>> response = new MediatorLiveData<>();
    MediatorLiveData<CheckCategoryListResponse.CategoryName>  meun = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<CheckCategoryListResponse>> getResponse() {
        return response;
    }

    //
//    public ArrayList<CheckCategoryListResponse.CategoryName> mResponses;
//    public BGABindingRecyclerViewAdapter<CheckCategoryListResponse.CategoryName, ItemRvCheckNodoneBinding> nolinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_check_nodone);
    public CategoryListAdapter mCategoryListAdapter =  new CategoryListAdapter();
//    public LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(mCategoryListAdapter);
    @Override
    public void onCreateViewModel() {

    }
    public void getList(){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getCategoryList()
                .flatMap(new ResultDataParse<CheckCategoryListResponse>())
                .compose(new RxSchedulerTransformer<CheckCategoryListResponse>())
                .subscribe(new Consumer<CheckCategoryListResponse>() {
                    @Override
                    public void accept(CheckCategoryListResponse taskListResponse) throws Exception {
                        response.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        response.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
    public void sort(CheckCategoryListResponse.CategoryName popuModel){
        meun.setValue(popuModel);
    }
}
