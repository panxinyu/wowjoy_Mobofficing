package cn.wowjoy.office.materialinspection.check.task.abnormal;

import android.os.Bundle;
import android.view.View;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.data.response.CheckBroadResultResponse;
import cn.wowjoy.office.databinding.ActivityAbNormalBinding;

public class AbNormalActivity extends NewBaseActivity<ActivityAbNormalBinding,AbNormalViewModel> implements View.OnClickListener {
    private int error;
    @Override
    protected Class<AbNormalViewModel> getViewModel() {
        return AbNormalViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ab_normal;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.abnormalTitle.titleTextTv.setText("错误");
        binding.abnormalTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.abnormalTitle.titleBackTv.setText("");
        binding.abnormalTitle.titleBackLl.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
         error = getIntent().getIntExtra("error",0);
        if(0 == error){
                //无法识别的状态
            binding.notRecognition.setVisibility(View.VISIBLE);
            binding.rlNotFind.setVisibility(View.GONE);
            binding.rlCardno.setVisibility(View.GONE);
            binding.rlCardname.setVisibility(View.GONE);
        }
        if (1 == error){
            //无法识别的状态
            binding.notRecognition.setVisibility(View.GONE);
            binding.rlNotFind.setVisibility(View.VISIBLE);
            binding.rlCardno.setVisibility(View.VISIBLE);
            binding.rlCardname.setVisibility(View.VISIBLE);
            CheckBroadResultResponse c = (CheckBroadResultResponse) getIntent().getSerializableExtra("data");
            //设定值
            if(null != c && null != c.getCardNo() ){
                binding.tvCardNo.setText(c.getCardNo());
            }
            if(null != c && null != c.getCardName() ){
                binding.tvDeviceNameDevicedetail.setText(c.getCardName());
            }
        }

    }

    @Override
    public void onClick(View v) {
         switch (v.getId()){
             case R.id.title_back_ll:
                 finish();
                 break;
         }
    }
}
