package cn.wowjoy.office.materialinspection.applyfor.produce;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.github.jdsjlzx.interfaces.OnItemClickListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.databinding.ActivityStockApplyProduceDetailBinding;
import cn.wowjoy.office.materialinspection.applyfor.produce.qrdetail.StockQrDetailActivity;

public class StockApplyProduceDetailActivity extends NewBaseActivity<ActivityStockApplyProduceDetailBinding,StockApplyProduceDetailViewModel> {
    private ApplyDemandHeadDetailItem response;  //申请

    private StockForOtherOutResponse mOutResponse; //其他
    private int type;
    @Override
    protected Class<StockApplyProduceDetailViewModel> getViewModel() {
        return StockApplyProduceDetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_apply_produce_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.lrAdapter);

        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.emptyView.emptyContent.setText("空空的列表");

        viewModel.lrAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(type == Constants.OTHER_CODE_APPLY){
                    String qrCode = mOutResponse.getqRVOList().get(position).getQrCode();
                    Intent intent = new Intent(StockApplyProduceDetailActivity.this, StockQrDetailActivity.class);
                    intent.putExtra("qrCode",qrCode);
                    startActivity(intent);


                }else if(type == Constants.RECEIPTS_CODE_APPLY_FOR){
                    String qrCode = response.getqRVOList().get(position).getQrCode();
                    Intent intent = new Intent(StockApplyProduceDetailActivity.this, StockQrDetailActivity.class);
                    intent.putExtra("qrCode",qrCode);
                    startActivity(intent);
                }


            }
        });


        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
         type = getIntent().getIntExtra("type", 1);

        if(type == Constants.OTHER_CODE_APPLY){

            //其他出库  点击详情
            mOutResponse  = (StockForOtherOutResponse) getIntent().getSerializableExtra("model");
            binding.setType(false);
//            binding.setModelOther(mOutResponse);

            if(mOutResponse.isHand()){
                binding.rlKufang.setVisibility(View.GONE);
                binding.content.setVisibility(View.GONE);
            }else{
                binding.rlKufang.setVisibility(View.VISIBLE);
                binding.content.setVisibility(View.VISIBLE);
            }
            viewModel.setWData(mOutResponse.getqRVOList());
            binding.tvTypeOutdetail.setText(mOutResponse.getqRVOList().size()+"");

        }else if(type == Constants.RECEIPTS_CODE_APPLY_FOR){
            //申请出库  点击详情
            response = (ApplyDemandHeadDetailItem) getIntent().getSerializableExtra("model");
            binding.setType(true);
//            binding.setModelApply(response);
            if("n".equalsIgnoreCase(response.getIsQr())){
                binding.rlKufang.setVisibility(View.GONE);
                binding.content.setVisibility(View.GONE);
            }else{
                binding.rlKufang.setVisibility(View.VISIBLE);
                binding.content.setVisibility(View.VISIBLE);
            }

            viewModel.setWData(response.getqRVOList());
            binding.tvTypeOutdetail.setText(response.getqRVOList().size()+"");
        }
        binding.setModelOther(mOutResponse);
        binding.setModelApply(response);

    }
}
