package cn.wowjoy.office.materialinspection.stockout.hand;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.StockOutSearchAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.StockForOtherOutRequest;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;



/**
 * Created by Administrator on 2018/4/12.
 */

public class StockHandAddViewModel extends NewBaseViewModel {

    @Inject
    public StockHandAddViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setmEventListener(this);
    }

    @Override
    public void onCreateViewModel() {

    }
    @Inject
    ApiService apiService;
    private MediatorLiveData<LiveDataWrapper<List<StockForOtherOutResponse>>> searchData = new MediatorLiveData<>();
    private MediatorLiveData<StockForOtherOutResponse> jump = new MediatorLiveData<>();

    public MediatorLiveData<StockForOtherOutResponse> getJump() {
        return jump;
    }

    public MediatorLiveData<LiveDataWrapper<List<StockForOtherOutResponse>>> getSearchData() {
        return searchData;
    }

    public ArrayList<StockForOtherOutResponse> datas;
    StockOutSearchAdapter adapter = new StockOutSearchAdapter();
  //  public BGABindingRecyclerViewAdapter<StockForOtherOutResponse, ItemRvStockOtherHandAddBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_stock_other_hand_add);
    public LRecyclerViewAdapter searchAdapter = new LRecyclerViewAdapter(adapter);


    //获取未完成任务的List
    public void getSearchListInfo(String billinfo,String storageCode){
        searchData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getStockForOtherOut(new StockForOtherOutRequest(billinfo,"n",storageCode))
                .flatMap(new ResultDataParse<List<StockForOtherOutResponse>>())
                .compose(new RxSchedulerTransformer<List<StockForOtherOutResponse>>())
                .subscribe(new Consumer<List<StockForOtherOutResponse>>() {
                    @Override
                    public void accept(List<StockForOtherOutResponse> taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        searchData.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        searchData.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);
    }
    public void jump(StockForOtherOutResponse response){
        jump.setValue(response);
    }
}
