package cn.wowjoy.office.materialinspection.applyfor.detail;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.io.Serializable;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.StockApplyDemandHeadDetailResponse;
import cn.wowjoy.office.databinding.ActivityStockApplyDetailBinding;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceActivity;

public class StockApplyDetailActivity extends NewBaseActivity<ActivityStockApplyDetailBinding, StockApplyDetailViewModel> implements View.OnClickListener {
    private String headCode;
    private boolean isDone;

    private MDialog waitDialog;

    @Override
    protected Class<StockApplyDetailViewModel> getViewModel() {
        return StockApplyDetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_apply_detail;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        headCode = getIntent().getStringExtra("headCode");
        isDone = getIntent().getBooleanExtra("isDone",false);
   //     viewModel.isDone = isDone;
        if(isDone){
            binding.tvSubmitOtherStock.setVisibility(View.GONE);
            binding.bottom.setVisibility(View.GONE);
            binding.total.setText("总出库数量: ");
        }
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.lrAdapter);
   //     viewModel.mAdapter.setDone(isDone);
        viewModel.lrAdapter.removeFooterView();
        viewModel.lrAdapter.removeHeaderView();
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setPullRefreshEnabled(false);

        binding.back.setOnClickListener(this);

        binding.tvSubmitOtherStock.setOnClickListener(this);
        binding.tvSubmitOtherStock.setEnabled(false);
        binding.tvAllSelect.setOnClickListener(this);
        binding.quanxuan.setOnClickListener(this);

        viewModel.getListInfo(headCode);
        initObserve();
    }

    private void initObserve() {
//        viewModel.jump.observe(this, new Observer<ApplyDemandHeadDetailItem>() {
//            @Override
//            public void onChanged(@Nullable ApplyDemandHeadDetailItem applyDemandHeadDetailItem) {
//                Intent intent = new Intent(StockApplyDetailActivity.this, StockApplyProduceDetailActivity.class);
//                intent.putExtra("model", applyDemandHeadDetailItem);
//                startActivity(intent);
//            }
//        });
       viewModel.selectCount.observe(this, new Observer<String>() {
           @Override
           public void onChanged(@Nullable String s) {
               if(null != s){
                   if("0".equalsIgnoreCase(s)){
                       binding.tvSubmitOtherStock.setEnabled(false);
                   }else{
                       binding.tvSubmitOtherStock.setEnabled(true);
                   }
//                   if(s.equalsIgnoreCase(binding.tvAllNum.getText().toString())){
//
//                   }
                   binding.tvAlreadySelect.setText(s);
               }

           }
       });
        viewModel.getData().observe(this, new Observer<LiveDataWrapper<StockApplyDemandHeadDetailResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StockApplyDemandHeadDetailResponse> listLiveDataWrapper) {
                switch (listLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(StockApplyDetailActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockApplyDetailActivity.this, waitDialog);
                        }
                        if(null != listLiveDataWrapper.data){
                            viewModel.setData(listLiveDataWrapper.data.getList());
                            viewModel.mResponse = listLiveDataWrapper.data;
                            // 共多少项
                             binding.tvAllNum.setText(""+listLiveDataWrapper.data.getList().size());
                            if(isDone){
                                binding.count.setText(listLiveDataWrapper.data.getTotalOutNumber());
                            }else{
                                binding.count.setText(listLiveDataWrapper.data.getTotalCanOutNumber());
                            }

                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockApplyDetailActivity.this, waitDialog);
                        }
                            handleException(listLiveDataWrapper.error, false);
                        break;
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                onBackPressed();
                break;
            case R.id.tv_submit_other_stock:
                Intent intent =new Intent(StockApplyDetailActivity.this, StockApplyProduceActivity.class);
                if(null != viewModel.mResponse){
//                  intent.putExtra("model",viewModel.mResponse);
                  intent.putExtra("storageView",viewModel.mResponse.getStorageView());
                  intent.putExtra("commitDeptView",viewModel.mResponse.getCommitDeptView());
                }
                if(null != viewModel.mAdapter.getSelectList() && viewModel.mAdapter.getSelectList().size()>0){
                    intent.putExtra("selectList", (Serializable) viewModel.mAdapter.getSelectList());
                }
               startActivityForResult(intent, Constants.REQUEST_CODE);
                break;
            case R.id.tv_all_select:
            case R.id.quanxuan:
                if(binding.tvAllSelect.isChecked()){
                    viewModel.mAdapter.selectAll(true);
                    binding.tvAlreadySelect.setText(binding.tvAllNum.getText());

                }else{
                    viewModel.mAdapter.selectAll(false);
                    binding.tvAlreadySelect.setText("0");
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
  //      binding.tvAlreadySelect.setText("0");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // 选择去向的结果
            case Constants.REQUEST_CODE:
                switch (resultCode) {
                    case Constants.RESULT_OK:
                        //出库提交成功后
                       onBackPressed();
                        break;
                    case Constants.RESULT_FAIL:
                        //Back回来 要把状态重新置为初始状态

                        break;
                }
                break;
        }

    }

}
