package cn.wowjoy.office.materialinspection.xunjian;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.InspectionTotalItemInfo;
import cn.wowjoy.office.data.response.InspectionTotalResponse;
import cn.wowjoy.office.databinding.ItemRvMaterialInspectionDoneBinding;
import cn.wowjoy.office.databinding.ItemRvMaterialInspectionNodoneBinding;
import cn.wowjoy.office.homepage.MenuHelper;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2017/10/31.
 */

public class MaterialViewModel extends NewBaseViewModel{
    @Inject
    ApiService apiService;
    //已完成数据
    MediatorLiveData<LiveDataWrapper<InspectionTotalResponse>> doneData = new MediatorLiveData<>();
    //未完成数据
    MediatorLiveData<LiveDataWrapper<InspectionTotalResponse>> nodoneData = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<InspectionTotalResponse>> getNodoneData() {
        return nodoneData;
    }

    public MediatorLiveData<LiveDataWrapper<InspectionTotalResponse>> getDoneData() {
        return doneData;
    }

    //已完成的Task
    public ArrayList<InspectionTotalItemInfo> mDonedatas;
    public BGABindingRecyclerViewAdapter<InspectionTotalItemInfo, ItemRvMaterialInspectionDoneBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_material_inspection_done);
    public LRecyclerViewAdapter doneAdapter = new LRecyclerViewAdapter(wlinnerAdapter);

    //未完成的Task
    public ArrayList<InspectionTotalItemInfo> mNoDonedatas;
    public BGABindingRecyclerViewAdapter<InspectionTotalItemInfo, ItemRvMaterialInspectionNodoneBinding> nolinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_material_inspection_nodone);
    public LRecyclerViewAdapter noDoneAdapter = new LRecyclerViewAdapter(nolinnerAdapter);

    public void setDoneData(List<InspectionTotalItemInfo> data) {
        if (null == mDonedatas)
            mDonedatas = new ArrayList<>();
        mDonedatas.clear();
        mDonedatas.addAll(data);

        wlinnerAdapter.setData(mDonedatas);
        doneAdapter.removeFooterView();
        doneAdapter.removeHeaderView();
        doneAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void setNoDoneData(List<InspectionTotalItemInfo> data) {
        if (null == mNoDonedatas)
            mNoDonedatas = new ArrayList<>();
        mNoDonedatas.clear();
        mNoDonedatas.addAll(data);

        nolinnerAdapter.setData(mNoDonedatas);
        noDoneAdapter.removeFooterView();
        noDoneAdapter.removeHeaderView();
        noDoneAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void clearNoDoneData(){
        if (null == mNoDonedatas)
            mNoDonedatas = new ArrayList<>();
        mNoDonedatas.clear();

        nolinnerAdapter.setData(mNoDonedatas);
        noDoneAdapter.removeFooterView();
        noDoneAdapter.removeHeaderView();
        noDoneAdapter.notifyDataSetChanged();
    }
    public void clearDoneData(){
        if (null == mDonedatas)
            mDonedatas = new ArrayList<>();
        mDonedatas.clear();

        nolinnerAdapter.setData(mDonedatas);
        doneAdapter.removeFooterView();
        doneAdapter.removeHeaderView();
        doneAdapter.notifyDataSetChanged();
    }

//    未完成   taskStatus = 91-2,91-3   并且  inspect_record_bill_status ! =92-1,92-2(可能为空)
//    已完成   taskStatus = 91-3，91-4  并且  inspect_record_bill_status ! =92-0
    public boolean isDone(InspectionTotalItemInfo itemInfo){
        if("91-2".equals(itemInfo.getTaskStatus()) || "91-3".equals(itemInfo.getTaskStatus())){
                if(null == itemInfo.getInspectRecordBillStatus()){
                   return false;
                }else if(!"92-1".equals(itemInfo.getInspectRecordBillStatus()) || !"92-2".equals(itemInfo.getInspectRecordBillStatus())){
                    return false;
                }
        }else if("91-3".equals(itemInfo.getTaskStatus()) || "91-4".equals(itemInfo.getTaskStatus())){
           if(null != itemInfo.getInspectRecordBillStatus() && !"92-0".equals(itemInfo.getInspectRecordBillStatus()) ){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreateViewModel() {

    }

    @Inject
    public MaterialViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    //获取已完成任务的List
    public void getDoneListInfo(String isDone , String inspectUserId ,String startIndex , String pageSize){
        doneData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getTaskListInfo("y", MenuHelper.getAuidS().get("zc_inspect"))
                .flatMap(new ResultDataParse<InspectionTotalResponse>())
                .compose(new RxSchedulerTransformer<InspectionTotalResponse>())
                .subscribe(new Consumer<InspectionTotalResponse>() {
                    @Override
                    public void accept(InspectionTotalResponse taskListResponse) throws Exception {
                        doneData.setValue(LiveDataWrapper.success(taskListResponse));
//                        setDoneData(taskListResponse.getResultList());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        doneData.setValue(LiveDataWrapper.error(throwable,null));
//                        binding.rvDoneFragment.refreshComplete(1);
//                        mDoneFragment.handleException(throwable,true);
                    }
                });
        addDisposable(disposable);
    }
    //获取未完成任务的List
    public void getNoDoneListInfo(String isDone, String inspectUserId, String startIndex, String pageSize) {
        nodoneData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getTaskListInfo("n", MenuHelper.getAuidS().get("zc_inspect"))
                .flatMap(new ResultDataParse<InspectionTotalResponse>())
                .compose(new RxSchedulerTransformer<InspectionTotalResponse>())
                .subscribe(new Consumer<InspectionTotalResponse>() {
                    @Override
                    public void accept(InspectionTotalResponse taskListResponse) throws Exception {
                    nodoneData.setValue(LiveDataWrapper.success(taskListResponse));
//                    setNoDoneData(taskListResponse.getResultList());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        nodoneData.setValue(LiveDataWrapper.error(throwable,null));
//                        mNoDoneFragment.handleException(throwable,true);
//                        binding.rvNodoneFragment.refreshComplete(1);

                    }
                });
        addDisposable(disposable);
    }
}
