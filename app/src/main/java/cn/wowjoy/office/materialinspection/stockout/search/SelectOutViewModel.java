package cn.wowjoy.office.materialinspection.stockout.search;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.StockSelectAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.StockSelectResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/4/12.
 */

public class SelectOutViewModel extends NewBaseViewModel {

    @Inject
    public SelectOutViewModel(@NonNull NewMainApplication application) {
        super(application);
        mStockSelectAdapter.setmEventListener(this);
    }

    @Inject
    ApiService apiService;

    MediatorLiveData<LiveDataWrapper<List<StockSelectResponse>>> response = new MediatorLiveData<>();
    MediatorLiveData<StockSelectResponse>  meun = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<List<StockSelectResponse>>> getResponse() {
        return response;
    }
    MediatorLiveData<LiveDataWrapper<List<StockSelectResponse>>> search = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<List<StockSelectResponse>>> getSearch() {
        return search;
    }
    //
//    public ArrayList<StockSelectResponse.SelectResponse> mResponses;
//    public BGABindingRecyclerViewAdapter<StockSelectResponse.SelectResponse, ItemRvCheckNodoneBinding> nolinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_check_nodone);
    public StockSelectAdapter mStockSelectAdapter =  new StockSelectAdapter(1);
    public LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(mStockSelectAdapter);
    @Override
    public void onCreateViewModel() {

    }
    public void getList(){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.selectStockOut()
                .flatMap(new ResultDataParse<List<StockSelectResponse>>())
                .compose(new RxSchedulerTransformer<List<StockSelectResponse>>())
                .subscribe(new Consumer<List<StockSelectResponse>>() {
                               @Override
                               public void accept(List<StockSelectResponse> taskListResponse) throws Exception {
                                   response.setValue(LiveDataWrapper.success(taskListResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                response.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }
    public void queryList(String info){
        search.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.selectStockOutVague(info)
                .flatMap(new ResultDataParse<List<StockSelectResponse>>())
                .compose(new RxSchedulerTransformer<List<StockSelectResponse>>())
                .subscribe(new Consumer<List<StockSelectResponse>>() {
                               @Override
                               public void accept(List<StockSelectResponse> taskListResponse) throws Exception {
                                   search.setValue(LiveDataWrapper.success(taskListResponse));
                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                search.setValue(LiveDataWrapper.error(throwable,null));
                            }
                        });
        addDisposable(disposable);
    }
    public void jump(StockSelectResponse popuModel){
        meun.setValue(popuModel);
    }
}
