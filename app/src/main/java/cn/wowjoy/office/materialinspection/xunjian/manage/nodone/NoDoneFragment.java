package cn.wowjoy.office.materialinspection.xunjian.manage.nodone;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;

import com.github.jdsjlzx.interfaces.OnItemClickListener;
import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InspectionTotalResponse;
import cn.wowjoy.office.databinding.FragmentNoDoneBinding;
import cn.wowjoy.office.materialinspection.xunjian.MaterialViewModel;
import cn.wowjoy.office.materialinspection.xunjian.task.PatrolTaskActivity;
import cn.wowjoy.office.utils.ToastUtil;
import cn.wowjoy.office.utils.dialog.DialogUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoDoneFragment extends NewBaseFragment<FragmentNoDoneBinding,MaterialViewModel> {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MDialog waitDialog ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NoDoneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NoDoneFragment newInstance(String param1, String param2) {
        NoDoneFragment doneFragment = new NoDoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        doneFragment.setArguments(args);
        return doneFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        binding.rvNodoneFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvNodoneFragment.setAdapter(viewModel.noDoneAdapter);
        binding.rvNodoneFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("没有未完成的巡检任务");
        binding.rvNodoneFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel. getNoDoneListInfo("n","","","");
            }
        });
        binding.rvNodoneFragment.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                //下拉刷新 请求服务器新数据
                viewModel. getNoDoneListInfo("n","","","");
            }
        });
        binding.rvNodoneFragment.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //加载更多

            }
        });
        initObserve();
    }
    private void initObserve(){
        viewModel.getNodoneData().observe(this, new Observer<LiveDataWrapper<InspectionTotalResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InspectionTotalResponse> inspectionTotalResponseLiveDataWrapper) {
                switch (inspectionTotalResponseLiveDataWrapper.status){
                    case LOADING:
                       DialogUtils.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(getActivity());
                        if(null != inspectionTotalResponseLiveDataWrapper.data){
                            viewModel. setNoDoneData(inspectionTotalResponseLiveDataWrapper.data.getResultList());
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(getActivity());
                        binding.rvNodoneFragment.refreshComplete(1);
                        handleException(inspectionTotalResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });
    }
    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        binding.rvNodoneFragment.refresh();
        viewModel.noDoneAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (null !=  viewModel.mNoDonedatas){
                    if(!viewModel.mNoDonedatas.get(position).isTime()){
                        ToastUtil.toastWarning(getActivity(),"当前任务不在该时间段内",-1);
                        return;
                    }
                    Intent mIntent = new Intent(getActivity(), PatrolTaskActivity.class);
                    mIntent.putExtra("taskId",viewModel.mNoDonedatas.get(position).getId());
                    mIntent.putExtra("isDone",false);
                    startActivity(mIntent);
                }
            }
        });
    }

    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel. getNoDoneListInfo("n","","","");
    }
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_no_done;
    }


    @Override
    protected Class<MaterialViewModel> getViewModel() {
        return MaterialViewModel.class;
    }
}
