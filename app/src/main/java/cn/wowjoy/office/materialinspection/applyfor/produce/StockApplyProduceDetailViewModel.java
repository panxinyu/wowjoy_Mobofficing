package cn.wowjoy.office.materialinspection.applyfor.produce;

import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.request.GoodStockRequest;
import cn.wowjoy.office.databinding.ItemLrStockApplyProduceDetailBinding;

/**
 * Created by Administrator on 2018/4/19.
 */

public class StockApplyProduceDetailViewModel extends NewBaseViewModel {

    public BGABindingRecyclerViewAdapter<GoodStockRequest, ItemLrStockApplyProduceDetailBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_lr_stock_apply_produce_detail);
    public LRecyclerViewAdapter lrAdapter = new LRecyclerViewAdapter(wlinnerAdapter);

    @Inject
    public StockApplyProduceDetailViewModel(@NonNull NewMainApplication application) {
        super(application);

    }
    public ArrayList<GoodStockRequest> datas;
    public void setWData(List<GoodStockRequest> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        wlinnerAdapter.setData(datas);
        lrAdapter.removeFooterView();
        lrAdapter.removeHeaderView();
        lrAdapter.notifyDataSetChanged();
    }
    @Override
    public void onCreateViewModel() {

    }
}
