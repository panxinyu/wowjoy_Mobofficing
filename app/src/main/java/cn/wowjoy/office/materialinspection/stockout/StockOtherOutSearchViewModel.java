package cn.wowjoy.office.materialinspection.stockout;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2018/6/5.
 */

public class StockOtherOutSearchViewModel extends NewBaseViewModel {
    @Inject
    public StockOtherOutSearchViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
