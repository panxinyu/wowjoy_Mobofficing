package cn.wowjoy.office.materialinspection.stockout.otherout;

import android.app.Activity;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.AutoEditItemAdapter;
import cn.wowjoy.office.common.adapter.StockOtherOutLrAdapter;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.GoodStockRequest;
import cn.wowjoy.office.data.request.StockOtherOutSubmitRequest;
import cn.wowjoy.office.data.response.AssetsCardQueryCondition;
import cn.wowjoy.office.data.response.CheckerResponse;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.data.response.StockStorageResponse;
import cn.wowjoy.office.utils.PreferenceManager;
import cn.wowjoy.office.utils.ToastUtil;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.RequestBody;

import static cn.wowjoy.office.data.constant.Constants.MEDIA_TYPE_JSON;

/**
 * Created by Administrator on 2018/4/11.
 */

public class StockOtherOutViewModel extends NewBaseViewModel {

    public StockOtherOutLrAdapter mOtherOutLrAdapter = new StockOtherOutLrAdapter();
    public LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(mOtherOutLrAdapter);


    /**
     * 储存当前页面上的集合  比较**
     */
    public List<StockForOtherOutResponse> mStockForOtherOutResponses = new ArrayList<>();

    @Inject
    ApiService apiService;

    @Inject
    PreferenceManager preferenceManager;






    private MediatorLiveData<StockForOtherOutResponse> jump = new MediatorLiveData<>();
    public MediatorLiveData<StockForOtherOutResponse> getJump() {
        return jump;
    }

    @Inject
    public StockOtherOutViewModel(@NonNull NewMainApplication application) {
        super(application);
        mOtherOutLrAdapter.setmItemEventHandler(this);
        mAutoEditAdapter.setmEventListener(this);
    }

    @Override
    public void onCreateViewModel() {

    }

    /**
     * 界面删除   数据存储删除
     * 右滑删除
     *
     * @param model
     */
    public void delete(StockForOtherOutResponse model) {
        mStockForOtherOutResponses.remove(model);
        mOtherOutLrAdapter.deleteDate(model);
    }

    private Map<String, RequestBody> initSaveInfo(StockOtherOutSubmitRequest request, String isSuccess, String isConfirm) {
        Gson gson = new Gson();

        Map<String, RequestBody> bodyMap = new ArrayMap<>();
        RequestBody bodyJson = RequestBody.create(MEDIA_TYPE_JSON, gson.toJson(request));
        bodyMap.put("inspectRecordDtlVO", bodyJson);
        bodyMap.put("isSuccess", bodyJson);
        bodyMap.put("isConfirm", bodyJson);
        return bodyMap;
    }

    MediatorLiveData<LiveDataWrapper<String>>  pdaSubmit = new MediatorLiveData<>();

    public void submit(StockOtherOutSubmitRequest request,Activity activity) {
        pdaSubmit.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.submitAddOtherOtherStorage(request)
                .flatMap(new ResultDataParse<String>())
                .compose(new RxSchedulerTransformer<String>())
                .subscribe(new Consumer<String>() {
                               @Override
                               public void accept(String resultEmpty) throws Exception {
                                   pdaSubmit.setValue(LiveDataWrapper.success(resultEmpty));

                               }
                           },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                pdaSubmit.setValue(LiveDataWrapper.error(throwable, null));

                  //              ToastUtil.toastImage(getApplication().getApplicationContext(), throwable.getMessage(), -1);
                            }
                        });
        addDisposable(disposable);
    }
    private MediatorLiveData<LiveDataWrapper<StockForOtherOutResponse>> scanData = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<StockForOtherOutResponse>> getScanData() {
        return scanData;
    }
    public void queryByScan(String qcCode) {
        scanData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getQRByQRCodePda(qcCode)
                .flatMap(new ResultDataParse<StockForOtherOutResponse>())
                .compose(new RxSchedulerTransformer<StockForOtherOutResponse>())
                .subscribe(new Consumer<StockForOtherOutResponse>() {
                    @Override
                    public void accept(StockForOtherOutResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        scanData.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        scanData.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }
    private MediatorLiveData<LiveDataWrapper<List<PopuModel>>> userId = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<List<PopuModel>>> getUserId() {
        return userId;
    }
    public void getStorageUserId() {
        userId.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getStock()
                .flatMap(new ResultDataParse<List<PopuModel>>())
                .compose(new RxSchedulerTransformer<List<PopuModel>>())
                .subscribe(new Consumer<List<PopuModel>>() {
                    @Override
                    public void accept(List<PopuModel> taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        userId.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        userId.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }



    private MediatorLiveData<LiveDataWrapper<StockStorageResponse>> permission = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<StockStorageResponse>> getPermission() {
        return permission;
    }
    public void applyPermission() {
        permission.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.isExamineAndConfirmOut()
                .flatMap(new ResultDataParse<StockStorageResponse>())
                .compose(new RxSchedulerTransformer<StockStorageResponse>())
                .subscribe(new Consumer<StockStorageResponse>() {
                    @Override
                    public void accept(StockStorageResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        permission.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        permission.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }

    public void jump(StockForOtherOutResponse model) {
        jump.setValue(model);
    }

    private void smoothMoveToPosition(int position, LinearLayoutManager layoutManager, RecyclerView rv) {
        int firstItem = layoutManager.findFirstVisibleItemPosition();
        int lastItem = layoutManager.findLastVisibleItemPosition();
        if (position <= firstItem) {
            rv.smoothScrollToPosition(position);
        } else if (position <= lastItem) {
            int top = rv.getChildAt(position - firstItem).getTop();
            rv.smoothScrollBy(0, top);
        } else {
            rv.smoothScrollToPosition(position);
            // move = true;
        }

    }

    public GoodStockRequest addDVO(StockForOtherOutResponse model) {
        GoodStockRequest g = new GoodStockRequest();
        g.setGoodsCode(model.getGoodsCode());
        g.setInStorageDetailBatchNumber(model.getBatchNumber());
        g.setInStorageDetailEffecticeDate(model.getEffectiveDate());
        g.setQrCode(model.getQrCode());
        g.setInStorageDetailPrice(model.getPriceDetail());

        g.setEffectiveDate(model.getEffectiveDate());
        g.setPrice(model.getPrice());
        g.setBatchNumber(model.getBatchNumber());
        return g;
    }

    /**
     * 要保存扫到的正确货物  无论是手工的还是扫码的
     * 页面上显示   List要新加
     *
     * @param model
     */
    public void addDate(StockForOtherOutResponse model, Activity activity, LinearLayoutManager layoutManager, RecyclerView rv) {
        if (null == mStockForOtherOutResponses) {
            mStockForOtherOutResponses = new ArrayList<>();
        }
        if (mStockForOtherOutResponses.size() == 0) {
            //第一次添加
            mStockForOtherOutResponses.add(0, model);

            if (model.isScan()) {
                model.addqRVOList(addDVO(model));
            }

            mOtherOutLrAdapter.addDate(model);
            totalMoney();
            return;
        }
        /** 1.扫到同一种物品  改变数量
         判断当前的可出库数量 和 现有的数量做比较
         a.库存不足  提示
         b.库存充足  数量+1  页面更新
         2.手动添加同一种物品
         a.通过 编辑添加的

         b.通过 搜索添加的
         *
         */

        int sum = 0;
        boolean isOver = false;
        Set<String> qrCodeSet = new HashSet<>();
        for (StockForOtherOutResponse s : mStockForOtherOutResponses) {
            List<GoodStockRequest> goodStockRequests = s.getqRVOList();
            for(GoodStockRequest request : goodStockRequests){
                if(qrCodeSet.contains(request.getQrCode())){
                    //提示
                    ToastUtil.toastWarning(activity, "添加失败,该物资已在本单中添加过了",-1);
                    return;
                }else{
                    qrCodeSet.add(request.getQrCode());
                }
            }
        }
        if(qrCodeSet.contains(model.getQrCode())){
            //提示
            ToastUtil.toastWarning(activity, "添加失败,该物资已在本单中添加过了",-1);
            return;
        }

        for (StockForOtherOutResponse s : mStockForOtherOutResponses) {
             sum++;
            if (!TextUtils.isEmpty(s.getQrCode()) && !TextUtils.isEmpty(model.getQrCode())) {
                Log.e("PXY", "isScan():" + model.isScan());
                Log.e("PXY", "isHand():" + model.isHand());
                if (s.getGoodsCode().equals(model.getGoodsCode()) && s.getPrice().equals(model.getPrice())
                        && s.getEffectiveDate().equals(model.getEffectiveDate()) && s.getBatchNumber().equals(model.getBatchNumber())) {
                    if (model.isScan() && s.isScan()) {
                        if (s.isFull(model)) {
                            s.setFactOut(s.getFactOut() + 1);
                            //页面更新
                            int position = update(s);
                            smoothMoveToPosition(position, layoutManager, rv);
                            mOtherOutLrAdapter.changeItem(s);
                            s.addqRVOList(addDVO(model));
                            totalMoney();
                            Log.e("PXY", "addDate: sao添加成功");
                            isOver = true;
                            break;
                        } else {
                            //提示 库存不足
                            ToastUtil.toastWarning(activity, "库存不足", -1);
                            isOver = true;
                            break;
                        }
                    }


                }
            } else if (model.isHand() && s.isHand()) {
                if(s.getGoodsCode().equals(model.getGoodsCode())){
                    if (s.isFull(model)) {
                        s.setFactOut(s.getFactOut() + model.getFactOut());
                        //页面更新
                        int position = update(s);
                        smoothMoveToPosition(position, layoutManager, rv);
                        mOtherOutLrAdapter.changeItem(s);
                        totalMoney();
                        Log.e("PXY", "addDate: shou添加成功");
                        isOver = true;
                        break;
                    } else {
                        //提示 库存不足
                        ToastUtil.toastWarning(activity, "库存不足", -1);
                        isOver = true;
                        break;
                    }
                }

            }



        }
        if (sum == mStockForOtherOutResponses.size() && !isOver) {
            if (model.isScan()) {
                model.addqRVOList(addDVO(model));
            }
            mStockForOtherOutResponses.add(0, model);
            mOtherOutLrAdapter.addDate(model);
            totalMoney();
        }
    }

    public int update(StockForOtherOutResponse s) {
        int position = mStockForOtherOutResponses.indexOf(s);
        mStockForOtherOutResponses.remove(position);
        mStockForOtherOutResponses.add(position, s);
        return position;
    }

    MediatorLiveData<StockForOtherOutResponse> edit = new MediatorLiveData<>();

    public void editItemStock(StockForOtherOutResponse recordResponse, int position) {
        recordResponse.setNotifyPosition(position);
        edit.setValue(recordResponse);
 //       totalMoney();
    }

    MediatorLiveData<StockForOtherOutResponse> delete = new MediatorLiveData<>();

    public void delete(StockForOtherOutResponse recordResponse, int position) {
        recordResponse.setDeletePosition(position);
        delete.setValue(recordResponse);
        totalMoney();
    }

    MediatorLiveData<LiveDataWrapper<String>> queryBarCode = new MediatorLiveData<>();
    public void formQuery(String barCode) {
        queryBarCode.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.isBarCode(barCode)
                .flatMap(new ResultDataParse<String>())
                .compose(new RxSchedulerTransformer<String>())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        queryBarCode.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        queryBarCode.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }
    /**
     * 总金额的计算
     *
     * @param model
     * @return
     */

    MediatorLiveData<Float> money = new MediatorLiveData<>();

    public void totalMoney() {
        float sumMoney = 0;
        if (null == mStockForOtherOutResponses) {
            mStockForOtherOutResponses = new ArrayList<>();
            money.setValue((float) 0);
            return;
        }

        // 单价 * 实际个数
        for (StockForOtherOutResponse s : mStockForOtherOutResponses) {
            float price = Float.parseFloat(s.getPriceDetail());
            sumMoney += price * s.getFactOut();
        }
        money.setValue(sumMoney);
//       return sumMoney;
    }
     MediatorLiveData<LiveDataWrapper<CheckerResponse>> userName = new MediatorLiveData<>();
    public void getUserName(){
        userName.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getStaffInfoByUserId()
                .flatMap(new ResultDataParse<CheckerResponse>())
                .compose(new RxSchedulerTransformer<CheckerResponse>())
                .subscribe(new Consumer<CheckerResponse>() {
                    @Override
                    public void accept(CheckerResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        userName.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        userName.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }


    public AutoEditItemAdapter mAutoEditAdapter =new AutoEditItemAdapter(2);
    MediatorLiveData<LiveDataWrapper<List<AssetsCardQueryCondition>>> patientInfo = new MediatorLiveData<>();
//    public void getPatientList(String info){
//        patientInfo.setValue(LiveDataWrapper.loading(null));
//        Disposable disposable = apiService.getPatientList(new StockForOtherOutRequest(info))
//                .flatMap(new ResultDataParse<List<AssetsCardQueryCondition>>())
//                .compose(new RxSchedulerTransformer<List<AssetsCardQueryCondition>>())
//                .subscribe(new Consumer<List<AssetsCardQueryCondition>>() {
//                    @Override
//                    public void accept(List<AssetsCardQueryCondition> taskListResponse) throws Exception {
//                        //获取的数据发送给Activity来使用
//                        patientInfo.setValue(LiveDataWrapper.success(taskListResponse));
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        //异常处理
//                        patientInfo.setValue(LiveDataWrapper.error(throwable, null));
//                    }
//                });
//        addDisposable(disposable);
//    }

    private MediatorLiveData<AssetsCardQueryCondition> click = new MediatorLiveData<>();
    public MediatorLiveData<AssetsCardQueryCondition> getClick() {
        return click;
    }
}
