package cn.wowjoy.office.materialinspection.applyfor.index;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.event.CountEvent;
import cn.wowjoy.office.data.event.StorageCodeEvent;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListResponse;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListSingle;
import cn.wowjoy.office.databinding.FragmentStockNoDoneBinding;
import cn.wowjoy.office.materialinspection.applyfor.StockApplyIndexViewModel;
import cn.wowjoy.office.materialinspection.applyfor.detail.StockApplyDetailActivity;
import cn.wowjoy.office.utils.ToastUtil;


public class StockNoDoneFragment extends NewBaseFragment<FragmentStockNoDoneBinding,StockApplyIndexViewModel> {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MDialog waitDialog ;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String storageCode;

    public StockNoDoneFragment() {
        // Required empty public constructor
    }
    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
        viewModel.setStorageCode(storageCode);
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StockNoDoneFragment newInstance(String param1, String param2) {
        StockNoDoneFragment doneFragment = new StockNoDoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        doneFragment.setArguments(args);
        return doneFragment;
    }

    @Override
    protected Class<StockApplyIndexViewModel> getViewModel() {
        return StockApplyIndexViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_stock_no_done;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        viewModel.setType(1);
    }

    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        binding.rvNodoneFragment.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvNodoneFragment.setAdapter(viewModel.noDoneAdapter);
        binding.rvNodoneFragment.addItemDecoration(new SimpleItemDecoration(getContext(),SimpleItemDecoration.VERTICAL_LIST));
        binding.emptyView.emptyContent.setText("没有待出库任务");
        binding.rvNodoneFragment.setEmptyView(binding.emptyView.getRoot());
        binding.emptyView.emptyContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel. getNoDoneApplyHeadList(true);
            }
        });
//        binding.rvNodoneFragment.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                //下拉刷新 请求服务器新数据
//                if ( !TextUtils.isEmpty(storageCode))
//                    viewModel.getNoDoneApplyHeadList(true);
//            }
//        });

        initObserve();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StorageCodeEvent event) {
        if ( !TextUtils.isEmpty(event.getStorageCode())){
            viewModel.getNoDoneApplyHeadList(true);
        }
    }
    private void initObserve(){
        viewModel.jump.observe(this, new Observer<StockApplyDemandHeadListSingle>() {
            @Override
            public void onChanged(@Nullable StockApplyDemandHeadListSingle stockApplyDemandHeadListSingle) {
                Intent mIntent = new Intent(getActivity(), StockApplyDetailActivity.class);
                if(null != stockApplyDemandHeadListSingle.getHeadCode()){
                    mIntent.putExtra("headCode",stockApplyDemandHeadListSingle.getHeadCode());
                    mIntent.putExtra("isDone",false);
                    startActivity(mIntent);
                }else{
                    ToastUtil.toastWarning(getActivity(),"服务器的锅，联系管理员",-1);
                }

            }
        });
        viewModel.getNodoneData().observe(this, new Observer<LiveDataWrapper<StockApplyDemandHeadListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StockApplyDemandHeadListResponse> StockApplyDemandHeadListResponseLiveDataWrapper) {
                switch (StockApplyDemandHeadListResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(getActivity());
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        EventBus.getDefault().post(new CountEvent(StockApplyDemandHeadListResponseLiveDataWrapper.data.getTotalCount()));
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(getActivity(), waitDialog);
                        }
                        binding.rvNodoneFragment.refreshComplete(1);
                        handleException(StockApplyDemandHeadListResponseLiveDataWrapper.error,true);
                        break;
                }
            }
        });
    }
    @Override
    protected void onResumeLazy() {
        super.onResumeLazy();
        if(!TextUtils.isEmpty(storageCode)){
            viewModel.getNoDoneApplyHeadList(true);
        }
//        RxBus.getInstance().tObservable(202, String.class).subscribe(new Consumer<String>() {
//            @Override
//            public void accept(String code) throws Exception {
//                viewModel.setStorageCode(code);
//                viewModel.getNoDoneApplyHeadList(true);
//            }
//        });
    }
    @Override
    protected void onStartLazy() {
        super.onStartLazy();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroyViewLazy() {
        super.onDestroyViewLazy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected ViewGroup getErrorViewRoot() {
        return binding.flError;
    }

    @Override
    protected void refreshError() {
        super.refreshError();
        viewModel.getNoDoneApplyHeadList(true);
    }
}
