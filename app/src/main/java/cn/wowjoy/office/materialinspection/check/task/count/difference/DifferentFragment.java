package cn.wowjoy.office.materialinspection.check.task.count.difference;


import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.CheckCountResponse;
import cn.wowjoy.office.databinding.FragmentDifferentBinding;
import cn.wowjoy.office.materialinspection.check.task.count.BarChartManager;
import cn.wowjoy.office.materialinspection.check.task.count.CheckCountViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DifferentFragment extends NewBaseFragment<FragmentDifferentBinding, CheckCountViewModel> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String taskId;
    private MDialog waitDialog;

    private List<Float> mMoneySum = new ArrayList<>();

    private List<Float> mNumberSum = new ArrayList<>();

    private BarChart mBarChartValue;

    private BarChart mBarChartCount;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DifferentFragment newInstance(String param1, String param2) {
        DifferentFragment differentFragment = new DifferentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        differentFragment.setArguments(args);
        return differentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected Class<CheckCountViewModel> getViewModel() {
        return CheckCountViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_different;
    }


    @Override
    protected void onCreateViewLazy(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        mBarChartValue = binding.chart1;
        mBarChartCount = binding.chart2;

        taskId = getActivity().getIntent().getStringExtra("taskId");
        viewModel.count(taskId);

        initObserver();
    }

    private int normalCount;
    private float normalSum;

    private int pankuiCount;
    private float pankuiSum;

    private int panyinCount;
    private float panyinSum;

    private void initObserver() {

        viewModel.getResponse()
                .observe(this, new Observer<LiveDataWrapper<CheckCountResponse>>() {
                    @Override
                    public void onChanged(@Nullable LiveDataWrapper<CheckCountResponse> countResponseLiveDataWrapper) {
                        switch (countResponseLiveDataWrapper.status) {
                            case LOADING:
                                waitDialog = CreateDialog.waitingDialog(getActivity());
                                break;
                            case SUCCESS:
                                waitDialog.dismiss();
                                Map<String, String> normal = countResponseLiveDataWrapper.data.getNormal();
                                Map<String, String> pankui = countResponseLiveDataWrapper.data.getPankui();
                                Map<String, String> panyin = countResponseLiveDataWrapper.data.getPanyin();
                                Set<String> normalSet = normal.keySet();
                                for (String key : normalSet) {
                                    if ("count".equals(key)) {
                                        normalCount = Integer.parseInt(normal.get(key));
                                    }
                                    if ("sumOriginalValue".equals(key)) {
                                        normalSum = Float.parseFloat(normal.get(key));
                                    }
                                }
                                Set<String> pankuiSet = pankui.keySet();
                                for (String key : pankuiSet) {
                                    if ("count".equals(key)) {
                                        pankuiCount = Integer.parseInt(pankui.get(key));
                                    }
                                    if ("sumOriginalValue".equals(key)) {
                                        pankuiSum = Float.parseFloat(pankui.get(key));
                                    }
                                }
                                Set<String> panyinSet = normal.keySet();
                                for (String key : panyinSet) {
                                    if ("count".equals(key)) {
                                        panyinCount = Integer.parseInt(panyin.get(key));
                                    }
                                    if ("sumOriginalValue".equals(key)) {
                                        panyinSum = Float.parseFloat(panyin.get(key));
                                    }
                                }

                                BarChartManager barChartManager1 = new BarChartManager(binding.chart1);
                                BarChartManager barChartManager2 = new BarChartManager(binding.chart2);
                                //设置x轴的数据
                                ArrayList<Float> xValues = new ArrayList<>();
                                xValues.add((float) 0);
                                xValues.add((float) 1);
                                xValues.add((float) 2);

                                //设置y轴的数据()
                                List<Float> yValues = new ArrayList<>();
                                yValues.add(normalSum);
                                yValues.add(panyinSum);
                                yValues.add(pankuiSum);


                                //颜色集合
                                List<Integer> colours = new ArrayList<>();
                                colours.add(getResources().getColor(R.color.chart_color_normal));
                                colours.add(getResources().getColor(R.color.chart_color_panying));
                                colours.add(getResources().getColor(R.color.chart_color_pankui));

                                //线的名字集合
                                List<String> names = new ArrayList<>();
                                names.add("资产原值");
                                names.add("数量");
                                names.add("数量2");
                                String[] strs = new String[]{"正常", "盘盈", "盘亏"};
                                //创建多条折线的图表
                                barChartManager1.showBarChart(xValues, yValues, names.get(0), colours, strs);
                                barChartManager1.setDescription("");

                                ArrayList<Float> xValuesCount = new ArrayList<>();
                                xValuesCount.add((float) 0);
                                xValuesCount.add((float) 1);
                                xValuesCount.add((float) 2);

                                //设置y轴的数据()
                                List<Float> yValuesCount = new ArrayList<>();
                                yValuesCount.add((float)(Math.round(normalCount*10))/10);
                                yValuesCount.add((float)(Math.round(panyinCount*10))/10);
                                yValuesCount.add((float)(Math.round(pankuiCount*10))/10);


                                barChartManager2.showBarChart(xValuesCount, yValuesCount, names.get(0), colours, strs);
                                barChartManager2.setDescription("");
                                break;
                            case ERROR:
                                waitDialog.dismiss();
                                Throwable throwable = countResponseLiveDataWrapper.error;
                                handleException(throwable, false);
                                getActivity().finish();
                                break;
                        }
                    }
                });
    }

}
