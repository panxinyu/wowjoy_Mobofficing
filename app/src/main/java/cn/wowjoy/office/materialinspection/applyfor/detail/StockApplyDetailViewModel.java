package cn.wowjoy.office.materialinspection.applyfor.detail;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.StockApplyDetailAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.data.response.StockApplyDemandHeadDetailResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/4/17.
 */

public class StockApplyDetailViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;

    //   public BGABindingRecyclerViewAdapter<ApplyDemandHeadDetailItem, ItemRvStockApplySingleDetailBinding> nolinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_stock_apply_single_detail);
    public boolean isDone;
    public StockApplyDetailAdapter mAdapter;
    public LRecyclerViewAdapter lrAdapter;

    public StockApplyDemandHeadDetailResponse mResponse;
    public List<ApplyDemandHeadDetailItem> mDetailItems;
    private MediatorLiveData<LiveDataWrapper<StockApplyDemandHeadDetailResponse>> data = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<StockApplyDemandHeadDetailResponse>> getData() {
        return data;
    }

    public void setData(List<ApplyDemandHeadDetailItem> data) {
        mDetailItems = data;
        mAdapter.setItemModels(data);
        //    refreshComplete();
    }

    @Inject
    public StockApplyDetailViewModel(@NonNull NewMainApplication application) {
        super(application);
        boolean isDone = application.getTopActivity().getIntent().getBooleanExtra("isDone", false);
        if (null == mAdapter) {
            mAdapter = new StockApplyDetailAdapter(isDone);
            lrAdapter = new LRecyclerViewAdapter(mAdapter);
        }
        mAdapter.setEventListener(this);
    }
//    MediatorLiveData<ApplyDemandHeadDetailItem> jump = new MediatorLiveData<>();
//   public void  jump(ApplyDemandHeadDetailItem item){
//       jump.setValue(item);
//   }
    @Override
    public void onCreateViewModel() {

    }

    //获取未完成任务的List
    public void getListInfo(String headCode) {
        data.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getDemandHead(headCode)
                .flatMap(new ResultDataParse<StockApplyDemandHeadDetailResponse>())
                .compose(new RxSchedulerTransformer<StockApplyDemandHeadDetailResponse>())
                .subscribe(new Consumer<StockApplyDemandHeadDetailResponse>() {
                    @Override
                    public void accept(StockApplyDemandHeadDetailResponse taskListResponse) throws Exception {
                        data.setValue(LiveDataWrapper.success(taskListResponse));
//                    setNoDoneData(taskListResponse.getResultList());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        data.setValue(LiveDataWrapper.error(throwable, null));
//                        mNoDoneFragment.handleException(throwable,true);
//                        binding.rvNodoneFragment.refreshComplete(1);

                    }
                });
        addDisposable(disposable);
    }

    public MediatorLiveData<String> selectCount = new MediatorLiveData<>();

    public void isSelectNum(int num) {
        selectCount.setValue("" + num);
    }


}
