package cn.wowjoy.office.materialinspection.stockout.hand;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.keyborad.CustomBaseKeyboard;
import cn.wowjoy.office.common.widget.keyborad.CustomKeyboardManager;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.databinding.ActivityStockHandWriteBinding;
import cn.wowjoy.office.utils.ToastUtil;

public class StockHandWriteActivity extends NewBaseActivity<ActivityStockHandWriteBinding, StockHandWriteViewModel> implements View.OnClickListener, CustomBaseKeyboard.OnTextChangeListener {
   private StockForOtherOutResponse response;

    private CustomKeyboardManager customKeyboardManager;
    private CustomBaseKeyboard priceKeyboard;
    @Override
    protected Class<StockHandWriteViewModel> getViewModel() {
        return StockHandWriteViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_hand_write;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        response = (StockForOtherOutResponse) getIntent().getSerializableExtra("model");

        binding.selectTitle.titleTextTv.setText("输入出库数量");
        binding.selectTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.selectTitle.titleBackTv.setText("");
        binding.selectTitle.titleBackLl.setOnClickListener(this);
        binding.selectTitle.titleMenuConfirm.setText("确认");
        binding.selectTitle.titleMenuConfirm.setVisibility(View.VISIBLE);
        binding.selectTitle.titleMenuConfirm.setOnClickListener(this);

  //      binding.numStockOut.addTextChangedListener(new MyTextWatcher(binding.tvLimitCardName, binding.numStockOut, 16, StockHandWriteActivity.this));
        customKeyboardManager = new CustomKeyboardManager(this);
        priceKeyboard = new CustomBaseKeyboard(this, R.xml.stock_price_num_keyboard) {
            @Override
            public boolean handleSpecialKey(EditText etCurrent, int primaryCode) {
                return false;
            }
        };
        binding.numStockOut.setOnClickListener(this);
        priceKeyboard.setOnTextChangeListener(this);
        customKeyboardManager.setShowUnderView(binding.rlZichan);
        customKeyboardManager.attachTo(binding.numStockOut, priceKeyboard);

    }

    @Override
    public void onBackPressed() {
        setResult(Constants.RESULT_FAIL);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back_ll:
               setResult(Constants.RESULT_FAIL);
               finish();
                break;
            case R.id.title_menu_confirm:
                //判断数字框  是否输入
                if(TextUtils.isEmpty(binding.numStockOut.getText().toString().trim())){
                    ToastUtil.toastWarning(StockHandWriteActivity.this, "请输入数量", -1);
                    return;
                }
                float now = Float.parseFloat(binding.numStockOut.getText().toString().trim());
                float total =Float.parseFloat(response.getAmountDetail()) ;
                if(now > total){
                    ToastUtil.toastWarning(StockHandWriteActivity.this, "当前库存"+total+",输入的数量超出库存", -1);
                    return;
                }
                Intent intent =new Intent();
                intent.putExtra("num",binding.numStockOut.getText().toString().trim());
                intent.putExtra("model",response);
                setResult(Constants.RESULT_OK,intent);
                finish();
                break;
            case R.id.num_stock_out:
                customKeyboardManager.focus();
                break;
        }

    }
    @Override
    public void onChange(StringBuilder sb) {
        if(!TextUtils.isEmpty(sb.toString().trim()) && sb.toString().contains(".")){
            if(sb.toString().substring(sb.toString().indexOf(".")+1,sb.toString().length()).length() > 2){
                return;
            }
        }
        if (TextUtils.isEmpty(sb.toString().trim())){
            binding.tvLimitCardName.setSelected(false);
            binding.tvLimitCardName.setText("(0/16)");
        } else if (sb.toString().contains(".")){
            if (sb.toString().contains("-")){
                if (sb.toString().trim().length() >= 18){
                    binding.tvLimitCardName.setSelected(true);
                    binding.tvLimitCardName.setText("(16/16)");
                } else {
                    binding.tvLimitCardName.setSelected(false);
                    binding.tvLimitCardName.setText("("+ (sb.toString().trim().length() - 2) +"/16)");
                }
            } else {
                if (sb.toString().trim().length() >= 17){
                    binding.tvLimitCardName.setSelected(true);
                    binding.tvLimitCardName.setText("(16/16)");
                } else {
                    binding.tvLimitCardName.setSelected(false);
                    binding.tvLimitCardName.setText("("+ (sb.toString().trim().length() - 1) +"/16)");
                }
            }

        } else if (sb.toString().contains("-")){
            if (sb.toString().trim().length() >= 17){
                binding.tvLimitCardName.setSelected(true);
                binding.tvLimitCardName.setText("(16/16)");
            } else {
                binding.tvLimitCardName.setSelected(false);
                binding.tvLimitCardName.setText("("+ (sb.toString().trim().length() - 1) +"/16)");
            }
        } else {
            binding.tvLimitCardName.setSelected(false);
            binding.tvLimitCardName.setText("("+ (sb.toString().trim().length()) +"/16)");
        }
    }
}
