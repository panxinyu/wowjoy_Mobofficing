package cn.wowjoy.office.materialinspection.stockout.hand;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.databinding.ActivityStockHandAddBinding;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class StockHandAddActivity extends NewBaseActivity<ActivityStockHandAddBinding,StockHandAddViewModel> {
    private MDialog waitDialog;
    private String key;

    private String storageCode;
    private int type;
    private String  info;
    @Override
    protected Class<StockHandAddViewModel> getViewModel() {
        return StockHandAddViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_hand_add;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        type = getIntent().getIntExtra("type", 1);
        info = getIntent().getStringExtra("info");
        storageCode = getIntent().getStringExtra("storageCode");

        binding.selectTitle.titleTextTv.setText("手动添加物料");
        binding.selectTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.selectTitle.titleBackTv.setText("");
        binding.selectTitle.titleBackLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Constants.RESULT_FAIL);
                finish();
            }
        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(viewModel.searchAdapter);
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setLoadMoreEnabled(false);
        binding.recyclerView.setPullRefreshEnabled(false);
         addKeyboard();
         initListener();
         initObserve();
    }

    private void initObserve() {
        viewModel.getSearchData().observe(this, new Observer<LiveDataWrapper<List<StockForOtherOutResponse>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<StockForOtherOutResponse>> InventoryTaskListResponseLiveDataWrapper) {
                switch (InventoryTaskListResponseLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(StockHandAddActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(StockHandAddActivity.this);
                        if (null != InventoryTaskListResponseLiveDataWrapper.data) {
//                            viewModel.judgeDone(InventoryTaskListResponseLiveDataWrapper.data.getResultList());
                            viewModel.adapter.setPopuModels(InventoryTaskListResponseLiveDataWrapper.data,key);
                            hideSoftInput();
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(StockHandAddActivity.this);
                        handleException(InventoryTaskListResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });

        viewModel.getJump().observe(this, new Observer<StockForOtherOutResponse>() {
            @Override
            public void onChanged(@Nullable StockForOtherOutResponse response) {
                //TODO:  点击条目去输入数量   并且输完销毁
                Intent intent = new Intent(StockHandAddActivity.this,StockHandWriteActivity.class);
                intent.putExtra("model",response);
                startActivityForResult(intent, Constants.REQUEST_CODE);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            // 选择去向的结果
            case Constants.REQUEST_CODE:
                switch (resultCode) {
                    case Constants.RESULT_OK:
                        //填写了数量  并且要和 实体一起传回到首页
                        String num = data.getStringExtra("num");
                        StockForOtherOutResponse response = (StockForOtherOutResponse) data.getSerializableExtra("model");
                        Intent intent =new Intent();
                        intent.putExtra("num",num);
                        intent.putExtra("model",response);
                        setResult(Constants.RESULT_OK,intent);
                        finish();
                        break;
                    case Constants.RESULT_FAIL:
                        //back 回来的   没写传结果回来   就什么都不做

                        break;
                }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(type == 2){
            binding.searchEditText.setText(info);
            binding.searchEditText.setSelection(info.length());

            viewModel.getSearchListInfo(info,storageCode);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Constants.RESULT_FAIL);
        finish();
        super.onBackPressed();
    }

    /**
     * 获取焦点，调起软键盘
     */
    private void addKeyboard() {
        binding.searchEditText.setFocusable(true);
        binding.searchEditText.setFocusableInTouchMode(true);
        binding.searchEditText.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
    private void initListener() {
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();

                key = binding.searchEditText.getText().toString().trim();
                viewModel.getSearchListInfo(key,storageCode);

//                materialListPresenter.getInfo();
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(binding.searchEditText.getText().toString())){
                    binding.delete.setVisibility(View.VISIBLE);
                } else {
                    binding.delete.setVisibility(View.GONE);
                    key = null;
                }

            }
        });
        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchEditText.setText("");
                binding.delete.setVisibility(View.GONE);
                key = null;
            }
        });
        viewModel.searchAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //TODO: 已完成与未完成的跳转
                if (null != viewModel.datas) {
                    //TODO:  点击条目去输入数量   并且输完销毁
                    Intent intent = new Intent(StockHandAddActivity.this,StockHandWriteActivity.class);
                    intent.putExtra("model",viewModel.datas.get(position));
                    startActivityForResult(intent, Constants.REQUEST_CODE);
                }
            }
        });
    }

}
