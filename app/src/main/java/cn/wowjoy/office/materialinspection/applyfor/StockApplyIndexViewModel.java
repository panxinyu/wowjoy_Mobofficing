package cn.wowjoy.office.materialinspection.applyfor;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.ApplyDemandItemAdapter;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.StockApplyDemandHeadListRequest;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListResponse;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListSingle;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/4/12.
 */

public class StockApplyIndexViewModel extends NewBaseViewModel {

    private String storageCode;
    private int type;

    @Inject
    public StockApplyIndexViewModel(@NonNull NewMainApplication application) {
        super(application);
        wlinnerAdapter.setHandler(this);
        nolinnerAdapter.setHandler(this);
    }

    @Override
    public void onCreateViewModel() {

    }

    @Override
    public void loadData(boolean ref) {
        super.loadData(ref);
        if(type == 1){
            getNoDoneApplyHeadList(ref);
        }else if(type == 2){
            getDoneApplyHeadList(ref);
        }
    }

    @Inject
    ApiService apiService;

    private int startIndex = 0;
    private int pageSize = 10;

    public MediatorLiveData<LiveDataWrapper<StockApplyDemandHeadListResponse>> getNodoneData() {
        return nodoneData;
    }

    public MediatorLiveData<LiveDataWrapper<StockApplyDemandHeadListResponse>> getDoneData() {
        return doneData;
    }

    //已完成的Task
    public ArrayList<StockApplyDemandHeadListSingle> mDonedatas;
    ApplyDemandItemAdapter wlinnerAdapter = new ApplyDemandItemAdapter();
    public LRecyclerViewAdapter doneAdapter = new LRecyclerViewAdapter(wlinnerAdapter);
    //已完成数据
    MediatorLiveData<LiveDataWrapper<StockApplyDemandHeadListResponse>> doneData = new MediatorLiveData<>();


    //未完成的Task
    public ArrayList<StockApplyDemandHeadListSingle> mNoDonedatas;
    ApplyDemandItemAdapter nolinnerAdapter = new ApplyDemandItemAdapter();
    public LRecyclerViewAdapter noDoneAdapter = new LRecyclerViewAdapter(nolinnerAdapter);
    //未完成数据
    MediatorLiveData<LiveDataWrapper<StockApplyDemandHeadListResponse>> nodoneData = new MediatorLiveData<>();


    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setDoneData(boolean isRefresh, List<StockApplyDemandHeadListSingle> data) {
        if (isRefresh) {
            wlinnerAdapter.refresh(data);
        } else {
            if (data != null)
                wlinnerAdapter.loadMore(data);
        }
        doneAdapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty())) {
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview, null, false);
            doneAdapter.addFooterView(view);

        }
        doneAdapter.notifyDataSetChanged();
        refreshComplete();
    }

    public void setNoDoneData(boolean isRefresh, List<StockApplyDemandHeadListSingle> data) {

        if (isRefresh) {
            nolinnerAdapter.refresh(data);
        } else {
            if (data != null)
                nolinnerAdapter.loadMore(data);
        }
        noDoneAdapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty())) {
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview, null, false);
            noDoneAdapter.addFooterView(view);

        }
        noDoneAdapter.notifyDataSetChanged();
        refreshComplete();
    }

    public void getNoDoneApplyHeadList(boolean isRefresh) {
        if (isRefresh) {
            startIndex = 0;
        } else {
            ++startIndex;
        }
        nodoneData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getDemandHeadList(new StockApplyDemandHeadListRequest(storageCode, "", "y", startIndex, pageSize))
                .flatMap(new ResultDataParse<StockApplyDemandHeadListResponse>())
                .compose(new RxSchedulerTransformer<StockApplyDemandHeadListResponse>())
                .subscribe(new Consumer<StockApplyDemandHeadListResponse>() {
                    @Override
                    public void accept(StockApplyDemandHeadListResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        nodoneData.setValue(LiveDataWrapper.success(taskListResponse));
                        if (taskListResponse == null) {
                            setNoDoneData(isRefresh, null);
                        } else {
                            setNoDoneData(isRefresh, taskListResponse.getList());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        nodoneData.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }

    public void getDoneApplyHeadList(boolean isRefresh) {
        if (isRefresh) {
            startIndex = 0;
        } else {
            ++startIndex;
        }
        doneData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getDemandHeadList(new StockApplyDemandHeadListRequest(storageCode, "y", "", startIndex, pageSize))
                .flatMap(new ResultDataParse<StockApplyDemandHeadListResponse>())
                .compose(new RxSchedulerTransformer<StockApplyDemandHeadListResponse>())
                .subscribe(new Consumer<StockApplyDemandHeadListResponse>() {
                    @Override
                    public void accept(StockApplyDemandHeadListResponse taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        doneData.setValue(LiveDataWrapper.success(taskListResponse));
                        if (taskListResponse == null) {
                            setDoneData(isRefresh, null);
                        } else {
                            setDoneData(isRefresh, taskListResponse.getList());
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        doneData.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }

    public MediatorLiveData<StockApplyDemandHeadListSingle> jump = new MediatorLiveData<>();

    public void jump(StockApplyDemandHeadListSingle single) {
        jump.setValue(single);
    }

    private MediatorLiveData<LiveDataWrapper<List<PopuModel>>> userId = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<List<PopuModel>>> getUserId() {
        return userId;
    }

    public void getStorageUserId() {
        userId.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getStock()
                .flatMap(new ResultDataParse<List<PopuModel>>())
                .compose(new RxSchedulerTransformer<List<PopuModel>>())
                .subscribe(new Consumer<List<PopuModel>>() {
                    @Override
                    public void accept(List<PopuModel> taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        userId.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        userId.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }


}
