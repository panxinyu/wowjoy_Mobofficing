package cn.wowjoy.office.materialinspection.stockout.otherout.edit;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Administrator on 2018/4/15.
 */

public class StockOutHandEditViewModel extends NewBaseViewModel {

    @Inject
    public StockOutHandEditViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
