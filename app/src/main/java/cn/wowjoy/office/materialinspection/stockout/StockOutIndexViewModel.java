package cn.wowjoy.office.materialinspection.stockout;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.ItemStockRvIndexAdapter;
import cn.wowjoy.office.common.adapter.MenuAdapter;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.response.InspectTaskDtlVO;
import cn.wowjoy.office.data.response.InspectionTaskControllerResponse;
import cn.wowjoy.office.data.response.SingleTaskController;

/**
 * Created by Administrator on 2018/4/10.
 */

public class StockOutIndexViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;
    private List<PopuModel> popuModels;
    private List<PopuModel> popuModels2;
    public MenuAdapter allKindMenu_Adapter = new MenuAdapter();
    public MenuAdapter allStateMenu_Adapter = new MenuAdapter();

    // 筛选的adapter
    public ItemStockRvIndexAdapter mItemRvDetailAdapter = new ItemStockRvIndexAdapter();
    public LRecyclerViewAdapter wladapter = new LRecyclerViewAdapter(mItemRvDetailAdapter);


    private MediatorLiveData<LiveDataWrapper<InspectionTaskControllerResponse>> response = new MediatorLiveData<>();
    private MediatorLiveData<Boolean> load = new MediatorLiveData<>();
    private MediatorLiveData<InspectTaskDtlVO> jump = new MediatorLiveData<>();

    public void createMenu(){
        if (null == popuModels)
            popuModels = new ArrayList<>();
        if (popuModels.isEmpty()){
            popuModels.add(new PopuModel("全部类型",true,""));
            popuModels.add(new PopuModel("物料出库",false,""));
            popuModels.add(new PopuModel("退货出库",false,""));
            popuModels.add(new PopuModel("出库分红",false,""));
            popuModels.add(new PopuModel("盘亏出库",false,""));
            popuModels.add(new PopuModel("其他出库",false,""));
            popuModels.add(new PopuModel("调拨出库",false,""));
        }
        allKindMenu_Adapter.setPopuModels(popuModels);
        if (null == popuModels2)
            popuModels2 = new ArrayList<>();
        if (popuModels2.isEmpty()){
            popuModels2.add(new PopuModel("全部状态",true,""));
            popuModels2.add(new PopuModel("等待审批",false,""));
            popuModels2.add(new PopuModel("审批驳回",false,""));
            popuModels2.add(new PopuModel("已经作废",false,""));
            popuModels2.add(new PopuModel("待出库确认",false,""));
            popuModels2.add(new PopuModel("待收货确认",false,""));
            popuModels2.add(new PopuModel("出库完成",false,""));
        }
        allStateMenu_Adapter.setPopuModels(popuModels2);
    }
   private  MediatorLiveData<PopuModel> menu_allKind_ld = new MediatorLiveData<>();
    private  MediatorLiveData<PopuModel> menu_allState_ld = new MediatorLiveData<>();
    public MediatorLiveData<PopuModel> getMenuAllKindLD() {
        return menu_allKind_ld;
    }
    public MediatorLiveData<PopuModel> getMenuAllStateLD() {
        return menu_allState_ld;
    }
    public void sortKind(PopuModel popuModel){
        menu_allKind_ld.setValue(popuModel);
    }
    public void sortState(PopuModel popuModel){
        menu_allState_ld.setValue(popuModel);
    }
   @Inject
    public StockOutIndexViewModel(@NonNull NewMainApplication application) {
        super(application);
       allKindMenu_Adapter.setmEventListener(this);
       mItemRvDetailAdapter.setItemEventHandler(this);
    }
    public SingleTaskController single;
    public ArrayList<InspectTaskDtlVO> datas;
    public void setWData(List<InspectTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        mItemRvDetailAdapter.setData(datas);
        refreshComplete();
    }
    public void refreshData(boolean isRefresh ,List<InspectTaskDtlVO> data) {
        if (null == datas)
            datas = new ArrayList<>();
        datas.clear();
        datas.addAll(data);
        if (isRefresh) {
            mItemRvDetailAdapter.refresh(data);
        } else {
            mItemRvDetailAdapter.loadMore(data);
        }
        wladapter.removeFooterView();
        if (!isRefresh && (null == data || data.isEmpty() )){
            LayoutInflater inflater = LayoutInflater.from(getApplication().getApplicationContext());
            View view = inflater.inflate(R.layout.nodata_footview,null,false);
            wladapter.addFooterView(view);
        }
        wladapter.notifyDataSetChanged();
        refreshComplete();
    }
    private final int pageSize = 10;
    private int pageIndex = 0;
//    public void getInspectTaskControllerByPageSize(String taskId,String billInfo,String isExist,String notInspect,String appearanceStatus,String assetsStatus,boolean isRefresh,String filter){
////        this.taskId = taskId;
////        this.filter = filter;
//        if (isRefresh){
//            pageIndex = 0;
//        } else {
//            ++pageIndex;
//        }
//        response.setValue(LiveDataWrapper.loading(null));
//        Disposable disposable = apiService.getInspectTaskControllerByPageSize(taskId,billInfo,isExist,notInspect,appearanceStatus,assetsStatus,pageIndex,pageSize)
//                .flatMap(new ResultDataParse<>())
//                .compose(new RxSchedulerTransformer<>())
//                .subscribe((InspectionTaskControllerResponse taskListResponse) -> {
//                    //TODO: 设置界面上的详细数据 着重区分下！！！ 已完成和未完成显示的页面区别
////                    if(null != waitDialog){
////                        CreateDialog.dismiss(mPatrolTaskActivity, waitDialog);
////                    }
//                    response.setValue(LiveDataWrapper.success(taskListResponse));
//                    this.single = taskListResponse.getSingle();
//                    refreshData(isRefresh ,taskListResponse.getSingle().getDtlListPDA());
////                    mPatrolTaskActivity.updateUI(taskListResponse,filter);
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        //异常处理
//                        response.setValue(LiveDataWrapper.error(throwable,null));
////                        if(null != waitDialog){
////                            CreateDialog.dismiss(mPatrolTaskActivity, waitDialog);
////                        }
////                        mPatrolTaskActivity.handleException(throwable,true);
//                    }
//                });
//        addDisposable(disposable);
//    }
    @Override
    public void onCreateViewModel() {


    }
}
