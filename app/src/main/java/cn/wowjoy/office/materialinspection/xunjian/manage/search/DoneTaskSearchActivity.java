package cn.wowjoy.office.materialinspection.xunjian.manage.search;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.github.jdsjlzx.interfaces.OnItemClickListener;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.response.InspectionTotalResponse;
import cn.wowjoy.office.databinding.ActivityDoneTaskSearchBinding;
import cn.wowjoy.office.materialinspection.xunjian.task.PatrolTaskActivity;

public class DoneTaskSearchActivity extends NewBaseActivity<ActivityDoneTaskSearchBinding, DoneTaskSearchViewModel> {

    private MDialog waitDialog;

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);

        setSupportActionBar(binding.toolbar);
        addKeyboard();
        initView();
        initListener();
        initObserve();
    }

    private void initObserve() {
        viewModel.getSearchData().observe(this, new Observer<LiveDataWrapper<InspectionTotalResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<InspectionTotalResponse> inspectionTotalResponseLiveDataWrapper) {
                switch (inspectionTotalResponseLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(DoneTaskSearchActivity.this);
                        break;
                    case SUCCESS:
                        waitDialog.dismiss();
                        if (null != inspectionTotalResponseLiveDataWrapper.data) {
                            viewModel.judgeDone(inspectionTotalResponseLiveDataWrapper.data.getResultList());
                            viewModel.setWData(inspectionTotalResponseLiveDataWrapper.data.getResultList());
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(DoneTaskSearchActivity.this, waitDialog);
                        }
                        handleException(inspectionTotalResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });
    }

    private void initView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.searchAdapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setLoadMoreEnabled(false);
    }

    private void initListener() {
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                hideSoftInput();
                String key = binding.searchEditText.getText().toString().trim();
                // TODO：联网模糊查询
                if (!key.isEmpty()) {
                    //  搜索清空的时候
                    viewModel.getSearchListInfo(key.trim());
                    return true;
                }
                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO：联网模糊查询
                if (s.toString().isEmpty()) {
                    //  搜索清空的时候
                    viewModel.clearData();
                }
            }
        });
        viewModel.searchAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //TODO: 已完成与未完成的跳转
                if (null != viewModel.datas) {
                    Intent mIntent = new Intent(DoneTaskSearchActivity.this, PatrolTaskActivity.class);
                    mIntent.putExtra("taskId", viewModel.datas.get(position).getId());
                    mIntent.putExtra("isDone", viewModel.datas.get(position).isDone());
                    startActivity(mIntent);
                    finish();
                }
            }
        });
    }

    /**
     * 获取焦点，调起软键盘
     */
    private void addKeyboard() {
        binding.searchEditText.setFocusable(true);
        binding.searchEditText.setFocusableInTouchMode(true);
        binding.searchEditText.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_done_task_search;
    }

    @Override
    protected Class<DoneTaskSearchViewModel> getViewModel() {
        return DoneTaskSearchViewModel.class;
    }
}
