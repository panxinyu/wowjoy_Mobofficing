package cn.wowjoy.office.materialinspection.stockout.search;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.StockSelectAdapter;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.request.StockForOtherOutRequest;
import cn.wowjoy.office.data.response.StockSelectResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/5/18.
 */

public class SelectPatientViewModel extends NewBaseViewModel {

    @Inject
    public SelectPatientViewModel(@NonNull NewMainApplication application) {
        super(application);
        mStockSelectAdapter.setmEventListener(this);
    }
    @Inject
    ApiService apiService;

  

//    public ArrayList<StockSelectResponse.SelectResponse> mResponses;
//    public BGABindingRecyclerViewAdapter<StockSelectResponse.SelectResponse, ItemRvCheckNodoneBinding> nolinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_check_nodone);
    public StockSelectAdapter mStockSelectAdapter =  new StockSelectAdapter(2);
    public LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(mStockSelectAdapter);
    @Override
    public void onCreateViewModel() {

    }
    MediatorLiveData<StockSelectResponse>  meun = new MediatorLiveData<>();




    MediatorLiveData<LiveDataWrapper<List<StockSelectResponse>>> search = new MediatorLiveData<>();
    public MediatorLiveData<LiveDataWrapper<List<StockSelectResponse>>> getSearch() {
        return search;
    }
    public void getPatientList(String info){
        search.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getPatientList(new StockForOtherOutRequest(info))
                .flatMap(new ResultDataParse<List<StockSelectResponse>>())
                .compose(new RxSchedulerTransformer<List<StockSelectResponse>>())
                .subscribe(new Consumer<List<StockSelectResponse>>() {
                    @Override
                    public void accept(List<StockSelectResponse> taskListResponse) throws Exception {
                        //获取的数据发送给Activity来使用
                        search.setValue(LiveDataWrapper.success(taskListResponse));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        search.setValue(LiveDataWrapper.error(throwable, null));
                    }
                });
        addDisposable(disposable);
    }
    public void jump(StockSelectResponse popuModel){
        meun.setValue(popuModel);
    }
}
