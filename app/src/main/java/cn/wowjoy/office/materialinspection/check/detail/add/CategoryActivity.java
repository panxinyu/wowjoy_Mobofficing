package cn.wowjoy.office.materialinspection.check.detail.add;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.response.CheckCategoryListResponse;
import cn.wowjoy.office.databinding.ActivityCategoryBinding;

public class CategoryActivity extends NewBaseActivity<ActivityCategoryBinding,CategoryViewModel> implements View.OnClickListener {


    private MDialog waitDialog;
    String name = "";
    @Override
    protected Class<CategoryViewModel> getViewModel() {
        return CategoryViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_category;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);


         name = getIntent().getStringExtra("name");
        binding.categoryTitle.titleTextTv.setText("请选择");
        binding.categoryTitle.titleBackLl.setVisibility(View.VISIBLE);
        binding.categoryTitle.titleBackTv.setText("");
        binding.categoryTitle.titleBackLl.setOnClickListener(this);

        binding.lrCategory.setLayoutManager(new LinearLayoutManager(this));
        binding.lrCategory.setAdapter(viewModel.mCategoryListAdapter);
//        binding.lrCategory.setEmptyView(binding.emptyView.getRoot());
//        binding.lrCategory.setLoadMoreEnabled(false);
//        binding.lrCategory.setPullRefreshEnabled(false);

       initObserve();
    }

    private void initObserve() {
        viewModel.getResponse().observe(this, new Observer<LiveDataWrapper<CheckCategoryListResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<CheckCategoryListResponse> checkCategoryListResponseLiveDataWrapper) {
                switch (checkCategoryListResponseLiveDataWrapper.status){
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(CategoryActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(CategoryActivity.this, waitDialog);
                        }
                        if(null !=  checkCategoryListResponseLiveDataWrapper.data ){
                            List<CheckCategoryListResponse.CategoryName> categoryNameList = checkCategoryListResponseLiveDataWrapper.data.getCategoryNameList();
                            if (!"".equals(name)){
                               for(CheckCategoryListResponse.CategoryName c: categoryNameList){
                                   if(name.equals(c.getCategoryName())){
                                       c.setSelected(true);
                                   }
                               }
                            }
                            viewModel.mCategoryListAdapter.setPopuModels(categoryNameList);

                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(CategoryActivity.this, waitDialog);
                        }
                        handleException(checkCategoryListResponseLiveDataWrapper.error, true);
                        break;
                }
            }
        });

        viewModel.meun.observe(this, new Observer<CheckCategoryListResponse.CategoryName>() {
            @Override
            public void onChanged(@Nullable CheckCategoryListResponse.CategoryName popuModel) {
                sort(popuModel);
            }
        });
    }

    private void sort(CheckCategoryListResponse.CategoryName popuModel) {
        Intent intent = new Intent();
        intent.putExtra("name",popuModel.getCategoryName());
        intent.putExtra("categoryId",popuModel.getId());
        setResult(Constants.RESULT_OK,intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.getList();
    }

    @Override
    public void onBackPressed() {
        setResult(Constants.RESULT_FAIL);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.title_back_ll:
                setResult(Constants.RESULT_FAIL);
                finish();
                break;
        }
    }
}
