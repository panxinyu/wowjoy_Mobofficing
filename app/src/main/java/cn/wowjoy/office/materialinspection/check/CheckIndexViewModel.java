package cn.wowjoy.office.materialinspection.check;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingRecyclerViewAdapter;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.InventoryTaskListResponse;
import cn.wowjoy.office.data.response.InventoryTotalItemInfo;
import cn.wowjoy.office.databinding.ItemRvCheckDoneBinding;
import cn.wowjoy.office.databinding.ItemRvCheckNodoneBinding;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2017/12/27.
 */

public class CheckIndexViewModel extends NewBaseViewModel {

    @Inject
    public CheckIndexViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
    @Inject
    ApiService apiService;
    //已完成数据
    MediatorLiveData<LiveDataWrapper<InventoryTaskListResponse>> doneData = new MediatorLiveData<>();
    //未完成数据
    MediatorLiveData<LiveDataWrapper<InventoryTaskListResponse>> nodoneData = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<InventoryTaskListResponse>> getNodoneData() {
        return nodoneData;
    }

    public MediatorLiveData<LiveDataWrapper<InventoryTaskListResponse>> getDoneData() {
        return doneData;
    }

    //已完成的Task
    public ArrayList<InventoryTotalItemInfo> mDonedatas;
    public BGABindingRecyclerViewAdapter<InventoryTotalItemInfo, ItemRvCheckDoneBinding> wlinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_check_done);
    public LRecyclerViewAdapter doneAdapter = new LRecyclerViewAdapter(wlinnerAdapter);

    //未完成的Task
    public ArrayList<InventoryTotalItemInfo> mNoDonedatas;
    public BGABindingRecyclerViewAdapter<InventoryTotalItemInfo, ItemRvCheckNodoneBinding> nolinnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_rv_check_nodone);
    public LRecyclerViewAdapter noDoneAdapter = new LRecyclerViewAdapter(nolinnerAdapter);

    public void setDoneData(List<InventoryTotalItemInfo> data) {
        if (null == mDonedatas)
            mDonedatas = new ArrayList<>();
        mDonedatas.clear();
        mDonedatas.addAll(data);

        wlinnerAdapter.setData(mDonedatas);
        doneAdapter.removeFooterView();
        doneAdapter.removeHeaderView();
        doneAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void setNoDoneData(List<InventoryTotalItemInfo> data) {
        if (null == mNoDonedatas)
            mNoDonedatas = new ArrayList<>();
        mNoDonedatas.clear();
        mNoDonedatas.addAll(data);

        nolinnerAdapter.setData(mNoDonedatas);
        noDoneAdapter.removeFooterView();
        noDoneAdapter.removeHeaderView();
        noDoneAdapter.notifyDataSetChanged();
        refreshComplete();
    }
    public void clearNoDoneData(){
        if (null == mNoDonedatas)
            mNoDonedatas = new ArrayList<>();
        mNoDonedatas.clear();

        nolinnerAdapter.setData(mNoDonedatas);
        noDoneAdapter.removeFooterView();
        noDoneAdapter.removeHeaderView();
        noDoneAdapter.notifyDataSetChanged();
    }
    public void clearDoneData(){
        if (null == mDonedatas)
            mDonedatas = new ArrayList<>();
        mDonedatas.clear();

        nolinnerAdapter.setData(mDonedatas);
        doneAdapter.removeFooterView();
        doneAdapter.removeHeaderView();
        doneAdapter.notifyDataSetChanged();
    }


    //获取已完成任务的List
    public void getDoneListInfo(){
        doneData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getInventoryTaskListInfo("24-2")
                .flatMap(new ResultDataParse<InventoryTaskListResponse>())
                .compose(new RxSchedulerTransformer<InventoryTaskListResponse>())
                .subscribe(new Consumer<InventoryTaskListResponse>() {
                    @Override
                    public void accept(InventoryTaskListResponse taskListResponse) throws Exception {
                        doneData.setValue(LiveDataWrapper.success(taskListResponse));
//                        setDoneData(taskListResponse.getResultList());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        doneData.setValue(LiveDataWrapper.error(throwable,null));
//                        binding.rvDoneFragment.refreshComplete(1);
//                        mDoneFragment.handleException(throwable,true);
                    }
                });
        addDisposable(disposable);
    }
    //获取未完成任务的List
    public void getNoDoneListInfo() {
        nodoneData.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getInventoryTaskListInfo("24-1")
                .flatMap(new ResultDataParse<InventoryTaskListResponse>())
                .compose(new RxSchedulerTransformer<InventoryTaskListResponse>())
                .subscribe(new Consumer<InventoryTaskListResponse>() {
                    @Override
                    public void accept(InventoryTaskListResponse taskListResponse) throws Exception {
                        nodoneData.setValue(LiveDataWrapper.success(taskListResponse));
//                    setNoDoneData(taskListResponse.getResultList());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        nodoneData.setValue(LiveDataWrapper.error(throwable,null));
//                        mNoDoneFragment.handleException(throwable,true);
//                        binding.rvNodoneFragment.refreshComplete(1);

                    }
                });
        addDisposable(disposable);
    }
}
