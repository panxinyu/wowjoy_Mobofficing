package cn.wowjoy.office.materialinspection.stockout.otherout;

import android.app.Instrumentation;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.common.widget.AlphaManager;
import cn.wowjoy.office.common.widget.AutoKeyPopup;
import cn.wowjoy.office.common.widget.CommonDialog;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.common.widget.timepicker.BottomMenuPopupStock;
import cn.wowjoy.office.data.constant.Constants;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.remote.ResultException;
import cn.wowjoy.office.data.request.Good;
import cn.wowjoy.office.data.request.OutStorageDetail;
import cn.wowjoy.office.data.request.StockOtherOutSubmitRequest;
import cn.wowjoy.office.data.response.AssetsCardQueryCondition;
import cn.wowjoy.office.data.response.CheckerResponse;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.data.response.StockStorageResponse;
import cn.wowjoy.office.databinding.ActivityStockOtherOutBinding;
import cn.wowjoy.office.materialinspection.Thread.ThreadManager;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceDetailActivity;
import cn.wowjoy.office.materialinspection.stockout.hand.StockHandAddActivity;
import cn.wowjoy.office.materialinspection.stockout.otherout.detail.StockOutDetailActivity;
import cn.wowjoy.office.materialinspection.stockout.otherout.edit.StockOutHandEditActivity;
import cn.wowjoy.office.materialinspection.stockout.search.SelectOutActivity;
import cn.wowjoy.office.materialinspection.stockout.search.SelectPatientActivity;
import cn.wowjoy.office.utils.MyTextWatcher;
import cn.wowjoy.office.utils.ToastUtil;
import cn.wowjoy.office.utils.dialog.DialogUtils;

public class StockOtherOutActivity extends NewBaseActivity<ActivityStockOtherOutBinding, StockOtherOutViewModel> implements View.OnClickListener {

    LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);

    private String code;

    private String patientName;//病人姓名
    private String patientId;//病人ID

    private String storageCode = "";
    private String permission = "";
    private MDialog waitDialog;
    //记录当前编辑数量的位置
    private int notifyPosition;
    private BottomMenuPopupStock mMenuPop1;
    private MDialog mDialog;
    private CommonDialog warningDialog;
    private boolean isHasFlid;

    //病人复选框
    private AutoKeyPopup mKeyPopup;
    private TextWatcher watcher;
    @Override
    protected Class<StockOtherOutViewModel> getViewModel() {
        return StockOtherOutViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_stock_other_out;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewModel(viewModel);
        mHandler = new MyHandler(this, viewModel);
        hideSoftInput();
        initView();
        initListener();
        initObserve();

        //获取权限  和  id
        viewModel.getStorageUserId();


    }

    private void initObserve() {
        viewModel.getClick().observe(this, new Observer<AssetsCardQueryCondition>() {
            @Override
            public void onChanged(@Nullable AssetsCardQueryCondition assetsCardQueryCondition) {
                hideSoftInput();
                if (null != mKeyPopup) {
                    mKeyPopup.dismiss();
                }
                binding.tvPatientName.removeTextChangedListener(watcher);
                addEntity = assetsCardQueryCondition;
                //更新UI界面
                if (null != assetsCardQueryCondition && !"".equals(assetsCardQueryCondition.getProduceNo())) {
                    binding.tvPatientName.setText(assetsCardQueryCondition.getPatientName()+"("+assetsCardQueryCondition.getPatientId()+")");
                    binding.tvPatientName.setSelection(binding.tvPatientName.getText().length());
                }

                binding.tvPatientName.addTextChangedListener(watcher);
            }
        });

        viewModel.patientInfo.observe(this, new Observer<LiveDataWrapper<List<AssetsCardQueryCondition>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<AssetsCardQueryCondition>> listLiveDataWrapper) {
                switch (listLiveDataWrapper.status) {
                    case LOADING:
//                        waitDialog = CreateDialog.waitingDialog(StockOtherOutActivity.this);
//                        break;
                    case SUCCESS:
//                        if (null != waitDialog) {
//                            CreateDialog.dismiss(StockOtherOutActivity.this, waitDialog);
//                        }
                        //获取List模糊集合
                        if (listLiveDataWrapper.data != null) {
                            List<AssetsCardQueryCondition> detailList = listLiveDataWrapper.data;
                            if (null == mKeyPopup) {
                                mKeyPopup = new AutoKeyPopup(StockOtherOutActivity.this);
                                mKeyPopup.setAdapter(viewModel.mAutoEditAdapter);
                            }
                            if (0 != listLiveDataWrapper.data.size()) {
                                mKeyPopup.setDates(detailList);
                                mKeyPopup.showAsDropDown(binding.tvPatientName,0,1);
                            }else{
                                if (mKeyPopup != null && mKeyPopup.isShowing()) {
                                    mKeyPopup.dismiss();
                                }
                            }

                        }
                        break;
                    case ERROR:
//                        if (null != waitDialog) {
//                            CreateDialog.dismiss(StockOtherOutActivity.this, waitDialog);
//                        }
//                          handleException(listLiveDataWrapper.error, false);
                        break;
                }
            }
        });



        viewModel.queryBarCode.observe(this, new Observer<LiveDataWrapper<String>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<String> stringLiveDataWrapper) {
                switch (stringLiveDataWrapper.status) {
                    case LOADING:
                        waitDialog = CreateDialog.waitingDialog(StockOtherOutActivity.this);
                        break;
                    case SUCCESS:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockOtherOutActivity.this, waitDialog);
                        }
                        if("y".equals(stringLiveDataWrapper.data)){
                            Intent intent = new Intent(StockOtherOutActivity.this, StockHandAddActivity.class);
                            intent.putExtra("type", 2);
                            intent.putExtra("info", qrMsg);
                            intent.putExtra("storageCode", storageCode);
                            startActivityForResult(intent, Constants.REQUEST_CODE_ADD);
                        }else if("n".equals(stringLiveDataWrapper.data)){
                            //不在范围  错误的二维码
                             ToastUtil.toastWarning(StockOtherOutActivity.this, "添加失败,未在系统中找到相关物资信息", -1);
                        }
                        break;
                    case ERROR:
                        if (null != waitDialog) {
                            CreateDialog.dismiss(StockOtherOutActivity.this, waitDialog);
                        }
                        CreateDialog.submitDialog(StockOtherOutActivity.this, "失败", stringLiveDataWrapper.error.getMessage(), 101, null, "确定");

                        //  handleException(stockForOtherOutResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });
        viewModel.delete.observe(this, new Observer<StockForOtherOutResponse>() {
            @Override
            public void onChanged(@Nullable StockForOtherOutResponse recordResponse) {
                if (null != viewModel.mStockForOtherOutResponses && viewModel.mStockForOtherOutResponses.size() > 0) {
                    viewModel.mStockForOtherOutResponses.remove(recordResponse);
                }
                viewModel.mOtherOutLrAdapter.deleteDate(recordResponse);
            }
        });
        viewModel.money.observe(this, new Observer<Float>() {
            @Override
            public void onChanged(@Nullable Float aFloat) {
                DecimalFormat fnum  =   new  DecimalFormat("##0.00");
                binding.zongjine.setText(" ￥" +  fnum.format(aFloat));
            }
        });
        viewModel.edit.observe(this, new Observer<StockForOtherOutResponse>() {
            @Override
            public void onChanged(@Nullable StockForOtherOutResponse recordResponse) {
                notifyPosition = recordResponse.getNotifyPosition();
                Intent i = new Intent(StockOtherOutActivity.this, StockOutHandEditActivity.class);
                i.putExtra("factOut", recordResponse.getFactOut());
                i.putExtra("amount", recordResponse.getAmountDetail());
                startActivityForResult(i, Constants.REQUEST_CODE_EDIT);
            }
        });
        viewModel.getScanData().observe(this, new Observer<LiveDataWrapper<StockForOtherOutResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StockForOtherOutResponse> stockForOtherOutResponseLiveDataWrapper) {
                switch (stockForOtherOutResponseLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(StockOtherOutActivity.this);
                        break;
                    case SUCCESS:
                            DialogUtils.dismiss(StockOtherOutActivity.this);
                        if (null != stockForOtherOutResponseLiveDataWrapper.data) {
                            viewModel.addDate(stockForOtherOutResponseLiveDataWrapper.data, StockOtherOutActivity.this, mLinearLayoutManager, binding.recyclerView);
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                        CreateDialog.submitDialog(StockOtherOutActivity.this, "失败", stockForOtherOutResponseLiveDataWrapper.error.getMessage(), 101, null, "确定");

                      //  handleException(stockForOtherOutResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });
        viewModel.getUserId().observe(this, new Observer<LiveDataWrapper<List<PopuModel>>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<List<PopuModel>> stockForOtherOutResponseLiveDataWrapper) {
                switch (stockForOtherOutResponseLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(StockOtherOutActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                        if (null != stockForOtherOutResponseLiveDataWrapper.data) {
                            storageCode = stockForOtherOutResponseLiveDataWrapper.data.get(0).getStorageCode();
                            viewModel.applyPermission();
                            viewModel.getUserName();
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                        if (stockForOtherOutResponseLiveDataWrapper.error instanceof ResultException) {
                            initDialog();
                        }else{
                        //    CreateDialog.submitDialog(StockOtherOutActivity.this, "错误", stockForOtherOutResponseLiveDataWrapper.error.getMessage(), 101, null, "确定");
                            handleException(stockForOtherOutResponseLiveDataWrapper.error, false);
                        }
                        break;
                }
            }
        });
        viewModel.userName.observe(this, new Observer<LiveDataWrapper<CheckerResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<CheckerResponse> checkerResponseLiveDataWrapper) {
                switch (checkerResponseLiveDataWrapper.status) {
                    case LOADING:
                        break;
                    case SUCCESS:
                        if (null != checkerResponseLiveDataWrapper.data) {
                            binding.tvRequestPeopleName.setText(checkerResponseLiveDataWrapper.data.getName());
                            binding.tvRequestPeopleName.setSelection(checkerResponseLiveDataWrapper.data.getName().length());
                        }
                        break;
                    case ERROR:

                        break;
                }
            }
        });
        viewModel.getPermission().observe(this, new Observer<LiveDataWrapper<StockStorageResponse>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<StockStorageResponse> stockForOtherOutResponseLiveDataWrapper) {
                switch (stockForOtherOutResponseLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(StockOtherOutActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                        if (null != stockForOtherOutResponseLiveDataWrapper.data) {
                            permission = stockForOtherOutResponseLiveDataWrapper.data.getPermission();
                        }
                        break;
                    case ERROR:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                            handleException(stockForOtherOutResponseLiveDataWrapper.error, false);
                        break;
                }
            }
        });
        viewModel.getJump().observe(this, new Observer<StockForOtherOutResponse>() {
            @Override
            public void onChanged(@Nullable StockForOtherOutResponse response) {
                if(response.isScan()){
                    Intent intent = new Intent(StockOtherOutActivity.this, StockApplyProduceDetailActivity.class);
                    intent.putExtra("model", response);
                    intent.putExtra("type",Constants.OTHER_CODE_APPLY);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(StockOtherOutActivity.this, StockOutDetailActivity.class);
                    intent.putExtra("model", response);
                    intent.putExtra("type",Constants.OTHER_CODE_APPLY);
                    startActivity(intent);
                }

            }
        });
        viewModel.pdaSubmit.observe(this, new Observer<LiveDataWrapper<String>>() {
            @Override
            public void onChanged(@Nullable LiveDataWrapper<String> stringLiveDataWrapper) {
                switch (stringLiveDataWrapper.status) {
                    case LOADING:
                        DialogUtils.waitingDialog(StockOtherOutActivity.this);
                        break;
                    case SUCCESS:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                        ToastUtil.toastImage(StockOtherOutActivity.this, "提交成功", -1);
                        finish();
                        break;
                    case ERROR:
                        DialogUtils.dismiss(StockOtherOutActivity.this);
                        CreateDialog.submitDialog(StockOtherOutActivity.this, "提交失败", stringLiveDataWrapper.error.getMessage(), 101, null, "确定");

                        break;
                }
            }

        });
    }

    private int type;
    private AssetsCardQueryCondition addEntity;
    private void initListener() {

        binding.fab.setOnClickListener(this);
        binding.tvSubmitOtherStock.setOnClickListener(this);
        binding.back.setOnClickListener(this);
        binding.rlDirection.setOnClickListener(this);
        binding.rlPatient.setOnClickListener(this);

        binding.tvRequestPeopleName.addTextChangedListener(new MyTextWatcher(binding.tvLimitCardName, binding.tvRequestPeopleName, 25, StockOtherOutActivity.this));
        binding.tvPatientName.addTextChangedListener(new MyTextWatcher(binding.tvLimitCardModel, binding.tvPatientName, 25, StockOtherOutActivity.this));
        binding.remark.addTextChangedListener(new MyTextWatcher(binding.tvLimit, binding.remark, 100, StockOtherOutActivity.this));
        binding.tvSubmitOtherStock.setOnClickListener(this);

        watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                addEntity = null;

                if (mHandler.hasMessages(MSG_SEARCH)) {
                    mHandler.removeMessages(MSG_SEARCH);
                }
                //联网请求模糊数据
                if (!s.toString().trim().isEmpty()) {
                    Message message = mHandler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("data", s.toString().trim());// 将服务器返回的订单号传到Bundle中，，再通过handler传出
                    message.setData(bundle);
                    message.what = MSG_SEARCH;
                    mHandler.sendMessageDelayed(message, 1000);
//                    viewModel.getQueryList(s.toString().trim());
                }else{
                    //  搜索清空的时候
                    if (mKeyPopup != null && mKeyPopup.isShowing()) {
                        mKeyPopup.dismiss();
                    }
                }
            }
        };
        binding.tvPatientName.addTextChangedListener(watcher);
    }

    private void initView() {
        binding.leibie.setText(Html.fromHtml(getString(R.string.stock_notice1)));
        binding.zichanmingcheng.setText(Html.fromHtml(getString(R.string.stock_notice2)));


        binding.recyclerView.setLayoutManager(mLinearLayoutManager);
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.adapter);
        viewModel.adapter.removeFooterView();
        viewModel.adapter.removeHeaderView();
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this, SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setLoadMoreEnabled(false);


        binding.appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset < 0){
                    isHasFlid = true;
                } else {
                    isHasFlid = false;
                }
            }
        });
    }

    public StockOtherOutSubmitRequest getRequest(String isSuccess, String isConfirm) {
        StockOtherOutSubmitRequest request = new StockOtherOutSubmitRequest();
        if (null != storageCode) {
            request.setStorageCode(storageCode);
        }
        if (null != code) {
            request.setApplyDepartmentCode(code);
        }
        if (null != patientName) {
            request.setPatientName(patientName);
            request.setBrblhm(patientName);
        }
        if (null != patientId) {
            request.setPatientId(patientId);
        }
        if (!TextUtils.isEmpty(binding.tvRequestPeopleName.getText().toString().trim())) {
            request.setDemandUser(binding.tvRequestPeopleName.getText().toString().trim());
        }
//        if (!TextUtils.isEmpty(binding.tvPatientName.getText().toString().trim())) {
//            request.setPatientName(binding.tvPatientName.getText().toString().trim());
//        }
        if (!TextUtils.isEmpty(binding.remark.getText().toString().trim())) {
            request.setRemark(binding.remark.getText().toString().trim());
        }
        request.setIsConfirm(isConfirm);
        request.setIsSuccess(isSuccess);
        List<OutStorageDetail> mLists = new ArrayList<>();
        if (null != viewModel.mStockForOtherOutResponses && viewModel.mStockForOtherOutResponses.size() > 0) {
            for (StockForOtherOutResponse s : viewModel.mStockForOtherOutResponses) {
                OutStorageDetail outStorageDetail = new OutStorageDetail();
                outStorageDetail.setAmount(s.getFactOut() + "");
                outStorageDetail.setBatchNumber(s.getBatchNumber());
                if (!TextUtils.isEmpty(s.getEffectiveDate())) {
                    outStorageDetail.setEffectiveDate(s.getEffectiveDate());
                }
                outStorageDetail.setGoodsCode(s.getGoodsCode());
                outStorageDetail.setPrice(s.getPriceDetail());

                Good good = new Good();
                if (s.isHand()) {
                    good.setIsQR("n");
                } else {
                    good.setIsQR("y");
                    outStorageDetail.setqRVOList(s.getqRVOList());
                }
                outStorageDetail.setGood(good);
                mLists.add(outStorageDetail);
            }
        }
        request.setOutStorageList(mLists);
        request.setSfzdjf("n");
        return request;
    }

    //为弹出窗口实现监听类
    private View.OnClickListener itemsOnClick_OutAdded = new View.OnClickListener() {

        public void onClick(View v) {
            if (null != mMenuPop1 && mMenuPop1.isShowing()) {
                mMenuPop1.dismiss();
            }
            switch (v.getId()) {
                case R.id.sure_submit:
                    viewModel.submit(getRequest("y", "y"), StockOtherOutActivity.this);
                    break;
//                case R.id.sure_other:
//
//                    break;
                case R.id.only_check:
                    viewModel.submit(getRequest("y", "n"), StockOtherOutActivity.this);
                    break;
                case R.id.only_submit:
                    viewModel.submit(getRequest("n", "n"), StockOtherOutActivity.this);
                    break;
                case R.id.only_que:
                    viewModel.submit(getRequest("n", "n"), StockOtherOutActivity.this);
                    break;
                case R.id.only_cancel:
                    mMenuPop1.setVisibility();
                    break;
            }
        }
    };

    public void initPop() {
        //实例化SelectPicPopupWindow
        if (null == mMenuPop1) {
            mMenuPop1 = new BottomMenuPopupStock(StockOtherOutActivity.this, itemsOnClick_OutAdded, permission,"其他出库");
            mMenuPop1.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    AlphaManager.backgroundAlpha(1f, StockOtherOutActivity.this);
                    mMenuPop1.setVisibility();
                }
            });
        }
        //显示窗口
        mMenuPop1.showAtLocation(StockOtherOutActivity.this.findViewById(R.id.main), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
        AlphaManager.backgroundAlpha(0.7f, StockOtherOutActivity.this);
    }

    @Override
    public void onBackPressed() {
        if(!TextUtils.isEmpty(binding.tvType.getText()) || !TextUtils.isEmpty(binding.tvRequestPeopleName.getText()) ||
                !TextUtils.isEmpty(binding.tvPatientName.getText())  ||  !TextUtils.isEmpty(binding.remark.getText()) ||
                viewModel.mStockForOtherOutResponses.size()>0
                ){
            mDialog = CreateDialog.infoDialog(StockOtherOutActivity.this, "提醒", "当前出库任务还未提交，是否退出", "否", "是", v12 -> {
                if (null != mDialog) {
                    mDialog.cancel();
                }
            }, v1 -> {
                if (null != mDialog) {
                    mDialog.cancel();
                    finish();
                }
            });
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                Intent intent = new Intent(this, StockHandAddActivity.class);
                intent.putExtra("type", 1);
                intent.putExtra("info", "");
                intent.putExtra("storageCode", storageCode);
                startActivityForResult(intent, Constants.REQUEST_CODE_ADD);
                break;
            case R.id.tv_submit_other_stock:
                if (TextUtils.isEmpty(binding.tvType.getText().toString().trim())) {
                    CreateDialog.submitDialog(StockOtherOutActivity.this, "提交失败", "您还未选择出库去向，请选择后提交!", 101, null, "确定");
                    return;
                }
                if (TextUtils.isEmpty(binding.tvRequestPeopleName.getText().toString().trim())) {
                    CreateDialog.submitDialog(StockOtherOutActivity.this, "提交失败", "您还未输入需求人员姓名，请输入后提交", 101, null, "确定");
                    return;
                }
                if (viewModel.mStockForOtherOutResponses.size() == 0) {
                    CreateDialog.submitDialog(StockOtherOutActivity.this, "提交失败", "当前还没有出库明细清单，请先扫码或者手工录入", 101, null, "确定");
                    return;
                }
                initPop();

                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.rl_direction:
                Intent intent2 = new Intent(StockOtherOutActivity.this, SelectOutActivity.class);
                if ("".equals(binding.tvType.getText().toString().trim())) {
                    intent2.putExtra("name", "");
                } else {
                    intent2.putExtra("name", binding.tvType.getText().toString().trim());
                }
                startActivityForResult(intent2, Constants.REQUEST_CODE);
                break;
            case R.id.rl_patient:
                Intent intent3 = new Intent(StockOtherOutActivity.this, SelectPatientActivity.class);
                startActivityForResult(intent3, Constants.REQUEST_CODE_PATIENT);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // 选择去向的结果
            case Constants.REQUEST_CODE:
                switch (resultCode) {
                    case Constants.RESULT_OK:
                        //选择了
                        binding.tvType.setText(data.getStringExtra("name"));
                        code = data.getStringExtra("code");
                        break;
                    case Constants.RESULT_FAIL:

                        break;
                }
                break;
            // 选择病人的结果
            case Constants.REQUEST_CODE_PATIENT:
                switch (resultCode) {
                    case Constants.RESULT_OK:
                        //选择了
                        patientName = data.getStringExtra("name");
                        patientId = data.getStringExtra("code");
                        binding.tvTypePatient.setText(patientName+"("+patientId+")");

                        break;
                    case Constants.RESULT_FAIL:

                        break;
                }
                break;
            //手动添加的结果
            case Constants.REQUEST_CODE_ADD:
                switch (resultCode) {
                    case Constants.RESULT_OK:
                        // 加入到
                        StockForOtherOutResponse response = (StockForOtherOutResponse) data.getSerializableExtra("model");
                        String num = data.getStringExtra("num");
                        response.setFactOut(Float.parseFloat(num));

                        Log.e("PXY", "onActivityResult: " + response.toString());
                        Log.e("PXY", "onActivityResult: " + num);

                        viewModel.addDate(response, StockOtherOutActivity.this, mLinearLayoutManager, binding.recyclerView);

                        break;
                    case Constants.RESULT_FAIL:

                        break;
                }
                break;
            //手动编辑的结果
            case Constants.REQUEST_CODE_EDIT:
                switch (resultCode) {
                    case Constants.RESULT_OK:
                        // 加入到
                        if (null != data) {
                            String count = data.getStringExtra("count");
                            viewModel.mOtherOutLrAdapter.notifyRealStock(notifyPosition, count);
                            //计算金额
                            viewModel.mStockForOtherOutResponses.get(notifyPosition).setFactOut(Float.parseFloat(count));
                            viewModel.totalMoney();
                        }


                        break;
                    case Constants.RESULT_FAIL:

                        break;
                }
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSoftInput();
        openBroadCast();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeBroadCast();
    }
   private String qrMsg;
    private ThreadManager mThreadManager;
    @Override
    protected void brodcast(Context context, String msg) {
        super.brodcast(context, msg);
        qrMsg = msg;
        if(null == mThreadManager){
            mThreadManager = new ThreadManager();
        }
        if (msg.contains("code")) {
        //    mHandler.sendEmptyMessage(1);
            if (!isHasFlid){
                mThreadManager.doFolid(binding.appbar.getHeight());
            }

      //     mockFliding();
//            String replace = msg.replace("二维码编码", "qrCode");
//            Log.e("PXY", "brodcast: " + replace);
            String code = msg.substring(msg.indexOf(":") + 1, msg.length());
            Log.e("PXY", "code: "+ code);
            viewModel.queryByScan(code);

        } else {
            viewModel.formQuery(msg);
        }

    }
    /**
     * 模拟滑动事件
     */
    private void mockFliding(){
        if (!isHasFlid)
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Instrumentation inst = new Instrumentation();
                    float x = 45.0f;
                    float y = binding.appbar.getHeight();
                    long dowTime = SystemClock.uptimeMillis();
                    inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime,
                            MotionEvent.ACTION_DOWN, x, y,0));
                    inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime,
                            MotionEvent.ACTION_MOVE, x, y - 50,0));
                    inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+20,
                            MotionEvent.ACTION_MOVE, x, y - 100,0));
                    inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+30,
                            MotionEvent.ACTION_MOVE, x, y - 150,0));
                    inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+40,
                            MotionEvent.ACTION_MOVE, x, y - 200,0));
                    inst.sendPointerSync(MotionEvent.obtain(dowTime,dowTime+40,
                            MotionEvent.ACTION_UP, x, y - 250,0));
                }
            }).start();

    }

    private void initDialog() {
        warningDialog = new CommonDialog.Builder()
                .showTitle(false)
                .setContent("获取权限失败，请确认是否有权限！")
                .showCancel(false)
                .setConfirmBtn("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        StockOtherOutActivity.this.finish();
                    }
                })
                .create();
        warningDialog.setCancelable(false);
        warningDialog.show(getSupportFragmentManager(), "");
    }


    private static final int MSG_SEARCH = 1;
    private Handler mHandler;

    static class MyHandler extends Handler {
        // WeakReference to the outer class's instance.
        private WeakReference<StockOtherOutActivity> mOuter;
        private StockOtherOutViewModel mStockOtherOutViewModel;

        public MyHandler(StockOtherOutActivity activity, StockOtherOutViewModel vm) {
            mOuter = new WeakReference<StockOtherOutActivity>(activity);
            this.mStockOtherOutViewModel = vm;
        }

        @Override
        public void handleMessage(Message msg) {
            StockOtherOutActivity outer = mOuter.get();
            if (outer != null) {
                if (msg.what == MSG_SEARCH) {
//                    mStockOtherOutViewModel.getPatientList(msg.getData().getString("data"));
                }
            }
        }
    }

}
