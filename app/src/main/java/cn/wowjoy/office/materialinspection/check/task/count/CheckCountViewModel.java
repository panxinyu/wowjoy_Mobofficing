package cn.wowjoy.office.materialinspection.check.task.count;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.CheckCountResponse;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/1/8.
 */

public class CheckCountViewModel extends NewBaseViewModel {
    @Inject
    ApiService apiService;
    @Inject
    public CheckCountViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
    private MediatorLiveData<LiveDataWrapper<CheckCountResponse>> response = new MediatorLiveData<>();

    public MediatorLiveData<LiveDataWrapper<CheckCountResponse>> getResponse() {
        return response;
    }

    public void  count(String taskId){
        response.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.countCheckTaskDtlByPda(taskId)
                .flatMap(new ResultDataParse<>())
                .compose(new RxSchedulerTransformer<>())
                .subscribe((CheckCountResponse countResponse) -> {
                    //TODO: 设置界面上的详细数据 着重区分下！！！ 已完成和未完成显示的页面区别
                    response.setValue(LiveDataWrapper.success(countResponse));
//                    this.single = taskListResponse.getSingle();
//                    refreshData(isRefresh ,taskListResponse.getDetailList());
//                    mPatrolTaskActivity.updateUI(taskListResponse,filter);
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //异常处理
                        response.setValue(LiveDataWrapper.error(throwable,null));
                    }
                });
        addDisposable(disposable);


    }



}
