package cn.wowjoy.office.homepage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.AppInfo;

public class MenuHelper {
    private final static Map<String,Integer> mMenu = new HashMap<>();
    private static Map<String,String> auidS = new HashMap<>();
    private static Map<String,String> appNames = new HashMap<>();
//            appInfos.add(new AppInfo("主数据",R.drawable.zhushuju_selector,false,1004));
//        appInfos.add(new AppInfo("库存盘点", R.drawable.kucunpandian_selector,false,1001));
//        appInfos.add(new AppInfo("库存详情",R.drawable.kucunxiangqing_selector,false,1003));
//        appInfos.add(new AppInfo("巡检管理",R.drawable.inspection_res_selector,false,1002));
//        appInfos.add(new AppInfo("设备盘点",R.drawable.material_res_selector,false,1011));
//        appInfos.add(new AppInfo("其他出库",R.drawable.otherout_res_selector,false,1012));
//        appInfos.add(new AppInfo("申领出库",R.drawable.shenling_res_selector,false,1013));
//        appInfos.add(new AppInfo("申请出库",R.drawable.shenqing_res_selector,false,1014));
//        appInfos.add(new AppInfo("调拨出库",R.drawable.inspection_res_selector,false,1015));
//        appInfos.add(new AppInfo("PM管理",R.drawable.pm_res_selector,false,1016));
//         appInfos.add(new AppInfo("更多",R.drawable.more_res_selector,false,1011));
//        appInfos.add(new AppInfo("敬请期待",R.drawable.more_res_selector,false,1005));
//         appInfos.add(new AppInfo("维修申请",R.drawable.maintanece_res_selector,false,1003));
    static {
         mMenu.put("zc_inventory", R.drawable.kucunpandian_selector);
        appNames.put("zc_inventory","盘点管理");

         mMenu.put("zc_pm", R.drawable.pm_res_selector);
        appNames.put("zc_pm","PM管理");

        mMenu.put("zc_inspect", R.drawable.inspection_res_selector);
        appNames.put("zc_inspect","巡检管理");

        mMenu.put("wz_zsj", R.drawable.zhushuju_selector);
        appNames.put("wz_zsj","主数据");

        mMenu.put("wz_qtck", R.drawable.otherout_res_selector);
        appNames.put("wz_qtck","其他出库");

        mMenu.put("wz_dbck", R.drawable.inspection_res_selector);
        appNames.put("wz_dbck","调拨出库");

        mMenu.put("wz_slck", R.drawable.shenling_res_selector);
        appNames.put("wz_slck","申领出库");

        mMenu.put("wz_sqck", R.drawable.shenqing_res_selector);
        appNames.put("wz_sqck","申请出库");

        mMenu.put("wz_kcpd", R.drawable.kucunpandian_selector);
        appNames.put("wz_kcpd","库存盘点");

        mMenu.put("wz_kcxq", R.drawable.kucunxiangqing_selector);
        appNames.put("wz_kcxq","库存详情");
    }

    public static int getIconByKey(String key){
        if(!mMenu.containsKey(key)) {
            return -1;
        }
        return mMenu.get(key);
    }
    public static void setAuidList(List<AppInfo> mLists){
         if(null != mLists && mLists.size()>0){
             for(AppInfo app: mLists){
                 auidS.put(app.getPmsKey(),app.getAuid());
             }
         }
    }

    public static Map<String, String> getAuidS() {
        return auidS;
    }

    public static Map<String, String> getAppNames() {
        return appNames;
    }
}
