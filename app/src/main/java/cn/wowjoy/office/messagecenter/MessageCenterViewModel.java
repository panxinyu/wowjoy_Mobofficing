package cn.wowjoy.office.messagecenter;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Sherily on 2017/9/13.
 */

public class MessageCenterViewModel extends NewBaseViewModel{
    @Override
    public void onCreateViewModel() {

    }

    @Inject
    public MessageCenterViewModel(@NonNull NewMainApplication application) {
        super(application);
    }
}
