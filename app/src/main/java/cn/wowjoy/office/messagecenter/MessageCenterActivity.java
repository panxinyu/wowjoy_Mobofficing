package cn.wowjoy.office.messagecenter;

import android.os.Bundle;


import cn.wowjoy.office.R;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.databinding.ActivityMessageCenterBinding;

public class MessageCenterActivity extends NewBaseActivity<ActivityMessageCenterBinding,MessageCenterViewModel> {


    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_center;
    }

    @Override
    protected Class<MessageCenterViewModel> getViewModel() {
        return MessageCenterViewModel.class;
    }
}
