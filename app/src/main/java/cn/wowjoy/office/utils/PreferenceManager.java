/**
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.wowjoy.office.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import javax.inject.Singleton;

import cn.wowjoy.office.BuildConfig;

@Singleton
public class PreferenceManager {
    public static final String PREFERENCE_NAME = "saveInfo";
    private static SharedPreferences mSharedPreferences;
    private static PreferenceManager mPreferencemManager;
    private static SharedPreferences.Editor editor;


    private static String SHARED_KEY_CURRENTUSER_USERACCESSTOKEN = "SHARED_KEY_CURRENTUSER_USERACCESSTOKEN";
    private static String SHARED_KEY_W_USERINFO = "SHARED_KEY_W_USERINFO";
    private static String LOGIN_HOST = "LOGIN_HOST";
    private static String API_HOST = "API_HOST";
    private static String UPDATE_HOST = "UPDATE_HOST";
    private static String MDID = "mdid";
    private static String STUFF_INFO = "STUFF_INFO";
    private final String API_HOST_DEFAULT = BuildConfig.API_HOST;
    private final String LOGIN_HOST_DEFAULT = BuildConfig.LOGIN_HOST;
    private final String UPDATE_HOST_DEFAULT = BuildConfig.UPDATE_HOST;


    private PreferenceManager(Context cxt) {
        mSharedPreferences = cxt.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = mSharedPreferences.edit();
    }

    public static synchronized void init(Context cxt) {
        if (mPreferencemManager == null) {
            mPreferencemManager = new PreferenceManager(cxt);
        }
    }

    public synchronized static PreferenceManager getInstance() {
        if (mPreferencemManager == null) {
            throw new RuntimeException("please init first!");
        }

        return mPreferencemManager;
    }


    public void clearAllCacheMessgae() {
        editor.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }


    public void setCurrentUserAccessToken(String accessToken) {
        editor.putString(SHARED_KEY_CURRENTUSER_USERACCESSTOKEN, accessToken);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        } else {
            editor.commit();
        }
    }
    public void clearToken(){
        editor.putString(SHARED_KEY_CURRENTUSER_USERACCESSTOKEN, "");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    public String getCurrentUserAccessToken() {
        return mSharedPreferences.getString(SHARED_KEY_CURRENTUSER_USERACCESSTOKEN, null);
    }

    public String getHeaderToken(){
        return "Bearer " + getCurrentUserAccessToken();
    }

    public void setStaffNum(String u) {
        editor.putString(SHARED_KEY_W_USERINFO, u);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }
    public void setStaffInfo(String u) {
        editor.putString(STUFF_INFO, u);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }
    public String getStuffInfo(){
        return mSharedPreferences.getString(STUFF_INFO, "");
    }
    public void setMDID(String s){
        editor.putString(MDID, s);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }

    public String getDevMDID(){
        return mSharedPreferences.getString(MDID, "84e71f389a4b4fe5ad79261b4d56262e");
    }
    public String getStaffNum() {
        return mSharedPreferences.getString(SHARED_KEY_W_USERINFO, null);
    }

    public void setLoginHost(String u) {
        editor.putString(LOGIN_HOST, u);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }

    public String getLoginHost() {
        return mSharedPreferences.getString(LOGIN_HOST, LOGIN_HOST_DEFAULT);
    }
    public void setUpdateHost(String u) {
        editor.putString(UPDATE_HOST, u);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }

    public String getUpdateHost() {
        return mSharedPreferences.getString(UPDATE_HOST, UPDATE_HOST_DEFAULT);
    }
    public void setAPIHost(String u) {
        editor.putString(API_HOST, u);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }else {
            editor.commit();
        }
    }

    public String getAPIHost() {
        return mSharedPreferences.getString(API_HOST, API_HOST_DEFAULT);
    }
}
