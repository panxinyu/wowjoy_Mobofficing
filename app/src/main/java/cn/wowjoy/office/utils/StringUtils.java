package cn.wowjoy.office.utils;

/**
 * Created by Sherily on 2017/11/15.
 * Description:
 */
public class StringUtils {

    public static boolean isNotEmptyString(final String str) {
        return str != null && str.length() > 0;
    }

    public static boolean isEmptyString(final String str) {
        return str == null || str.length() <= 0;
    }
}
