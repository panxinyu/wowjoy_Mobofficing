package cn.wowjoy.office.utils;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.github.jdsjlzx.interfaces.OnLoadMoreListener;
import com.github.jdsjlzx.interfaces.OnRefreshListener;
import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;
import com.github.jdsjlzx.recyclerview.ProgressStyle;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.bingoogolapple.badgeview.BGABadgeImageView;
import cn.wowjoy.office.R;
import cn.wowjoy.office.base.BasePresenter;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;


public class BindingEvent {

//    @BindingAdapter("frescoImgUrl")
//    public static void setFrescoImgUrl(final SimpleDraweeView sdv, String url) {
//        FrescoUtils.display(sdv, url);
//    }


    @BindingAdapter("glideUrl")
    public static void setGlideUrl(final BGABadgeImageView iv, String url){
        Glide.with(iv.getContext())
                .load(url)
                .centerCrop()
                .crossFade()
                .error(R.mipmap.maintenance_application_normal)
                .placeholder(R.mipmap.maintenance_application_normal)
                .into(iv);
    }

    @BindingAdapter({"glideUrlHeader","mToken"})
    public static void setGlideUrlHeader(final ImageView iv, String glideUrlHeader,String mToken){
        if (!TextUtils.isEmpty(glideUrlHeader)){
            GlideUrl  glideUrl = new GlideUrl(glideUrlHeader, new LazyHeaders.Builder()
                    .addHeader("Content-Type", "application/json;charset=UTF-8")
                    .addHeader("Authorization", mToken)
                    .build());
            Glide.with(iv.getContext())
                    .load(glideUrl)
                    .centerCrop()
                    .crossFade()
                    .error(R.mipmap.pic_fail)
                    .placeholder(R.mipmap.pic_loading)
                    .into(iv);
        } else {
            iv.setImageResource(R.mipmap.pic_small);
        }


    }

    @BindingAdapter("glideUrl")
    public static void setGlideUrl(final ImageView iv, String url){
        Glide.with(iv.getContext())
                .load(url)
                .centerCrop()
                .crossFade()
                .error(R.mipmap.pic_fail)
                .placeholder(R.mipmap.pic_loading)
                .into(iv);
    }
    @BindingAdapter("glideUrlInt")
    public static void setGlideUrlInt(final ImageView iv, int url){
        Glide.with(iv.getContext())
                .load(url)
                .centerCrop()
                .crossFade()
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(iv);
    }


    @BindingAdapter({"keyWords","textStr", "keyColor","keySize"})
    public static void setHighLighTextView(final TextView tv,String key,String str,int keyColor,float keySize){
        if ( keySize == 0.0f )
            keySize = 15f;
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(key)){
            int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, keySize, tv.getResources().getDisplayMetrics());
            SpannableStringBuilder ss = new SpannableStringBuilder(str);
            Pattern pattern = Pattern.compile(key);
            Matcher matcher = pattern.matcher(ss);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                ss.setSpan(new ForegroundColorSpan(keyColor), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new AbsoluteSizeSpan(size), start , end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            tv.setText(ss);
        } else {
            tv.setText(str);
        }

    }

    @BindingAdapter("menuIcon")
    public static void setMenuIcon(final TextView tv, String url){

    }
    @BindingAdapter("adapter")
    public static void setAdapter(final LRecyclerView rv, LRecyclerViewAdapter adapter) {
        rv.setAdapter(adapter);
    }

    @BindingAdapter("layoutManager")
    public static void setLayoutManager(final LRecyclerView rv, RecyclerView.LayoutManager layoutManager) {
        if (null == layoutManager) layoutManager = new LinearLayoutManager(rv.getContext());
        rv.setLayoutManager(layoutManager);
    }

    @BindingAdapter("OnFreshListener")
    public static void OnFreshListener(final LRecyclerView rv, final BasePresenter presenter) {
        rv.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader); //设置下拉刷新Progress的样式
        rv.setArrowImageView(R.drawable.ic_pulltorefresh_arrow);  //设置下拉刷新箭头
        rv.setHeaderViewColor(R.color.colorAccent, R.color.appText3, android.R.color.white);
        rv.setFooterViewColor(R.color.colorAccent, R.color.appText3, android.R.color.white);
        //设置底部加载文字提示
        rv.setFooterViewHint("加载中", "我也是有底线的", "网络不给力啊，点击重试");
        rv.setLoadingMoreProgressStyle(ProgressStyle.BallSpinFadeLoader);

        rv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadData(true);
            }
        });
        rv.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                presenter.loadData(false);
            }
        });
    }
    @BindingAdapter("OnFreshListener2")
    public static void OnFreshListener2(final LRecyclerView rv, final NewBaseViewModel presenter) {
        rv.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader); //设置下拉刷新Progress的样式
        rv.setArrowImageView(R.drawable.ic_pulltorefresh_arrow);  //设置下拉刷新箭头
        rv.setHeaderViewColor(R.color.colorAccent, R.color.appText3, android.R.color.white);
        rv.setFooterViewColor(R.color.colorAccent, R.color.appText3, android.R.color.white);
        //设置底部加载文字提示
        rv.setFooterViewHint("加载中", "我也是有底线的", "网络不给力啊，点击重试");
        rv.setLoadingMoreProgressStyle(ProgressStyle.BallSpinFadeLoader);

        rv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadData(true);
            }
        });
        rv.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                presenter.loadData(false);
            }
        });
    }
    @BindingAdapter("isRefreshing")
    public static void onFinishFreshAndLoad(final LRecyclerView rv, boolean isRefreshing) {
        if (!isRefreshing) rv.refreshComplete(10);
    }

    @BindingAdapter("android:layout_marginTop")
    public static void setTopMargin(View view, float topMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, Math.round(topMargin), layoutParams.rightMargin, layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }


}
