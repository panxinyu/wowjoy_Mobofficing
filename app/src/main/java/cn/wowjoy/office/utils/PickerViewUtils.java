package cn.wowjoy.office.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;

import java.util.Calendar;
import java.util.List;

import cn.wowjoy.office.R;


public class PickerViewUtils<T extends Object>{


    public PickerViewUtils() {
    }

//    private static volatile PickerViewUtils mPickerViewUtils;
//    public static PickerViewUtils getInstence(){
//        if(null == mPickerViewUtils){
//            synchronized (PickerViewUtils.class){
//                if(null == mPickerViewUtils){
//                    mPickerViewUtils = new PickerViewUtils();
//                }
//            }
//        }
//        return mPickerViewUtils;
//    }

    /**
     * 选择选项对话框
     * @param options  选项列表
     * @param title    标题
     * @param context  上下文
     * @param listener 回调监听
     */
    public OptionsPickerView getOptionPickerViewT(String title, List<T> options, int position, Context context,
                                                 OptionsPickerView.OnOptionsSelectListener listener) {
        OptionsPickerView.Builder builder = new OptionsPickerView.Builder(context, listener);
        builder.setCancelColor(ContextCompat.getColor(context, R.color.C_555555))
                .setSubmitColor(ContextCompat.getColor(context, R.color.C_317DCD))
                .setTitleText(title)
                .setCancelText("取消")
                .setSubmitText("确认")
                .setSelectOptions(position)
                .setCyclic(false, false, false);
        OptionsPickerView optionsPickerView = builder.build();
        optionsPickerView.setPicker(options);
        return optionsPickerView;
    }
    /**
     * 选择选项对话框
     * @param options  选项列表
     * @param title    标题
     * @param context  上下文
     * @param listener 回调监听
     */
    public static OptionsPickerView getOptionPickerView(String title, List<String> options, int position, Context context,
                                                        OptionsPickerView.OnOptionsSelectListener listener) {
        OptionsPickerView.Builder builder = new OptionsPickerView.Builder(context, listener);
        builder.setCancelColor(ContextCompat.getColor(context, R.color.C_555555))
                .setSubmitColor(ContextCompat.getColor(context, R.color.C_317DCD))
                .setTitleText(title)
                .setCancelText("取消")
                .setSubmitText("确认")
                .setSelectOptions(position)
                .setCyclic(false, false, false);
        OptionsPickerView optionsPickerView = builder.build();
        optionsPickerView.setPicker(options);
        return optionsPickerView;
    }
    /**
     * 日期选择对话框
     *
     * @param title    标题
     * @param context  上下文
     * @param listener 回掉监听
     */
    public static TimePickerView getTimePickerView(String title, Context context, Calendar calendar, TimePickerView.OnTimeSelectListener listener) {
        TimePickerView.Builder builder = new TimePickerView.Builder(context, listener);
        builder.setType(getTimeType())
                .setLabel("", "", "", "", "", "")
                .setCancelColor(ContextCompat.getColor(context, R.color.C_555555))
                .setSubmitColor(ContextCompat.getColor(context, R.color.C_317DCD))
                .setTitleText(title)
                .setCancelText("取消")
                .setSubmitText("确认")
                .isCyclic(true)//是否循环滚动
                .setContentSize(16);
        TimePickerView timePickerView = builder.build();
        timePickerView.setDate(calendar);
        return timePickerView;
    }

    /**
     * 日期选择对话框
     *
     * @param title    标题
     * @param context  上下文
     * @param listener 回掉监听
     */
    public static TimePickerView getDatePickerView(String title, Context context, Calendar calendar, TimePickerView.OnTimeSelectListener listener) {
        TimePickerView.Builder builder = new TimePickerView.Builder(context, listener);
        builder.setType(getDateType())
                .setLabel("", "", "", "", "", "")
                .setCancelColor(ContextCompat.getColor(context, R.color.C_555555))
                .setSubmitColor(ContextCompat.getColor(context, R.color.C_317DCD))
                .setCancelText("取消")
                .setSubmitText("确认")
                .setTitleText(title)
                .isCyclic(true)//是否循环滚动
                .setContentSize(16);
        TimePickerView timePickerView = builder.build();
        timePickerView.setDate(calendar);
        return timePickerView;
    }

    private static boolean[] getDateType() {
        boolean[] type = new boolean[6];
        for (int i = 0; i < 6; i++) {
            if (i > 2) {
                type[i] = false;
            } else {
                type[i] = true;
            }
        }
        return type;
    }

    private static boolean[] getTimeType() {
        boolean[] type = new boolean[6];
        for (int i = 0; i < 6; i++) {
            if (i > 4) {
                type[i] = false;
            } else {
                type[i] = true;
            }
        }
        return type;
    }
    public static String getCheckType(String type){
        checkType = type;
        switch (type){
            case "86-1":
                return "验收";
            case "86-2":
                return "预防性检测";
            case "86-3":
                return "稳定性检测";
        }
        return "";
    }
    public static int getCheckTypePosition(String type){
        switch (type){
            case "验收":
                return 0;
            case "预防性检测":
                return 1;
            case "稳定性检测":
                return 2;
        }
        return 0;
    }
    public static int getCheckOrganizationPosition(String type){
        switch (type){
            case "医院":
                return 0;
            case "生产厂家":
                return 1;
            case "第三方服务机构":
                return 2;
        }
        return 0;
    }
    public static String getCheckOrganization(String type){
        checkOrganization = type;
        switch (type){
            case "87-1":
                return "医院";
            case "87-2":
                return "生产厂家";
            case "87-3":
                return "第三方服务机构";
        }
        return "";
    }
    public static String checkType ="86-2";
    public static String checkOrganization ="87-1";
    public static String checkMode = "";
    public static OptionsPickerView.OnOptionsSelectListener getListener(TextView mText,List<String> list,int type){
        return new OptionsPickerView.OnOptionsSelectListener() {
           @Override
           public void onOptionsSelect(int options1, int options2, int options3, View v) {
               mText.setText(list.get(options1));
               if(type == 1){
                   switch (list.get(options1)){
                       case "验收":
                           checkType = "86-1";
                           break;
                       case "预防性检测":
                           checkType = "86-2";
                           break;
                       case "稳定性检测":
                           checkType = "86-3";
                           break;
                   }
               }else if (type == 2){
                   switch (list.get(options1)){
                       case "医院":
                           checkOrganization = "87-1";
                           break;
                       case "生产厂家":
                           checkOrganization = "87-2";
                           break;
                       case "第三方服务机构":
                           checkOrganization = "87-3";
                           break;
                   }
               }else if(type == 3){
                   checkMode = list.get(options1);
               }

           }
       };
    }
    public static OptionsPickerView.OnOptionsSelectListener getListener(TextView mText,List<String> list){
        return new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                mText.setText(list.get(options1));
                checkMode = list.get(options1);

            }
        };
    }
}
