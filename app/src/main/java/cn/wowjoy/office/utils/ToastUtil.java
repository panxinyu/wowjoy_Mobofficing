package cn.wowjoy.office.utils;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.Toast;

import cn.wowjoy.office.R;
import cn.wowjoy.office.databinding.ToastImgTvViewBinding;
import cn.wowjoy.office.databinding.WarningToastBinding;

/**
 * Created by Sherily on 2017/11/14.
 * Description:
 */

public class ToastUtil {
    /**
     * 带图片的toast，不需要更改图片的情况下传resId < 0,图片在上部;
     * @param context
     * @param text
     * @param resId
     */
    public static void toastImage(Context context,String text, int resId){
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        LayoutInflater inflater = LayoutInflater.from(context);
        ToastImgTvViewBinding binding = DataBindingUtil.inflate(inflater, R.layout.toast_img_tv_view,null,false);
        binding.contentRoot.getBackground().setAlpha(128);
        binding.content.setText(text);
//        @DrawableRes int left,
//        @DrawableRes int top, @DrawableRes int right, @DrawableRes int bottom
        if ( resId > 0){
            binding.content.setCompoundDrawablesWithIntrinsicBounds(0,resId,0,0);


        }
        toast.setView(binding.getRoot());
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    /**
     * 带图片的toast，不需要更改图片的情况下传resId < 0,图片在左侧;
     * @param context
     * @param text
     * @param resId
     */
    public static void toastWarning(Context context,String text, int resId){
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        LayoutInflater inflater = LayoutInflater.from(context);
        WarningToastBinding binding = DataBindingUtil.inflate(inflater, R.layout.warning_toast,null,false);
//        binding.contentRoot.getBackground().setAlpha(128);
        binding.content.setText(text);
//        @DrawableRes int left,
//        @DrawableRes int top, @DrawableRes int right, @DrawableRes int bottom
        if ( resId > 0){
            binding.content.setCompoundDrawablesWithIntrinsicBounds(resId,0,0,0);


        }
        toast.setView(binding.getRoot());
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
    /**
     * 带图片的toast，不需要更改图片的情况下传resId < 0,图片在左侧;
     * @param context
     * @param text
     * @param resId
     */
    public static void toastWarning(Context context,String text, int resId,boolean isVertial){
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        LayoutInflater inflater = LayoutInflater.from(context);
        WarningToastBinding binding = DataBindingUtil.inflate(inflater, R.layout.warning_toast,null,false);
//        binding.contentRoot.getBackground().setAlpha(128);
        binding.content.setText(text);
//        @DrawableRes int left,
//        @DrawableRes int top, @DrawableRes int right, @DrawableRes int bottom
        if ( resId > 0 ){
            if(!isVertial){
                binding.content.setCompoundDrawablesWithIntrinsicBounds(resId,0,0,0);
            }else{
                binding.content.setCompoundDrawablesWithIntrinsicBounds(0,resId,0,0);
            }


        }
        toast.setView(binding.getRoot());
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
}
