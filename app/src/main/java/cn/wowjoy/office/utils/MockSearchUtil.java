package cn.wowjoy.office.utils;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.data.response.AppInfo;

/**
 * Created by Sherily on 2017/11/9.
 * Description:
 */

public class MockSearchUtil {

    public static List<AppInfo> searchResult(String str,List<AppInfo> appInfos){
        if (!TextUtils.isEmpty(str) && !appInfos.isEmpty()){
            List<AppInfo> result = new ArrayList<>();
            for (AppInfo appInfo : appInfos){
                if (appInfo.getName().contains(str)){
                    appInfo.setKeyWords(str);
                    result.add(appInfo);
                }
            }
            return result;
        }
        return null;
    }
}
