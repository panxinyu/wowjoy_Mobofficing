package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.Type;
import cn.wowjoy.office.databinding.StockMainTypeItemViewBinding;
import cn.wowjoy.office.materialmanage.stock.MaterialStockViewModel;

/**
 * Created by Sherily on 2017/12/29.
 * Description:
 */

public class StockMainTypeAdapter extends RecyclerView.Adapter<StockMainTypeAdapter.ItemViewHolder> {

    private LayoutInflater inflater;
    private Object mEventHandle;
    private List<Type> types;
    private int selectionPosition = 0;
    private int lastPosition = 0;
    private int totalSub = 0;
    private int temSub = 0;

    private boolean isClear = false;


    public void setTypes(List<Type> types,int totalSub) {
        this.types = types;
        this.totalSub = totalSub;
        initSelected();
        temSub = totalSub;
        notifyDataSetChanged();
    }

    public int getTemSub() {
        if (temSub > 99)
            return 99;
        return temSub;
    }


    public List<String> getSelectIds(){
        List<String> ids = new ArrayList<>();
        if (null != types){
            for (Type type : types){
                if (type.isSelected()) {
                    ids.add(type.getId().trim());
                } else {
                    for (Type type1 : type.getSubType()){
                        if (type1.isSelected())
                            ids.add(type1.getId().trim());
                    }
                }
            }
        }
        return ids;
    }

    private void notifyItem(ItemViewHolder holder){
        if (selectionPosition == holder.getAdapterPosition()){
            return;
        } else {

            if (null != mEventHandle){
                //显示二级列表
                if (0 == holder.getAdapterPosition()){
                    ((MaterialStockViewModel)mEventHandle).setSub(null,holder.getAdapterPosition(),!isClear,0);
                } else {
                    Type type = types.get(holder.getAdapterPosition() - 1);
                    ((MaterialStockViewModel)mEventHandle).setSub(type.getSubType(),holder.getAdapterPosition(),type.isSelected(),type.getSelectedCount());
                }

            }
            lastPosition = selectionPosition;
            selectionPosition = holder.getAdapterPosition();
            notifyItemChanged(lastPosition);
            notifyItemChanged(selectionPosition);

        }
    }

    public void initSelected(){
        isClear = false;
        temSub = totalSub;
        for (Type type : types){
            type.setSelected(true);
            if (null != type.getSubType())
                type.setSelectedCount(type.getSubType().size());
            if(null != type.getSubType() && type.getSubType().size()>0){
                for (Type type1 : type.getSubType()){
                    type1.setSelected(false);
                }
            }
        }
    }
    public void clear(){
        isClear = true;
        temSub = 0;

        for (Type type: types){
            type.setSelected(false);
            type.setSelectedCount(0);
            for (Type type1 : type.getSubType()){
                type1.setSelected(false);
            }
        }
        notifyItemRangeChanged(0,getItemCount());
        //通过哦viewmodel改变当前选中后的二级分类
        if (null != mEventHandle) {
            if (0 == selectionPosition) {
                ((MaterialStockViewModel) mEventHandle).setSub(null, selectionPosition, !isClear,0);
            } else {
                Type type = types.get(selectionPosition - 1);
                ((MaterialStockViewModel) mEventHandle).setSub(type.getSubType(), selectionPosition, type.isSelected(),type.getSelectedCount());
            }
        }

    }
    public void selectMain(boolean selected, int mainIndex,int subIndex,int count){
//      mainIndex = 0 的状况已经单独处理
        int subCount = types.get(mainIndex -1).getSubType().size();
        if (subIndex == 0){
            setSubListStatus(selected,mainIndex - 1);
        } else if (count == subCount) {
            setSubListStatus(selected,mainIndex - 1);
        }else {
            types.get(mainIndex - 1).getSubType().get(subIndex - 1).setSelected(selected);
            types.get(mainIndex - 1).setSelected(count == subCount);
            types.get(mainIndex - 1).setSelectedCount(count);
        }
        if (isSelectedAll()){
            temSub = totalSub;
            initSelected();
            notifyItemRangeChanged(0,getItemCount());
        } else {
            notifyItemChanged(mainIndex);
            if (!isClear){
                isClear = true;
                temSub = 0;
                notifyItemChanged(0);
            }

        }


    }

    //当二级列表的全部点击选中时需要将当前一种选中并且将二级都取消选中，当二级全部取消的时候，需要将当前一级取消选中状态。
    private void setSubListStatus(boolean selected, int main){
        int subCount = types.get(main).getSubType().size();
        types.get(main).setSelected(selected);
        types.get(main).setSelectedCount(selected ? subCount : 0);
        if (selected){
            for (Type type : types.get(main).getSubType()){
                type.setSelected(false);
            }
        }
    }


    private boolean isSelectedAll(){
        int temp = 0;
        for (Type type : types){
            temp += type.getSelectedCount();
        }
        return temp == totalSub;
    }
    public void setmEventHandle(Object mEventHandle) {
        this.mEventHandle = mEventHandle;
    }

    public StockMainTypeAdapter() {
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){

            inflater = LayoutInflater.from(parent.getContext());
        }
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.stock_main_type_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.binding.itemLl.setSelected(selectionPosition == holder.getAdapterPosition());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyItem(holder);
            }
        });
        if (0 == holder.getAdapterPosition()){
            holder.bind("全部");
            holder.status(getTemSub());
//            holder.binding.count.setVisibility(temSub > 0 ? View.VISIBLE : View.INVISIBLE);
//            holder.binding.count.setText(getTemSub());

        } else {
            holder.bind(types.get(position-1).getName());
            int count = types.get(position-1).getSelectedCount();
            holder.status(count);
//            holder.binding.count.setVisibility(show? View.VISIBLE : View.INVISIBLE);
//            holder.binding.count.setText(count);
        }


    }

    @Override
    public int getItemCount() {
        return null == types ? 0 : types.size()+1;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private StockMainTypeItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (StockMainTypeItemViewBinding) binding;
        }

        public void bind(String name){
            binding.setName(name);
        }

        public void status(int count){
            binding.countTv.setVisibility(count > 0 ? View.VISIBLE : View.INVISIBLE);
            binding.countTv.setText(count + "");
        }

    }
}
