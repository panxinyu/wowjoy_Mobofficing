package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ViewDataBinding;
import android.nfc.NfcEvent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.appedit.AppEditViewModel;
import cn.wowjoy.office.data.response.AppInfo;
import cn.wowjoy.office.databinding.MenuItemViewBinding;

/**
 * Created by Sherily on 2017/9/25.
 */

public class DragRVAdapter extends RecyclerView.Adapter<DragRVAdapter.DragItemViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private boolean isEdited = false;
    private List<AppInfo> datas;
    private MenuItemViewBinding binding;
    private final ObservableBoolean edit = new ObservableBoolean(false);
    protected Object mItemEventHandler;

    public void setEdit(boolean edit) {
        this.edit.set(edit);
    }


    public void setmItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }

    public DragRVAdapter() {
    }

    public void setAppInfos(List<AppInfo> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void refreshStatus(boolean isEdited){
        this.isEdited = isEdited;
        notifyDataSetChanged();
    }

    public void addMore(AppInfo appInfo){
        if (null == datas)
            datas = new ArrayList<>();
        datas.add(appInfo);
        notifyDataSetChanged();
    }


    @Override
    public DragItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.menu_item_view,parent,false);
        return new DragItemViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(DragItemViewHolder holder, int position) {

        binding.setEdit(edit);
        binding.setModel(datas.get(holder.getAdapterPosition()));
        binding.setItemeventhadle((AppEditViewModel) mItemEventHandler);
        binding.ivIcon.setImageResource(datas.get(position).getIcon());
//        Glide.with(context).load(datas.get(position).getIcon()).error(R.mipmap.ic_launcher_round).placeholder(R.mipmap.ic_launcher_round).centerCrop().into(binding.ivIcon);
        binding.ivHadle.setVisibility(edit.get() ? View.VISIBLE : View.GONE);
        binding.ivHadle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != onItemRemoveListener){
                    onItemRemoveListener.onRemove(datas.get(position));
                }
                datas.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });

    }

    private OnItemRemoveListener onItemRemoveListener;

    public void setOnItemRemoveListener(OnItemRemoveListener onItemRemoveListener) {
        this.onItemRemoveListener = onItemRemoveListener;
    }

    public interface OnItemRemoveListener{
        void onRemove(AppInfo appInfo);
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class DragItemViewHolder<B extends ViewDataBinding> extends RecyclerView.ViewHolder {
        public DragItemViewHolder(View itemView) {
            super(itemView);
        }
    }
}
