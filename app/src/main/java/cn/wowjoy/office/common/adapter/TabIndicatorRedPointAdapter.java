package cn.wowjoy.office.common.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shizhefei.view.indicator.IndicatorViewPager;

import java.util.List;

import cn.bingoogolapple.badgeview.BGABadgeTextView;
import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;

public class TabIndicatorRedPointAdapter extends IndicatorViewPager.IndicatorFragmentPagerAdapter {
    private LayoutInflater inflater;
    private List<String> titles;
    private List<NewBaseFragment> fragments;

    public TabIndicatorRedPointAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void setData(List<String> titles,List<NewBaseFragment> fragments){
        this.titles = titles;
        this.fragments = fragments;
        notifyDataSetChanged();

    }
    @Override
    public int getCount() {
        return null == titles ? 0 : titles.size();
    }

    @Override
    public View getViewForTab(int position, View convertView, ViewGroup container) {
        if (null == inflater)
            inflater = LayoutInflater.from(container.getContext());
        if (null == convertView)
            convertView = inflater.inflate(R.layout.tab_main2, container, false);
        BGABadgeTextView textView = (BGABadgeTextView) convertView;
        textView.setText(titles.get(position));
        textView.getBadgeViewHelper().setBadgeTextSizeSp(5);
        textView.getBadgeViewHelper().setBadgeBorderWidthDp(1);
        textView.getBadgeViewHelper().setBadgeBgColorInt(container.getContext().getResources().getColor(R.color.custom_key_red_pressed));
//        textView.getBadgeViewHelper().showCirclePointBadge();
        return textView;
    }

    @Override
    public Fragment getFragmentForPage(int position) {
        return fragments.get(position);
    }

}
