package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListSingle;
import cn.wowjoy.office.databinding.ItemRvStockApplyNodoneBinding;
import cn.wowjoy.office.materialinspection.applyfor.StockApplyIndexViewModel;

public class ApplyDemandItemAdapter extends RecyclerView.Adapter<ApplyDemandItemAdapter.ItemViewHolder> {

    private LayoutInflater inflater;
    private List<StockApplyDemandHeadListSingle> datas;
    private Object handler;

    public void refresh(List<StockApplyDemandHeadListSingle> data){
        if (null != datas){
            datas.clear();
        }
        this.datas = data;
        notifyDataSetChanged();
    }

    public void loadMore(List<StockApplyDemandHeadListSingle> data){
        this.datas.addAll(data);
        notifyDataSetChanged();
    }


    public void setHandler(Object handler) {
        this.handler = handler;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());
        ItemRvStockApplyNodoneBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_rv_stock_apply_nodone,parent,false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(handler,datas.get(position));
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemRvStockApplyNodoneBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemRvStockApplyNodoneBinding) binding;
        }

        public void bind(Object viewModel, StockApplyDemandHeadListSingle model){
            binding.setViewModel((StockApplyIndexViewModel) viewModel);
            binding.setModel(model);
        }
    }
}
