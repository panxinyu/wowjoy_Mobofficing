package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.databinding.StockRecordItemViewBinding;
import cn.wowjoy.office.materialmanage.addReport.AddStockViewModel;

/**
 * Created by Sherily on 2018/1/17.
 * Description:
 */

public class  StockRecordAdapter extends RecyclerView.Adapter<StockRecordAdapter.ItemViewHodler> {

    private LayoutInflater inflater;
    private List<RecordResponse> datas;
    private Object mEventHandler;

    private List<RecordResponse> saveList;
    private boolean isCanNotEdit;


    public List<RecordResponse> getSaveList() {
        return saveList;
    }

    public void setCanNotEdit(boolean canNotEdit) {
        isCanNotEdit = canNotEdit;
    }

    public void setDatas(List<RecordResponse> datas) {
        this.datas = datas;
        saveList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void notifyRealStock(int position,String count){
        datas.get(position).setRealStockCount(count);
        RecordResponse recordResponse = datas.get(position);
        if (!isContian(recordResponse)){
            saveList.add(recordResponse);
        }
        notifyItemChanged(position);
    }

    public void changeItem(int position,RecordResponse recordResponse){
         if(null != datas){
             datas.remove(position);
             datas.add(position,recordResponse);
             if (!isContian(recordResponse)){
                 saveList.add(recordResponse);
             }
             notifyItemChanged(position);
         }

    }


    private boolean isContian(RecordResponse recordResponse){
        return saveList.contains(recordResponse);
    }
    public void addRecord(RecordResponse recordResponse){
        if (null == this.datas)
            this.datas = new ArrayList<>();
        this.datas.add(0,recordResponse);
        if (!isContian(recordResponse))
            saveList.add(recordResponse);
        notifyDataSetChanged();
        notifyItemChanged(0);
    }

    public void deleteRecord(RecordResponse recordResponse){
        this.datas.remove(recordResponse);
        if (isContian(recordResponse)){
            saveList.remove(recordResponse);
        }
        notifyDataSetChanged();
    }
    public void setmEventHandler(Object mEventHandler) {
        this.mEventHandler = mEventHandler;
    }

    @Override
    public ItemViewHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            inflater = LayoutInflater.from(parent.getContext());
        }
        return new ItemViewHodler(DataBindingUtil.inflate(inflater, R.layout.stock_record_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHodler holder, int position) {
        holder.bind(mEventHandler,datas.get(holder.getAdapterPosition()),isCanNotEdit);
        holder.binding.historyTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isOpen = datas.get(holder.getAdapterPosition()).isOpen();
                datas.get(holder.getAdapterPosition()).setOpen(!isOpen);
                holder.showHide(datas.get(holder.getAdapterPosition()));
            }
        });
        if (!isCanNotEdit){
            holder.binding.tvBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mEventHandler){
                        ((AddStockViewModel)mEventHandler).editItemStock(datas.get(holder.getAdapterPosition()),holder.getAdapterPosition());
                    }
                }
            });
            //三月份版本暂时不支持修改
//            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (null != mEventHandler){
//                        ((AddStockViewModel)mEventHandler).jumpTo(datas.get(holder.getAdapterPosition()),holder.getAdapterPosition());
//                    }
//                }
//            });
            holder.binding.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mEventHandler){
                        ((AddStockViewModel)mEventHandler).delete(datas.get(holder.getAdapterPosition()),holder.getAdapterPosition());
                    }
                }
            });
        } else {
            holder.binding.tvBtn.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public class ItemViewHodler extends  RecyclerView.ViewHolder{
        private StockRecordItemViewBinding binding;
        public ItemViewHodler(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (StockRecordItemViewBinding) binding;
        }

        public void bind(Object mEventHandler,RecordResponse response,boolean isCanNotEdit){
            binding.setViewmodel((AddStockViewModel) mEventHandler);
            binding.setModel(response);
            initRV(response);
            binding.realStock.setSelected(TextUtils.isEmpty(response.getRealStockCount()));

//            if (!isCanNotEdit){
//                if (!TextUtils.isEmpty(response.getRealStockCount())){
//                    binding.realStock.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.edit_res_selector,0);
//                } else {
//                    binding.realStock.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
//                }
//            }
            binding.historyTitle.setSelected(response.isOpen());
            binding.recyclerView.setVisibility(response.isOpen() ? View.VISIBLE : View.GONE);
        }

        private void initRV(RecordResponse response){
            if (null != response.getStaffRecordResponses() && !response.isCanDelete()) {
                binding.historyRl.setVisibility(response.getStaffRecordResponses().isEmpty() ? View.GONE : View.VISIBLE);
                if (!response.getStaffRecordResponses().isEmpty()) {
                    RecordAdapter adapter = new RecordAdapter(response.getStaffRecordResponses());
                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(binding.recyclerView.getContext()));
                    binding.recyclerView.setAdapter(adapter);
                    binding.recyclerView.addItemDecoration(new SimpleItemDecoration(binding.recyclerView.getContext(), SimpleItemDecoration.VERTICAL_LIST));
                }
            } else {
                binding.historyRl.setVisibility(View.GONE);
            }
        }

        public void showHide(RecordResponse response){
            binding.historyTitle.setSelected(response.isOpen());
            binding.recyclerView.setVisibility(response.isOpen() ? View.VISIBLE : View.GONE);
        }


    }
}
