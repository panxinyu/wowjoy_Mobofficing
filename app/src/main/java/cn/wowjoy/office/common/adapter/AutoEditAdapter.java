package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.AssetsCardQueryCondition;

/**
 * Created by Administrator on 2018/1/22.
 */

public class AutoEditAdapter extends BaseAdapter implements Filterable {
    private List<AssetsCardQueryCondition> mList;
    private Context mContext;
    private ArrayFilter mFilter;
    private ArrayList<AssetsCardQueryCondition> mUnfilteredData;

    public AutoEditAdapter(List<AssetsCardQueryCondition> list, Context context) {
        mList = list;
        mContext = context;
    }

    public AutoEditAdapter(Context context) {
        mContext = context;
    }

    public void setFilterList(List<AssetsCardQueryCondition> mLists){
        if(null == mList){
            mList = new ArrayList<>();
        }
        mList.clear();
        mList.addAll(mLists);
//        this.mList = mLists;
//        notifyDataSetChanged();
   }
    @Override
    public int getCount() {
        return null == mList ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
//        Log.e("PXY", "getItem: "+ mList.get(position).getProduceNo());
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_add_autoedit, null, false);

            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        AssetsCardQueryCondition pc = mList.get(position);
        if (null != pc.getProduceNo()) {
            holder.tv_no.setText(pc.getProduceNo());
        }

        return view;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }

    static class ViewHolder {
        public TextView tv_no;
        public LinearLayout mLinearLayout;

        public ViewHolder(View itemView) {
            tv_no = itemView.findViewById(R.id.produce_no);
            mLinearLayout=itemView.findViewById(R.id.ll_add);
        }
    }

    private class ArrayFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

//            if (mUnfilteredData == null) {
//                mUnfilteredData = new ArrayList<AssetsCardQueryCondition>(mList);
//            }
//
//            if (prefix == null || prefix.length() == 0) {
//                ArrayList<AssetsCardQueryCondition> list = mUnfilteredData;
//                results.values = list;
//                results.count = list.size();
//            } else {
//                String prefixString = prefix.toString().toLowerCase();
//
//                ArrayList<AssetsCardQueryCondition> unfilteredValues = mUnfilteredData;
//                int count = unfilteredValues.size();
//
//                ArrayList<AssetsCardQueryCondition> newValues = new ArrayList<AssetsCardQueryCondition>(count);
//
//                for (int i = 0; i < count; i++) {
//                    AssetsCardQueryCondition pc = unfilteredValues.get(i);
//                    if (pc != null) {
//
//                        if (pc.getProduceNo() != null && pc.getProduceNo().startsWith(prefixString)) {
//
//                            newValues.add(pc);
//                        }
//                    }
//
//                    results.values = newValues;
//                    results.count = newValues.size();
//                }
//            }
            results.values = mList;
            results.count = mList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mList = (List<AssetsCardQueryCondition>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }

}