package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.AppInfo;
import cn.wowjoy.office.databinding.SearchAppItemViewBinding;

import cn.wowjoy.office.search.SearchViewModle;

/**
 * Created by Sherily on 2017/11/9.
 * Description:
 */

public class SearchAppInfoAdapter extends RecyclerView.Adapter<SearchAppInfoAdapter.SearchAppInfoItemViewHolder> {

    private SearchAppItemViewBinding binding;
    private LayoutInflater inflater;
    private Context mContext;
    private List<AppInfo> datas;
    private String keyWord;

    public void setDatas(List<AppInfo> datas, String keyWord) {

        this.datas = datas;
        this.keyWord = keyWord;

    }

    public SpannableStringBuilder matcherSearchText(int color, float textSize, String text, String keyword) {
        if ( textSize == 0.0f )
            textSize = 15f;
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, textSize, mContext.getResources().getDisplayMetrics());
        SpannableStringBuilder ss = new SpannableStringBuilder(text);
        Pattern pattern = Pattern.compile(keyword);
        Matcher matcher = pattern.matcher(ss);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            ss.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(new AbsoluteSizeSpan(size), start , end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        return ss;
    }

    public SearchAppInfoAdapter() {
    }

    private Object mEventHandleListener;

    public void setmEventHandleListener(Object mEventHandleListener) {
        this.mEventHandleListener = mEventHandleListener;
    }

    @Override
    public SearchAppInfoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if ( null == mContext){
            mContext = parent.getContext();
            inflater = LayoutInflater.from(mContext);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.search_app_item_view,parent,false);
        return new SearchAppInfoItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(SearchAppInfoItemViewHolder holder, int position) {
        holder.bindData(mEventHandleListener,keyWord,datas.get(position));

        Log.d("key",keyWord+position);
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class SearchAppInfoItemViewHolder extends RecyclerView.ViewHolder{
        private SearchAppItemViewBinding binding;
        public SearchAppInfoItemViewHolder(SearchAppItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(Object presenter,String keyWord,AppInfo appInfo){
            binding.setPresenter((SearchViewModle) presenter);
            binding.setKeyword(keyWord);
            binding.setModel(appInfo);
            binding.icon.setImageResource(appInfo.getIcon());
        }
    }
}
