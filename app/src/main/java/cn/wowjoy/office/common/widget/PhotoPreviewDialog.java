package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.common.adapter.ViewPagerAdapter;
import cn.wowjoy.office.databinding.PhotoBrowserLayoutBinding;

/**
 * Created by Sherily on 2017/11/27.
 * Description:
 */

public class PhotoPreviewDialog extends DialogFragment implements ViewPagerAdapter.OnDismissListener, ViewPager.OnPageChangeListener {
    private static String KEY = "ARGS_KEY";
    private Context context;
    private int index = -1;
    private List<ImageView> indicators;
    private int mIndicatorSelectedResId = R.drawable.gray_radius;
    private int mIndicatorUnselectedResId = R.drawable.white_radius;
    private int mIndicatorMargin = PADDING_SIZE;
    private int mIndicatorWidth = INDICATOR_SIZE;
    private int mIndicatorHeight = INDICATOR_SIZE;
    public static final int INDICATOR_SIZE=20;
    public static final int PADDING_SIZE=10;
    private int lastPosition=0;
    private ViewPagerAdapter adapter;
    private PhotoBrowserLayoutBinding binding;

    public static PhotoPreviewDialog newInstance(Param param) {
        Bundle args = new Bundle();
        args.putParcelable(KEY, param);
        PhotoPreviewDialog fragment = new PhotoPreviewDialog();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onStart()
    {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( dm );
        getDialog().getWindow().setLayout( dm.widthPixels, getDialog().getWindow().getAttributes().height );

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.BlackNoTitle);
//        setCancelable(true);
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.photo_browser_layout,container,false);
        return binding.getRoot();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
    }
    private void createIndicator(int count,int position) {
        if ( count > 1 ){
            binding.indicator.setVisibility(View.VISIBLE);
            if (indicators == null)
                indicators = new ArrayList<>();
            else
                indicators.clear();
            binding.indicator.removeAllViews();
            for (int i = 0; i < count; i++) {
                ImageView imageView = new ImageView(getContext());
                indicators.add(imageView);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mIndicatorWidth,mIndicatorHeight);
                params.leftMargin = mIndicatorMargin;
                params.rightMargin = mIndicatorMargin;
                if(i == position){
                    imageView.setImageResource(mIndicatorSelectedResId);
                    lastPosition = position;
                }else{
                    imageView.setImageResource(mIndicatorUnselectedResId);
                }
                binding.indicator.addView(imageView, params);
            }
        } else
            binding.indicator.setVisibility(View.GONE);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null) {
            Param param = args.getParcelable(KEY);
            List<String> filePaths = param.filePaths;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int count = param.filePaths.size();
            index = index == -1 ? param.index : index;
            List<View> views = new ArrayList<>();
            if (adapter == null && filePaths != null && count != 0) {
                createIndicator(count, index);
                String token = param.head;
                adapter = new ViewPagerAdapter(getContext(), filePaths,token);
                adapter.setOnDismissListener(this);
                binding.viewPager.setAdapter(adapter);
                binding.viewPager.setCurrentItem(index);
                binding.viewPager.addOnPageChangeListener(this);
            }

        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        indicators.get(lastPosition).setImageResource(mIndicatorUnselectedResId);
        indicators.get(position).setImageResource(mIndicatorSelectedResId);
        lastPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void dimiss() {
        dismissAllowingStateLoss();
    }


    public static class Builder {
        Param param;
        int index;
        List<String> filePaths;
        String heard;

        public Builder() {
            param = new Param();
        }

        public Builder index(int index) {
            this.index = index;
            return this;
        }

        public Builder path(List<String> filePaths) {
            this.filePaths = filePaths;
            return this;
        }

        public Builder needHeader(String heard){
            this.heard = heard;
            return this;
        }

        public PhotoPreviewDialog build() {
            param.index = index;
            param.filePaths = filePaths;
            param.head = heard;
            PhotoPreviewDialog dialog = PhotoPreviewDialog.newInstance(param);
            return dialog;
        }
    }

    public static class Param implements Parcelable {
        public int index;
        public List<String> filePaths;
        String head;


        public Param() {
        }

        protected Param(Parcel in) {
            index = in.readInt();
            filePaths = in.createStringArrayList();
            head = in.readString();
        }

        public static final Creator<Param> CREATOR = new Creator<Param>() {
            @Override
            public Param createFromParcel(Parcel in) {
                return new Param(in);
            }

            @Override
            public Param[] newArray(int size) {
                return new Param[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(index);
            dest.writeStringList(filePaths);
            dest.writeString(head);
        }
    }



}
