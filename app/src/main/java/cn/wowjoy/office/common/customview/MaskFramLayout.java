package cn.wowjoy.office.common.customview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

/**
 * Created by Sherily on 2017/11/13.
 * Description:
 */

public class MaskFramLayout extends FrameLayout {
    public MaskFramLayout(@NonNull Context context) {
        super(context);
        init();
    }

    public MaskFramLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MaskFramLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private float downx = 0;
    private float downy = 0;
    private float upx = 0;
    private float upy = 0;

    /**
     *初始化拦截焦点操作，不传递到下面的view
     */
    private void init(){
        this.setClickable(true);
    }

    /**
     * 目前只做最简单的只有一个child的拦截透明区域的操作
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getChildAt(0);
        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                downx = ev.getX();
                downy = ev.getY();

                break;
            case MotionEvent.ACTION_UP:
                upx = ev.getX();
                upy = ev.getY();
                double tanValue = Math.abs(upy - downy) / Math.abs(upx - downx);
                if (!(upx > view.getLeft() && upx < view.getLeft() + view.getWidth() && upy > view.getTop() && upy < view.getTop() + view.getHeight())/* && tanValue < 1*/){
                    if (null != dimissListener){
                        dimissListener.onDismiss();
                    }
                    return true;
                }
                break;
        }

        return super.dispatchTouchEvent(ev);
    }
    public void setBgAlapht(int alapht){
        this.getBackground().setAlpha(alapht);
    }
    public interface DimissListener{
        void onDismiss();
    }
//    public interface DisplayListener{
//        void onDisplay(boolean isOpen);
//    }
//
//    private DisplayListener displayListener;
//
//    public void setDisplayListener(DisplayListener displayListener) {
//        this.displayListener = displayListener;
//    }
//
//    private void hideSoftInput(){
//        InputMethodManager imm = (InputMethodManager) this.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
//    }
//
//    /**
//     * 文件夹列表打开关闭方式：从上往下打开，从下往上关闭
//     */
//    public void downOpenUpClose(boolean isOpen, int initHeight) {
//
//        hideSoftInput();
//        if (!isOpen) {
//            getChildAt(0).setVisibility(View.VISIBLE);
//            if (getChildAt(0).getHeight() != 0){
//                initHeight = getChildAt(0).getHeight();
//            }
//            ObjectAnimator animator = ObjectAnimator.ofFloat(getChildAt(0), "translationY",
//                    -initHeight, 0).setDuration(300);
//            animator.addListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationStart(Animator animation) {
//                    super.onAnimationStart(animation);
//                    getChildAt(0).setVisibility(View.VISIBLE);
//                }
//            });
//            animator.start();
//            isOpen = true;
//            displayListener.onDisplay(isOpen);
//
//        } else {
//            if (getChildAt(0).getHeight() != 0){
//                initHeight = getChildAt(0).getHeight();
//            }
//            ObjectAnimator animator = ObjectAnimator.ofFloat(getChildAt(0), "translationY",
//                    0,-initHeight).setDuration(300);
//            animator.addListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    super.onAnimationEnd(animation);
//                    getChildAt(0).setVisibility(View.GONE);
//                    getChildAt(0).setVisibility(View.GONE);
//                }
//            });
//            animator.start();
//            isOpen = false;
//            displayListener.onDisplay(isOpen);
//
//        }
//    }
//
//    public void downCloseUpOpen(boolean isOpen, int initHeight) {
//
//        hideSoftInput();
//        if (!isOpen) {
//            getChildAt(0).setVisibility(View.VISIBLE);
//            if (getChildAt(0).getHeight() != 0){
//                initHeight = getChildAt(0).getHeight();
//            }
//            ObjectAnimator animator = ObjectAnimator.ofFloat(getChildAt(0), "translationY",
//                    0, initHeight).setDuration(300);
//            animator.addListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationStart(Animator animation) {
//                    super.onAnimationStart(animation);
//                    getChildAt(0).setVisibility(View.VISIBLE);
//                }
//            });
//            animator.start();
//            isOpen = true;
//            displayListener.onDisplay(isOpen);
//        } else {
//            if (getChildAt(0).getHeight() != 0){
//                initHeight = getChildAt(0).getHeight();
//            }
//            ObjectAnimator animator = ObjectAnimator.ofFloat(getChildAt(0), "translationY",
//                    initHeight,0).setDuration(300);
//            animator.addListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    super.onAnimationEnd(animation);
//                    getChildAt(0).setVisibility(View.GONE);
//                    getChildAt(0).setVisibility(View.GONE);
//                }
//            });
//            animator.start();
//            isOpen = false;
//            displayListener.onDisplay(isOpen);
//        }
//    }


    private DimissListener dimissListener;

    public void setDimissListener(DimissListener dimissListener) {
        this.dimissListener = dimissListener;
    }
}
