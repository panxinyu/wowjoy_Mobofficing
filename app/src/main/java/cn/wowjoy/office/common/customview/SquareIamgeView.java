package cn.wowjoy.office.common.customview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by Sherily on 2017/11/16.
 * Description:正方形的ImageView
 */

public class SquareIamgeView extends AppCompatImageView {
    public SquareIamgeView(Context context) {
        super(context);
    }

    public SquareIamgeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareIamgeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
