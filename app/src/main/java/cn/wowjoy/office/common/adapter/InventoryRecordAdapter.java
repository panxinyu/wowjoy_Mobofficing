package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InventoryRecordResponse;
import cn.wowjoy.office.databinding.InventoryRecordItemViewBinding;
import cn.wowjoy.office.materialmanage.inventoryRecord.InventoryRecordViewModel;

/**
 * Created by Sherily on 2018/1/12.
 * Description:
 */

public class InventoryRecordAdapter extends RecyclerView.Adapter<InventoryRecordAdapter.ItemViewHolder> {
   private List<InventoryRecordResponse> datas;
   private LayoutInflater inflater;
    private Object mEventHandle;

    public void setmEventHandle(Object mEventHandle) {
        this.mEventHandle = mEventHandle;
    }

    public void setDatas(List<InventoryRecordResponse> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.inventory_record_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(mEventHandle,datas.get(position));
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private InventoryRecordItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (InventoryRecordItemViewBinding) binding;
        }

        public void bind(Object handler,InventoryRecordResponse response){
            binding.setViewmodel((InventoryRecordViewModel) handler);
            binding.setModel(response);
        }
    }
}
