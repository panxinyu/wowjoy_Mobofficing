package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.HashSet;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.applystock.receipts.ReceiptsDetailViewModel;
import cn.wowjoy.office.common.widget.MyToast;
import cn.wowjoy.office.data.response.DetailVoInfo;
import cn.wowjoy.office.data.response.QRVoInfo;
import cn.wowjoy.office.databinding.ReceiptsDetailItemViewBinding;

public class ReceiptsDetailAdapter extends RecyclerView.Adapter<ReceiptsDetailAdapter.ItemViewHolder> {

    private List<DetailVoInfo> datas;
    private LayoutInflater inflater;
    private Context context;
    private boolean isSwipe;
    private boolean isRequisition;
    private int matchPosition;
    private Object handle;
    private HashSet<String> qrCodeSet;


    public void setHandle(Object handle) {
        this.handle = handle;
    }

    public void setSwipe(boolean swipe) {
        isSwipe = swipe;
    }

    public void setRequisition(boolean requisition) {
        isRequisition = requisition;
    }

    public void setDatas(List<DetailVoInfo> datas){
        this.datas = datas;
        notifyDataSetChanged();
    }

    public List<DetailVoInfo> getDatas() {
        return datas;
    }

    public int addQRVoInfo(QRVoInfo info, boolean isRequisition){
        if (null == qrCodeSet)
            qrCodeSet = new HashSet<>();
        boolean isMatch = false;
        for (DetailVoInfo detailVoInfo : datas){
            if (TextUtils.equals(info.getGoodsCode(),detailVoInfo.getGoodsCode2()) && detailVoInfo.isNeedQR()){
                matchPosition = datas.indexOf(detailVoInfo);
                isMatch = true;
                if (isRequisition){
                    //调拨单
                    if (datas.get(matchPosition).isQRDone()){
                        //toast
                        matchPosition = -1;
                        new MyToast(context).showinfo("添加失败，已超过本笔调拨单最大数量");
//                        Toast.makeText(context, "添加失败，已超过本笔调拨单最大数量", Toast.LENGTH_SHORT).show();
                    } else {
                        if (qrCodeSet.contains(info.getQrCode())){
                            matchPosition = -1;
                            new MyToast(context).showinfo("添加失败，该物质已在本单添加过了");

//                            Toast.makeText(context, "添加失败，该物质已在本单添加过了", Toast.LENGTH_SHORT).show();
                        } else {
                            qrCodeSet.add(info.getQrCode());
                            datas.get(matchPosition).addqRList(info);
                            notifyItemChanged(matchPosition);
                        }

                    }
                } else {
                    // 申请单
                    if (datas.get(matchPosition).isQRVODone()){
                        //toast
                        matchPosition = -1;
                        new MyToast(context).showinfo("添加失败，已超过本笔调拨单最大数量");
//                        Toast.makeText(context, "添加失败，已超过本笔申领单最大数量", Toast.LENGTH_SHORT).show();

                    } else {
                        if (qrCodeSet.contains(info.getQrCode())){
                            matchPosition = -1;
                            new MyToast(context).showinfo("添加失败，该物质已在本单添加过了");
//                            Toast.makeText(context, "添加失败，该物质已在本单添加过了", Toast.LENGTH_SHORT).show();

                        } else {
                            qrCodeSet.add(info.getQrCode());
                            datas.get(matchPosition).addQRVOList(info);
                            notifyItemChanged(matchPosition);

                        }
                    }
                }
            } else {
                matchPosition = -1;

            }
        }
        if (!isMatch)
            new MyToast(context).showinfo("该物质不在本笔明细单内，请核对后扫码");

//        Toast.makeText(context, "该物质不在本笔明细单内，请核对后扫码", Toast.LENGTH_SHORT).show();


        return matchPosition;

    }
    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        ReceiptsDetailItemViewBinding binding = DataBindingUtil.inflate(inflater, R.layout.receipts_detail_item_view,parent,false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.binding.itemLl.setSwipeEnable(isSwipe);
        holder.bind(handle,datas.get(position));
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private ReceiptsDetailItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ReceiptsDetailItemViewBinding) binding;
        }

        public void bind(Object viewModel,DetailVoInfo detailVoInfo){
            binding.setViewModel((ReceiptsDetailViewModel) viewModel);
            binding.setModel(detailVoInfo);
        }
    }
}
