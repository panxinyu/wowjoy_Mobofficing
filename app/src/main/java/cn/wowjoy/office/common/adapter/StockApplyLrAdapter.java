package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.common.customview.SwipeMenuView;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.databinding.ItemRvStockApplyInfoBinding;
import cn.wowjoy.office.databinding.ItemRvStockApplyInfoHandBinding;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceViewModel;

/**
 * Created by Administrator on 2018/4/12.
 */

public class StockApplyLrAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private LayoutInflater inflater;
    private ItemRvStockApplyInfoBinding bindingScan;
    private ItemRvStockApplyInfoHandBinding bindingHand;

    protected Object mItemEventHandler;
    public List<ApplyDemandHeadDetailItem> datas;

    private String keyWord;
    private String mToken;
    public void setmItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }

    public void setDates(List<ApplyDemandHeadDetailItem> list){
        if(null == datas){
            datas = new ArrayList<>();
        }
        datas.clear();
        datas.addAll(list);
        notifyDataSetChanged();
    }


//    public void addDate(ApplyDemandHeadDetailItem recordResponse){
//        if (null == this.datas)
//            this.datas = new ArrayList<>();
//
//        this.datas.add(0,recordResponse);
////        if (!isContian(recordResponse))
////            saveList.add(recordResponse);
//        notifyDataSetChanged();
////        notifyItemChanged(0);
//    }
    public void deleteDate(ApplyDemandHeadDetailItem data){
        if (null == datas || datas.size() == 0){
           return;
        }
        datas.remove(data);
        notifyDataSetChanged();
    }

    public void notifyRealStock(int position,String count){
        DecimalFormat fnum  =   new  DecimalFormat("##0.00");
        datas.get(position).setNeedNumber(Float.parseFloat(fnum.format(Float.parseFloat(count))));
        notifyItemChanged(position);
    }
    public void changeItem(ApplyDemandHeadDetailItem item){
        if(null != datas){
            int  position = datas.indexOf(item);
          //  datas.get(position).setFactOut(datas.get(position).getFactOut()+item.getFactOut());
            datas.remove(position);
            datas.add(position,item);
            notifyDataSetChanged();
        }

    }


    public static final int RECYCLEVIEW_ITEM_TYPE_1 = 1;
    public static final int RECYCLEVIEW_ITEM_TYPE_2 = 2;

//    public enum Item_Type {
//        RECYCLEVIEW_ITEM_TYPE_1, //扫码
//        RECYCLEVIEW_ITEM_TYPE_2  //手动
//    }

    //返回值赋值给onCreateViewHolder的参数 viewType
    @Override
    public int getItemViewType(int position) {
        if ("y".equals(datas.get(position).getIsQr())) {
            return RECYCLEVIEW_ITEM_TYPE_1;
        } else {
            return RECYCLEVIEW_ITEM_TYPE_2;
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        //扫码
        if (viewType == RECYCLEVIEW_ITEM_TYPE_1) {
            bindingScan = DataBindingUtil.inflate(inflater, R.layout.item_rv_stock_apply_info, parent,false);
            return new StockItemViewHolder(bindingScan);

        //手动
        } else /* if (viewType == RECYCLEVIEW_ITEM_TYPE_2) { */
            bindingHand = DataBindingUtil.inflate(inflater, R.layout.item_rv_stock_apply_info_hand, parent,false);
            return new StockItemViewHolderHand(bindingHand);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        ApplyDemandHeadDetailItem item = datas.get(position);
        if (holder instanceof StockItemViewHolder) {
            ((StockItemViewHolder) holder).bindData(item,mItemEventHandler);
            ((StockItemViewHolder) holder).binding.handleTv.setText("删除");
            ((StockItemViewHolder) holder).binding.handleTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("PXY", "getAdapterPosition: "+ holder.getAdapterPosition() +"  position"+ position+ "  data"+ datas.size());
                    if (null != mItemEventHandler){
                        ((StockApplyProduceViewModel)mItemEventHandler).delete(datas.get(position),position);
                    }
                }
            });
        } else if (holder instanceof StockItemViewHolderHand) {
            ((StockItemViewHolderHand) holder).bindData(item,mItemEventHandler);
            ((StockItemViewHolderHand) holder).binding.editHand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("PXY", "getAdapterPosition: "+ holder.getAdapterPosition() + "  data"+ datas.size());
                    if (null != mItemEventHandler){
                        ((StockApplyProduceViewModel)mItemEventHandler).editItemStock(datas.get(position),position);
                    }
                }
            });
            ((StockItemViewHolderHand) holder).binding.handleTv.setText("删除");
            ((StockItemViewHolderHand) holder).binding.handleTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("PXY", "getAdapterPosition: "+ holder.getAdapterPosition() +"  position"+ position+ "  data"+ datas.size());
                    if (null != mItemEventHandler){
                        ((StockApplyProduceViewModel)mItemEventHandler).delete(datas.get(position),position);
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
          return null == datas ? 0 :datas.size();
    }

    public static class StockItemViewHolder extends RecyclerView.ViewHolder {
        ItemRvStockApplyInfoBinding binding;
        public StockItemViewHolder(ItemRvStockApplyInfoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            ((SwipeMenuView)itemView).setIos(false).setLeftSwipe(true);
        }

        public void bindData(ApplyDemandHeadDetailItem response, Object mItemEventHandler){

            binding.setPresenter((StockApplyProduceViewModel) mItemEventHandler);
            binding.setModel(response);
            binding.executePendingBindings();
            binding.handleTv.setSelected(false);
        }
    }

    /**
     *  手动添加的ViewHolder
     */
    public static class StockItemViewHolderHand extends RecyclerView.ViewHolder {
        ItemRvStockApplyInfoHandBinding binding;
        public StockItemViewHolderHand(ItemRvStockApplyInfoHandBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            ((SwipeMenuView)itemView).setIos(false).setLeftSwipe(true);
        }

        public void bindData(ApplyDemandHeadDetailItem response, Object mItemEventHandler){

            binding.setPresenter((StockApplyProduceViewModel) mItemEventHandler);
            binding.setModel(response);
            binding.executePendingBindings();
            binding.handleTv.setSelected(false);

        }
    }
}
