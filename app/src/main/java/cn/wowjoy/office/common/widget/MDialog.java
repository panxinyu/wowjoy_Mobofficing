package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.jdsjlzx.recyclerview.LRecyclerView;
import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.List;

import javax.inject.Inject;

import cn.bingoogolapple.androidcommon.adapter.BGABindingViewHolder;
import cn.wowjoy.office.R;
import cn.wowjoy.office.utils.PreferenceManager;

public class MDialog extends Dialog {
    @Inject
    PreferenceManager preferenceManager;
    private String mTitle = null;
    private String mHintStr = null;
    private String mBottonL = null;
    private String mBottonR = null;
    private String mEdit0 = null;
    private String mEdit1 = null;
    private TextView mTileTextView;
    private TextView mHintTextView;
    private EditText mLoginUrl;
    private EditText mApiUrl;
    private View.OnClickListener mCancleClickListener = null;
    private View.OnClickListener mConfirmClickListener = null;

    private int type;
    private Button mCancelBtn = null;
    private Button mConfirmBtn = null;
    private List<String> data;
    private ViewDataBinding vb;
    private LRecyclerView mListRV;
    private LayoutInflater inflater;
    private Activity context;
    //    public BGABindingRecyclerViewAdapter<String, ItemAreaBinding> areainnerAdapter;
    public LRecyclerViewAdapter areaAdapter;
    public OnListClickedInterface mACInterface;

    public MDialog(Activity context, int theme) {
        super(context, theme);
    }

    public MDialog(Activity context, int theme, String title, String hint, String BottonL, String BottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        super(context, theme);
        mTitle = title;
        mHintStr = hint;
        mBottonL = BottonL;
        mBottonR = BottonR;
        mCancleClickListener = cancelBtnClickListener;
        mConfirmClickListener = confirmBtnClickListener;
    }

    public MDialog(Activity context, int theme, String title, String hint, int type, View.OnClickListener cancelBtnClickListener, String cancelText) {
        super(context, theme);
        this.context = context;
        mCancleClickListener = cancelBtnClickListener;
        mBottonL = cancelText;
        mTitle = title;
        mHintStr = hint;
        this.type = type;
    }

    public MDialog(Activity context, int theme, String hint, String BottonL, String BottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        super(context, theme);
        mHintStr = hint;
        mBottonL = BottonL;
        mBottonR = BottonR;
        mCancleClickListener = cancelBtnClickListener;
        mConfirmClickListener = confirmBtnClickListener;
    }

    public MDialog(Activity context, int theme, String edit0, String edit1) {
        super(context, theme);
        mEdit0 = edit0;
        mEdit1 = edit1;
    }

    public MDialog(Activity context, int theme, List<String> data) {
        super(context, theme);
        this.data = data;
        this.context = context;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (TextUtils.isEmpty(mTitle) && !TextUtils.isEmpty(mHintStr)) {
            setContentView(R.layout.dialog_alert);
            mHintTextView = (TextView) findViewById(R.id.alert_dialog_message);
            mConfirmBtn = (Button) findViewById(R.id.alert_dialog_confirm_btn);
            mConfirmBtn.setOnClickListener(mConfirmClickListener);
            mCancelBtn = (Button) findViewById(R.id.alert_dialog_cancel_btn);
            mCancelBtn.setOnClickListener(mCancleClickListener);

            mHintTextView.setText(mHintStr);
            mCancelBtn.setText(mBottonL);
            mConfirmBtn.setText(mBottonR);

        } else if (!TextUtils.isEmpty(mTitle) && !TextUtils.isEmpty(mHintStr) && type == 101) {
            setContentView(R.layout.dialog_submit);
            mTileTextView = (TextView) findViewById(R.id.alert_dialog_title);
            mHintTextView = findViewById(R.id.alert_dialog_message);
            mTileTextView.setText(mTitle);
            mHintTextView.setText(mHintStr);
            mCancelBtn = (Button) findViewById(R.id.alert_dialog_cancel_btn);
            mCancelBtn.setText(mBottonL);
            mCancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancel();
                }
            });
            ScrollView scrollView = findViewById(R.id.scroll_view);

            mHintTextView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {


                    ViewGroup.LayoutParams layoutParams = scrollView.getLayoutParams();
                    if (mHintTextView.getHeight() > 100) {
                        layoutParams.height = 92;
                    } else {
                        layoutParams.height = mHintTextView.getHeight();
                    }
                    scrollView.setLayoutParams(layoutParams);
//                    Log.i("Alex", "控件布局改变啦");
//                    Log.i("Alex", "getMeasuredHeight=" + mHintTextView.getMeasuredHeight());
//                    Log.i("Alex", "getMeasuredState=" + mHintTextView.getMeasuredState());
//                    Log.i("Alex", "getHeight=" + mHintTextView.getHeight());
//                    Log.i("Alex", "getWidth=" + mHintTextView.getWidth());
//                    Log.i("Alex", "getMeasuredWidth=" + mHintTextView.getMeasuredWidth());
                }
            });
        } else if (!TextUtils.isEmpty(mTitle) && !TextUtils.isEmpty(mHintStr) && type == 102) {
            setContentView(R.layout.dialog_submit);
            mTileTextView = (TextView) findViewById(R.id.alert_dialog_title);
            mHintTextView = findViewById(R.id.alert_dialog_message);
            mTileTextView.setText(mTitle);
            mHintTextView.setText(mHintStr);
            mCancelBtn = (Button) findViewById(R.id.alert_dialog_cancel_btn);
            mCancelBtn.setText(mBottonL);
            mCancelBtn.setTextColor(context.getResources().getColor(R.color.appTextDialog));
            mCancelBtn.setOnClickListener(mCancleClickListener);

        } else if (!TextUtils.isEmpty(mTitle) && !TextUtils.isEmpty(mHintStr)) {
            setContentView(R.layout.dialog_info);
            mTileTextView = (TextView) findViewById(R.id.alert_dialog_title);
            mHintTextView = (TextView) findViewById(R.id.alert_dialog_message);
            mConfirmBtn = (Button) findViewById(R.id.alert_dialog_confirm_btn);
            mConfirmBtn.setOnClickListener(mConfirmClickListener);
            mCancelBtn = (Button) findViewById(R.id.alert_dialog_cancel_btn);
            mCancelBtn.setOnClickListener(mCancleClickListener);

            mTileTextView.setText(mTitle);
            mHintTextView.setText(mHintStr);
            mCancelBtn.setText(mBottonL);
            mConfirmBtn.setText(mBottonR);
            if (TextUtils.isEmpty(mBottonL))
                mCancelBtn.setVisibility(View.GONE);
            if (TextUtils.isEmpty(mBottonR))
                mConfirmBtn.setVisibility(View.GONE);

        }
//        else if (data != null) {
//            vb = DataBindingUtil.inflate(inflater, R.layout.dialog_list, null, false);
//            setContentView(vb.getRoot());
//            areainnerAdapter = new BGABindingRecyclerViewAdapter<>(R.layout.item_dialog_list);
//            areainnerAdapter.setData(data);
//            areainnerAdapter.setItemEventHandler(this);
//            areaAdapter = new LRecyclerViewAdapter(areainnerAdapter);
//            mListRV = (LRecyclerView) vb.getRoot().findViewById(R.id.rv);
//            mListRV.setLayoutManager(new LinearLayoutManager(context));
//            mListRV.setPullRefreshEnabled(false);
//            mListRV.setAdapter(areaAdapter);
//            areaAdapter.removeHeaderView();
//            areaAdapter.removeFooterView();
//        }
        else if (TextUtils.isEmpty(mHintStr) && !TextUtils.isEmpty(mEdit0)) {
            setContentView(R.layout.dialog_edit);
            mLoginUrl = (EditText) findViewById(R.id.loginurl);
            mApiUrl = (EditText) findViewById(R.id.apiurl);
            mConfirmBtn = (Button) findViewById(R.id.alert_dialog_confirm_btn);
            mConfirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManager.getInstance().setLoginHost(mLoginUrl.getText().toString());
                    PreferenceManager.getInstance().setAPIHost(mApiUrl.getText().toString());
                    if (onConfirmListener != null) {
                        onConfirmListener.onConfirm();
                    }
                    cancel();
                }
            });
            mCancelBtn = (Button) findViewById(R.id.alert_dialog_cancel_btn);
            mCancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancel();
                }
            });

            mLoginUrl.setText(mEdit0);
            mApiUrl.setText(mEdit1);
        } else if (TextUtils.isEmpty(mHintStr))
            setContentView(R.layout.dialog_waiting);
    }

    public TextView getmTextView() {
        return mHintTextView;
    }

    public void OnListItemClicked(BGABindingViewHolder holder, String model) {
        if (mACInterface != null)
            mACInterface.OnListClicked(model);
        dismiss();
    }

    public void OnListClickItem(OnListClickedInterface onListClickedInterface) {
        mACInterface = onListClickedInterface;
    }


    public interface OnListClickedInterface {
        void OnListClicked(String ai);
    }

    public interface OnConfirmListener {
        void onConfirm();
    }

    private OnConfirmListener onConfirmListener;

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }
}
