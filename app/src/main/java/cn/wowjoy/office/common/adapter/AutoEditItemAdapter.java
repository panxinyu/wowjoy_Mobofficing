package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.AssetsCardQueryCondition;
import cn.wowjoy.office.materialinspection.check.detail.add.CheckAddViewModel;
import cn.wowjoy.office.materialinspection.stockout.otherout.StockOtherOutViewModel;

/**
 * Created by Administrator on 2018/1/23.
 */

public class AutoEditItemAdapter extends RecyclerView.Adapter<AutoEditItemAdapter.MyItemViewHolder> {
    private final int type;
    private List<AssetsCardQueryCondition> mAssetsCardQueryConditions;

    private Context context;
    private LayoutInflater inflater;
    //    private MeunItemViewBinding binding;
    private Object mEventListener;

    public void setAssetsCardQueryConditions(List<AssetsCardQueryCondition> assetsCardQueryConditions) {
        mAssetsCardQueryConditions = assetsCardQueryConditions;
        notifyDataSetChanged();
    }

    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    public AutoEditItemAdapter(int type) {
        this.type = type;
    }

    @Override
    public MyItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == context){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.meun_item_view,parent,false);
        return new MyItemViewHolder(inflater.inflate(R.layout.item_rv_auto_dialog,parent,false));
    }

    private int lastPosition = 0;

    @Override
    public void onBindViewHolder(MyItemViewHolder holder, int position) {
//        Log.d("menulist",holder.getAdapterPosition()+"::::"+popuModels.get(holder.getAdapterPosition()).getName()+"------datasize::::"+popuModels.size());
        if(type == 1){
            AssetsCardQueryCondition popuModel = mAssetsCardQueryConditions.get(holder.getAdapterPosition());
//        binding.setModel(popuModel);
//        Log.e("mConditions", "onBindViewHolder: "+ popuModel.getProduceNo());
            holder.mTextView.setText(popuModel.getProduceNo());
            holder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                if (lastPosition != holder.getAdapterPosition()){
                    notifyItemChanged(lastPosition);
                    notifyItemChanged(holder.getAdapterPosition());
                    Log.d("menulist","lastposition::::"+lastPosition+"------curposition::::"+holder.getAdapterPosition());
                    lastPosition = holder.getAdapterPosition();
                    ((CheckAddViewModel) mEventListener).getClick().setValue(mAssetsCardQueryConditions.get(lastPosition));
//                }

                }
            });
        }else  if(type == 2){
            AssetsCardQueryCondition popuModel = mAssetsCardQueryConditions.get(holder.getAdapterPosition());
//        binding.setModel(popuModel);
//        Log.e("mConditions", "onBindViewHolder: "+ popuModel.getProduceNo());
            holder.mTextView.setText(popuModel.getPatientName()+"("+popuModel.getPatientId()+")");
            holder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                if (lastPosition != holder.getAdapterPosition()){
                    notifyItemChanged(lastPosition);
                    notifyItemChanged(holder.getAdapterPosition());
                    Log.d("menulist","lastposition::::"+lastPosition+"------curposition::::"+holder.getAdapterPosition());
                    lastPosition = holder.getAdapterPosition();
                    ((StockOtherOutViewModel) mEventListener).getClick().setValue(mAssetsCardQueryConditions.get(lastPosition));
//                }

                }
            });
        }



    }
    @Override
    public int getItemCount() {
        return null == mAssetsCardQueryConditions ? 0 : mAssetsCardQueryConditions.size();
    }
//    @Override
//    public int getItemCount() {
//        if(null != mAssetsCardQueryConditions && mAssetsCardQueryConditions.size()<=3){
//            return  mAssetsCardQueryConditions.size();
//        }
//        return null == mAssetsCardQueryConditions ? 0 : 3;
//    }

    static  class MyItemViewHolder extends RecyclerView.ViewHolder{
       TextView mTextView;

       LinearLayout mLinearLayout;

       public MyItemViewHolder(View itemView) {
           super(itemView);
           mTextView = itemView.findViewById(R.id.produce_no);
           mLinearLayout = itemView.findViewById(R.id.ll_auto);
       }
   }
}
