package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InventoryTaskDtlVO;
import cn.wowjoy.office.databinding.ItemRvInspectCheckDetailBinding;
import cn.wowjoy.office.materialinspection.check.task.CheckTaskListViewModel;

public class ItemInventoryRvDetailAdapter extends RecyclerView.Adapter<ItemInventoryRvDetailAdapter.ViewHolder> {

    private List<InventoryTaskDtlVO> mEntities;
    private ItemRvInspectCheckDetailBinding binding;
    private Context context;
    private LayoutInflater layoutInflater;

    private Object mItemEventHandler;
    public void setItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }
    public void refresh(List<InventoryTaskDtlVO> data){
        if (null != mEntities){
            mEntities.clear();
        }
        this.mEntities = data;
        notifyDataSetChanged();
    }
    public void clearDate(){
        if (null != mEntities){
            mEntities.clear();
        }
        notifyDataSetChanged();
    }
    public int getDateSize(){
       return null == mEntities ? 0 :mEntities.size();
    }
    public void setData(List<InventoryTaskDtlVO> data){
        if (null == mEntities){
           mEntities = new ArrayList<>();
        }
        mEntities.clear();
        mEntities.addAll(data);
        notifyDataSetChanged();
    }

    public void loadMore(List<InventoryTaskDtlVO> data){
        this.mEntities.addAll(data);
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == layoutInflater) {
            context = parent.getContext();
            layoutInflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_rv_inspect_check_detail, parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        InventoryTaskDtlVO InventoryTaskDtlVO = mEntities.get(position);
//        Log.e("PXY", "position: "+position );
        holder.bindData(InventoryTaskDtlVO,mItemEventHandler);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
       return null == mEntities ? 0 :mEntities.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        ItemRvInspectCheckDetailBinding binding;
        public void setBgAndText( InventoryTaskDtlVO InventoryTaskDtlVO){
            //  设备状态鉴定      // 未盘 0    盘盈1   已盘 2  差异3  盘亏4  正常5
            GradientDrawable drawable = (GradientDrawable) binding.textState.getBackground();
            if(null != InventoryTaskDtlVO.getInventoryStatus()){
                switch (InventoryTaskDtlVO.getInventoryStatus()){
                    case "未盘点" :
                        drawable.setColor(context.getResources().getColor(R.color.menu_not_find));
                        break;
                    case "盘盈" :
                        drawable.setColor(context.getResources().getColor(R.color.menu_all));
                        break;
                    case "盘亏" :
                        drawable.setColor(context.getResources().getColor(R.color.menu_look_error));
                        break;
                    case "正常" :
                        drawable.setColor(context.getResources().getColor(R.color.menu_normal));
                        break;
                }
            }
        }

        public ViewHolder(ItemRvInspectCheckDetailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bindData(InventoryTaskDtlVO response,Object mItemEventHandler){

            binding.setViewModel((CheckTaskListViewModel) mItemEventHandler);
            binding.setModel(response);
            setBgAndText(response);
            binding.executePendingBindings();
        }
    }
}
