package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.ArrayList;

import cn.wowjoy.office.R;

import cn.wowjoy.office.common.widget.album.selector.ImageSelectorViewModel;
import cn.wowjoy.office.data.mock.Image;
import cn.wowjoy.office.databinding.ImageItemViewBinding;
import cn.wowjoy.office.databinding.TakePhotoViewBinding;
import cn.wowjoy.office.utils.ToastUtil;

/**
 * Created by Sherily on 2017/11/16.
 * Description:
 */

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TAKE_PHOTO = 0;
    private static final int PHPTO_REVIEW = 1;
    private Context mContext;
    private ArrayList<Image> mImages;
    private LayoutInflater mInflater;

    private ImageItemViewBinding imageItemViewBinding;
    private TakePhotoViewBinding takePhotoViewBinding;

    private Object mEventListener;

    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    //保存选中的图片
    private ArrayList<Image> mSelectImages = new ArrayList<>();

    private int mMaxCount;
    private boolean isSingle;
    @Override
    public int getItemViewType(int position) {
        if (0 == position){
            return TAKE_PHOTO;
        } else {
            return PHPTO_REVIEW;
        }
    }

    public ImageAdapter() {
    }

    public void initAdapter(int count, boolean isSingle){
        mMaxCount = count;
        this.isSingle = isSingle;
    }

    public ArrayList<Image> getData() {
        return mImages;
    }

    public void refresh(ArrayList<Image> data) {
        clearSelecteImages();
        mImages = data;
        notifyDataSetChanged();
    }
//    /**
//     * 设置图片选中和未选中的效果
//     */
//    private void setItemSelect( boolean isSelect) {
//        imageItemViewBinding.ivSelect.setSelected(isSelect);
//        if (isSelect) {
//            imageItemViewBinding.ivMasking.setAlpha(0.5f);
//        } else {
//            imageItemViewBinding.ivMasking.setAlpha(0.2f);
//        }
//    }

    private void clearImageSelect() {
        if (mImages != null && mSelectImages.size() == 1) {
            int index = mImages.indexOf(mSelectImages.get(0));
            if(index != -1){
                mSelectImages.clear();
                notifyItemChanged(index+1);
            }
        }
    }

    public ArrayList<Image> getSelectImages() {
        return mSelectImages;
    }
    /**
     * 选中图片
     * @param image
     */
    private void selectImage(Image image){
        mSelectImages.add(image);
        if (mEventListener != null) {
            ((ImageSelectorViewModel) mEventListener).onImageSelect(image, true, mSelectImages.size());
        }
    }



    /**
     * 取消选中图片
     * @param image
     */
    private void unSelectImage(Image image){
        mSelectImages.remove(image);
        if (mEventListener != null) {
            ((ImageSelectorViewModel) mEventListener).onImageSelect(image, true, mSelectImages.size());
        }

    }


    /***
     * 更换相册的时候需要清空选中的相片
     * @return
     */
    private void clearSelecteImages(){
        mSelectImages.clear();
        if (mEventListener != null) {
            ((ImageSelectorViewModel) mEventListener).onImageSelect(null, true, 0);
        }
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == mInflater){
            mContext = parent.getContext();
            mInflater = LayoutInflater.from(mContext);
        }

        if (viewType == TAKE_PHOTO){
            takePhotoViewBinding = DataBindingUtil.inflate(mInflater, R.layout.take_photo_view,parent,false);
            return new TakePhotoViewHolder(takePhotoViewBinding);

        } else {
            imageItemViewBinding = DataBindingUtil.inflate(mInflater,R.layout.image_item_view,parent,false);
            return new ImageItemViewHolder(imageItemViewBinding);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (0 == position){
            ((TakePhotoViewHolder)holder).bindData(mEventListener);
        } else {

//            if (mEventListener != null)
//                imageItemViewBinding.setPresenter((ImageSelectorPresenter) mEventListener);
            Image image = mImages.get(holder.getAdapterPosition() - 1);
//            Glide.with(mContext).load(new File(image.getPath()))
//                    .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageItemViewBinding.ivImage);
//
//            setItemSelect(mSelectImages.contains(image));
            ((ImageItemViewHolder)holder).bindData(mEventListener,image,mSelectImages);

            //点击选中/取消选中图片
            ((ImageItemViewHolder) holder).imageItemViewBinding.ivSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSelectImages.contains(image)) {
                        //如果图片已经选中，就取消选中
                        unSelectImage(image);
                        ((ImageItemViewHolder)holder).setItemSelect(false);
                    } else if (isSingle) {
                        //如果是单选，就先清空已经选中的图片，再选中当前图片
                        clearImageSelect();
                        selectImage(image);
                        ((ImageItemViewHolder)holder).setItemSelect(true);
                    } else if (mMaxCount <= 0 || mSelectImages.size() < mMaxCount) {
                        //如果不限制图片的选中数量，或者图片的选中数量
                        // 还没有达到最大限制，就直接选中当前图片。
                        selectImage(image);
                        ((ImageItemViewHolder)holder).setItemSelect(true);
                    } else if (mMaxCount > 0 && mSelectImages.size() == mMaxCount){
                        ToastUtil.toastWarning(mContext,"最多添加"+mMaxCount+"张图片",-1);

                    }
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mEventListener != null){
                        ((ImageSelectorViewModel) mEventListener).onItemClick(image,holder.getAdapterPosition());
                    }
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return null == mImages ? 1 : mImages.size()+1;
    }

    static class TakePhotoViewHolder extends RecyclerView.ViewHolder{
        private TakePhotoViewBinding takePhotoViewBinding;

        public TakePhotoViewBinding getTakePhotoViewBinding() {
            return takePhotoViewBinding;
        }

        public TakePhotoViewHolder(TakePhotoViewBinding takePhotoViewBinding) {
            super(takePhotoViewBinding.getRoot());
            this.takePhotoViewBinding = takePhotoViewBinding;

        }
        public void bindData(Object presenter){
            if (presenter != null)
                takePhotoViewBinding.setPresenter((ImageSelectorViewModel) presenter);
        }
    }

    static class ImageItemViewHolder extends RecyclerView.ViewHolder{
        private ImageItemViewBinding imageItemViewBinding;
        public ImageItemViewHolder(ImageItemViewBinding imageItemViewBinding) {
            super(imageItemViewBinding.getRoot());
            this.imageItemViewBinding = imageItemViewBinding;
        }

        public void bindData(Object presenter, Image image, ArrayList<Image> mSelectImages){
            if (presenter != null)
                imageItemViewBinding.setPresenter((ImageSelectorViewModel) presenter);
            Glide.with(imageItemViewBinding.ivImage.getContext()).load(new File(image.getPath()))
                    .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageItemViewBinding.ivImage);

            setItemSelect(mSelectImages.contains(image));
        }

        /**
         * 设置图片选中和未选中的效果
         */
        public void setItemSelect( boolean isSelect) {
            imageItemViewBinding.ivSelect.setSelected(isSelect);
            if (isSelect) {
                imageItemViewBinding.ivMasking.setAlpha(0.5f);
            } else {
                imageItemViewBinding.ivMasking.setAlpha(0.2f);
            }
        }
    }
}
