package cn.wowjoy.office.common.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.PopupWindow;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.common.adapter.PopuAdapter;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.databinding.MorePopuWindowLayoutBinding;

/**
 * Created by Sherily on 2017/11/2.
 */

public class PopuListWindow extends PopupWindow implements PopuAdapter.OnActionListener {
    private MorePopuWindowLayoutBinding binding;
    private LayoutInflater inflater;
    private PopuAdapter popuAdapter;


    public PopuListWindow(Context context,OnHandlerListener onHandlerListener) {
        super(context);
        this.onHandlerListener = onHandlerListener;
        if (null == inflater)
            inflater = LayoutInflater.from(context);
        binding = DataBindingUtil.inflate(inflater, R.layout.more_popu_window_layout,null,false);
        setContentView(binding.getRoot());
        //让popup把焦点从外部抢夺过来，setFocusable(true)即可，至于setTouchable(true)，不用也行。这个时候也不用判断状态了，按钮中只处理显示代码就行了
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(true);
//        setFocusable(false);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        initView();
    }
    private void initView(){
        popuAdapter = new PopuAdapter();
        popuAdapter.setOnActionListener(this);
        binding.recyclerView.setAdapter(popuAdapter);
    }

    public void addItemDecoration(SimpleItemDecoration simpleItemDecoration){
        binding.recyclerView.addItemDecoration(simpleItemDecoration);
    }
    public void setList(List<PopuModel> datas){
        popuAdapter.setData(datas);
    }


    public interface OnHandlerListener{
        void onHandle(PopuModel popuModel);
    }
    private OnHandlerListener onHandlerListener;

    public void setOnHandlerListener(OnHandlerListener onHandlerListener) {
        this.onHandlerListener = onHandlerListener;
    }

    @Override
    public void onAction(PopuModel popuModel) {
        if (null != onHandlerListener){
            onHandlerListener.onHandle(popuModel);
        }
    }
}
