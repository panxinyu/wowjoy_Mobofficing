package cn.wowjoy.office.common.widget;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.wowjoy.office.R;
import cn.wowjoy.office.databinding.UpdateDialogViewBinding;

/**
 * Created by Sherily on 2017/11/15.
 * Description:
 */


public class UpdateDialog extends DialogFragment {

    private static final String KEY_PARAM = "PARAM";
    private UpdateDialogViewBinding binding;
    private View.OnClickListener mCancleClickListener;
    private View.OnClickListener mConfirmClickListener;

    public void setmCancleClickListener(View.OnClickListener mCancleClickListener) {
        this.mCancleClickListener = mCancleClickListener;
    }

    public void setmConfirmClickListener(View.OnClickListener mConfirmClickListener) {
        this.mConfirmClickListener = mConfirmClickListener;
    }

    private DialogParam mParam;


    @SuppressLint("ValidFragment")
    private UpdateDialog() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.SlsStyleDialog);
    }
    static UpdateDialog newInstance(DialogParam param){
        Bundle args = new Bundle();
        args.putParcelable(KEY_PARAM, param);
        UpdateDialog fragment = new UpdateDialog();
        fragment.setArguments(args);
        return fragment;
    }

    static class DialogParam implements Parcelable{
        String content = "";
        String cancelStr = "";
        String confirmStr = "";

        public DialogParam() {

        }

        protected DialogParam(Parcel in) {
            content = in.readString();
            cancelStr = in.readString();
            confirmStr = in.readString();
        }

        public static final Creator<DialogParam> CREATOR = new Creator<DialogParam>() {
            @Override
            public DialogParam createFromParcel(Parcel in) {
                return new DialogParam(in);
            }

            @Override
            public DialogParam[] newArray(int size) {
                return new DialogParam[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(content);
            dest.writeString(cancelStr);
            dest.writeString(confirmStr);
        }
    }

    public static class Builder {
        private DialogParam mParam;

        private View.OnClickListener mCancelListener;

        private View.OnClickListener mConfirmListener;

        public Builder() {
            mParam = new DialogParam();
        }
        public UpdateDialog create() {
            UpdateDialog fragment = newInstance(mParam);
            fragment.setmCancleClickListener(mCancelListener);
            fragment.setmConfirmClickListener(mConfirmListener);
            return fragment;
        }

        public Builder setContent(String str){
            mParam.content = str;
            return this;
        }

        public Builder setCancel(String str, View.OnClickListener listener){
            mParam.cancelStr = str;
            mCancelListener = listener;
            return this;
        }
        public Builder setConfirm(String str, View.OnClickListener listener){
            mParam.confirmStr = str;
            mConfirmListener = listener;
            return this;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.update_dialog_view,container,false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (null != getArguments()){
            mParam = getArguments().getParcelable(KEY_PARAM);
            binding.function.setText(mParam.content);
            binding.buttonCancel.setText(mParam.cancelStr);
            binding.buttonConfirm.setText(mParam.confirmStr);
        }
        binding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mCancleClickListener)
                    mCancleClickListener.onClick(v);
            }
        });

        binding.buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mConfirmClickListener){
                    mConfirmClickListener.onClick(v);
                }
            }
        });
        return binding.getRoot();
    }
}
