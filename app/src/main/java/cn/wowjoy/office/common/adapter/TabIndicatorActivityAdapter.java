package cn.wowjoy.office.common.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shizhefei.view.indicator.IndicatorViewPager;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;

/**
 * Created by Sherily on 2017/9/7.
 */

public class TabIndicatorActivityAdapter extends IndicatorViewPager.IndicatorFragmentPagerAdapter {
    private LayoutInflater inflater;
    private List<String> titles;
    private List<NewBaseFragment> fragments;

    public TabIndicatorActivityAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void setData(List<String> titles,List<NewBaseFragment> fragments){

        this.titles = titles;
        this.fragments = fragments;
        notifyDataSetChanged();

    }
    @Override
    public int getCount() {
        return null == titles ? 0 : titles.size();
    }

    @Override
    public View getViewForTab(int position, View convertView, ViewGroup container) {
        if (null == inflater)
            inflater = LayoutInflater.from(container.getContext());
        if (null == convertView)
            convertView = inflater.inflate(R.layout.tab_main, container, false);
        TextView textView = (TextView) convertView;
        textView.setText(titles.get(position));
//        textView.setCompoundDrawablesWithIntrinsicBounds(0, icons.get(position), 0, 0);
        return textView;
    }

    @Override
    public Fragment getFragmentForPage(int position) {
        return fragments.get(position);
    }


}
