package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.StaffRecordResponse;
import cn.wowjoy.office.databinding.RecordItemViewBinding;

/**
 * Created by Sherily on 2018/1/17.
 * Description:
 */

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.ItemViewHolder> {
    private LayoutInflater inflater;
    private List<StaffRecordResponse> datas;

    public RecordAdapter(List<StaffRecordResponse> datas) {
        this.datas = datas;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.record_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(datas.get(holder.getAdapterPosition()));
    }



    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private RecordItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (RecordItemViewBinding) binding;
        }
        public void bind(StaffRecordResponse response){
            binding.setModel(response);
        }
    }
}
