package cn.wowjoy.office.common.widget.timepicker;

/**
 * Created by Sherily on 2018/1/19.
 */
public interface IPickerViewData {
    String getPickerViewText();
}
