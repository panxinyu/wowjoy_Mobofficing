package cn.wowjoy.office.common.widget.album.selector;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.FloderAdapter;
import cn.wowjoy.office.common.adapter.ImageAdapter;
import cn.wowjoy.office.data.mock.Folder;
import cn.wowjoy.office.data.mock.Image;
import cn.wowjoy.office.data.response.GoodsDetailResponse;

/**
 * Created by Sherily on 2017/11/16.
 * Description:
 */

public class ImageSelectorViewModel extends NewBaseViewModel {

    public ImageAdapter adapter = new ImageAdapter();
    public FloderAdapter floderAdapter = new FloderAdapter();

    MediatorLiveData<Integer> selectorCount = new MediatorLiveData<>();
    MediatorLiveData<Boolean> takephoto = new MediatorLiveData<>();
    MediatorLiveData<ImageSelection> click = new MediatorLiveData<>();
    MediatorLiveData<Folder> folderMediatorLiveData = new MediatorLiveData<>();
    @Override
    public void onCreateViewModel() {

    }

    public MediatorLiveData<Integer> getSelectorCount() {
        return selectorCount;
    }

    public MediatorLiveData<Boolean> getTakephoto() {
        return takephoto;
    }

    public MediatorLiveData<ImageSelection> getClick() {
        return click;
    }

    public MediatorLiveData<Folder> getFolderMediatorLiveData() {
        return folderMediatorLiveData;
    }

    @Inject
    public ImageSelectorViewModel(@NonNull NewMainApplication application) {
        super(application);
        adapter.setmEventListener(this);
        floderAdapter.setmEventListener(this);
    }

    public void initAlbumAdapter(int count, boolean isSigle){
       adapter.initAdapter(count, isSigle);
    }
    public void initFloder(ArrayList<Folder> folders){
        floderAdapter.setmFolders(folders);
    }
    public void  onImageSelect(Image image, boolean isSelect, int selectCount){
//        activity.setSelectImageCount(selectCount);
        selectorCount.setValue(selectCount);
    }

    public void onItemClick(Image image, int position){
//        activity.toPreviewActivity(adapter.getData(),position - 1);
        click.setValue(new ImageSelection(adapter.getData(),position - 1));
    }

    public void takePhoto(){
        takephoto.setValue(true);
//        activity.selectPictureViaTakingPhoto();
    }

    public void onFolderSelect(Folder folder){
        folderMediatorLiveData.setValue(folder);
//        activity.setFolder(folder);
//        activity.closeFolder();
    }
}
