package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.applystock.ApplyStockViewModel;
import cn.wowjoy.office.data.response.ApplyHeadInfoResponse;
import cn.wowjoy.office.databinding.ApplyStockItemViewBinding;

public class ApplyItemAdapter extends RecyclerView.Adapter<ApplyItemAdapter.ItemViewHolder> {

    private LayoutInflater inflater;
    private List<ApplyHeadInfoResponse> datas;
    private Object handler;

    public void refresh(List<ApplyHeadInfoResponse> data){
        if (null != datas){
            datas.clear();
        }
        this.datas = data;
        notifyDataSetChanged();
    }

    public void loadMore(List<ApplyHeadInfoResponse> data){
        this.datas.addAll(data);
        notifyDataSetChanged();
    }


    public void setHandler(Object handler) {
        this.handler = handler;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());
        ApplyStockItemViewBinding binding = DataBindingUtil.inflate(inflater, R.layout.apply_stock_item_view,parent,false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(handler,datas.get(position));
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private ApplyStockItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ApplyStockItemViewBinding) binding;
        }

        public void bind(Object viewModel, ApplyHeadInfoResponse model){
            binding.setViewModel((ApplyStockViewModel) viewModel);
            binding.setModel(model);

        }
    }
}
