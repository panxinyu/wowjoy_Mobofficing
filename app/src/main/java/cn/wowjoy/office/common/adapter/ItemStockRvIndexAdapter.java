package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InspectTaskDtlVO;
import cn.wowjoy.office.databinding.ItemRvOtheroutIndexBinding;
import cn.wowjoy.office.materialinspection.stockout.StockOutIndexViewModel;

/**
 * Created by Administrator on 2018/6/5.
 */

public class ItemStockRvIndexAdapter extends RecyclerView.Adapter<ItemStockRvIndexAdapter.ViewHolder> {

    private List<InspectTaskDtlVO> mEntities;
    private ItemRvOtheroutIndexBinding binding;
    private Context context;
    private LayoutInflater layoutInflater;

    private Object mItemEventHandler;
    public void setItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }
    public void refresh(List<InspectTaskDtlVO> data){
        if (null != mEntities){
            mEntities.clear();
        }
        this.mEntities = data;
        notifyDataSetChanged();
    }
    public int getDateSize(){
        return null == mEntities ? 0 :mEntities.size();
    }
    public void setData(List<InspectTaskDtlVO> data){
        if (null == mEntities){
            mEntities = new ArrayList<>();
        }
        mEntities.clear();
        mEntities.addAll(data);
        notifyDataSetChanged();
    }

    public void loadMore(List<InspectTaskDtlVO> data){
        this.mEntities.addAll(data);
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == layoutInflater) {
            context = parent.getContext();
            layoutInflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_rv_otherout_index, parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        InspectTaskDtlVO inspectTaskDtlVO = mEntities.get(position);
//        Log.e("PXY", "position: "+position );
        holder.bindData(inspectTaskDtlVO,mItemEventHandler);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return null == mEntities ? 0 :mEntities.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        ItemRvOtheroutIndexBinding binding;
        public void setBgAndText( InspectTaskDtlVO inspectTaskDtlVO){
            //  设备状态鉴定
            GradientDrawable drawable = (GradientDrawable) binding.textState.getBackground();
            if(null == inspectTaskDtlVO.getIsExist() && null == inspectTaskDtlVO.getAppearanceStatus() && null == inspectTaskDtlVO.getAssetsStatus()){
                //设备未检测状态
                binding.textState.setText("未检测");
                drawable.setColor(context.getResources().getColor(R.color.menu_no_check));
            }else if( null != inspectTaskDtlVO.getIsExist() && "n".equalsIgnoreCase(inspectTaskDtlVO.getIsExist())){
                //设备未找到
                binding.textState.setText("设备未找到");
                drawable.setColor(context.getResources().getColor(R.color.menu_not_find));
            }else if( null != inspectTaskDtlVO.getAssetsStatus() && "n".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus())
                    && null != inspectTaskDtlVO.getAppearanceStatus() && "n".equalsIgnoreCase(inspectTaskDtlVO.getAppearanceStatus())){
                //设备异常-------------2个都异常的情况
                binding.textState.setText("设备异常");
                drawable.setColor(context.getResources().getColor(R.color.menu_device_error));
            }else if( null != inspectTaskDtlVO.getAssetsStatus() && "n".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus())){
                //设备异常
                binding.textState.setText("设备异常");
                drawable.setColor(context.getResources().getColor(R.color.menu_device_error));
            }else if( null != inspectTaskDtlVO.getAppearanceStatus() && "n".equalsIgnoreCase(inspectTaskDtlVO.getAppearanceStatus())){
                // 外观异常
                binding.textState.setText("外观异常");
                drawable.setColor(context.getResources().getColor(R.color.menu_look_error));
            }else if(null != inspectTaskDtlVO.getAssetsStatus() && "y".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus())
                    && null != inspectTaskDtlVO.getIsExist() && "y".equalsIgnoreCase(inspectTaskDtlVO.getIsExist())
                    && null != inspectTaskDtlVO.getAppearanceStatus() && "y".equalsIgnoreCase(inspectTaskDtlVO.getAppearanceStatus())
                    ){
                binding.textState.setText("正常");
                drawable.setColor(context.getResources().getColor(R.color.menu_normal));
            }
        }

        public ViewHolder(ItemRvOtheroutIndexBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bindData(InspectTaskDtlVO response,Object mItemEventHandler){

            binding.setViewModel((StockOutIndexViewModel) mItemEventHandler);
            binding.setModel(response);
            setBgAndText(response);
            binding.executePendingBindings();
        }
    }
}

