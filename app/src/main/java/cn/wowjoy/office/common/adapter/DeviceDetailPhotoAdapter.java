package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InspectTaskDtlVO;
import cn.wowjoy.office.materialinspection.xunjian.detail.PictureBean;
import cn.wowjoy.office.utils.PreferenceManager;

/**
 * Created by Administrator on 2017/11/16.
 */

public class DeviceDetailPhotoAdapter extends RecyclerView.Adapter<DeviceDetailPhotoAdapter.PhotoItemViewHolder> {

    private Context mContext;

    private LayoutInflater mLayoutInflater;

    private List<String> mBitmaps;

    private List<PictureBean> mUrls;

    private List<InspectTaskDtlVO> mDtlVOS;

    private String mToken;

    private boolean isGone;

    public void setToken(String mToken) {
        this.mToken = mToken;
    }

    public void addBitmap(String bitmap) {
        if (null == mBitmaps) {
            mBitmaps = new ArrayList<>();
        }
        mBitmaps.add(bitmap);
        notifyDataSetChanged();
    }

    public void setBitmaps(String bitmap) {
        if (null == mBitmaps) {
            mBitmaps = new ArrayList<>();
        }
        mBitmaps.clear();
        mBitmaps.add(bitmap);
        notifyDataSetChanged();
    }

    public void setBitmaps(PictureBean bean) {
        if (null == mUrls) {
            mUrls = new ArrayList<>();
        }
        mUrls.clear();
        mUrls.add(bean);
        notifyDataSetChanged();
    }
    public  void setDeleteGone(boolean isGone){
        this.isGone = isGone ;
    }

    //    public void setBitmaps(InspectTaskDtlVO inspectTaskDtlVO) {
//        if (null == mUrls) {
//            mUrls = new ArrayList<>();
//        }
//        mUrls.clear();
//        mUrls.add(url);
//        notifyDataSetChanged();
//    }
    public GlideUrl getGlideUrlHeaderUrl(String glideUrlHeader, String mToken) {
        GlideUrl glideUrl = null;
        if (!TextUtils.isEmpty(glideUrlHeader)) {
            glideUrl = new GlideUrl(glideUrlHeader, new LazyHeaders.Builder()
                    .addHeader("Content-Type", "application/json;charset=UTF-8")
                    .addHeader("Authorization", mToken)
                    .build());
        }
        return glideUrl;
    }

    @Override
    public PhotoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        if (null == mLayoutInflater) {
            mLayoutInflater = LayoutInflater.from(mContext);
        }

        return new PhotoItemViewHolder(mLayoutInflater.inflate(R.layout.item_rv_devicedetail_photos, parent, false));
    }

    @Override
    public void onBindViewHolder(PhotoItemViewHolder holder, int position) {
        if (mUrls.get(position).getThumbnailUrl().startsWith("http")) {
            GlideUrl glideUrlHeaderUrl = getGlideUrlHeaderUrl(mUrls.get(position).getThumbnailUrl(), PreferenceManager.getInstance().getHeaderToken());
            Glide.with(holder.mImageView.getContext())
                    .load(glideUrlHeaderUrl)
                    .centerCrop()
                    .crossFade()
                    .error(R.mipmap.pic_fail)
                    .placeholder(R.mipmap.pic_loading)
                    .into(holder.mImageView);
        } else {
            Glide.with(holder.mImageView.getContext())
                    .load(new File(mUrls.get(position).getThumbnailUrl()))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(holder.mImageView);
        }
        if(isGone){
            holder.delete.setVisibility(View.GONE);
        }else{
            holder.delete.setVisibility(View.VISIBLE);
        }
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null != mUrls && mUrls.size()> 0){
                    mOnclickItemListener.enlarge(mUrls.get(position).getPictureUrl());
                }
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUrls.remove(position);
                mOnclickItemListener.delete();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == mUrls ? 0 : mUrls.size();
    }

    public static class PhotoItemViewHolder extends RecyclerView.ViewHolder {
        //相册点
        private ImageView mImageView;
        //删除
        private ImageView delete;

        public PhotoItemViewHolder(View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.imgv_select_photo);
            delete = itemView.findViewById(R.id.delete_image_view);
        }

    }

    private OnclickItemListener mOnclickItemListener;

    public void setOnclickItemListener(OnclickItemListener onclickItemListener) {
        this.mOnclickItemListener = onclickItemListener;
    }

    public interface OnclickItemListener {
        void delete();
        void enlarge(String url);
    }
}
