package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.databinding.MorePopuWindowItemBinding;

/**
 * Created by Sherily on 2017/11/2.
 */

public class PopuAdapter extends RecyclerView.Adapter<PopuAdapter.PopuItemViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private List<PopuModel> data;
    private MorePopuWindowItemBinding binding;


    public PopuAdapter() {
    }

    public void setData(List<PopuModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public PopuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.more_popu_window_item,parent,false);
        return new PopuItemViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(PopuItemViewHolder holder, int position) {
        binding.setModel(data.get(holder.getAdapterPosition()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onActionListener){
                    onActionListener.onAction(data.get(holder.getAdapterPosition()));
                }
            }
        });
    }
    private OnActionListener onActionListener;

    public void setOnActionListener(OnActionListener onActionListener) {
        this.onActionListener = onActionListener;
    }

    public interface OnActionListener{
        void onAction(PopuModel popuModel);
    }

    @Override
    public int getItemCount() {
        return null == data ? 0 : data.size();
    }

    public static class PopuItemViewHolder extends RecyclerView.ViewHolder {
        public PopuItemViewHolder(View itemView) {
            super(itemView);
        }
    }
}
