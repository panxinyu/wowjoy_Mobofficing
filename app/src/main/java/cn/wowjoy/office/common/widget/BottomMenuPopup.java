package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.wowjoy.office.R;

/**
 * Created by Administrator on 2017/11/9.
 */

public class BottomMenuPopup extends PopupWindow {
    private View mMenuView;
    private TextView mStateNormal;
    private ImageView mIconRight1;
    private TextView mStateError;
    private ImageView mIconRight2;
    private RelativeLayout mRlNormal;
    private RelativeLayout mRlError;




    public BottomMenuPopup(Activity context, View.OnClickListener itemsOnClick, String text) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.bottom_menu_state, null);
        mStateNormal = (TextView) mMenuView.findViewById(R.id.state_normal);
        mIconRight1 = (ImageView) mMenuView.findViewById(R.id.icon_right1);
        mStateError = (TextView) mMenuView.findViewById(R.id.state_error);
        mIconRight2 = (ImageView) mMenuView.findViewById(R.id.icon_right2);
        mRlNormal = (RelativeLayout) mMenuView.findViewById(R.id.rl_normal);
        mRlError = (RelativeLayout) mMenuView.findViewById(R.id.rl_error);
//        //取消按钮
//        btn_cancel.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                //销毁弹出框
//                dismiss();
//            }
//        });
        setRight(text);

        //设置按钮监听
        mRlNormal.setOnClickListener(itemsOnClick);
        mRlError.setOnClickListener(itemsOnClick);
        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.ll_pop).getTop();
                int y=(int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y<height){
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
    public  void  setRight(String text){
        if("正常".equals(text)){
            mIconRight1.setVisibility(View.VISIBLE);
            mIconRight2.setVisibility(View.GONE);
        }else if("异常".equals(text)){
            mIconRight1.setVisibility(View.GONE);
            mIconRight2.setVisibility(View.VISIBLE);
        }
    }
}
