package cn.wowjoy.office.common.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.chrisbanes.photoview.OnOutsidePhotoTapListener;
import com.github.chrisbanes.photoview.OnPhotoTapListener;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.utils.WindowUtil;
//import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Sherily on 2017/11/27.
 * Description:图片预览PagerAdapter
 */

public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private List<String> images;
    private SparseArray<View> cacheView;
    private ViewGroup containerTemp;

    private String token;



    public ViewPagerAdapter(Context context, List<String> images,String token) {
        this.context = context;
        this.images = images;
        this.token = token;
        cacheView = new SparseArray<>(images.size());
    }

    public interface OnDismissListener{
        void dimiss();
    }
    private OnDismissListener onDismissListener;

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }



    private int top;
    private int bott;
    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        if(containerTemp == null) containerTemp = container;


        View view = cacheView.get(position);
        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.vp_item_image,container,false);
            view.setTag(position);
            PhotoView image = (PhotoView) view.findViewById(R.id.image);
            String url = images.get(position);
            if ( !TextUtils.isEmpty(url) && url.startsWith("http")){
               GlideUrl glideUrl = new GlideUrl(url, new LazyHeaders.Builder()
                        .addHeader("Content-Type", "application/json;charset=UTF-8")
                        .addHeader("Authorization", token)
                        .build());
               Glide.with(context).load(glideUrl)
                       .asBitmap()
                       .fitCenter()
                       .dontAnimate()
                       .error(R.mipmap.pic_fail)
                       .placeholder(R.mipmap.pic_loading)
                       .into(image);
            } else {
                Glide.with(context).load(new File(url))
                        .asBitmap()
                        .fitCenter()
                        .dontAnimate()
                        .error(R.mipmap.pic_fail)
                        .placeholder(R.mipmap.pic_loading)
                        .into(image);
            }
            image.setOnOutsidePhotoTapListener(new OnOutsidePhotoTapListener() {
                @Override
                public void onOutsidePhotoTap(ImageView imageView) {
                    if (onDismissListener != null){
                        onDismissListener.dimiss();

                    }
                }
            });
            image.setOnPhotoTapListener(new OnPhotoTapListener() {
                @Override
                public void onPhotoTap(ImageView view, float x, float y) {
                    if (onDismissListener != null){
                        onDismissListener.dimiss();

                    }
                }
            });


            cacheView.put(position,view);
        }
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    private class MyTarget extends SimpleTarget<Bitmap> {

        //        private PhotoViewAttacher viewAttacher;
        private PhotoView photoView;

        public MyTarget(PhotoView photoView){
            this.photoView = photoView;
        }



        @Override
        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            int width = resource.getWidth();
            int height = resource.getHeight();
//
//            int newWidth = width;
//            int newHeight = height;
            int screenWidth = WindowUtil.getInstance().getScreenWidth((Activity) context);
            int screenHeight = WindowUtil.getInstance().getScreenHeight((Activity) context);

            int newWidth = screenWidth;
            int newHeight = newWidth/width * height;


            if(width > screenWidth){
                newWidth = screenWidth;
            }

            if(height > screenHeight || newHeight > screenHeight){
                newHeight = screenHeight;
            }




            if(newWidth == width && newHeight == height){
//                viewAttacher.getImageView().setImageBitmap(resource);
//                viewAttacher.update();
                photoView.setImageBitmap(resource);
                return;
            }

            //计算缩放比例
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float)newHeight) / height;

            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth,scaleHeight);

            Log.v("size",width + "");
            Log.v("size",height + "");

            Bitmap resizeBitmap = Bitmap.createBitmap(resource,0,0,width,height,matrix,true);

//
            photoView.setImageBitmap(resizeBitmap);
        }

    }

}
