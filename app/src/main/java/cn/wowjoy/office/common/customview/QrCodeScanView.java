package cn.wowjoy.office.common.customview;

import android.content.Context;
import android.util.AttributeSet;

import cn.bingoogolapple.qrcode.zxing.ZXingView;

/**
 * Created by Sherily on 2017/9/8.
 */

public class QrCodeScanView extends ZXingView {
    public QrCodeScanView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    public void stopCamera() {
        stopSpotAndHiddenRect();
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mPreview.stopCameraPreview();
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
    }
}
