package cn.wowjoy.office.common.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.Arrays;

import cn.wowjoy.office.R;
import cn.wowjoy.office.common.adapter.GridViewApdapter;
import cn.wowjoy.office.data.response.PatrolInfo;
import cn.wowjoy.office.utils.DensityUtil;


/**
 * Created by 84429 on 2017/4/1.
 */

public class ScanDialog extends Dialog {
    private Context ct;
    private ClickListenerInterface clickListenerInterface;
    private TagFlowLayout mStatus;
    private TagFlowLayout mCondition;
    private Button mConfirm;
    private ImageView mClose;
    private LayoutInflater inflater;
    private String status;
    private String condition;
    private GridView mGV;
    private GridViewApdapter mGridViewApdapter;

    public ScanDialog(Context context) {
        super(context, R.style.dialog_no_bg);
        ct = context;
    }

    public interface ClickListenerInterface {
        public void doConfirm(PatrolInfo patrolInfo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        inflater = LayoutInflater.from(ct);
        View view = inflater.inflate(R.layout.scan_dialog, null);
        setContentView(view);
        setCanceledOnTouchOutside(false);

        mStatus = (TagFlowLayout) view.findViewById(R.id.tag_status);
        mStatus.setMaxSelectCount(1);
        mStatus.setAdapter(new TagAdapter<String>(Arrays.asList("住院", "手术", "检查", "请假")) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) inflater.inflate(R.layout.item_tag_scan, mStatus, false);
                tv.setText(s);
                return tv;
            }
        });
        mStatus.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                status = (String) mStatus.getAdapter().getItem(position);
                if (status.equals("住院")) {
                    mCondition.setEnabled(true);
                } else {
                    mCondition.setEnabled(false);
                    mCondition.onChanged();
                    condition = null;
                }
                return true;
            }
        });

        mCondition = (TagFlowLayout) view.findViewById(R.id.tag_condition);
        mCondition.setMaxSelectCount(1);
        mCondition.setAdapter(new TagAdapter<String>(Arrays.asList("平", "左", "右")) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) inflater.inflate(R.layout.item_tag_scan, mCondition, false);
                tv.setText(s);
                return tv;
            }
        });
        mCondition.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                condition = (String) mCondition.getAdapter().getItem(position);
                return true;
            }
        });

        mConfirm = (Button) view.findViewById(R.id.confirm);
        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(status))
                    new MyToast(ct).showinfo("请选择病人状态");
//                if (clickListenerInterface != null)
//                    clickListenerInterface.doConfirm(new PatrolInfo("0902", "老季", "男", TextUtils.isEmpty(condition) ? status : "在院-" + condition, DateUtils.getCurrTime(), mGridViewApdapter.getSelectedMed()));
            }
        });
        mClose = (ImageView) view.findViewById(R.id.close);
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mGV = (GridView) view.findViewById(R.id.gview);
        mGridViewApdapter = new GridViewApdapter(ct, Arrays.asList("aaaa","bbbb","cccc"));
        mGV.setAdapter(mGridViewApdapter);

        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.width = DensityUtil.dip2px(ct, 300);
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }
}
