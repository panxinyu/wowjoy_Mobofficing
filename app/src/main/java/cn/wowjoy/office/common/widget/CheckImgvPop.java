package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;

import cn.wowjoy.office.R;

/**
 * Created by Administrator on 2017/11/13.
 */

public class CheckImgvPop extends PopupWindow {

    private Activity mActivity;
    private View conentView;
    private ImageView mImgvCheck;
    private Button mBtExitCheck;



    public CheckImgvPop(final Activity context,   GlideUrl glideUrlHeaderUrl) {
        mActivity=context;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = inflater.inflate(R.layout.check_imgv, null);
        int h = context.getWindowManager().getDefaultDisplay().getHeight();
        int w = context.getWindowManager().getDefaultDisplay().getWidth();
        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(w);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        // mPopupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
        // 设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.AnimationPreview);
        mImgvCheck = (ImageView) conentView.findViewById(R.id.imgv_check);
        mBtExitCheck = (Button) conentView.findViewById(R.id.bt_exit_check);


        Glide.with(mImgvCheck.getContext())
                .load(glideUrlHeaderUrl)
                .centerCrop()
                .crossFade()
                .error(R.mipmap.pic_fail)
                .placeholder(R.mipmap.pic_loading)
                .into(mImgvCheck);

        mBtExitCheck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                backgroundAlpha(1.0f,mActivity);
                CheckImgvPop.this.dismiss();
            }
        });
        //添加pop窗口关闭事件
        this.setOnDismissListener(new poponDismissListener());
    }
    /**
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha, Activity activity)
    {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        activity.getWindow().setAttributes(lp);
    }
    /**
     * 添加新笔记时弹出的popWin关闭的事件，主要是为了将背景透明度改回来
     * @author cg
     *
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            //Log.v("List_noteTypeActivity:", "我是关闭事件");
            backgroundAlpha(1f,mActivity);
        }

    }
}
