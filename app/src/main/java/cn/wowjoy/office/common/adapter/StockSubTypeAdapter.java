package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.Type;
import cn.wowjoy.office.databinding.StockSubTypeItemViewBinding;
import cn.wowjoy.office.materialmanage.stock.MaterialStockViewModel;

/**
 * Created by Sherily on 2017/12/29.
 * Description:
 */

public class StockSubTypeAdapter extends RecyclerView.Adapter<StockSubTypeAdapter.ItemViewHolder> {

    private List<Type> types;
    private LayoutInflater inflater;
    private boolean isAll;
    private int mainIndex;
    //count只在非第一项的item点击时候进行加减操作。
    private int count;
    private Object mEventHandle;
    private int totalSub;

    private int clickPosition;


    public void setmEventHandle(Object mEventHandle) {
        this.mEventHandle = mEventHandle;
    }

    private void clickItem(ItemViewHolder holder){
        if (0 == holder.getAdapterPosition()){
//            count = 0;
            if (isAll){
                count = 0;
                isAll = false;
                if (mainIndex == 0){
                    //相当于外部的clear
                    ((MaterialStockViewModel)mEventHandle).clear();
                } else {

                    //在第一级全选的状态下要取消第一级的全选状态，改变当前第一级及第一级的全部的状态,重置二级的选中数量
                    notifyItemChanged(0);
                    ((MaterialStockViewModel)mEventHandle).selectMain(isAll,mainIndex,holder.getAdapterPosition(),0);
                    initAllStatus(isAll);
                }
            } else {
                isAll = true;
                if (0 == mainIndex){
                    //选中全部分类，初始化一级列表为全选中，二级为全不选中
                    ((MaterialStockViewModel)mEventHandle).initType();
                } else {
//                    notifySelectedAll();
                    count = types.size();

                    //选中当前所在的第一级，设置第一级的flag值，改变提示数字
                    notifyItemChanged(0);
                    ((MaterialStockViewModel)mEventHandle).selectMain(isAll,mainIndex,holder.getAdapterPosition(),types.size());
                    initAllStatus(isAll);

                }

            }
            if (0 == mainIndex){
                notifyItemChanged(0);
            } else {
                notifyItemRangeChanged(1,getItemCount());
            }

        } else {
//            if (isAll){
//                isAll = false;
//                count = 0;
//                notifyItemRangeChanged(1,getItemCount());
//            }
            boolean isSelected = types.get(holder.getAdapterPosition() - 1).isSelected();
            if (isSelected){
                if (count > 0)
                    count--;
            } else {
                if (count < types.size())
                    count++;
            }
            if (count == types.size()){
                if (!isAll){
                    isAll = true;
                    notifyItemChanged(0);
                }

            } else {
                if (isAll){
                    isAll = false;
                    notifyItemChanged(0);
                }

            }
//            if (count == types.size()){
//                notifySelectedAll();
//                notifyItemRangeChanged(1,getItemCount());
//            } else {
                types.get(holder.getAdapterPosition() - 1).setSelected(!isSelected);
                notifyItemChanged(holder.getAdapterPosition());
//                notifyItemChanged(0);
//            }
            //通知第一级显示数字
            ((MaterialStockViewModel)mEventHandle).selectMain(types.get(holder.getAdapterPosition() -1).isSelected(),mainIndex,holder.getAdapterPosition(),count);
            if (isAll){
                initAllStatus(true);
            }

        }

    }



    public StockSubTypeAdapter() {
    }

    private void initAllStatus(boolean isSelected){
        for (Type type : types){
            type.setSelected(isSelected);
        }
    }
    public void setTypes(List<Type> types,int mainCode,boolean isAll,int count) {
        this.isAll = isAll;
        this.types = types;
        this.count = count;
        if (isAll && null != types && !types.isEmpty()){
            initAllStatus(true);
        }
//        if (!isAll){
//            this.count = count;
//        } else {
//            this.count = 0;
//        }
        this.mainIndex = mainCode;
        notifyDataSetChanged();
    }

    public void setTotalSub(int totalSub) {
        this.totalSub = totalSub;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            inflater = LayoutInflater.from(parent.getContext());
        }
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.stock_sub_type_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if (0 == holder.getAdapterPosition()){
            if (mainIndex == 0){
                holder.binding.name.setSelected(false);
                holder.bind("全部（共"+totalSub+"项"+"）");
                holder.binding.status.setVisibility(isAll ? View.VISIBLE : View.INVISIBLE);
            } else {
                holder.bind("全部（共"+types.size()+"项"+"）");
                holder.binding.status.setVisibility(View.INVISIBLE);
                holder.binding.name.setSelected(isAll);
            }
//            holder.binding.status.setVisibility(isAll ? View.VISIBLE : View.INVISIBLE);
        } else {
            holder.binding.name.setSelected(false);
            holder.bind(types.get(position - 1).getName());
            boolean show = isAll || types.get(holder.getAdapterPosition() - 1).isSelected();
            holder.binding.status.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItem(holder);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (0 == mainIndex){
            return 1;
        }
        return null == types ? 0 : types.size() + 1;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private StockSubTypeItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (StockSubTypeItemViewBinding) binding;
        }

        public void bind(String name){
            binding.setName(name);
        }

    }
}
