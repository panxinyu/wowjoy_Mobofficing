package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.CheckCategoryListResponse;
import cn.wowjoy.office.materialinspection.check.detail.add.CategoryViewModel;

/**
 * Created by pxy on 2017/11/13.
 * Description:
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MenuItemViewHolder> {
    private Context context;
    private LayoutInflater inflater;
//    private MeunItemViewBinding binding;
    private Object mEventListener;
    private List<CheckCategoryListResponse.CategoryName> popuModels;
    private int type;

    public void setType(int type) {
        this.type = type;
    }

    public void setPopuModels(List<CheckCategoryListResponse.CategoryName> popuModels) {
        this.popuModels = popuModels;
        notifyDataSetChanged();
    }

    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == context){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        return new MenuItemViewHolder(inflater.inflate(R.layout.item_rv_category_view,parent,false));
    }

    private int lastPosition = 0;

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, int position) {
        Log.d("menulist",holder.getAdapterPosition()+"::::"+popuModels.get(holder.getAdapterPosition()).getCategoryName()+"------datasize::::"+popuModels.size());
        CheckCategoryListResponse.CategoryName popuModel = popuModels.get(holder.getAdapterPosition());
//        binding.setModel(popuModel);
        holder.name.setSelected(popuModel.isSelected());
        holder.name.setText(popuModel.getCategoryName());
//        holder.icon.setVisibility(popuModel.isSelected() ? View.VISIBLE : View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastPosition != holder.getAdapterPosition()){
                    popuModel.setSelected(true);
                    popuModels.get(lastPosition).setSelected(false);
                    notifyItemChanged(lastPosition);
                    notifyItemChanged(holder.getAdapterPosition());
                    Log.d("menulist","lastposition::::"+lastPosition+"------curposition::::"+holder.getAdapterPosition());
                    lastPosition = holder.getAdapterPosition();
                }
                ((CategoryViewModel) mEventListener).sort(popuModel);
            }
        });


    }

    @Override
    public int getItemCount() {
        return null == popuModels ? 0 : popuModels.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView icon;
        public MenuItemViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}
