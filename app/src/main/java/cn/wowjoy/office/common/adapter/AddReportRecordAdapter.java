package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InventoryResponse;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.databinding.ItemRvAddReportBinding;
import cn.wowjoy.office.materialmanage.add.AddReportViewModel;

/**
 * Created by Sherily on 2018/1/25.
 * Description:
 */

public class AddReportRecordAdapter extends RecyclerView.Adapter<AddReportRecordAdapter.ItemViewHolder> {

    private LayoutInflater inflater;
    private List<RecordResponse> datas;
    private Object mEventObject;

    private String keyWord;


    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public void setmEventObject(Object mEventObject) {
        this.mEventObject = mEventObject;
    }


    public void refresh(List<RecordResponse> data){
        if (null != datas){
            datas.clear();
        }
        this.datas = data;
        notifyDataSetChanged();
    }

    public void loadMore(List<RecordResponse> data){
        this.datas.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_rv_add_report,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(mEventObject,datas.get(position),keyWord);
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemRvAddReportBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemRvAddReportBinding) binding;
        }

        public void bind(Object mEventHandler, RecordResponse model,String keyWord){
            binding.setViewModel((AddReportViewModel) mEventHandler);
            binding.setModel(model);
            binding.setKeyWord(keyWord);
            changeStatus(model);
        }
        private void changeStatus(RecordResponse model){
            if (Double.parseDouble(model.getProfitLossMoney()) == 0){
                binding.tvNum.setText(matcherSearchText(0xff666666,12f,model.formatProfit(),model.formatProfitLossMoney()));

            } else if (Double.parseDouble(model.getProfitLossMoney()) < 0){
                binding.tvNum.setText(matcherSearchText(0xffFB6769,12f,model.formatProfit(),model.formatProfitLossMoney()));

            } else {
                binding.tvNum.setText(matcherSearchText(0xff0BC69C,12f,model.formatProfit(),model.formatProfitLossMoney()));
            }
        }
        private SpannableStringBuilder matcherSearchText(int color, float textSize, String text, String keyword) {
            if ( textSize == 0.0f )
                textSize = 12f;
            int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, textSize, binding.tvNum.getResources().getDisplayMetrics());
            SpannableStringBuilder ss = new SpannableStringBuilder(text);
            Pattern pattern = Pattern.compile(keyword);
            Matcher matcher = pattern.matcher(ss);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                ss.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new AbsoluteSizeSpan(size), start , end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            return ss;
        }
    }
}
