package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InventoryResponse;
import cn.wowjoy.office.data.response.MaterialSearchResponse;
import cn.wowjoy.office.databinding.InventoryItemViewBinding;
import cn.wowjoy.office.materialmanage.inventory.MaterialInventoryViewModel;

/**
 * Created by Sherily on 2018/1/12.
 * Description:
 */

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.ItemViewHolder> {
    private LayoutInflater inflater;
    private List<InventoryResponse> datas;
    private Object mEventHandle;
    private String keyword;

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void refresh(List<InventoryResponse> data){
        if (null != datas){
            datas.clear();
        }
        this.datas = data;
        notifyDataSetChanged();
    }

    public void loadMore(List<InventoryResponse> data){
        this.datas.addAll(data);
        notifyDataSetChanged();
    }

    public void setmEventHandle(Object mEventHandle) {
        this.mEventHandle = mEventHandle;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.inventory_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(mEventHandle,datas.get(position),keyword);
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private InventoryItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (InventoryItemViewBinding) binding;
        }
        public void bind(Object handler, InventoryResponse model,String key ){
            binding.setKeyword(key);
            binding.setModel(model);
            binding.setViewmodel((MaterialInventoryViewModel) handler);
            changeStatus(model.getStatusView());

        }

        private void changeStatus(String status){
            switch (status){
                case "待确认":
                    int color1 = binding.status.getResources().getColor(R.color.menu_not_find);
                    ((GradientDrawable) binding.status.getBackground()).setColor(color1);
                    break;
                case "等待审批":
                    int color2 = binding.status.getResources().getColor(R.color.notice_text);
                    ((GradientDrawable) binding.status.getBackground()).setColor(color2);
                    break;
                case "待提交":
                    int color3 = binding.status.getResources().getColor(R.color.appText14);
                    ((GradientDrawable) binding.status.getBackground()).setColor(color3);
                    break;
                case "审批通过":
                    int color4 = binding.status.getResources().getColor(R.color.menu_normal);
                    ((GradientDrawable) binding.status.getBackground()).setColor(color4);
                    break;
                case "审批驳回":
                    int color5 = binding.status.getResources().getColor(R.color.menu_look_error);
                    ((GradientDrawable) binding.status.getBackground()).setColor(color5);
                    break;
            }

        }

    }
}
