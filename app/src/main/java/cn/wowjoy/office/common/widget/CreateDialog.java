package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.view.View;

import java.util.List;

import cn.wowjoy.office.R;


public class CreateDialog {
    static MDialog mAlertDialog;

    static public MDialog waitingDialog(final Activity activity) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }
        final MDialog waitDialog = new MDialog(activity, R.style.dialog_no_bg);
        waitDialog.setCanceledOnTouchOutside(false);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (waitDialog != null && !waitDialog.isShowing())
                    waitDialog.show();
            }
        });

        return waitDialog;
    }

    static public MDialog alertDialog(Activity activity, String hint, String bottonL, String bottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }

        MDialog alertDialog = new MDialog(activity, R.style.dialog_no_bg, hint, bottonL, bottonR, cancelBtnClickListener, confirmBtnClickListener);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
        return alertDialog;
    }

    static public MDialog infoDialog(Activity activity, String title, String hint, String bottonL, String bottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }

        MDialog alertDialog = new MDialog(activity, R.style.dialog_no_bg, title, hint, bottonL, bottonR, cancelBtnClickListener, confirmBtnClickListener);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
        return alertDialog;
    }
    static public MDialog submitDialog(Activity activity, String title, String hint, int type, View.OnClickListener mCancleListener, String cancelStr) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }

        MDialog alertDialog = new MDialog(activity, R.style.dialog_no_bg, title, hint, type,mCancleListener,cancelStr);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
        return alertDialog;
    }

    static public MDialog listDialog(Activity activity, List<String> data) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }

        MDialog alertDialog = new MDialog(activity, R.style.dialog_no_bg, data);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.show();
        return alertDialog;
    }

    static public MDialog editDialog(Activity activity, String edit0, String edit1) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }

        MDialog alertDialog = new MDialog(activity, R.style.dialog_no_bg, edit0, edit1);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
        return alertDialog;
    }
    static public MDialog updateDialog(Activity activity, String title, String hint, String bottonL, String bottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }

        MDialog alertDialog = new MDialog(activity, R.style.dialog_no_bg, title, hint, bottonL, bottonR, cancelBtnClickListener, confirmBtnClickListener);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
        return alertDialog;
    }
    public static void dismiss(Activity activity, final MDialog waitDialog) {
        if (activity != null && !activity.isFinishing() && waitDialog != null && waitDialog.isShowing()) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    waitDialog.cancel();
                }
            });
        }
    }
}
