package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.mock.PopuModel;

import cn.wowjoy.office.materialinspection.check.task.CheckTaskListViewModel;
import cn.wowjoy.office.materialinspection.stockout.StockOutIndexViewModel;
import cn.wowjoy.office.materialmanage.inventory.MaterialInventoryViewModel;
import cn.wowjoy.office.materialmanage.materiallist.MaterialListViewModel;
import cn.wowjoy.office.materialmanage.stock.MaterialStockViewModel;

/**
 * Created by Sherily on 2017/11/13.
 * Description:
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuItemViewHolder> {
    private Context context;
    private LayoutInflater inflater;
//    private MeunItemViewBinding binding;
    private Object mEventListener;
    private List<PopuModel> popuModels;
    private int type;

    public void setType(int type) {
        this.type = type;
    }

    public void setPopuModels(List<PopuModel> popuModels) {
        this.popuModels = popuModels;
        notifyDataSetChanged();
    }

    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == context){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
//        binding = DataBindingUtil.inflate(inflater, R.layout.meun_item_view,parent,false);
        return new MenuItemViewHolder(inflater.inflate(R.layout.meun_item_view,parent,false));
    }

    private int lastPosition = 0;

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, int position) {
        Log.d("menulist",holder.getAdapterPosition()+"::::"+popuModels.get(holder.getAdapterPosition()).getName()+"------datasize::::"+popuModels.size());

        PopuModel popuModel = popuModels.get(holder.getAdapterPosition());
//        binding.setModel(popuModel);
        holder.name.setSelected(popuModel.isSelected());
        holder.name.setText(popuModel.getName());
        holder.icon.setVisibility(popuModel.isSelected() ? View.VISIBLE : View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastPosition != holder.getAdapterPosition()){
                    popuModel.setSelected(true);
                    popuModels.get(lastPosition).setSelected(false);
//                    binding.icon.setVisibility(View.VISIBLE);
//                    binding.name.setSelected(true);
                    notifyItemChanged(lastPosition);
                    notifyItemChanged(holder.getAdapterPosition());
                    Log.d("menulist","lastposition::::"+lastPosition+"------curposition::::"+holder.getAdapterPosition());
                    lastPosition = holder.getAdapterPosition();
                }
                if (mEventListener instanceof MaterialListViewModel){
                    ((MaterialListViewModel) mEventListener).sort(popuModel);
                } else if (mEventListener instanceof MaterialStockViewModel){
                    ((MaterialStockViewModel) mEventListener).sort(popuModel);
                } else if (mEventListener instanceof MaterialInventoryViewModel){
                    ((MaterialInventoryViewModel) mEventListener).sort(popuModel);
                }else if (mEventListener instanceof CheckTaskListViewModel) {
                    ((CheckTaskListViewModel) mEventListener).sort(popuModel);
                }else if (mEventListener instanceof StockOutIndexViewModel) {
                    if(type == 1){
                        ((StockOutIndexViewModel) mEventListener).sortKind(popuModel);
                    }else if(type == 2){
                        ((StockOutIndexViewModel) mEventListener).sortState(popuModel);
                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return null == popuModels ? 0 : popuModels.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView icon;
        public MenuItemViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}
