package cn.wowjoy.office.common.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.shizhefei.view.indicator.IndicatorViewPager;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.BannerInfo;

/**
 * Created by Sherily on 2017/9/7.
 */

public class BannerAdapter extends IndicatorViewPager.IndicatorViewPagerAdapter {
    private List<BannerInfo> bannerInfos;
    private LayoutInflater inflater;
    public BannerAdapter() {
    }

    public void setBannerInfos(List<BannerInfo> bannerInfos) {
        this.bannerInfos = bannerInfos;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == bannerInfos ? 0 : bannerInfos.size();
    }

    @Override
    public View getViewForTab(int position, View convertView, ViewGroup container) {
        if (null == inflater)
            inflater = LayoutInflater.from(container.getContext());
        if (null == convertView)
            convertView = inflater.inflate(R.layout.tab_guide,container,false);

        return convertView;
    }

    @Override
    public View getViewForPage(int position, View convertView, ViewGroup container) {
        if (null == convertView) {
            convertView = new ImageView(container.getContext());
            convertView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        ImageView imageView = (ImageView) convertView;
        Glide.with(imageView.getContext())
                .load(bannerInfos.get(position).getUrl())
                .centerCrop()
                .crossFade()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageView);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        imageView.setImageResource(images[position]);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onItemClickListener){
                    onItemClickListener.onItemClick(position,bannerInfos.get(position));
                }
            }
        });
        return convertView;
    }

    @Override
    public int getItemPosition(Object object) {
        //这是ViewPager适配器的特点,有两个值 POSITION_NONE，POSITION_UNCHANGED，默认就是POSITION_UNCHANGED,
        // 表示数据没变化不用更新.notifyDataChange的时候重新调用getViewForPage
        return PagerAdapter.POSITION_NONE;
    }

    public interface OnItemClickListener{
        void onItemClick(int position,BannerInfo bannerInfo);
    }
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
