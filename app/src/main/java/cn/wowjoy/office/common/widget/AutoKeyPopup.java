package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.AssetsCardQueryCondition;
import cn.wowjoy.office.common.adapter.AutoEditItemAdapter;

/**
 * Created by Administrator on 2018/1/23.
 */

public class AutoKeyPopup extends PopupWindow {
    private View mMenuView;
    private RecyclerView mRecyclerView;
    private List<AssetsCardQueryCondition> mConditions;
    private AutoEditItemAdapter autoAdapter;
    private Activity mActivity;

   public void setAdapter(AutoEditItemAdapter adapter){
       this.autoAdapter = adapter;
       mRecyclerView.setAdapter(autoAdapter);
       mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
       autoAdapter.notifyDataSetChanged();
   }
   public  void setDates( List<AssetsCardQueryCondition> mdate){

//       this.mConditions = mdate ;
       if(null != autoAdapter){
           autoAdapter.setAssetsCardQueryConditions(mdate);
       }

   }
    public AutoKeyPopup(Activity context) {
        super(context);
        this.mActivity = context;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.dialog_auto_key, null);

        mRecyclerView = mMenuView.findViewById(R.id.rv_auto_dialog);
        //设置按钮监听

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(mActivity.getResources().getDimensionPixelSize(R.dimen.poup_add_width));
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(mActivity.getResources().getDimensionPixelSize(R.dimen.poup_add_height));
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(false);

        //设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
//        ColorDrawable dw = new ColorDrawable(0xb0000000);
//        //设置SelectPicPopupWindow弹出窗体的背景
//        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.ll_pop).getTop();
                int y=(int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y<height){
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

}
