package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.common.customview.SwipeMenuView;
import cn.wowjoy.office.data.response.GoodsDetailResponse;
import cn.wowjoy.office.databinding.MaterialListItemViewBinding;

import cn.wowjoy.office.materialmanage.materiallist.MaterialListViewModel;

/**
 * Created by Sherily on 2017/11/2.
 */

public class MaterialListAdapter extends RecyclerView.Adapter<MaterialListAdapter.MaterialItemViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private MaterialListItemViewBinding binding;

    protected Object mItemEventHandler;
    private List<GoodsDetailResponse> datas;
    private String keyWord;

    private String mToken;

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public void setmItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }

    public void refresh(List<GoodsDetailResponse> data){
        if (null != datas){
            datas.clear();
        }
        this.datas = data;
        notifyDataSetChanged();
    }

    public void loadMore(List<GoodsDetailResponse> data){
        this.datas.addAll(data);
        notifyDataSetChanged();
    }
    @Override
    public MaterialItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.material_list_item_view, parent,false);
        return new MaterialItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MaterialItemViewHolder holder, final int position) {

//        holder.setIsRecyclable(false);
        Log.d("listadapter",position+"::::"+datas.get(position).getName()+"------datasize::::"+datas.size());
        GoodsDetailResponse goodsDetailResponse = datas.get(position);

        holder.bindData(goodsDetailResponse,mItemEventHandler,keyWord,mToken);
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 :datas.size();
    }

    public static class MaterialItemViewHolder extends RecyclerView.ViewHolder {
        MaterialListItemViewBinding binding;
        public MaterialItemViewHolder(MaterialListItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            ((SwipeMenuView)itemView).setIos(false).setLeftSwipe(true);
        }

        public void bindData(GoodsDetailResponse response, Object mItemEventHandler,String keyWord,String token){

            binding.setPresenter((MaterialListViewModel) mItemEventHandler);
//        binding.setPosition(position);
            binding.setModel(response);
            binding.setKeyWord(keyWord);
            binding.setToken(token);
            binding.executePendingBindings();
            binding.status.setSelected(response.stopStatus());
            binding.handleTv.setSelected(response.stopStatus());

        }
    }
}
