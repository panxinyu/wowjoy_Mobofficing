package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.StockSelectResponse;
import cn.wowjoy.office.databinding.ItemRvCategoryViewBinding;
import cn.wowjoy.office.materialinspection.stockout.search.SelectOutViewModel;
import cn.wowjoy.office.materialinspection.stockout.search.SelectPatientViewModel;

/**
 * Created by Administrator on 2018/4/12.
 */

public class StockSelectAdapter extends RecyclerView.Adapter<StockSelectAdapter.ItemViewHolder>{
    private Context context;
    private LayoutInflater inflater;
    private Object mEventListener;
    private List<StockSelectResponse> popuModels;
    private int type;
   private String keyWord;

    public StockSelectAdapter(int type) {
        this.type = type;
    }

    public void setPopuModels(List<StockSelectResponse> popuModels, String keyWord) {
        this.popuModels = popuModels;
        this.keyWord = keyWord;
        notifyDataSetChanged();
    }
    public void setPopuModels(List<StockSelectResponse> popuModels) {
        this.popuModels = popuModels;
        notifyDataSetChanged();
    }
    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_rv_category_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(mEventListener,keyWord,popuModels.get(position),type);
    }

    @Override
    public int getItemCount() {
        return null == popuModels ? 0 : popuModels.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemRvCategoryViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemRvCategoryViewBinding) binding;
        }

        public void bind(Object mEventHandler,String keyWord,StockSelectResponse response,int type){
            binding.setKeyWord(keyWord);
            if(type == 1){  //选择出库去向
              binding.setType(true);
              binding.setViewModelOut((SelectOutViewModel) mEventHandler);
            }else{
                binding.setType(false);
                binding.setViewModelPatient((SelectPatientViewModel) mEventHandler);
            }
            binding.setModel(response);

            binding.rlAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type == 1){
                        ( (SelectOutViewModel) mEventHandler).jump(response);
                    }else if(type == 2){
                        ( (SelectPatientViewModel) mEventHandler).jump(response);
                    }
                }
            });

        }
    }
}
