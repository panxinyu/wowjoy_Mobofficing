package cn.wowjoy.office.common.adapter;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.List;

import cn.wowjoy.office.R;

import cn.wowjoy.office.data.response.AppInfo;
import cn.wowjoy.office.databinding.ItemGridviewBinding;

import cn.wowjoy.office.homepage.HomeViewModel;
import cn.wowjoy.office.homepage.MenuHelper;

/**
 * Created by Sherily on 2017/10/25.
 */

public class ComAppAdapter extends RecyclerView.Adapter<ComAppAdapter.ComAppItemViewHolder>{
    private LayoutInflater inflater;
    private Context context;
    private List<AppInfo> datas;
    private ItemGridviewBinding binding;
    protected Object mItemEventHandler;


    public void setmItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }


    public void setDatas(List<AppInfo> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public ComAppAdapter() {
    }

    @Override
    public ComAppItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            context = parent.getContext();
            inflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(inflater, R.layout.item_gridview,parent,false);
        return new ComAppItemViewHolder(binding.getRoot());
}

    @Override
    public void onBindViewHolder(ComAppItemViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        binding.setItemeventhadle((HomeViewModel) mItemEventHandler);
        binding.setModel(datas.get(position));
        if(!TextUtils.isEmpty(datas.get(position).getPmsKey())){
            int iconByKey = MenuHelper.getIconByKey(datas.get(position).getPmsKey());
            if(iconByKey != -1){
                binding.ivGridview.setImageResource(iconByKey);
            }
            binding.tvGridview.setText(MenuHelper.getAppNames().get(datas.get(position).getPmsKey()));
        }
//        binding.ivGridview.setImageResource(datas.get(position).getIcon());
//        binding.tvGridview.setText(datas.get(position).getName());
//        Glide.with(context).load(datas.get(position).getIcon()).error(R.mipmap.ic_launcher_round).placeholder(R.mipmap.ic_launcher_round).centerCrop().into(binding.ivGridview);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onEventListener){
                    onEventListener.onItemClick(datas.get(position));
                }
//                Toast.makeText(context,position,Toast.LENGTH_SHORT).show();

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (null != onEventListener){
                    onEventListener.onItemLongClick(datas.get(position));
                }
//                Toast.makeText(context,"请点击更多跳转至编辑页面编辑应用",Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }

    private OnEventListener onEventListener;

    public void setOnEventListener(OnEventListener onEventListener) {
        this.onEventListener = onEventListener;
    }

    public interface OnEventListener {
        void onItemClick(AppInfo appInfo);
        void onItemLongClick(AppInfo appInfo);
    }
    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ComAppItemViewHolder<B extends ViewDataBinding>  extends RecyclerView.ViewHolder{
        public ComAppItemViewHolder(View itemView) {
            super(itemView);
        }
    }

}
