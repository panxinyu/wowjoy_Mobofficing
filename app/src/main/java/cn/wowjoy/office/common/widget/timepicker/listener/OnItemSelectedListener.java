package cn.wowjoy.office.common.widget.timepicker.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
