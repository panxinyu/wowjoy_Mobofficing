package cn.wowjoy.office.common.widget.album.selector;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.data.mock.Image;

/**
 * Created by Sherily on 2017/12/22.
 * Description:
 */

public class ImageSelection {
  private Image image;
  private boolean isSelect;
  private int selectCount;

  private ArrayList<Image> images;
  private int position;

    public ImageSelection(Image image, boolean isSelect, int selectCount) {
        this.image = image;
        this.isSelect = isSelect;
        this.selectCount = selectCount;
    }

    public ImageSelection(ArrayList<Image> images, int position) {
        this.images = images;
        this.position = position;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public int getSelectCount() {
        return selectCount;
    }

    public void setSelectCount(int selectCount) {
        this.selectCount = selectCount;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
