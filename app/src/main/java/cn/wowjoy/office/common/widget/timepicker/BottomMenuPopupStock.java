package cn.wowjoy.office.common.widget.timepicker;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import cn.wowjoy.office.R;

/**
 * Created by Administrator on 2017/11/9.
 */

public class BottomMenuPopupStock extends PopupWindow {
    private View mMenuView;
    public TextView sureSubmit;
    public TextView sureOther;
    private TextView onlyCheck;
    private TextView onlySubmit;
    private TextView onlyQue;
    private TextView cancel;

    private String type;
    private String permission;


    public BottomMenuPopupStock(Activity context, View.OnClickListener itemsOnClick,String permission,String type) {
        super(context);
        this.permission = permission;
        this.type =type;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mMenuView = inflater.inflate(R.layout.bottom_menu_state_stock, null);
        sureSubmit = (TextView) mMenuView.findViewById(R.id.sure_submit);
        sureOther = (TextView) mMenuView.findViewById(R.id.sure_other);
        onlyCheck = (TextView) mMenuView.findViewById(R.id.only_check);
        onlySubmit = (TextView) mMenuView.findViewById(R.id.only_submit);

        onlyQue = (TextView) mMenuView.findViewById(R.id.only_que);
        cancel = (TextView) mMenuView.findViewById(R.id.only_cancel);

//        //取消按钮
//        btn_cancel.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                //销毁弹出框
//                dismiss();
//            }
//        });
        if(type.equals("其他出库")){
            if(null != permission && !"".equals(permission)){
                if("y".equalsIgnoreCase(permission.trim())){
                    sureSubmit.setVisibility(View.VISIBLE);
                    sureOther.setVisibility(View.VISIBLE);

                    onlyQue.setVisibility(View.GONE);
                }else if("n".equalsIgnoreCase(permission.trim())){
                    sureSubmit.setVisibility(View.GONE);
                    sureOther.setVisibility(View.GONE);

                    onlyQue.setVisibility(View.VISIBLE);
                }
            }
            //设置按钮监听
            mMenuView .setOnClickListener(itemsOnClick);
            sureSubmit.setOnClickListener(itemsOnClick);
            sureOther .setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sureOther.setVisibility(View.GONE);

                    onlyCheck.setVisibility(View.VISIBLE);
                    onlySubmit.setVisibility(View.VISIBLE);
                }
            });
            onlyCheck .setOnClickListener(itemsOnClick);
            onlySubmit.setOnClickListener(itemsOnClick);
            onlyQue .setOnClickListener(itemsOnClick);
            cancel .setOnClickListener(itemsOnClick);
        }else if(type.equals("申请出库")){
            if(null != permission && !"".equals(permission)){
                if("y".equalsIgnoreCase(permission.trim())){
                    sureSubmit.setVisibility(View.VISIBLE);
                    sureOther.setVisibility(View.VISIBLE);
                    sureOther.setText("仅提交");
                    onlyQue.setVisibility(View.GONE);
                }else if("n".equalsIgnoreCase(permission.trim())){
                    sureSubmit.setVisibility(View.GONE);
                    sureOther.setVisibility(View.GONE);

                    onlyQue.setVisibility(View.VISIBLE);
                }
            }
            //设置按钮监听
            mMenuView .setOnClickListener(itemsOnClick);
            sureSubmit.setOnClickListener(itemsOnClick);
            sureOther .setOnClickListener(itemsOnClick);
            onlyCheck .setOnClickListener(itemsOnClick);
            onlySubmit.setOnClickListener(itemsOnClick);
            onlyQue .setOnClickListener(itemsOnClick);
            cancel .setOnClickListener(itemsOnClick);
        }else if(type.equals("PM报告")){
            sureSubmit.setText("仅保存");
            sureSubmit.setTextColor(context.getResources().getColor(R.color.C_333333));
            sureOther.setText("提交");
            sureOther.setTextColor(context.getResources().getColor(R.color.C_333333));
            cancel.setText("取消");
            cancel.setTextColor(context.getResources().getColor(R.color.C_333333));
            cancel.setBackground(context.getResources().getDrawable(R.drawable.corners_bg_f4f4f4));
            //设置按钮监听
            sureSubmit.setOnClickListener(itemsOnClick);
            sureOther .setOnClickListener(itemsOnClick);
            cancel .setOnClickListener(itemsOnClick);
        }

        //设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);
        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mMenuView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = mMenuView.findViewById(R.id.ll_pop).getTop();
                int y=(int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y<height){
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
     public void setVisibility(){
        if("其他出库".equalsIgnoreCase(type)){
            sureOther.setVisibility(View.VISIBLE);

            onlyCheck.setVisibility(View.GONE);
            onlySubmit.setVisibility(View.GONE);
        }
     }

}
