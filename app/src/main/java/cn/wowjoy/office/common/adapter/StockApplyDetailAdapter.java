package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.databinding.ItemRvStockApplySingleDetailBinding;
import cn.wowjoy.office.materialinspection.applyfor.detail.StockApplyDetailViewModel;

/**
 * Created by Administrator on 2018/4/12.
 */

public class StockApplyDetailAdapter extends RecyclerView.Adapter<StockApplyDetailAdapter.ItemViewHolder>{

    private Context context;
    private LayoutInflater inflater;
    private Object mEventListener;
    private boolean isDone ;

    private List<ApplyDemandHeadDetailItem> popuModels;

    private List<ApplyDemandHeadDetailItem> selectList;  //用户勾选 生成出库单的数据

    public List<ApplyDemandHeadDetailItem> getSelectList() {
        return selectList;
    }

    public void setItemModels(List<ApplyDemandHeadDetailItem> models) {
        this.popuModels = models;
        notifyDataSetChanged();
    }



    public void setDone(boolean done) {
        isDone = done;
        notifyDataSetChanged();
    }

    public void setEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    public StockApplyDetailAdapter() {
    }

    public StockApplyDetailAdapter(boolean isDone) {
        this.isDone = isDone;
    }

    public void selectAll(boolean state){
        if(null != popuModels && popuModels.size()>0){
            for(ApplyDemandHeadDetailItem a : popuModels){
                a.setSelect(state);
            }
        }
        if(null == selectList){
            selectList= new ArrayList<>();
        }
        if(state){
            selectList.clear();
            selectList.addAll(popuModels);
        }else{
            selectList.clear();
        }
         notifyDataSetChanged();
        ((StockApplyDetailViewModel) mEventListener).isSelectNum(selectList.size());
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_rv_stock_apply_single_detail,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Log.e("PXY", "select  position: "+position+ "  getAdapterPosition :"+holder.getAdapterPosition());
   //     holder.setIsRecyclable(false);
        holder.bind(mEventListener,popuModels.get(position),position);
        ApplyDemandHeadDetailItem response = popuModels.get(position);
        holder.binding.selectOut.setChecked(response.isSelect());
        if(isDone){
            holder.binding.selectOut.setVisibility(View.GONE);
//            holder.binding.llDetail.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ((StockApplyDetailViewModel)mEventListener).jump(response);
//                }
//            });
        }else{
            holder.binding.selectOut.setVisibility(View.VISIBLE);
        }

//        holder.binding.selectOut.setTag();

        holder.binding.selectOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PXY", "onClick: "+  holder.binding.selectOut.isChecked()
                        +"    position: "+position+"   response: "+response.toString());
                if( holder.binding.selectOut.isChecked()){
                    if(!selectList.contains(response)){
                        selectList.add(response);
                    }
                    response.setSelect(true);
                }else{
                    if(selectList.contains(response)){
                        selectList.remove(response);
                    }
                    response.setSelect(false);
                }
                holder. binding.selectOut.setChecked(response.isSelect());
                notifyItemChanged(position);
                ((StockApplyDetailViewModel) mEventListener).isSelectNum(selectList.size());
  //              Log.e("PXY", "selectList: "+ selectList.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == popuModels ? 0 : popuModels.size();
    }

      class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemRvStockApplySingleDetailBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemRvStockApplySingleDetailBinding) binding;
        }

        public void bind(Object mEventHandler,ApplyDemandHeadDetailItem response,int position){
            if(null == selectList){
                selectList = new ArrayList<>();
            }
            binding.setViewModel((StockApplyDetailViewModel) mEventHandler);
            binding.setModel(response);
        }
    }
}
