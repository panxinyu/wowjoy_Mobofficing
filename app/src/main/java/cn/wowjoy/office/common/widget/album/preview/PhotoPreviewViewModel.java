package cn.wowjoy.office.common.widget.album.preview;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import dagger.Module;

/**
 * Created by Sherily on 2017/11/16.
 * Description:
 */
public class PhotoPreviewViewModel extends NewBaseViewModel {

    @Inject
    public PhotoPreviewViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }
}
