package cn.wowjoy.office.common.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import cn.wowjoy.office.R;
import cn.wowjoy.office.utils.DensityUtil;


public class MyToast extends Toast {
    private Context ct;
    private LayoutInflater inflater;
    private LinearLayout.LayoutParams lp;
    private View layout;
    private TextView tv;
    private int textSize;
    private int screenWidth;

    public MyToast(Context context) {
        super(context);
        this.ct = context;
        inflater = LayoutInflater.from(ct);
    }

    public void showinfo(String s) {
        layout = inflater.inflate(R.layout.toast_bg, null);
        textSize = DensityUtil.dip2px(ct, 16);
        screenWidth = DensityUtil.getWidthPixels(ct);

        if (!TextUtils.isEmpty(s) && textSize * s.length() < screenWidth * 3 / 4)
            lp = new LinearLayout.LayoutParams(textSize * s.length() + 35, LinearLayout.LayoutParams.WRAP_CONTENT);
        else if (!TextUtils.isEmpty(s) && textSize * s.length() < screenWidth * 3 / 2) {
            lp = new LinearLayout.LayoutParams(screenWidth * 3 / 4 + 35, LinearLayout.LayoutParams.WRAP_CONTENT);
        } else {
            lp = new LinearLayout.LayoutParams(screenWidth * 3 / 4 + 35, LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        tv = (TextView) layout.findViewById(R.id.pop_bg_text);
        tv.setText(s);
        tv.setLayoutParams(lp);
        setGravity(Gravity.CENTER, 0, -DensityUtil.dip2px(ct, 30));
        setDuration(LENGTH_LONG);
        setView(layout);
        show();
    }

    public void showinfo(String s, int color) {
        layout = inflater.inflate(R.layout.toast_bg, null);
        textSize = DensityUtil.dip2px(ct, 16);
        screenWidth = DensityUtil.getWidthPixels(ct);

        if (textSize * s.length() < screenWidth * 3 / 4)
            lp = new LinearLayout.LayoutParams(textSize * s.length() + 35, LinearLayout.LayoutParams.WRAP_CONTENT);
        else if (textSize * s.length() < screenWidth * 3 / 2) {
            lp = new LinearLayout.LayoutParams(screenWidth * 3 / 4 + 35, LinearLayout.LayoutParams.WRAP_CONTENT);
        } else {
            lp = new LinearLayout.LayoutParams(screenWidth * 3 / 4 + 35, LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        tv = (TextView) layout.findViewById(R.id.pop_bg_text);
        tv.setText(s);
        tv.setLayoutParams(lp);
        tv.setTextColor(ct.getResources().getColor(color));
        setGravity(Gravity.CENTER, 0, -DensityUtil.dip2px(ct, 30));
        setDuration(LENGTH_SHORT);
        setView(layout);
        show();
    }
}
