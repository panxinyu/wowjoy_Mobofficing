package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.view.WindowManager;

/**
 * Created by Administrator on 2017/11/13.
 */

public class AlphaManager {
    /**
     * 设置添加屏幕的背景透明度
     * @param bgAlpha
     */
    public static void backgroundAlpha(float bgAlpha, Activity activity)
    {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        activity.getWindow().setAttributes(lp);
    }
}
