package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.MaterialSearchResponse;
import cn.wowjoy.office.data.response.StockDetailResponse;
import cn.wowjoy.office.databinding.StockDetailItemViewBinding;

/**
 * Created by Sherily on 2018/1/5.
 * Description:
 */

public class StockDetailAdapter extends RecyclerView.Adapter<StockDetailAdapter.ItemViewHolder> {
    private LayoutInflater inflater;
    private List<MaterialSearchResponse> stockDetailResponseList;

    public StockDetailAdapter(List<MaterialSearchResponse> stockDetailResponseList) {
        this.stockDetailResponseList = stockDetailResponseList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            inflater = LayoutInflater.from(parent.getContext());
        }
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.stock_detail_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(stockDetailResponseList.get(position));
    }

    @Override
    public int getItemCount() {
        return null == stockDetailResponseList ? 0 : stockDetailResponseList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        private StockDetailItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (StockDetailItemViewBinding) binding;
        }

        public void bind(MaterialSearchResponse response){
            binding.setModel(response);
        }
    }
}
