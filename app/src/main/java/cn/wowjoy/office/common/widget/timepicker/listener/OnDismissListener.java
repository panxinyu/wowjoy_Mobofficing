package cn.wowjoy.office.common.widget.timepicker.listener;

/**
 * Created by Sherily on 2018/1/19.
 */
public interface OnDismissListener {
    public void onDismiss(Object o);
}
