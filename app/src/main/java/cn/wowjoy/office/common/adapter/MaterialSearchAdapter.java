package cn.wowjoy.office.common.adapter;

import android.arch.lifecycle.LiveData;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.GoodsDetailResponse;
import cn.wowjoy.office.data.response.MaterialSearchResponse;
import cn.wowjoy.office.databinding.MaterialSearchItemViewBinding;
import cn.wowjoy.office.materialmanage.search.MaterialSearchViewModel;
import cn.wowjoy.office.materialmanage.stock.MaterialStockViewModel;

/**
 * Created by Sherily on 2017/12/28.
 * Description:
 */

public class MaterialSearchAdapter extends RecyclerView.Adapter<MaterialSearchAdapter.ItemViewHolder> {

    private LayoutInflater inflater;
    private List<MaterialSearchResponse> searchResponses;
    private Object mEventHandle;
    private String keyWord;

    public MaterialSearchAdapter() {
    }

//    public void setData(List<MaterialSearchResponse> searchResponses) {
//        this.searchResponses = searchResponses;
//        notifyDataSetChanged();
//    }

    public void refresh(List<MaterialSearchResponse> searchResponse){
        if (null != searchResponses){
            searchResponses.clear();
        }
        this.searchResponses = searchResponse;
        notifyDataSetChanged();
    }

    public void loadMore(List<MaterialSearchResponse> searchResponse){
        this.searchResponses.addAll(searchResponse);
        notifyDataSetChanged();
    }

    public void clean(){
        if (null != searchResponses && !searchResponses.isEmpty()){
            searchResponses.clear();
            notifyDataSetChanged();
        }
    }
    public void setmEventHandle(Object mEventHandle) {
        this.mEventHandle = mEventHandle;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater){
            inflater = LayoutInflater.from(parent.getContext());
        }
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.material_search_item_view,parent,false)) ;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(searchResponses.get(position),mEventHandle,keyWord);

    }

    @Override
    public int getItemCount() {
        return null == searchResponses ? 0 : searchResponses.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {

        private MaterialSearchItemViewBinding binding;
        public ItemViewHolder(MaterialSearchItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(MaterialSearchResponse response,Object mEventHandle,String keyWord){
            binding.setMEnventHandler((MaterialStockViewModel) mEventHandle);
            binding.setKeyword(keyWord);
            binding.setModel(response);
        }
    }
}
