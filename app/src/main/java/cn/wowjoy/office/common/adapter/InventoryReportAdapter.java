package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.databinding.InventoryRecordItemViewBinding;
import cn.wowjoy.office.databinding.InventoryReportItemViewBinding;
import cn.wowjoy.office.materialmanage.add.AddReportViewModel;
import cn.wowjoy.office.materialmanage.inventoryReport.InventoryReportViewModel;

/**
 * Created by Sherily on 2018/1/29.
 * Description:
 */

public class InventoryReportAdapter extends RecyclerView.Adapter<InventoryReportAdapter.ItemViewHolder> {
    private List<RecordResponse> datas;
    private LayoutInflater inflater;
    private Object mEventObject;


    public void setmEventObject(Object mEventObject) {
        this.mEventObject = mEventObject;
    }


    public void refresh(List<RecordResponse> data){
        if (null != datas){
            datas.clear();
        }
        this.datas = data;
        notifyDataSetChanged();
    }

    public void loadMore(List<RecordResponse> data){
        this.datas.addAll(data);
        notifyDataSetChanged();
    }


    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());
        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.inventory_report_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(mEventObject,datas.get(position));
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ItemViewHolder extends  RecyclerView.ViewHolder{
        private InventoryReportItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (InventoryReportItemViewBinding) binding;
        }
        public void bind(Object mEventHandler, RecordResponse model){
            binding.setViewModel((InventoryReportViewModel) mEventHandler);
            binding.setModel(model);
            changeStatus(model);
        }
        private void changeStatus(RecordResponse model){
            if (!TextUtils.isEmpty(model.getProfitLossMoney())){
                if (Double.parseDouble(model.getProfitLossMoney()) == 0){
                    binding.money.setTextColor(binding.money.getResources().getColor(R.color.appText10));
//                binding.money.setText(matcherSearchText(0xff666666,12f,model.formatProfit(),model.getProfitLossNumber()));

                } else if (Double.parseDouble(model.getProfitLossMoney()) < 0){
                    binding.money.setTextColor(binding.money.getResources().getColor(R.color.menu_look_error));
//                binding.money.setText(matcherSearchText(0xffFB6769,12f,model.formatProfit(),model.getProfitLossNumber()));

                } else {
                    binding.money.setTextColor(binding.money.getResources().getColor(R.color.decorateGreenHighLight));
//                binding.money.setText(matcherSearchText(0xff0BC69C,12f,model.formatProfit(),model.getProfitLossNumber()));
                }
            }

        }
        private SpannableStringBuilder matcherSearchText(int color, float textSize, String text, String keyword) {
            if ( textSize == 0.0f )
                textSize = 12f;
            int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, textSize, binding.money.getResources().getDisplayMetrics());
            SpannableStringBuilder ss = new SpannableStringBuilder(text);
            Pattern pattern = Pattern.compile(keyword);
            Matcher matcher = pattern.matcher(ss);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                ss.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new AbsoluteSizeSpan(size), start , end, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            return ss;
        }
    }
}
