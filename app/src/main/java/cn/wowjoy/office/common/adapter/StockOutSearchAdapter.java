package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.databinding.ItemRvStockOtherHandAddBinding;
import cn.wowjoy.office.materialinspection.stockout.hand.StockHandAddViewModel;

/**
 * Created by Administrator on 2018/4/12.
 */

public class StockOutSearchAdapter extends RecyclerView.Adapter<StockOutSearchAdapter.ItemViewHolder>{
    private Context context;
    private LayoutInflater inflater;
    private Object mEventListener;
    private List<StockForOtherOutResponse> popuModels;
   private String keyWord;


    public void setPopuModels(List<StockForOtherOutResponse> popuModels,String keyWord) {
        this.popuModels = popuModels;
        this.keyWord = keyWord;
        notifyDataSetChanged();
    }
//    public void setPopuModels(List<StockForOtherOutResponse> popuModels) {
//        this.popuModels = popuModels;
//        notifyDataSetChanged();
//    }
    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.item_rv_stock_other_hand_add,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
    //    Log.e("PXY", "Search  onclick: "+position );
        holder.setIsRecyclable(false);
        holder.bind(mEventListener,keyWord,popuModels.get(position));
    }

    @Override
    public int getItemCount() {
        return null == popuModels ? 0 : popuModels.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private ItemRvStockOtherHandAddBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (ItemRvStockOtherHandAddBinding) binding;
        }

        public void bind(Object mEventHandler,String keyWord,StockForOtherOutResponse response){
            binding.setKeyWord(keyWord);
            binding.setPresenter((StockHandAddViewModel) mEventHandler);
            binding.setModel(response);
        }
    }
}
