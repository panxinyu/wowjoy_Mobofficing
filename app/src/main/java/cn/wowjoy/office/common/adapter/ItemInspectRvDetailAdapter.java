package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.InspectTaskDtlVO;
import cn.wowjoy.office.databinding.ItemRvPatroltaskBinding;
import cn.wowjoy.office.materialinspection.xunjian.task.PatrolTaskViewModel;

public class ItemInspectRvDetailAdapter extends RecyclerView.Adapter<ItemInspectRvDetailAdapter.ViewHolder> {

    private List<InspectTaskDtlVO> mEntities;
    private  ItemRvPatroltaskBinding binding;
    private Context context;
    private LayoutInflater layoutInflater;

    private Object mItemEventHandler;
    public void setItemEventHandler(Object mItemEventHandler) {
        this.mItemEventHandler = mItemEventHandler;
    }
    public void refresh(List<InspectTaskDtlVO> data){
        if (null != mEntities){
            mEntities.clear();
        }
        this.mEntities = data;
        notifyDataSetChanged();
    }
    public int getDateSize(){
       return null == mEntities ? 0 :mEntities.size();
    }
    public void setData(List<InspectTaskDtlVO> data){
        if (null == mEntities){
           mEntities = new ArrayList<>();
        }
        mEntities.clear();
        mEntities.addAll(data);
        notifyDataSetChanged();
    }

    private String state = "全部";
    public void setData(List<InspectTaskDtlVO> data,String state){
        if (null == mEntities){
            mEntities = new ArrayList<>();
        }
        this.state = state;
        mEntities.clear();
        mEntities.addAll(data);
        notifyDataSetChanged();
    }
    public void loadMore(List<InspectTaskDtlVO> data){
        this.mEntities.addAll(data);
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == layoutInflater) {
            context = parent.getContext();
            layoutInflater = LayoutInflater.from(context);
        }
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_rv_patroltask, parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        InspectTaskDtlVO inspectTaskDtlVO = mEntities.get(position);
//        Log.e("PXY", "position: "+position );
        holder.bindData(inspectTaskDtlVO,mItemEventHandler);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
       return null == mEntities ? 0 :mEntities.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        ItemRvPatroltaskBinding binding;
        public void setBgAndText( InspectTaskDtlVO inspectTaskDtlVO){
            //  设备状态鉴定
//            if(!TextUtils.isEmpty(state) && ){
//
//            }
            GradientDrawable drawable = (GradientDrawable) binding.textState.getBackground();
            GradientDrawable drawableCheck = (GradientDrawable) binding.textStateCheck.getBackground();
            if(null == inspectTaskDtlVO.getIsExist() && null == inspectTaskDtlVO.getAppearanceStatus() && null == inspectTaskDtlVO.getAssetsStatus()){
                //设备未检测状态
                binding.textState.setText("未检测");
                drawable.setColor(context.getResources().getColor(R.color.menu_no_check));
                binding.textStateCheck.setVisibility(View.GONE);
            }else if( null != inspectTaskDtlVO.getIsExist() && "n".equalsIgnoreCase(inspectTaskDtlVO.getIsExist())){
                //设备未找到
                binding.textState.setText("设备未找到");
                drawable.setColor(context.getResources().getColor(R.color.menu_not_find));
                binding.textStateCheck.setVisibility(View.GONE);
            }else if( null != inspectTaskDtlVO.getAssetsStatus() && "m".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus())  ){//&& ("n".equals(inspectTaskDtlVO.getAppearanceStatus()) || "n".equals(inspectTaskDtlVO.getFunctionStatus()))
                //维修
                binding.textState.setText("维修");
                drawable.setColor(context.getResources().getColor(R.color.menu_not_find));
                if("n".equalsIgnoreCase(inspectTaskDtlVO.getLabelStatus()) || "n".equalsIgnoreCase(inspectTaskDtlVO.getCleanStatus())){
                    //又满足是外观异常
                    binding.textStateCheck.setText("外观异常");
                    binding.textStateCheck.setVisibility(View.VISIBLE);
                    drawableCheck.setColor(context.getResources().getColor(R.color.menu_look_error));
                }else{
                    binding.textStateCheck.setVisibility(View.GONE);
                }

            }else if("n".equalsIgnoreCase(inspectTaskDtlVO.getLabelStatus()) || "n".equalsIgnoreCase(inspectTaskDtlVO.getCleanStatus())){
                //外观异常
                binding.textState.setText("外观异常");
                drawable.setColor(context.getResources().getColor(R.color.menu_look_error));
                if( null != inspectTaskDtlVO.getAssetsStatus() && "m".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus())){
                    //又满足维修
                    binding.textStateCheck.setText("维修");
                    binding.textStateCheck.setVisibility(View.VISIBLE);
                    drawableCheck.setColor(context.getResources().getColor(R.color.menu_not_find));
                }else if( null != inspectTaskDtlVO.getAssetsStatus() && "n".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus()) && ("y".equals(inspectTaskDtlVO.getLabelStatus()) && "y".equals(inspectTaskDtlVO.getCleanStatus()))){
                    //又满足停用
                    binding.textStateCheck.setText("停用");
                    binding.textStateCheck.setVisibility(View.VISIBLE);
                    drawableCheck.setColor(context.getResources().getColor(R.color.menu_device_error));
                }else {
                    binding.textStateCheck.setVisibility(View.GONE);
                }
            }else if( null != inspectTaskDtlVO.getAssetsStatus() && "n".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus()) && ("y".equals(inspectTaskDtlVO.getLabelStatus()) && "y".equals(inspectTaskDtlVO.getCleanStatus()))){
                //停用
                binding.textState.setText("停用");
                drawable.setColor(context.getResources().getColor(R.color.menu_device_error));
                if("n".equalsIgnoreCase(inspectTaskDtlVO.getLabelStatus()) || "n".equalsIgnoreCase(inspectTaskDtlVO.getCleanStatus())){
                    //又满足是外观异常
                    binding.textStateCheck.setText("外观异常");
                    binding.textStateCheck.setVisibility(View.VISIBLE);
                    drawableCheck.setColor(context.getResources().getColor(R.color.menu_not_find));
                }else{
                    binding.textStateCheck.setVisibility(View.GONE);
                }
            }else if(null != inspectTaskDtlVO.getAssetsStatus() && "y".equalsIgnoreCase(inspectTaskDtlVO.getAssetsStatus())
                    && null != inspectTaskDtlVO.getIsExist() && "y".equalsIgnoreCase(inspectTaskDtlVO.getIsExist())
                    && null != inspectTaskDtlVO.getAppearanceStatus() && "y".equalsIgnoreCase(inspectTaskDtlVO.getAppearanceStatus())
                    ){
                binding.textState.setText("正常");
                drawable.setColor(context.getResources().getColor(R.color.menu_normal));
                binding.textStateCheck.setVisibility(View.GONE);
            }
        }

        public ViewHolder(ItemRvPatroltaskBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        public void bindData(InspectTaskDtlVO response,Object mItemEventHandler){

            binding.setViewModel((PatrolTaskViewModel) mItemEventHandler);
            binding.setModel(response);
            setBgAndText(response);
            binding.executePendingBindings();
        }
    }
}
