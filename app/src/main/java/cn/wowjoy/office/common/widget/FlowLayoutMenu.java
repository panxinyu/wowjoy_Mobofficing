package cn.wowjoy.office.common.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.Random;

import cn.wowjoy.office.R;

/**
 * Created by Administrator on 2017/11/13.
 */

public class FlowLayoutMenu extends PopupWindow {
    private Activity mActivity;
    private View conentView;
    private String[] strs;
    private FlowLayout mFlowMenu;
    private OnClickMenuItem mOnClickMenuItem;


    public FlowLayoutMenu(final Activity context,String[] strs,OnClickMenuItem mOnClickMenuItem) {
        mActivity = context;
        this.strs= strs;
        this.mOnClickMenuItem =mOnClickMenuItem;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = inflater.inflate(R.layout.flowlayout_menu, null);
        int h = context.getWindowManager().getDefaultDisplay().getHeight();
        int w = context.getWindowManager().getDefaultDisplay().getWidth();
        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(w);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        Random random = new Random();
        mFlowMenu = (FlowLayout) conentView.findViewById(R.id.flow_menu);
        // 循环添加TextView到容器
        for (int i = 0; i < strs.length; i++) {
            final TextView view = new TextView(mActivity);
            view.setText(strs[i]);
            view.setTextColor(Color.WHITE);
            view.setPadding(5, 5, 5, 5);
            view.setGravity(Gravity.CENTER);
            view.setTextSize(14);
//            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
//            layoutParams.topMargin=20;
//            layoutParams.bottomMargin=20;
//            view.setLayoutParams(layoutParams);
            // 设置点击事件
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(mActivity, view.getText().toString(), Toast.LENGTH_SHORT).show();
                    mOnClickMenuItem.onClick(view.getText().toString());
                }
            });

            // 设置彩色背景
            GradientDrawable normalDrawable = new GradientDrawable();
            normalDrawable.setShape(GradientDrawable.RECTANGLE);
            int a = 255;
            int r = 50 + random.nextInt(150);
            int g = 50 + random.nextInt(150);
            int b = 50 + random.nextInt(150);
            normalDrawable.setColor(Color.argb(a, r, g, b));

            // 设置按下的灰色背景
            GradientDrawable pressedDrawable = new GradientDrawable();
            pressedDrawable.setShape(GradientDrawable.RECTANGLE);
            pressedDrawable.setColor(Color.GRAY);

            // 背景选择器
            StateListDrawable stateDrawable = new StateListDrawable();
            stateDrawable.addState(new int[]{android.R.attr.state_pressed}, pressedDrawable);
            stateDrawable.addState(new int[]{}, normalDrawable);

            // 设置背景选择器到TextView上
            view.setBackground(stateDrawable);

            mFlowMenu.addView(view);
        }
        //添加pop窗口关闭事件
//        this.setOnDismissListener(new FlowLayoutMenu.poponDismissListener());
    }

  public interface OnClickMenuItem{
        void onClick(String text);
   }
    /**
     * 添加新笔记时弹出的popWin关闭的事件，主要是为了将背景透明度改回来
     * @author cg
     *
     */
    class poponDismissListener implements PopupWindow.OnDismissListener{

        @Override
        public void onDismiss() {
            // TODO Auto-generated method stub
            //Log.v("List_noteTypeActivity:", "我是关闭事件");
            AlphaManager.backgroundAlpha(1f,mActivity);
        }
    }

}
