package cn.wowjoy.office.common.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Random;

import cn.wowjoy.office.R;
import cn.wowjoy.office.data.response.CheckerResponse;
import cn.wowjoy.office.databinding.StaffItemViewBinding;
import cn.wowjoy.office.materialmanage.inventoryReport.InventoryReportViewModel;

/**
 * Created by Sherily on 2018/1/29.
 * Description:
 */

public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.ItemViewHolder> {
    private LayoutInflater inflater;
    private List<CheckerResponse> datas;
    private String keyWord;
    private Object mEventHandler;

    public void setDatas(List<CheckerResponse> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public void setmEventHandler(Object mEventHandler) {
        this.mEventHandler = mEventHandler;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == inflater)
            inflater = LayoutInflater.from(parent.getContext());

        return new ItemViewHolder(DataBindingUtil.inflate(inflater, R.layout.staff_item_view,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(mEventHandler,keyWord,datas.get(position));
    }

    @Override
    public int getItemCount() {
        return null == datas ? 0 : datas.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        private StaffItemViewBinding binding;
        public ItemViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = (StaffItemViewBinding) binding;
        }

        public void bind(Object mEventHandler,String keyWord,CheckerResponse response){
            binding.setKeyWord(keyWord);
            binding.setViewModel((InventoryReportViewModel) mEventHandler);
            binding.setModel(response);
            randomColor();
        }
        private void randomColor(){
            Random random = new Random();
            int[] ranColor ={0xffF1A155,0xff6399D9, 0xff82C736,
                    0xff21AFEF, 0xff61C778, 0xffEC7979};
            int randomcolor =random.nextInt(ranColor.length);
            ((GradientDrawable) binding.nameShortcut.getBackground()).setColor(ranColor[randomcolor]);

        }
    }
}
