package cn.wowjoy.office.common.widget;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import cn.wowjoy.office.R;
import cn.wowjoy.office.databinding.EngineActionSheetLayoutBinding;

/**
 * Created by Sherily on 2017/11/7.
 */

public class EngineActionSheet extends ActionSheet<EngineActionSheetLayoutBinding> {

    private static final String KEY_PARAM = "PARAM";

    @SuppressLint("ValidFragment")
    private EngineActionSheet() {
    }

     View.OnClickListener mTitleClickListener;
     View.OnClickListener mBtnClickListener;
     View.OnClickListener mCancelClickListener;

     private boolean canTitleClick = false;

    public void setmTitleClickListener(View.OnClickListener mTitleClickListener) {
        this.mTitleClickListener = mTitleClickListener;
    }

    public void setmBtnClickListener(View.OnClickListener mBtnClickListener) {
        this.mBtnClickListener = mBtnClickListener;
    }

    public void setmCancelClickListener(View.OnClickListener mCancelClickListener) {
        this.mCancelClickListener = mCancelClickListener;
    }

    private ActionSheetParams mParam;
    static EngineActionSheet newInstance(ActionSheetParams param){
        Bundle args = new Bundle();
        args.putParcelable(KEY_PARAM, param);
        EngineActionSheet fragment = new EngineActionSheet();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.engine_action_sheet_layout;
    }

    @Override
    protected void init() {
        super.init();
        if (null != getArguments()){
            mParam = getArguments().getParcelable(KEY_PARAM);
            binding.notice.setText(mParam.mTitle);
            binding.notice.setVisibility(mParam.isShowTitle? View.VISIBLE : View.GONE);
            binding.notice.setTextColor(getResources().getColor(mParam.mTitleColor));
            binding.notice.setTextSize(mParam.mTitleSize);
            canTitleClick = mParam.isTitleClickable;

            binding.handleTv.setText(mParam.mBtnText);
            binding.handleTv.setTextColor(getResources().getColor(mParam.mBtnTextColor));
            binding.handleTv.setTextSize(mParam.mBtnTextSize);

            binding.cancel.setText(mParam.mCancelText);
            binding.cancel.setTextColor(getResources().getColor(mParam.mCancelTextColor));
            binding.cancel.setTextSize(mParam.mCancelTextSize);
        }

        if (canTitleClick){
            binding.notice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mTitleClickListener){
                        mTitleClickListener.onClick(v);
                    }
                }
            });
        }

        binding.handleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mBtnClickListener){
                    mBtnClickListener.onClick(v);
                }
            }
        });

        binding.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mCancelClickListener){
                    mCancelClickListener.onClick(v);
                }
            }
        });

    }

    static class ActionSheetParams implements Parcelable {
        private String mTitle = "";
        private String mBtnText = "";
        private String mCancelText = "";
        int mTitleColor = R.color.appText3;
        int mBtnTextColor = R.color.red;
        int mCancelTextColor = R.color.appText;
        float mTitleSize = 14.0f;
        float mBtnTextSize = 17.0f;
        float mCancelTextSize = 17.0f;
        boolean isShowTitle = true;
        boolean isTitleClickable = false;




        public ActionSheetParams() {

        }

        protected ActionSheetParams(Parcel in) {
            mTitle = in.readString();
            mBtnText = in.readString();
            mCancelText = in.readString();
            mTitleColor = in.readByte();
            mBtnTextColor = in.readByte();
            mCancelTextColor = in.readByte();
            mTitleSize = in.readFloat();
            mBtnTextSize = in.readFloat();
            mCancelTextSize = in.readFloat();
            isShowTitle = in.readInt() == 0 ? false : true;
            isTitleClickable = in.readInt() == 0 ? false : true;
            isShowTitle = in.readInt() == 0 ? false : true;
        }

        public static final Creator<ActionSheetParams> CREATOR = new Creator<ActionSheetParams>() {
            @Override
            public ActionSheetParams createFromParcel(Parcel in) {
                return new ActionSheetParams(in);
            }

            @Override
            public ActionSheetParams[] newArray(int size) {
                return new ActionSheetParams[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mTitle);
            dest.writeString(mBtnText);
            dest.writeString(mCancelText);
            dest.writeInt(mTitleColor);
            dest.writeInt(mBtnTextColor);
            dest.writeInt(mCancelTextColor);
            dest.writeFloat(mTitleSize);
            dest.writeFloat(mBtnTextSize);
            dest.writeFloat(mCancelTextSize);
            dest.writeInt(isShowTitle ? 1 :0);
            dest.writeInt(isTitleClickable ? 1 :0);
            dest.writeInt(isShowTitle ? 1 :0);
        }
    }

    public static class Builder {
        private ActionSheetParams mParams;
        private View.OnClickListener mTitleClickListener;
        private View.OnClickListener mBtnClickListener;
        private View.OnClickListener mCancelClickListener;

        public Builder() {
            mParams = new ActionSheetParams();
        }

        public Builder showTitle(boolean show){
            mParams.isShowTitle = show;
            return this;
        }

        public Builder setTitle(String title, View.OnClickListener listener, boolean isClickable) {
            mParams.mTitle = title;
            mParams.isTitleClickable = isClickable;
            mTitleClickListener = listener;
            return this;
        }
        public Builder setBtn(String str, View.OnClickListener listener){
            mParams.mBtnText = str;
            mBtnClickListener = listener;
            return this;
        }

        public Builder setCancel(String str, View.OnClickListener listener){
            mParams.mCancelText = str;
            mCancelClickListener = listener;
            return this;
        }

        public Builder setTitleColor(int color){
            mParams.mTitleColor = color;
            return this;
        }

        public Builder setBtnColor(int color){
            mParams.mBtnTextColor = color;
            return this;
        }

        public Builder setCancelColor(int color){
            mParams.mCancelTextColor = color;
            return this;
        }

        public Builder setTitleSize(float size){
            mParams.mTitleSize = size;
            return this;
        }

        public Builder setBtnSize(float size){
            mParams.mBtnTextSize = size;
            return this;
        }

        public Builder setCancelSize(float size){
            mParams.mCancelTextSize = size;
            return this;
        }


        public EngineActionSheet create() {
            EngineActionSheet fragment = EngineActionSheet.newInstance(mParams);
            fragment.setmBtnClickListener(mBtnClickListener);
            fragment.setmCancelClickListener(mCancelClickListener);
            fragment.setmTitleClickListener(mTitleClickListener);
            return fragment;
        }
    }

}
