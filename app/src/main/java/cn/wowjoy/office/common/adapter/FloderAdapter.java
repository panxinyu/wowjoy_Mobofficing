package cn.wowjoy.office.common.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.ArrayList;

import cn.wowjoy.office.R;

import cn.wowjoy.office.common.widget.album.selector.ImageSelectorViewModel;
import cn.wowjoy.office.data.mock.Folder;
import cn.wowjoy.office.data.mock.Image;
import cn.wowjoy.office.databinding.FloderItemViewBinding;

/**
 * Created by Sherily on 2017/11/16.
 * Description:
 */

public class FloderAdapter extends RecyclerView.Adapter<FloderAdapter.FloderItemViewHolder>{

    private Context mContext;
    private ArrayList<Folder> mFolders;
    private LayoutInflater mInflater;
    private int mSelectItem = 0;
    private Object mEventListener;
    private FloderItemViewBinding binding;


    public void setmEventListener(Object mEventListener) {
        this.mEventListener = mEventListener;
    }

    public FloderAdapter() {
    }

    public void setmFolders(ArrayList<Folder> mFolders) {
        this.mFolders = mFolders;
        mFolders.get(0).setSelected(true);
        notifyDataSetChanged();
    }

    @Override
    public FloderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (null == mInflater){
            mContext = parent.getContext();
            mInflater = LayoutInflater.from(mContext);

        }
        binding = DataBindingUtil.inflate(mInflater, R.layout.floder_item_view,parent,false);
        return new FloderItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(FloderItemViewHolder holder, int position) {

        Folder folder = mFolders.get(holder.getAdapterPosition());
        holder.bindData(mEventListener,folder);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                folder.setSelected(true);
                mFolders.get(mSelectItem).setSelected(false);
                notifyItemChanged(mSelectItem);
                notifyItemChanged(holder.getAdapterPosition());
                mSelectItem = holder.getAdapterPosition();
                if(mEventListener != null){
                    ((ImageSelectorViewModel) mEventListener).onFolderSelect(folder);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return null == mFolders ? 0 : mFolders.size();
    }

    static class FloderItemViewHolder extends RecyclerView.ViewHolder{
        private FloderItemViewBinding binding;

        public FloderItemViewHolder(FloderItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(Object presenter,Folder folder){
            if (null != presenter){
            binding.setPresenter((ImageSelectorViewModel) presenter);
        }
            ArrayList<Image> images = folder.getImages();
            binding.tvFolderName.setText(folder.getName());
            binding.ivSelect.setVisibility(folder.isSelected() ? View.VISIBLE : View.GONE);
            if (images != null && !images.isEmpty()) {
                binding.tvFolderSize.setText(images.size() + "张");
                Glide.with(binding.ivImage.getContext()).load(new File(images.get(0).getPath()))
                        .diskCacheStrategy(DiskCacheStrategy.NONE).into(binding.ivImage);
            } else {
                binding.tvFolderSize.setText("0张");
                binding.ivImage.setImageBitmap(null);
            }



        }
    }
}
