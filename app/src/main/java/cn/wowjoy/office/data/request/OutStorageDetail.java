package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */

public class OutStorageDetail {
    //____________申请出库 提交参数__________
    private String currentOutNumber;//
    private String detailCode;
    private String headCode;
    private String isConfirm;
    private String isQr;
    //______________________

    private String amount;

    private String batchNumber;

    private String effectiveDate;

    private  String goodsCode;

    private String price;

    @SerializedName("qRVOList")
    private List<GoodStockRequest> qRVOList;

    @SerializedName("goods")
    private Good good ;

    public List<GoodStockRequest> getqRVOList() {
        return qRVOList;
    }

    public void setqRVOList(List<GoodStockRequest> qRVOList) {
        this.qRVOList = qRVOList;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }
}
