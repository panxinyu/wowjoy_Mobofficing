package cn.wowjoy.office.data.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.data.request.GoodStockRequest;

/**
 * Created by Administrator on 2018/4/18.
 */

public class ApplyDemandHeadDetailItem implements Serializable {
    //-----扫码的属性
    private String inStorageDetailBatchNumber;

//    private String inStorageDetailCode;

    private String inStorageDetailEffectiveDate;

    private String inStorageDetailPrice;

    private String qrCode;

    private String isConfirm;

//-----------------------------------------------------------------------------

    private String amount;

   private String effectiveDate; //有效期

    private String batchNumber;

    private String headCode;

    private String detailCode;

    private String price;

    private String goodsCode;

    private String demandNumber;//需求数量

    private String sumMoney;

    private String stockNumber; //总库存数

    private String specifications;

    private String unitView;

    private String goodsName;

    private String canOutNumber; //未出库数

    @SerializedName("isQR")
    private String isQr;
    private String currentOutNumber ;
    private float needNumber = 0; //实际数量

    private boolean isFull = false;
    private boolean isScan = false;  //是否是扫码

    private boolean isHand = false;  //是否是手动

    private boolean isSelect = false; //是否选择

    private int editIemPosition;

    private int notifyPosition;

    private int deletePosition;

    @SerializedName("qRVOList")
    private List<GoodStockRequest> mGoodStockRequests = new ArrayList<>();

    private DecimalFormat fnum  =   new  DecimalFormat("##0.00");

    public String getBatchNumber() {
        if(null == batchNumber){
            return "暂无";
        }
        return batchNumber;
    }

    public String getAmount() {
        if(null == amount){
            return "暂无";
        }
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEffectiveDate() {
        if(null  == effectiveDate){
            return "暂无";
        }
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getIsConfirm() {
        return isConfirm;
    }

    public void setIsConfirm(String isConfirm) {
        this.isConfirm = isConfirm;
    }

    public List<GoodStockRequest> getqRVOList() {
        return mGoodStockRequests;
    }

    public void setqRVOList(List<GoodStockRequest> qRVOList) {
        this.mGoodStockRequests = qRVOList;
    }
    public void addqRVOList(GoodStockRequest model) {
        if(null == mGoodStockRequests){
            mGoodStockRequests   = new ArrayList<>();
        }
        mGoodStockRequests.add(model);
    }
    public String getInStorageDetailBatchNumber() {
        return inStorageDetailBatchNumber;
    }

    public void setInStorageDetailBatchNumber(String inStorageDetailBatchNumber) {
        this.inStorageDetailBatchNumber = inStorageDetailBatchNumber;
    }

    public String getInStorageDetailEffectiveDate() {
        return inStorageDetailEffectiveDate;
    }

    public void setInStorageDetailEffectiveDate(String inStorageDetailEffectiveDate) {
        this.inStorageDetailEffectiveDate = inStorageDetailEffectiveDate;
    }

    public String getInStorageDetailPrice() {
        return inStorageDetailPrice;
    }

    public void setInStorageDetailPrice(String inStorageDetailPrice) {
        this.inStorageDetailPrice = inStorageDetailPrice;
    }

    public String getQrCode() {
        if(null  == qrCode){
            return "暂无";
        }
        return "二维码编号: "+qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public boolean isFull() {
        return isFull;
    }
    public boolean isHand() {
        return "n".equalsIgnoreCase(isQr);
    }

    public void setHand(boolean hand) {
        isHand = hand;
    }

    public int getNotifyPosition() {
        return notifyPosition;
    }

    public void setNotifyPosition(int notifyPosition) {
        this.notifyPosition = notifyPosition;
    }

    public int getDeletePosition() {
        return deletePosition;
    }

    public void setDeletePosition(int deletePosition) {
        this.deletePosition = deletePosition;
    }

    public boolean isFull(ApplyDemandHeadDetailItem add) {
        float demandNumberF = Float.parseFloat(demandNumber);
        //    float dashu = Float.parseFloat(amount.substring(0,amount.indexOf(".")));
        //   float xiaoshu = Float.parseFloat(amount.substring(amount.indexOf(".")+1,amount.length()));
        Log.e("PXY", "demandNumberF: "+demandNumberF+ "   factOut"+needNumber);

        return demandNumberF>(needNumber+add.getNeedNumber());
    }
    public boolean isFullT(ApplyDemandHeadDetailItem add) {
        float canOutNumberF = Float.parseFloat(canOutNumber);
        Log.e("PXY", "canOutNumberF: "+canOutNumberF+ "   factOut"+needNumber);

        return canOutNumberF>(needNumber+add.getNeedNumber());
    }
    public void setFull(boolean full) {
        isFull = full;
    }

    public boolean isScan() {
        return "y".equalsIgnoreCase(isQr);
    }

    public void setScan(boolean scan) {
        isScan = scan;
    }

    public int getEditIemPosition() {
        return editIemPosition;
    }

    public void setEditIemPosition(int editIemPosition) {
        this.editIemPosition = editIemPosition;
    }

    public float getNeedNumber() {
        return needNumber;
    }
    public String getNeedNumberC() {
        String needNumberS = fnum.format(needNumber);
        return "出库数量: "+needNumberS;
    }
    public void setNeedNumber(float needNumber) {
        this.needNumber = needNumber;
        this.currentOutNumber = ""+this.needNumber;
    }

    public String getIsQr() {
        return isQr;
    }

    public void setIsQr(String isQr) {
        this.isQr = isQr;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getDetailCode() {
        if(null == detailCode){
            return "暂无";
        }
        return "生产编号: "+detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getPrice() {
        if(null == price){
            return "暂无";
        }
        return "￥ "+price;
    }
    public String getPriceC() {
        if(null == price){
            return "暂无";
        }
        return "单价: ￥"+price;
    }
    public String getPriceUnit() {
        if(null == price){
            return "暂无";
        }
        return "物品单价: ￥" + price + " / " + unitView;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGoodsCode() {
        if(null == goodsCode){
            return "暂无";
        }
        return goodsCode;
    }

    public String getGoodsCodeC() {
        if(null == goodsCode){
            return "物资编码: 暂无";
        }
        return "物资编码: " + goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getDemandNumber() {
        return demandNumber;
    }

    public String getDemandNumberC() {

        return "需求数量: " + twoFloat(demandNumber);
    }

    public void setDemandNumber(String demandNumber) {
        this.demandNumber = demandNumber;
    }

    public String getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(String sumMoney) {
        this.sumMoney = sumMoney;
    }

    public String getStockNumber() {
        return stockNumber;
    }
    public String getStockNumberC() {
     //   String stockNumberS = fnum.format(stockNumber);
        return "库存数量: "+twoFloat(stockNumber);
    }
    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getSpecifications() {
        if(null == specifications){
            return "暂无";
        }
        return specifications;
    }
    public String getSpecificationsC() {
        if(null == specifications){
            return "规格型号: 暂无";
        }
        return "规格型号: "+specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getUnitView() {
        if(null == unitView){
            return "暂无";
        }
        return unitView;
    }

    public void setUnitView(String unitView) {
        this.unitView = unitView;
    }

    public String getGoodsName() {
        if(null == goodsName){
            return "暂无";
        }
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCanOutNumber() {
        if(null == canOutNumber){
            return "暂无";
        }
        return canOutNumber;
    }
    public String getCanOutNumberC() {

 //       String canOutNumberF = fnum.format(canOutNumber);
        return "未出库数量: "+twoFloat(canOutNumber);
    }
    public String getAlreadyNumberC() {
        float canOutNumberF = Float.parseFloat(canOutNumber);
        float demandNumberF = Float.parseFloat(demandNumber);
        if(demandNumberF >= canOutNumberF){
            float str = (demandNumberF-canOutNumberF);
            String strF = fnum.format(str);
                return "已出库数量: "+strF;

        }
        return "已出库数量: "+demandNumber;
    }
    public void setCanOutNumber(String canOutNumber) {
        this.canOutNumber = canOutNumber;
    }
   public String twoFloat(String string){
       float v = Float.parseFloat(string);
       return fnum.format(v);
   }

    @Override
    public String toString() {
        return "ApplyDemandHeadDetailItem{" +
                "goodsName='" + goodsName + '\'' +
                ", isSelect=" + isSelect +
                '}';
    }
}

