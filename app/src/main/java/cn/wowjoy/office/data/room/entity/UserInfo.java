package cn.wowjoy.office.data.room.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/12/20.
 * Description:
 */
@Entity(tableName = "users")
public class UserInfo {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    private long id;


    @SerializedName("phone")
    private String phone;

    @SerializedName("password")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

