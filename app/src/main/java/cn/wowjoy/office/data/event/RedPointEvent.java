package cn.wowjoy.office.data.event;

public class RedPointEvent {
    private int count;
    private String status;

    public RedPointEvent(String status) {
        this.status = status;
    }

    public RedPointEvent(int count, String status) {
        this.count = count;
        this.status = status;
    }

    public RedPointEvent(int count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "RedPointEvent{" +
                "count=" + count +
                ", status='" + status + '\'' +
                '}';
    }
}
