package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/1/24.
 */

public class InventoryTaskListDtlResponse {
    @SerializedName("inventoryTaskDtlVO")
    private InventoryTaskDtlVO mDtlVO;

    public InventoryTaskDtlVO getDtlVO() {
        return mDtlVO;
    }

    public void setDtlVO(InventoryTaskDtlVO dtlVO) {
        mDtlVO = dtlVO;
    }
}
