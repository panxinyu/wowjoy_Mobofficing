package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2017/11/15.
 */
//已完成巡检任务列表
public class InspectionTotalResponse {
    /**
     *  页面实际总条数
     */
    @SerializedName("totalCount")
       private int totalCount;
    /**
     * 总页面
     */
    @SerializedName("totalPage")
       private int totalPage;
    /**
     * 当前页号
     */
    @SerializedName("startIndex")
       private int startIndex;
    /**
     * 页面大小
     */
    @SerializedName("pageSize")
       private int pageSize;
    /**
     * 单条明细集合
     */
    @SerializedName("list")
    private List<InspectionTotalItemInfo> list;


    public InspectionTotalResponse() {
    }

    public InspectionTotalResponse(int totalCount, int totalPage, int startIndex, int pageSize, List<InspectionTotalItemInfo> resultList) {
        this.totalCount = totalCount;
        this.totalPage = totalPage;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
        this.list = resultList;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<InspectionTotalItemInfo> getResultList() {
        return list;
    }

    public void setResultList(List<InspectionTotalItemInfo> resultList) {
        this.list = resultList;
    }
}
