package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/11/17.
 * Description:
 */

public class GoodsDetailRequest {

    @SerializedName("goodsCodes")
    private String goodsCode;

    public GoodsDetailRequest(String goodsCode) {
        this.goodsCode = goodsCode;
    }


}
