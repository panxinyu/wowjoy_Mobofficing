package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by Administrator on 2018/1/17.
 */

public class CheckCountResponse {
     @SerializedName("panyin")
    private Map<String,String> panyin;

    @SerializedName("pankui")
    private Map<String,String> pankui;

    @SerializedName("normal")
    private Map<String,String> normal;

    public Map<String, String> getPanyin() {
        return panyin;
    }

    public void setPanyin(Map<String, String> panyin) {
        this.panyin = panyin;
    }

    public Map<String, String> getPankui() {
        return pankui;
    }

    public void setPankui(Map<String, String> pankui) {
        this.pankui = pankui;
    }

    public Map<String, String> getNormal() {
        return normal;
    }

    public void setNormal(Map<String, String> normal) {
        this.normal = normal;
    }
}
