package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2018/1/25.
 * Description:
 */

public class GetCountReportBySheetRequest {
    @SerializedName("headCodeSearch")
    private String headCodeSearch;

    public GetCountReportBySheetRequest(String headCodeSearch) {
        this.headCodeSearch = headCodeSearch;
    }
}
