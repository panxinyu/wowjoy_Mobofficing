package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/11/17.
 * Description:
 */

public class TestResponse {
    @SerializedName("version")
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
