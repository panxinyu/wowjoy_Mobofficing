package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2018/1/12.
 * Description:
 */

public class InventoryRecordResponse {
    @SerializedName("gmtCreated")
    private String lockTime;
    @SerializedName("storageName")
    private String lockCategory;
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("use")
    private String use;
    @SerializedName("storageCode")
    private String storageCode;
    @SerializedName("useView")
    private String useView;


    public InventoryRecordResponse(String lockTime, String lockCategory) {
        this.lockTime = lockTime;
        this.lockCategory = lockCategory;
    }

    public String formatLockTime() {
        if (!TextUtils.isEmpty(lockTime))
            return "锁定时间：" + lockTime;
        return "锁定时间：暂无";
    }

    public String formatCategory() {
        if (!TextUtils.isEmpty(lockCategory))
            return "锁定时间：" + lockCategory;
        return "锁定类目：暂无";
    }

    public String getLockTime() {
        return lockTime;
    }

    public void setLockTime(String lockTime) {
        this.lockTime = lockTime;
    }

    public String getLockCategory() {
        return lockCategory;
    }

    public void setLockCategory(String lockCategory) {
        this.lockCategory = lockCategory;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getUseView() {
        return useView;
    }

    public void setUseView(String useView) {
        this.useView = useView;
    }
}

