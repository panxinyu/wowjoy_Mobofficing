package cn.wowjoy.office.data.remote;

/**
 * Created by Sherily on 2017/10/9.
 * RxJava2 不允许emit null对象，所以封装一个空对象
 */

public class ResultEmpty {
    public static final ResultEmpty EMPTY = new ResultEmpty();

    private ResultEmpty() {
    }
}