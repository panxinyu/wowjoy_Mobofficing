package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/11/7.
 */

public class MyTeamInfo implements Serializable{
    private String part;

    @Override
    public String toString() {
        return "MyTeamInfo{" +
                "part='" + part + '\'' +
                '}';
    }

    public MyTeamInfo(String part) {
        this.part = part;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }
}
