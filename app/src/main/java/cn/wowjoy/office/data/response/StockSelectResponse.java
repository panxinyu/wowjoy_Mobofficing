package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/13.
 */

public class StockSelectResponse implements Serializable{

        private String code;

        private String financeCode;

        private String departmentStatus;

        private String departmentId;

        private String name;
        private boolean isSelected = false;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getFinanceCode() {
            return "("+financeCode+")";
        }

        public void setFinanceCode(String financeCode) {
            this.financeCode = financeCode;
        }

        public String getDepartmentStatus() {
            return departmentStatus;
        }

        public void setDepartmentStatus(String departmentStatus) {
            this.departmentStatus = departmentStatus;
        }

        public String getDepartmentId() {
            return departmentId;
        }

        public void setDepartmentId(String departmentId) {
            this.departmentId = departmentId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }



    //----------------------------------- 病人返回信息 --------------------------
    private String patientName;
    private String patientId;
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return "("+patientId+")";
    }
    public String getPatientIdC() {
        return patientId;
    }
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
}
