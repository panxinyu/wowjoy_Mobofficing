package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/11/21.
 * Description:
 */

public class SetGoodsJsonRequest {
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("barCode")
    private String barCode;

    public SetGoodsJsonRequest(String goodsCode, String barCode) {
        this.goodsCode = goodsCode;
        this.barCode = barCode;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }
}
