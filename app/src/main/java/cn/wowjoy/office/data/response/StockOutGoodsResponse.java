package cn.wowjoy.office.data.response;

/**
 * Created by Administrator on 2018/4/12.
 */

public class StockOutGoodsResponse {
    private int id;
    private String name;

    public StockOutGoodsResponse() {

    }

    public StockOutGoodsResponse(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
