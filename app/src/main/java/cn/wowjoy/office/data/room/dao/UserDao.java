package cn.wowjoy.office.data.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import cn.wowjoy.office.data.room.entity.UserInfo;

/**
 * Created by Sherily on 2017/12/20.
 * Description:
 */
@Dao
public interface UserDao {
    @Query("SELECT * FROM users")
    LiveData<List<UserInfo>> loadUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveUser(UserInfo userInfo);

//    @Query("SELECT * FROM users WHERE id=:id")
//    LiveData<UserInfo> getUser(long id );
}
