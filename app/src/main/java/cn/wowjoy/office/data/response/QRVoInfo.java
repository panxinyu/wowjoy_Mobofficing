package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QRVoInfo implements Serializable{
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("inStorageDetailBatchNumber") //生产批号
    private String inStorageDetailBatchNumber;
    @SerializedName("inStorageDetailEffectiveDate") //有效期
    private String inStorageDetailEffectiveDate;
    @SerializedName("inStorageDetailPrice")
    private String inStorageDetailPrice;
    @SerializedName("incId")
    private int incId;
    @SerializedName("qrCode")
    private String qrCode;
    @SerializedName("price")
    private String price;

    public QRVoInfo(String goodsCode, String inStorageDetailBatchNumber, String inStorageDetailEffectiveDate, String inStorageDetailPrice, String qrCode) {
        this.goodsCode = goodsCode;
        this.inStorageDetailBatchNumber = inStorageDetailBatchNumber;
        this.inStorageDetailEffectiveDate = inStorageDetailEffectiveDate;
        this.inStorageDetailPrice = inStorageDetailPrice;
        this.qrCode = qrCode;
    }
    public String getPriceC() {
        if(null == price){
            return "单价: 暂无";
        }
        return "单价: ￥"+price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getInStorageDetailBatchNumber() {
        return inStorageDetailBatchNumber;
    }

    public String getInStorageDetailBatchNumberC() {
        if(null == inStorageDetailBatchNumber){
            return "生产批号: 暂无";
        }
        return "生产批号: "+inStorageDetailBatchNumber;
    }
    public void setInStorageDetailBatchNumber(String inStorageDetailBatchNumber) {
        this.inStorageDetailBatchNumber = inStorageDetailBatchNumber;
    }

    public String getInStorageDetailEffectiveDate() {
        return inStorageDetailEffectiveDate;
    }
    public String getInStorageDetailEffectiveDateC() {
        if(null == inStorageDetailEffectiveDate){
            return "有效日期: 暂无";
        }
        return "有效日期: "+inStorageDetailEffectiveDate;
    }
    public void setInStorageDetailEffectiveDate(String inStorageDetailEffectiveDate) {
        this.inStorageDetailEffectiveDate = inStorageDetailEffectiveDate;
    }

    public String getInStorageDetailPrice() {
        return inStorageDetailPrice;
    }

    public void setInStorageDetailPrice(String inStorageDetailPrice) {
        this.inStorageDetailPrice = inStorageDetailPrice;
    }

    public int getIncId() {
        return incId;
    }

    public void setIncId(int incId) {
        this.incId = incId;
    }

    public String getQrCode() {
        return qrCode;
    }
    public String getQrCodeC() {
        if(null == qrCode){
            return "二维码编号: 暂无";
        }
           return "二维码编号: "+qrCode;
    }
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
