package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2017/11/22.
 */

public class SubmitPDARequest {
    /**
     * 巡检任务ID
     */
    @SerializedName("id")
    private String id ;
    /**
     * 巡检记录ID
     */
    @SerializedName("inspectRecordId")
    private String inspectRecordId;
    /**
     *
     */
    @SerializedName("useDepartmentId")
    private String useDepartmentId;


    private String  contentNote;  //巡检内容/问题
    private String  improvement; //改进措施
    private String  summary;     //巡检总结

    public String getContentNote() {
        return contentNote;
    }

    public void setContentNote(String contentNote) {
        this.contentNote = contentNote;
    }

    public String getImprovement() {
        return improvement;
    }

    public void setImprovement(String improvement) {
        this.improvement = improvement;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public SubmitPDARequest(String id, String inspectRecordId, String useDepartmentId) {
        this.id = id;
        this.inspectRecordId = inspectRecordId;
        this.useDepartmentId = useDepartmentId;
    }

    public SubmitPDARequest(String id, String inspectRecordId, String useDepartmentId, String contentNote, String improvement, String summary) {
        this.id = id;
        this.inspectRecordId = inspectRecordId;
        this.useDepartmentId = useDepartmentId;
        this.contentNote = contentNote;
        this.improvement = improvement;
        this.summary = summary;
    }

    public String getUseDepartmentId() {
        return useDepartmentId;
    }

    public void setUseDepartmentId(String useDepartmentId) {
        this.useDepartmentId = useDepartmentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInspectRecordId() {
        return inspectRecordId;
    }

    public void setInspectRecordId(String inspectRecordId) {
        this.inspectRecordId = inspectRecordId;
    }
}
