package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2017/11/15.
 */

public class InspectionTotalRequest {
    @SerializedName("isDone")
    private String isDone;
    @SerializedName("billInfo")
    private String billInfo;
    @SerializedName("inspectUserId")
    private String inspectUserId;
    @SerializedName("startIndex")
    private int startIndex;
    @SerializedName("pageSize")
    private int pageSize;

    public InspectionTotalRequest() {
    }

    public String getIsDone() {
        return isDone;
    }

    public void setIsDone(String isDone) {
        this.isDone = isDone;
    }

    public String getBillInfo() {
        return billInfo;
    }

    public void setBillInfo(String billInfo) {
        this.billInfo = billInfo;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
