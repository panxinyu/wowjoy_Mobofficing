package cn.wowjoy.office.data.event;

public class RedPointEventReport {
    private int count;
    private String status;

    public RedPointEventReport(String status) {
        this.status = status;
    }

    public RedPointEventReport(int count, String status) {
        this.count = count;
        this.status = status;
    }

    public RedPointEventReport(int count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "RedPointEvent{" +
                "count=" + count +
                ", status='" + status + '\'' +
                '}';
    }
}
