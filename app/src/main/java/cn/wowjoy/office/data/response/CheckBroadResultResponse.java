package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/1/26.
 */

public class CheckBroadResultResponse implements Serializable{
    private String cardName;

    private String cardNo;

    private String info;

    private String inSystem ="";

    public String getInSystem() {
        return inSystem;
    }

    public void setInSystem(String inSystem) {
        this.inSystem = inSystem;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
