package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2018/1/9.
 * Description:
 */

public class HistoryLogResponse {
    @SerializedName("condition")
    private String condition;
    @SerializedName("logId")
    private String logId;
    @SerializedName("application")
    private String application;
    @SerializedName("type")
    private String type;
    @SerializedName("creator")
    private String creator;
    @SerializedName("createTime")
    private String createTime;
    @SerializedName("creatorName")
    private String creatorName;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }
}
