package cn.wowjoy.office.data.response;

/**
 * Created by Administrator on 2018/4/17.
 */

public class StockApplyDemandHeadListSingle {
        private String headCode;

        private String actualDemandUser;

        private String patientName;

        private String gmtCreated;

        private String storageView;

        private String demandFromView;

        private String commitDeptView;

    public String getCommitDeptView() {
        if(null == commitDeptView){
            return "暂无";
        }
        return commitDeptView;
    }

    public void setCommitDeptView(String commitDeptView) {
        this.commitDeptView = commitDeptView;
    }

    public String getDemandFromView() {
        return demandFromView;
    }

    public void setDemandFromView(String demandFromView) {
        this.demandFromView = demandFromView;
    }

    public String getHeadCode() {
        if(null == headCode){
            return "暂无";
        }
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getActualDemandUser() {
        if(null == actualDemandUser){
            return "暂无";
        }
        return actualDemandUser;
    }

    public void setActualDemandUser(String actualDemandUser) {
        this.actualDemandUser = actualDemandUser;
    }

    public String getPatientName() {
        if(null == patientName){
            return "暂无";
        }
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(String gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getStorageView() {
        return storageView;
    }

    public void setStorageView(String storageView) {
        this.storageView = storageView;
    }
}
