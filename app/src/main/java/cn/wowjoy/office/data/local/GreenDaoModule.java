package cn.wowjoy.office.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import javax.inject.Named;
import javax.inject.Singleton;

import cn.wowjoy.office.BuildConfig;
import cn.wowjoy.office.data.local.entity.DaoMaster;
import cn.wowjoy.office.data.local.entity.DaoSession;
import cn.wowjoy.office.data.local.entity.UserInfoDao;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Sherily on 2017/9/30.
 */
@Module
public class GreenDaoModule {
    private final Context mContext;

    public GreenDaoModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    SQLiteDatabase provideSQLiteDatabase() {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, BuildConfig.GREEN_DAO_DB_NAME, null);
        return devOpenHelper.getWritableDatabase();
    }

    @Singleton
    @Provides
    DaoMaster provideDaoMaster(SQLiteDatabase sqLiteDatabase) {
        return new DaoMaster(sqLiteDatabase);
    }

    @Singleton
    @Provides
    DaoSession provideDaoSession(DaoMaster daoMaster) {
        return daoMaster.newSession();
    }

    @Singleton
    @Provides
    NoSqlHelper provideNoSqlHelper(DaoSession daoSession, @Named("GsonSerializer") EntitySerializer entitySerializer) {
        return new NoSqlHelper(daoSession, entitySerializer);
    }

    @Provides
    UserInfoDao provideUserInfoDao(DaoSession daoSession) {
        return daoSession.getUserInfoDao();
    }
}
