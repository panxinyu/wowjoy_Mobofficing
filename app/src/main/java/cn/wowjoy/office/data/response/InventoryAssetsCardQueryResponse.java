package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/1/22.
 */

public class InventoryAssetsCardQueryResponse {
    @SerializedName("count")
    private String totalCount ;
    @SerializedName("list")
    private List<AssetsCardQueryCondition> queryList;

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public List<AssetsCardQueryCondition> getDetailList() {
        return queryList;
    }

    public void setDetailList(List<AssetsCardQueryCondition> detailList) {
        this.queryList = detailList;
    }
}
