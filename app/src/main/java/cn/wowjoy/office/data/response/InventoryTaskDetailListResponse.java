package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/1/16.
 */

public class InventoryTaskDetailListResponse {
    @SerializedName("count")
    private String totalCount ;
    @SerializedName("list")
    private List<InventoryTaskDtlVO>  detailList;

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public List<InventoryTaskDtlVO> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<InventoryTaskDtlVO> detailList) {
        this.detailList = detailList;
    }
}
