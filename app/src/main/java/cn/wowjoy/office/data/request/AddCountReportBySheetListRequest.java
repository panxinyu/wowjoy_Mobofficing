package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import cn.wowjoy.office.data.response.RecordResponse;

/**
 * Created by Sherily on 2018/1/25.
 * Description:
 */

public class AddCountReportBySheetListRequest {
    @SerializedName("record")
    private List<RecordResponse> requests;

    public List<RecordResponse> getRequests() {
        return requests;
    }

    public void setRequests(List<RecordResponse> requests) {
        this.requests = requests;
    }
}
