package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import cn.wowjoy.office.utils.TextUtil;

/**
 * Created by Administrator on 2017/11/14.
 */

public class StaffResponse implements Serializable{
    @SerializedName("staffId")
    private String staffId;

    @SerializedName("auid")
    private String auid;

    @SerializedName("staffName")
    private String staffName;

    @SerializedName("staffNumber")
    private String staffNumber;

    @SerializedName("staffSexName")
    private String staffSexName;

    @SerializedName("staffSex")
    private String staffSex;

    @SerializedName("staffStatus")
    private String staffStatus;

    @SerializedName("staffPhone")
    private String staffPhone;

    @SerializedName("userEmail")
    private String userEmail;

    /**
     * 1009-01:身份证；1009-02：护照；1009-03：军人证
     */
    @SerializedName("identityType")
    private String identityType;

    @SerializedName("identityTypeName")
    private String identityTypeName;

    @SerializedName("identityNumber")
    private String identityNumber;

    @SerializedName("companyId")
    private String companyId;

    @SerializedName("companyFullName")
    private String companyFullName;

    @SerializedName("departmentId")
    private String departmentId;

    @SerializedName("departmentName")
    private String departmentName;

    @SerializedName("positionId")
    private String positionId;

    @SerializedName("positionName")
    private String positionName;

    public String formatStaffNum(){
        if (!TextUtils.isEmpty(staffNumber)){
            return "（"+staffNumber+"）";
        }
        return "（无）";
    }
    public String getAuid() {
        return auid;
    }

    public void setAuid(String auid) {
        this.auid = auid;
    }

    public String cropName(){
        if (!TextUtils.isEmpty(staffName)){
            if (staffName.length() > 2){
                return staffName.substring(staffName.length() - 2);
            } else {
                return staffName.substring(staffName.length() - 1);
            }

        }
        return "";
    }

    public String formBrithday(){
        if ("1009-01".equals(identityType)) {
            if (!TextUtils.isEmpty(identityNumber)) {
                int length = identityNumber.length();
                String str = identityNumber.substring(length - 8, length - 4);
                String month = str.substring(0,2);
                String date = str.substring(2);
                return month+"月"+date+"日";

            }
            return "无";
        }
        return "无";
    }
    public String formatStaffPhone(){
        if (TextUtils.isEmpty(staffPhone)){
            return "无";
        }
        return staffPhone;
    }
    public String formatEmail(){
        if (TextUtils.isEmpty(userEmail)){
            return "无";
        }
        return userEmail;
    }

    public String formatPositionName(){
        if (TextUtils.isEmpty(positionName)){
            return "无";
        }
        return positionName;
    }

    public String formatCompanyFullName(){
        if (TextUtils.isEmpty(companyFullName)){
            return "无";
        }
        return companyFullName;
    }

    public String formatStaffSex(){
        if (TextUtils.isEmpty(staffSex)){
            return "无";
        }
        return staffSex;
    }

    public String formatStaffName(){
        if (TextUtils.isEmpty(staffName)){
            return "无";
        }
        return staffName;
    }
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(String staffNumber) {
        this.staffNumber = staffNumber;
    }

    public String getStaffSexName() {
        return staffSexName;
    }

    public void setStaffSexName(String staffSexName) {
        this.staffSexName = staffSexName;
    }

    public String getStaffSex() {
        return staffSex;
    }

    public void setStaffSex(String staffSex) {
        this.staffSex = staffSex;
    }

    public String getStaffStatus() {
        return staffStatus;
    }

    public void setStaffStatus(String staffStatus) {
        this.staffStatus = staffStatus;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityTypeName() {
        return identityTypeName;
    }

    public void setIdentityTypeName(String identityTypeName) {
        this.identityTypeName = identityTypeName;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyFullName() {
        return companyFullName;
    }

    public void setCompanyFullName(String companyFullName) {
        this.companyFullName = companyFullName;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }
}
