package cn.wowjoy.office.data.request;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/13.
 */

public class StockForOtherOutRequest implements Serializable{
    private String goodsInfoSearch;

    private  String isQR;

    private  String storageCodeSearch;

    private String info;

    public StockForOtherOutRequest(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    //    private int startIndex;
//
//    private int pageSize;

    public String getStorageCodeSearch() {
        return storageCodeSearch;
    }

    public void setStorageCodeSearch(String storageCodeSearch) {
        this.storageCodeSearch = storageCodeSearch;
    }

    public String getIsQR() {
        return isQR;
    }

    public void setIsQR(String isQR) {
        this.isQR = isQR;
    }

    public String getGoodsInfoSearch() {
        return goodsInfoSearch;
    }

    public void setGoodsInfoSearch(String goodsInfoSearch) {
        this.goodsInfoSearch = goodsInfoSearch;
    }

//    public int getStartIndex() {
//        return startIndex;
//    }
//
//    public void setStartIndex(int startIndex) {
//        this.startIndex = startIndex;
//    }
//
//    public int getPageSize() {
//        return pageSize;
//    }
//
//    public void setPageSize(int pageSize) {
//        this.pageSize = pageSize;
//    }

    public StockForOtherOutRequest() {
    }

    public StockForOtherOutRequest(String goodsInfoSearch, String isQR, String storageCodeSearch) {
        this.goodsInfoSearch = goodsInfoSearch;
        this.isQR = isQR;
        this.storageCodeSearch = storageCodeSearch;
    }

    public StockForOtherOutRequest(String goodsInfoSearch, int startIndex, int pageSize) {
        this.goodsInfoSearch = goodsInfoSearch;
//        this.startIndex = startIndex;
//        this.pageSize = pageSize;
    }
}
