package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/1/30.
 */

public class CheckCategoryListResponse {
    private String count;
    @SerializedName("list")
    private List<CategoryName> mCategoryNameList;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<CategoryName> getCategoryNameList() {
        return mCategoryNameList;
    }

    public void setCategoryNameList(List<CategoryName> categoryNameList) {
        mCategoryNameList = categoryNameList;
    }

    public class CategoryName {
        private String id;

        private String categoryName;

        private String assetsStylr;

        private String use;

        private boolean isSelected = false;


        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getAssetsStylr() {
            return assetsStylr;
        }

        public void setAssetsStylr(String assetsStylr) {
            this.assetsStylr = assetsStylr;
        }

        public String getUse() {
            return use;
        }

        public void setUse(String use) {
            this.use = use;
        }
    }
}
