package cn.wowjoy.office.data.response;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.data.request.GoodStockRequest;

/**
 * Created by Administrator on 2018/4/13.
 */

public class StockForOtherOutResponse implements Serializable {

    //--------扫码的属性
    private String goodsName; //物资名字

    private String specifications ; //规格型号

    private String unitView;  // 计量单位

    private String qrCode;

    private String isQR;

    public String getIsQR() {
        return isQR;
    }

    public void setIsQR(String isQR) {
        this.isQR = isQR;
    }

    //-------------共有的属性
    private String goodsCode; //物资编码
    private String amount;//实际库存（可出库数量）
    private String batchNumber; //  生产编号
    private String  effectiveDate; //有效期
    private String price;  //单价

    private float factOut = 1; //扫码显示的出库数量


    private boolean isFull = false;
    private boolean isScan = false;  //是否是扫码

    private boolean isHand = false;  //是否是手动

    private int editIemPosition;

    private int notifyPosition;

    private int deletePosition;

    private String creator;

    private String flowNumber;

    private String gmtCreater;

    private String gmtModified;

    private String stockId;

    private String storageCode;

    private String storageName;

    private String sumMoney;

   private String detailCode;


    public String getDetailCode() {
        return detailCode;
    }
//    public String getDetailCodeC() {
//        if(null == detailCode){
//            return "生产批号: 暂无";
//        }
//        return "生产批号: "+detailCode;
//    }
    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    @Override
    public String toString() {
        return "StockForOtherOutResponse{" +
                "qrCode='" + qrCode + '\'' +
                ", goodsCode='" + goodsCode + '\'' +
                ", batchNumber='" + batchNumber + '\'' +
                ", effectiveDate='" + effectiveDate + '\'' +
                ", factOut=" + factOut +
                ", mGoodStockRequests=" + mGoodStockRequests +
                '}';
    }

    @SerializedName("goods")
    private OutGoods mOutGoods;
    private List<GoodStockRequest> mGoodStockRequests = new ArrayList<>();
    public List<GoodStockRequest> getqRVOList() {
        return mGoodStockRequests;
    }

    public void setqRVOList(List<GoodStockRequest> qRVOList) {
        this.mGoodStockRequests = qRVOList;
    }
    public void addqRVOList(GoodStockRequest model) {
        if(null == mGoodStockRequests){
            mGoodStockRequests   = new ArrayList<>();
        }
        mGoodStockRequests.add(model);
    }
    public int getEditIemPosition() {
        return editIemPosition;
    }

    public void setEditIemPosition(int editIemPosition) {
        this.editIemPosition = editIemPosition;
    }

    public int getDeletePosition() {
        return deletePosition;
    }

    public void setDeletePosition(int deletePosition) {
        this.deletePosition = deletePosition;
    }

    public int getNotifyPosition() {
        return notifyPosition;
    }

    public void setNotifyPosition(int notifyPosition) {
        notifyPosition = notifyPosition;
    }



    public boolean isFull(StockForOtherOutResponse add) {
        float amountF = Float.parseFloat(amount);
    //    float dashu = Float.parseFloat(amount.substring(0,amount.indexOf(".")));
     //   float xiaoshu = Float.parseFloat(amount.substring(amount.indexOf(".")+1,amount.length()));
        Log.e("PXY", "amount: "+amountF+ "   factOut"+factOut);

        return amountF>=(factOut+add.getFactOut());


    }

    public void setFull(boolean full) {
        isFull = full;
    }

    public float getFactOut() {
        return factOut;
    }
    public String getUIFactOut() {
        DecimalFormat fnum  =   new  DecimalFormat("##0.00");
//        String str = ""+factOut;
//        int dashu = Integer.parseInt(str.substring(0,str.indexOf(".")));
//        int xiaoshu = Integer.parseInt(str.substring(str.indexOf(".")+1,str.length()));
//        if(isScan()){
//            return "出库数量:"+dashu;
//        }
//        if( 0 == xiaoshu){
//            return "出库数量:"+dashu;
//        }

        return "出库数量:"+fnum.format(factOut);

    }
    public void setFactOut(float factOut) {
        this.factOut = factOut;
    }

    public String getGoodsName() {
        if(null == goodsName){
            return "暂无";
        }
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSpecifications() {
        if(null == specifications){
            return "暂无";
        }
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getUnitView() {
        if(null == unitView){
            return "暂无";
        }
        return unitView;
    }

    public void setUnitView(String unitView) {
        this.unitView = unitView;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }


    public boolean isScan() {

        return  !TextUtils.isEmpty(qrCode);
    }

    public void setScan(boolean scan) {
        isScan = scan;
    }

    public boolean isHand() {
        return TextUtils.isEmpty(qrCode);
    }

    public void setHand(boolean hand) {
        isHand = hand;
    }

    public String getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(String sumMoney) {
        this.sumMoney = sumMoney;
    }

    public OutGoods getOutGoods() {
        return mOutGoods;
    }

    public void setOutGoods(OutGoods outGoods) {
        mOutGoods = outGoods;
    }

    public String getAmount() {
        DecimalFormat fnum  =   new  DecimalFormat("##0.00");
        return "实际库存:"+fnum.format(Float.parseFloat(amount));
    }
    public String getAmountDetail() {
        return amount;
    }
    public String getAmountDetailFloat() {
        DecimalFormat fnum  =   new  DecimalFormat("##0.00");
        return fnum.format(Float.parseFloat(amount));
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBatchNumber() {
        if(null == batchNumber){
            return "暂无";
        }
        return batchNumber;
    }
    public String getBatchNumberC() {
        if(null == batchNumber){
            return "生产编号: 暂无";
        }
        return  "生产编号: "+batchNumber;
    }
    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEffectiveDate() {
        if(null == effectiveDate){
            return "暂无";
        }
        return effectiveDate;
    }
    public String getEffectiveDateC() {
        if(null == effectiveDate){
            return "有效日期: 暂无";
        }
        return "有效日期: "+effectiveDate;
    }
    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getFlowNumber() {
        return flowNumber;
    }

    public void setFlowNumber(String flowNumber) {
        this.flowNumber = flowNumber;
    }

    public String getGmtCreater() {
        return gmtCreater;
    }

    public void setGmtCreater(String gmtCreater) {
        this.gmtCreater = gmtCreater;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getGoodsCode() {
        if(null == goodsCode){
            return "暂无";
        }
        return goodsCode;
    }
    public String getGoodsCodeC() {
        if(null == goodsCode){
            return "物资编码: 暂无";
        }
        return "物资编码: " + goodsCode;
    }
    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getPrice() {
        return price;
    }
    public String getPriceDetail() {
        return price;
    }
    public String getPriceQ() {
        if(null == price){
            return "暂无";
        }
        return "￥"+price;
    }


    public void setPrice(String price) {
        this.price = price;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }



    public class OutGoods implements Serializable{
        private String goodsCode;

        private String manufacturers;

        private String name;  //物资名称

        private String specifications; //规格型号

        private String typeCodeView;

        private String unit;

        private String price; //单价

        private String unitView;//计量单位

        private String usageCode;

        private String usageCodeView;

        public String getPrice() {
            return "单价:"+price;
        }
        public String getPriceDetail() {
            return price;
        }
        public String getPriceQ() {
            if(null == price){
                return "暂无";
            }
            return "￥"+price;
        }
        public void setPrice(String price) {
            this.price = price;
        }

        public String getSpecifications() {
            return "规格型号:"+specifications;
        }
        public String getSpecificationsDetail() {
            if(null == specifications){
                return "暂无";
            }
            return specifications;
        }
        public void setSpecifications(String specifications) {
            this.specifications = specifications;
        }

        public String getGoodsCode() {
            if(null == goodsCode){
                return "暂无";
            }
            return goodsCode;
        }

        public void setGoodsCode(String goodsCode) {
            this.goodsCode = goodsCode;
        }

        public String getManufacturers() {
            return manufacturers;
        }

        public void setManufacturers(String manufacturers) {
            this.manufacturers = manufacturers;
        }

        public String getName() {
            if(null == name){
                return "暂无";
            }
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTypeCodeView() {
            return typeCodeView;
        }

        public void setTypeCodeView(String typeCodeView) {
            this.typeCodeView = typeCodeView;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getUnitView() {
            return "计量单位:"+unitView;
        }
        public String getUnitViewDetail() {
            if(null == unitView){
                return "暂无";
            }
            return unitView;
        }

        public void setUnitView(String unitView) {
            this.unitView = unitView;
        }

        public String getUsageCode() {
            return usageCode;
        }

        public void setUsageCode(String usageCode) {
            this.usageCode = usageCode;
        }

        public String getUsageCodeView() {
            return usageCodeView;
        }

        public void setUsageCodeView(String usageCodeView) {
            this.usageCodeView = usageCodeView;
        }
    }

}
