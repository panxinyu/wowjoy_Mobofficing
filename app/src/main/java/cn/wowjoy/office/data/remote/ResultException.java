package cn.wowjoy.office.data.remote;

/**
 * Created by Sherily on 2017/10/9.
 */

public class ResultException extends Exception {
    public static final int CODE_SUCCESS = 0;

    public int errorCode;


    public ResultException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }


    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
