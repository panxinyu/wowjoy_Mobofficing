package cn.wowjoy.office.data.response;

import android.text.TextUtils;
import android.widget.TextView;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import cn.wowjoy.office.utils.TextUtil;

/**
 * Created by Sherily on 2018/1/17.
 * Description:
 */

public class RecordResponse implements Serializable{
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("detailCode")
    private String detailCode;
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("amount")
    private String stockCount;
    @SerializedName("checkedAmount")
    private String realStockCount;
    @SerializedName("price")
    private String price;
    @SerializedName("batchNumber")
    private String batch;
    @SerializedName("remark")
    private String remark;
    @SerializedName("dealAmount")
    private String dealAmount;
    @SerializedName("dealType")
    private String dealType;
    @SerializedName("isManual")
    private String isManual;
    @SerializedName("creator")
    private String creator;
    @SerializedName("gmtCreated")
    private String gmtCreated;
    @SerializedName("modifier")
    private String modifier;
    @SerializedName("gmtModifier")
    private String gmtModifier;
    @SerializedName("isDeleted")
    private String isDeleted;
    @SerializedName("storageCode")
    private String storageCode;
    @SerializedName("storageName")
    private String storageName;
    @SerializedName("use")
    private String use;
    @SerializedName("useView")
    private String useView;
    @SerializedName("typeCode")
    private String typeCode;
    @SerializedName("typeView")
    private String typeView;
    @SerializedName("unit")
    private String unit;
    @SerializedName("unitView")
    private String unitView;
    @SerializedName("specifications")
    private String specifications;
    @SerializedName("goodsName")
    private String goodsName;
    @SerializedName("effectiveDate")
    private String indate;
    @SerializedName("countSheetHeadCode")
    private String countSheetHeadCode;
    @SerializedName("delTypeView")
    private String delTypeView;
    @SerializedName("profitLossNumber")
    private String profitLossNumber;
    @SerializedName("profitLossMoney")
    private String profitLossMoney;
    @SerializedName("barCode")
    private String barCode;
    @SerializedName("hasChecked")
    private String hasChecked;

    @SerializedName("countReportDetailInfoList")
    private List<StaffRecordResponse> staffRecordResponses;

    public String getProfitLossMoney() {
        return profitLossMoney;
    }

    public void setProfitLossMoney(String profitLossMoney) {
        this.profitLossMoney = profitLossMoney;
    }

    public String getHasChecked() {
        return hasChecked;
    }

    public void setHasChecked(String hasChecked) {
        this.hasChecked = hasChecked;
    }

    private boolean canDelete;

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    private int editIemPosition;

    private int notifyPosition;

    private int deletePosition;

    public int getNotifyPosition() {
        return notifyPosition;
    }

    public void setNotifyPosition(int notifyPosition) {
        this.notifyPosition = notifyPosition;
    }

    private String formatString(String sb){
        if (!TextUtils.isEmpty(sb)){
            String s = "0";
            if (sb.length() > 0)
                s = sb.toString().trim();
            BigDecimal bd = new BigDecimal(s);
            DecimalFormat df = new DecimalFormat("#############0.00");//小数点点不够两位补0，例如："0" --> 0.00（个位数补成0因为传入的是0则会显示成：.00，所以各位也补0；）
            String xs = df.format(bd.setScale(2, BigDecimal.ROUND_DOWN));
            return xs;
        } else {
            return null;
        }

    }
    public String formatProfitLossNumber(){
        if (!TextUtils.isEmpty(profitLossNumber)){
            if (Double.parseDouble(formatString(profitLossNumber)) > 0){
                return formatString(profitLossNumber);
            }
            return formatString(profitLossNumber);
        }
        return "暂无数据";

    }

    public String formatProfitLossMoney(){
        if (!TextUtils.isEmpty(profitLossMoney)){
            if (Double.parseDouble(formatString(profitLossMoney)) > 0){
                return formatString(profitLossMoney);
            }
            return formatString(profitLossMoney);
        }
        return "暂无数据";

    }
    public String formatIndate(){
        if (!TextUtils.isEmpty(indate)){
            return indate;
        }
        return "无";
    }
    public int getDeletePosition() {
        return deletePosition;
    }

    public void setDeletePosition(int deletePosition) {
        this.deletePosition = deletePosition;
    }

    public void setEditIemPosition(int editIemPosition) {
        this.editIemPosition = editIemPosition;
    }

    public int getEditIemPosition() {
        return editIemPosition;
    }

    private boolean isOpen;

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(String dealAmount) {
        this.dealAmount = dealAmount;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getIsManual() {
        return isManual;
    }

    public void setIsManual(String isManual) {
        this.isManual = isManual;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(String gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getGmtModifier() {
        return gmtModifier;
    }

    public void setGmtModifier(String gmtModifier) {
        this.gmtModifier = gmtModifier;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    public String getUseView() {
        return useView;
    }

    public void setUseView(String useView) {
        this.useView = useView;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeView() {
        return typeView;
    }

    public void setTypeView(String typeView) {
        this.typeView = typeView;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitView() {
        return unitView;
    }

    public void setUnitView(String unitView) {
        this.unitView = unitView;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCountSheetHeadCode() {
        return countSheetHeadCode;
    }

    public void setCountSheetHeadCode(String countSheetHeadCode) {
        this.countSheetHeadCode = countSheetHeadCode;
    }

    public String getDelTypeView() {
        return delTypeView;
    }

    public void setDelTypeView(String delTypeView) {
        this.delTypeView = delTypeView;
    }

    public String getProfitLossNumber() {
        return profitLossNumber;
    }

    public void setProfitLossNumber(String profitLossNumber) {
        this.profitLossNumber = profitLossNumber;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public List<StaffRecordResponse> getStaffRecordResponses() {
        return staffRecordResponses;
    }

    public void setStaffRecordResponses(List<StaffRecordResponse> staffRecordResponses) {
        this.staffRecordResponses = staffRecordResponses;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getIndate() {
        return indate;
    }

    public void setIndate(String indate) {
        this.indate = indate;
    }

    public String getStockCount() {
        return stockCount;
    }

    public void setStockCount(String stockCount) {
        this.stockCount = stockCount;
    }

    public String getRealStockCount() {
        return realStockCount;
    }

    public void setRealStockCount(String realStockCount) {
        this.realStockCount = realStockCount;
    }

    public String formatBatch(){
        if (TextUtils.isEmpty(batch))
            return "无";
        return batch;
    }
    public String formatRealStockCount(){
        if (TextUtils.isEmpty(realStockCount))
            return "请输入";
        return realStockCount;
    }

    public String formatRealStockCountShow(){

        if (TextUtils.equals("n",hasChecked))
            return "实际库存：待录入";
        return "实际库存：" + realStockCount;
    }

    public String formatStockCountShow(){
        if (TextUtils.isEmpty(stockCount))
            return "账面库存：暂无";
        return "账面库存：" + stockCount;
    }

    public String formatProfit(){
        if (TextUtils.isEmpty(profitLossMoney)){
            return "盘盈/盘亏：暂无";
        } else {
            if (Double.parseDouble(formatString(profitLossMoney)) > 0){
                return "盘盈/盘亏：" +formatString(profitLossMoney);
            }
            return "盘盈/盘亏：" + formatString(profitLossMoney);
        }

    }
}
