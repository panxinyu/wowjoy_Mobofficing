package cn.wowjoy.office.data.remote;


import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import cn.wowjoy.office.data.mock.PopuModel;
import cn.wowjoy.office.data.request.AddInventoryRequest;
import cn.wowjoy.office.data.request.ApplyFormQueryConditions;
import cn.wowjoy.office.data.request.CheckTaskUpdateListRequest;
import cn.wowjoy.office.data.request.GetCountReportBySheetRequest;
import cn.wowjoy.office.data.request.GoodsDetailRequest;
import cn.wowjoy.office.data.request.InspectRecordControllerRequest;
import cn.wowjoy.office.data.request.StockApplyDemandHeadListRequest;
import cn.wowjoy.office.data.request.StockForOtherOutRequest;
import cn.wowjoy.office.data.request.StockListRequest;
import cn.wowjoy.office.data.request.StockOtherOutSubmitRequest;
import cn.wowjoy.office.data.request.SubmitCheckTaskListDetailRequest;
import cn.wowjoy.office.data.request.SubmitPDARequest;
import cn.wowjoy.office.data.response.AppInfo;
import cn.wowjoy.office.data.response.ApplyDemandHeadDetailItem;
import cn.wowjoy.office.data.response.ApplyHeadInfoResponse;
import cn.wowjoy.office.data.response.ApplyHeadListResponse;
import cn.wowjoy.office.data.response.CheckBroadResultResponse;
import cn.wowjoy.office.data.response.CheckCategoryListResponse;
import cn.wowjoy.office.data.response.CheckCountResponse;
import cn.wowjoy.office.data.response.CheckerResponse;
import cn.wowjoy.office.data.response.GoodsDetailResponse;
import cn.wowjoy.office.data.response.HistoryLogResponse;
import cn.wowjoy.office.data.response.InspectRecordControllerResponse;
import cn.wowjoy.office.data.response.InspectTaskSelect;
import cn.wowjoy.office.data.response.InspectionTaskControllerResponse;
import cn.wowjoy.office.data.response.InspectionTotalResponse;
import cn.wowjoy.office.data.response.InventoryAssetsCardQueryResponse;
import cn.wowjoy.office.data.response.InventoryRecordResponse;
import cn.wowjoy.office.data.response.InventoryResponse;
import cn.wowjoy.office.data.response.InventoryTaskDetailListResponse;
import cn.wowjoy.office.data.response.InventoryTaskListDtlResponse;
import cn.wowjoy.office.data.response.InventoryTaskListResponse;
import cn.wowjoy.office.data.response.MaterialSearchResponse;
import cn.wowjoy.office.data.response.QRVoInfo;
import cn.wowjoy.office.data.response.RecordResponse;
import cn.wowjoy.office.data.response.StaffListResponse;
import cn.wowjoy.office.data.response.StockApplyDemandHeadDetailResponse;
import cn.wowjoy.office.data.response.StockApplyDemandHeadListResponse;
import cn.wowjoy.office.data.response.StockDetailListResponse;
import cn.wowjoy.office.data.response.StockForOtherOutResponse;
import cn.wowjoy.office.data.response.StockRecordResponse;
import cn.wowjoy.office.data.response.StockSelectResponse;
import cn.wowjoy.office.data.response.StockStorageResponse;
import cn.wowjoy.office.data.response.StockTypeResponse;
import cn.wowjoy.office.data.response.SubmitCheckTaskListDetailResponse;
import cn.wowjoy.office.data.response.SubmitPDAResponse;
import cn.wowjoy.office.data.response.TestResponse;
import cn.wowjoy.office.pm.data.CardResponse;
import cn.wowjoy.office.pm.data.DictDataResponse;
import cn.wowjoy.office.pm.data.PmManageCardInfo;
import cn.wowjoy.office.pm.data.PmManageResponse;
import cn.wowjoy.office.pm.data.PmProjectResponse;
import cn.wowjoy.office.pm.data.PmReportDetailResponse;
import cn.wowjoy.office.pm.data.PmReportIndexResponse;
import cn.wowjoy.office.pm.data.SubmitResponse;
import cn.wowjoy.office.pm.data.UploadImgResponse;
import cn.wowjoy.office.pm.data.request.PmReportDetailSubmit;
import io.reactivex.Flowable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiService {

    /**
     * 测试接口
     */
    @GET("version")
    Flowable<ResultWrapper<TestResponse>> getVersion();

    @GET("material/test")
    Flowable<ResultWrapper<TestResponse>> getInfo();


    /**
     * 物资查询列表
     */
    @GET("ms-mobile/goods/info/paging")
    Flowable<ResultWrapper<List<GoodsDetailResponse>>> getGoodsListInfo(@Query ("isStopSearch") @Nullable String isStopSearch, @Query("mixSerach") String mixSerach, @Query("startIndex")int Index, @Query("pageSize")int pageSize);

    /**
     * 物资详情
     *
     * @param code
     * @param code
     * @return
     */
    @GET("ms-mobile/goods/info/viewgoods")
    Flowable<ResultWrapper<GoodsDetailResponse>> getGoodsDetailInfo(@Query ("goodsCode") String code,@Query ("barCode") String barCode);

    /**
     * 停用物资
     *
     * @param request
     * @return
     */
    @POST("ms-mobile/goods/info/stopGoods")
    Flowable<ResultWrapper<ResultEmpty>> stopGoods(@Body GoodsDetailRequest request);

    /**
     * 启用物资
     *
     * @param request
     * @return
     */
    @POST("ms-mobile/goods/info/startGoods")
    Flowable<ResultWrapper<ResultEmpty>> startGoods(@Body GoodsDetailRequest request);


    /**
     * 修改物资
     *
     * @param multipartParams
     * @return
     */
    @Multipart
//    @Headers("Content-Type:multipart/form-data;")
    @POST("zuul/ms-mobile/goods/info/setGoods")
    Flowable<ResultWrapper<ResultEmpty>> setGoods(@PartMap Map<String, RequestBody> multipartParams);


    //-----------------------------------------------------------------------------------------------

    /**
     * "我的" 职工信息获取
     * @return
     */
    @GET("ms-mobile/base/staff/detail")
    Flowable<ResultWrapper<StaffListResponse>> getStaffInfo(@Query ("app") String app);

    /**
     * 巡检管理    已完成和未完成任务列表查询
     */
    @GET("ms-mobile/inspect/inspectTask/list")
    Flowable<ResultWrapper<InspectionTotalResponse>> getTaskListInfo(@Query("isDone") String isDone,@Query("currentUserId") String currentUserId);

    /**
     * 巡检管理    已完成和未完成任务列表模糊查询
     */
    @GET("ms-mobile/inspect/inspectTask/list")
    Flowable<ResultWrapper<InspectionTotalResponse>> getTaskListVagueInfo(@Query("billInfo") String billInfo,@Query("currentUserId") String currentUserId);
//    Flowable<ResultWrapper<InspectionTotalResponse>> getTaskListInfo(@Body InspectionTotalRequest request);

    /**
     * 巡检管理    巡检任务详情接口
     *
     * @param taskId           巡检任务主键ID
     * @param billInfo         多条件的模糊查询
     * @param isExist          是否存在
     * @param notInspect        尚未巡检
     * @param appAbNormal 外观状态是否正常
     * @param assetsStatus     设备状态是否正常
     * @param startIndex       开始页号
     * @param pageSize         页面大小
     * @return
     */
    //带分页
    @Headers("Content-Type: application/json; charset=utf-8")
    @GET("ms-mobile/inspect/inspectTask/select")
    Flowable<ResultWrapper<InspectionTaskControllerResponse>> getInspectTaskControllerByPageSize(@Query("taskId") String taskId, @Query("billInfo") String billInfo
            , @Query("isExist") String isExist, @Query("notInspect") String notInspect, @Query("appAbNormal") String appAbNormal, @Query("assetsStatus") String assetsStatus
            , @Query("appearanceStatus") String appearanceStatus,@Query("labelStatus") String labelStatus,@Query("functionStatus") String functionStatus,@Query("cleanStatus") String cleanStatus
            , @Query("startIndex") int startIndex, @Query("pageSize") int pageSize);

    @GET("ms-mobile/inspect/inspectTask/select")
    Flowable<ResultWrapper<InspectionTaskControllerResponse>> getInspectTaskController(@Query("taskId") String taskId, @Query("billInfo") String billInfo
            , @Query("isExist") String isExist, @Query("noInspect") String noInspect, @Query("appearanceStatus") String appearanceStatus
            , @Query("assetsStatus") String assetsStatus);

    /**
     *
     * @param taskId   列表任务ID
     * @param taskDtlId  明细ID
     * @param qrCode    扫码ID
     * @return
     */
    @GET("ms-mobile/inspect/inspectTask/selectDtl")
    Flowable<ResultWrapper<InspectTaskSelect>> selectDtlPDA(@Query("taskId") String taskId, @Query("taskDtlId") String taskDtlId, @Query("qrCode") String qrCode);


    /**
     *  提交整个巡检任务
     * @param request
     * @return
     */
    @POST("ms-mobile/inspect/inspectTask/save")
    Flowable<ResultWrapper<SubmitPDAResponse>> submitPDA(@Body SubmitPDARequest request,@Query("currentUserId") String currentUserId);

    /**
     * 巡检管理    巡检任务详情接口
     */
    @POST("zuul/ms-mobile/inspect/inspectTask/saveDtl")
     Flowable<ResultWrapper<InspectRecordControllerResponse>> uploadInspectRecordControllerInfo(@Body InspectRecordControllerRequest multipartParams, @Query("currentUserId") String currentUserId);

    /**
     *  fileId fileId
     * @return
     */
    @GET("ms-mobile/getphotobyte")
    Flowable<ResultWrapper<ResponseBody>> getPhotoByte(@Query("fileId") String fileId);

    /**
     *  fileId fileId
     * @return
     */
    @GET("ms-mobile/file/getFileUrl")
    Flowable<ResultWrapper<String>> getFileUrl(@Query("fileId") String fileId);

    /**
     *  获取图片前缀
     * @return
     */
    @GET("ms-mobile/file/preFileUrl")
    Flowable<ResultWrapper<String>> preFileUrl();


    /**
     * 上传测试
     */
    @POST("/zuul/ceph/v1/buckets/objects")
    Flowable<ResultWrapper<InspectRecordControllerResponse>> uploadImg(@PartMap Map<String, RequestBody> multipartParams);

//--------------------------------------     盘点     ------------------------------------------------------//

    /**
     *     已完成和未完成任务列表查询
     */
    @GET("ms-mobile/inventory/task/list")
    Flowable<ResultWrapper<InventoryTaskListResponse>> getInventoryTaskListInfo(@Query("inventoryStatus") String inventoryStatus);

    /**
     *     已完成和未完成任务列表模糊查询
     */
    @GET("ms-mobile/inventory/task/list")
    Flowable<ResultWrapper<InventoryTaskListResponse>> getInventoryListVagueInfo(@Query("pdaComplexCondition") String pdaComplexCondition);

    /**
     *
     * @param taskId
     * @param type
     * @param complexSearch
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Headers("Content-Type: application/json; charset=utf-8")
    @GET("ms-mobile/inventory/task/listInventoryTaskDtlPda")
    Flowable<ResultWrapper<InventoryTaskDetailListResponse>> getListInventoryTaskDtlPdaByPageSize(@Query("taskId") String taskId, @Query("type") String type,@Query("complexSearch") String complexSearch
    , @Query("startIndex") int startIndex, @Query("pageSize") int pageSize);

    /**
     *
     * @param taskId
     * @param complexSearch
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Headers("Content-Type: application/json; charset=utf-8")
    @GET("ms-mobile/inventory/task/listInventoryTaskDtlPda")
    Flowable<ResultWrapper<InventoryTaskDetailListResponse>> getListInventoryTaskDtlPdaByBillInfo(@Query("taskId") String taskId,@Query("complexSearch") String complexSearch , @Query("startIndex") int startIndex, @Query("pageSize") int pageSize);

    /**
     * 提交整个任务
     * @param request
     * @return
     */
    @POST("ms-mobile/inventory/report/submitInventoryReport")
    Flowable<ResultWrapper<SubmitCheckTaskListDetailResponse>> submitCheckTaskListDetail(@Body SubmitCheckTaskListDetailRequest request);

    /**
     * 统计表格数据
     * @param taskId
     * @return
     */
    @GET("ms-mobile/inventory/task/countInventoryTaskDtlByPda")
    Flowable<ResultWrapper<CheckCountResponse>> countCheckTaskDtlByPda(@Query("taskId") String taskId);



    /**
     *     新增盘盈列表出场编码模糊查询
     */
    @GET("ms-mobile/base/assets/card/list")
    Flowable<ResultWrapper<InventoryAssetsCardQueryResponse>> getCheckAssetsCardList(@Query("produceNoSearch") String produceNoSearch);


    /**
     *     单条明细详情
     */
    @GET("ms-mobile/inventory/task/getInventoryTaskDtlByPda")
    Flowable<ResultWrapper<InventoryTaskListDtlResponse>> getCheckTaskDtlByPda(@Query("taskId") String taskId, @Query("id") String taskDtlId);



    /**
     * 提交盘盈  明细 新增详情
     * @param request
     * @return
     */
    @POST("ms-mobile/inventory/task/saveInventoryTaskDtl")
    Flowable<ResultWrapper<SubmitCheckTaskListDetailResponse>> saveCheckTaskDtl(@Body SubmitCheckTaskListDetailRequest request);

    /**
     * 扫描接口
     * @param request
     * @return
     */
    @POST("ms-mobile/inventory/task/inventoryTaskDtlByPda")
    Flowable<ResultWrapper<CheckBroadResultResponse>> updateListByBroadCast(@Body CheckTaskUpdateListRequest request);


    /**
     *
     * @return
     */
    @GET("ms-mobile/base/assets/category/list")
    Flowable<ResultWrapper<CheckCategoryListResponse>> getCategoryList();


    //--------------------------------------------------------------------------------------------//
    /**
     * 库存分类树
     * @param usageCodeSearch
     * @param lessLevel >= 2,默认2级
     * @return
     */
    @GET("ms-mobile/goods/assortment/getTreeByCondition")
    Flowable<ResultWrapper<StockTypeResponse>> getStockType(@Query ("usageCodeSearch") String usageCodeSearch,@Query ("lessLevel") String lessLevel);


    /**
     * 库存用途
     * @param storageCode
     * @return
     */
    @GET("ms-mobile/storage/getUsageByStorageCode")
    Flowable<ResultWrapper<List<PopuModel>>> getUsage(@Query ("storageCode") String storageCode);

    /**
     * 仓库
     * @return
     */
    @GET("ms-mobile/storage/getStorageByUserId")
    Flowable<ResultWrapper<List<PopuModel>>> getStock();

    /**
     * 获取仓库详情列表
     */
    @POST("ms-mobile/stock/getStockList")
    Flowable<ResultWrapper<List<MaterialSearchResponse>>> getStockList(@Body StockListRequest request);
//    Flowable<ResultWrapper<StockListResponse>> getStockList(@Query("storageCodeSearch")String storageCodeSearch, @Query("useSearch")String useSearch, @Query("mixSearch")String mixSearch, @Query("startIndex")int startIndex, @Query("pageSize")int pageSize,@Query("typeListSearch")String typListSearch);

    @GET("ms-mobile/stock/getStockDetailList")
    Flowable<ResultWrapper<StockDetailListResponse>> getStockDetailList(@Query("goodsCode")String goodsCode);

    @GET("ms-mobile/log/getLog")
    Flowable<ResultWrapper<List<HistoryLogResponse>>> getHistory();

    @GET("ms-mobile/log/clean")
    Flowable<ResultWrapper<String>> cleanHistory();

    /**
     * 物资盘点
     */

    /**
     * 锁定库存
     * @param request
     * @return
     */
    @POST("ms-mobile/countSheet/addCountSheet")
    Flowable<ResultWrapper<ResultEmpty>> lockStock(@Body AddInventoryRequest request);

    /**
     * 检查仓库是否锁定
     * @param storageCode
     * @return
     */
    @GET("ms-mobile/countSheet/isChecked")
    Flowable<ResultWrapper<String>> isChecked(@Query("storageSearch")String storageCode);

    /**
     * 检查仓库是否释放
     * @param storageCode
     * @return
     */
    @GET("ms-mobile/countSheet/isRelease")
    Flowable<ResultWrapper<String>> isRelease(@Query("storageSearch")String storageCode);

    /**
     * 获取报告状态列表
     */
    @GET("ms-mobile/dict/reportStatus")
    Flowable<ResultWrapper<List<PopuModel>>> reportStatus();

    /**
     * 获取盘点报告列表
     */
    @GET("ms-mobile/countReport/getCountReportList")
    Flowable<ResultWrapper<List<InventoryResponse>>> getCountReportList(@Query("statusSearch")String statusSearch, @Query("fuzzySearch")String fuzzySearch, @Query("startIndex")int startIndex, @Query("pageSize")int pageSize);

    /**
     * 查询锁定记录
     * @param storageCode
     * @return
     */
    @GET("ms-mobile/countSheet/getCountSheetList")
    Flowable<ResultWrapper<List<InventoryRecordResponse>>> getInVentoryRecord(@Query("storageSearch")String storageCode);

    /**
     * 根据盘点记录生成盘点报告
     */
    @POST("ms-mobile/countReport/addCountReportBySheet")
    Flowable<ResultWrapper<String>> getCountReportBySheet(@Body GetCountReportBySheetRequest request);

    /**
     * 查询单条盘点报告明细
     * @param headCode
     * @param isChecked
     * @return y:已盘点；n:未盘点；null：全部
     */
    @GET("ms-mobile/countReport/getCountReportInfoByCodePDA")
    Flowable<ResultWrapper<StockRecordResponse>> getCountReportInfoByCodePDA(@Query("headCode")String headCode,@Query("isChecked")String isChecked,@Query("isProfit")String isProfit,@Query("goodsCode")String goodsCode,@Query("barCode")String barCode,@Query("pdaSearch")String pdaSearch, @Query("startIndex")Integer startIndex, @Query("pageSize")Integer pageSize);


    /**
     * 扫码查询盘点报告明细
     * @param headCode
     * @return
     */
    @GET("ms-mobile/countReport/getPDAReportList")
    Flowable<ResultWrapper<StockRecordResponse>> getPDAReportList(@Query("headCode")String headCode,@Query("barCode")String barCode);

    /**
     * 校验是否有校验记录
     * @param headCode
     * @param goodsCode
     * @param barCode
     * @return y:盘过，n:未盘过
     */
    @GET("ms-mobile/countReportDetail/isGoodsChecked")
    Flowable<ResultWrapper<Map<String,String>>> isGoodsChecked(@Query("headCode")String headCode,@Query("goodsCode")String goodsCode,@Query("barCode")String barCode);

    /**
     * 扫码录入盘点记录/新增盘点记录
     * @param request
     * @return
     */
    @POST("ms-mobile/countReportDetail/addDetailInfos")
    Flowable<ResultWrapper<ResultEmpty>> addDetailInfos(@Body List<RecordResponse> request);


    /**
     * 提交汇总
     * @param headCode
     * @return
     */
    @GET("ms-mobile/countReportChecker/confirmFinish")
    Flowable<ResultWrapper<ResultEmpty>> confirmFinish(@Query("headCode")String headCode);


    /**
     * 查询单条盘点报告头部信息
     */

    @GET("ms-mobile/countReport/getCountReportInfoByCode")
    Flowable<ResultWrapper<StockRecordResponse>> getCountReportInfoByCode(@Query("headCode")String headCode);

    @GET("ms-mobile/countReport/getCountReportByHeadCode")
    Flowable<ResultWrapper<StockRecordResponse>> getCountReportByHeadCode(@Query("headCode")String headCode);

    /**
     * 查询员工列表
     */
    @GET("ms-mobile/base/staff/getStaffInfoByfuzzy")
    Flowable<ResultWrapper<List<CheckerResponse>>> getStaffInfoByFuzzy(@Query("code")String code);


    /**
     * 提交审核
     */
    @POST("ms-mobile/countReport/pdaSubmit")
    Flowable<ResultWrapper<String>> pdaSubmit(@Body StockRecordResponse request);

    /**
     * 检验盘点报告是否可以提交
     */
    @GET("ms-mobile/countReport/isReadyForSubmit")
    Flowable<ResultWrapper<String>> isReadySubmit(@Query("headCode")String headCode);

//------------------------------------- 其他出库 ------------------------------------------------------------
    /**
     * 获取出库方向 的列表
     */
    @GET("ms-mobile/dept/lists")
    Flowable<ResultWrapper<List<StockSelectResponse>>> selectStockOut();
    /**
     * 获取出库方向 的列表
     */
    @GET("ms-mobile/dept/lists")
    Flowable<ResultWrapper<List<StockSelectResponse>>> selectStockOutVague(@Query("fuzzyCondition")String fuzzyCondition);

    /**
     * 手动添加出库物资的查询
     */
    @POST("ms-mobile/stock/getStockForOtherOut")
    Flowable<ResultWrapper<List<StockForOtherOutResponse>>> getStockForOtherOut(@Body StockForOtherOutRequest request);

    /**
     * 扫码添加出库物资的查询
     */
    @GET("ms-mobile/qr/getQRByQRCodePda")
    Flowable<ResultWrapper<StockForOtherOutResponse>> getQRByQRCodePda(@Query("qrCode")String qrCode);

    /**
     * 扫码添加出库物资的查询
     */

    @GET("ms-mobile/qr/getQRByQRCodePda")
    Flowable<ResultWrapper<ApplyDemandHeadDetailItem>> getQRByQRCodePdaApply(@Query("qrCode")String qrCode);

    @GET("ms-mobile/qr/getQRByQRCodePda")
    Flowable<ResultWrapper<QRVoInfo>> getQRVoInfo(@Query("qrCode")String qrCode);


    /**
     * 获取登陆人仓库
     */
    @GET("ms-mobile/storage/getStorageByUserId")
    Flowable<ResultWrapper<StockStorageResponse>> getStorageByUserId();

    /**
     * 是否有审批确认出库的权限
     */
    @GET("ms-mobile/role/isExamineAndConfirmOut")
    Flowable<ResultWrapper<StockStorageResponse>> isExamineAndConfirmOut();

    /**
     * 提交审批
     */
    @GET("ms-mobile/outstorage/addOtherOtherStorage")
    Flowable<ResultWrapper<StockForOtherOutResponse>> submitAddOtherOtherStorage();


    /**
     * 获取病人信息
     */
    @POST("ms-mobile/patient/getList")
    Flowable<ResultWrapper<List<StockSelectResponse>>> getPatientList(@Body StockForOtherOutRequest request);


//------------------------------------- 申领出库 ------------------------------------------------------------
    /**
     * 获取申领单列表
     */
    @POST("ms-mobile/apply/getApplyHeadList")
    Flowable<ResultWrapper<ApplyHeadListResponse>> getApplyHeadList(@Body ApplyFormQueryConditions request);

    /**
     * 获取单条申领单
     */
    @GET("ms-mobile/apply/getApplyHead")
    Flowable<ResultWrapper<ApplyHeadInfoResponse>> getApplyHead(@Query("headCode")String headCode);

    /**
     * 提交申领单
     */
    @POST("ms-mobile/outstorage/addOutStorageByApplyNew")
    Flowable<ResultWrapper<Object>> addOutStorageByApplyNew(@Body ApplyHeadInfoResponse request);

//------------------------------------- 调拨出库------------------------------------------------------------
    /**
     * 获取调拨单列表
     * @return
     */
    @GET("ms-mobile/allocation/getList")
    Flowable<ResultWrapper<ApplyHeadListResponse>> getAllotList(@Query("businessStatus")String businessStatus, @Query("billStatus")String billStatus,
                                                                @Query("outStorageCode")String outStorageCode, @Query("inStorageCode")String inStorageCode,
                                                                @Query("isEnd")String isEnd, @Query("startIndex")int startIndex, @Query("pageSize")int pageSize);

    /**
     * 获取调拨单详情接口
     * @return
     */
    @GET("ms-mobile/allocation/getAllocationHeadByHeadCodePda")
    Flowable<ResultWrapper<ApplyHeadInfoResponse>> getAllocationHeadByHeadCodePda(@Query("headCode")String headCode,
                                                                                    @Query("startIndex")Integer startIndex,
                                                                                    @Query("pageSize")Integer pageSize);

    /**
     * 提交调拨单
     * @return
     */
    @POST("ms-mobile/outstorage/addOutStorageByAllocationQR")
    Flowable<ResultWrapper<ApplyHeadInfoResponse>> addOutStorageByAllocationQR(@Body ApplyHeadInfoResponse request);

    @Multipart
    @POST("ms-mobile/outstorage/addOtherOtherStorage")
    Flowable<ResultWrapper<String>> submitAddOtherOtherStorage2();

    /**
     * 提交审批
     */

    @POST("ms-mobile/outstorage/addOtherOtherStorage")
    Flowable<ResultWrapper<String>> submitAddOtherOtherStorage(@Body StockOtherOutSubmitRequest multipartParams);


//------------------------------ 申请出库 ------------------------------------
    /**
     *     已完成和未完成任务列表查询
     */
    @POST("ms-mobile/demand/getDemandHeadList")
    Flowable<ResultWrapper<StockApplyDemandHeadListResponse>> getDemandHeadList(@Body StockApplyDemandHeadListRequest request);

    /**
     *     单条查询
     */
    @GET("ms-mobile/demand/getDemandHead")
    Flowable<ResultWrapper<StockApplyDemandHeadDetailResponse>> getDemandHead(@Query("headCode") String headCode);


    /**
     *     单条查询
     */
    @GET("ms-mobile/base/staff/getStaffInfoByUserId")
    Flowable<ResultWrapper<CheckerResponse>> getStaffInfoByUserId();


    /**
     * 提交审批
     */

    @POST("ms-mobile/outstorage/addOutStorageByDemandNew")
    Flowable<ResultWrapper<ApplyDemandHeadDetailItem>> submitOutStorageByDemandNew(@Body List<ApplyDemandHeadDetailItem> multipartParams);


    /**
     *     是否是院内二维码
     */
    @GET("ms-mobile/goods/info/isBarCode")
    Flowable<ResultWrapper<String>> isBarCode(@Query("barCode") String barCode);


    /**
     *  登陆后 获取各个Iaid
     */
    @GET("ms-mobile/login/getAuidsPms")
    Flowable<ResultWrapper<List<AppInfo>>> getAuidsPms();

    //-------------------------------- PM相关-------------------------------------------

    /**
     *  PM管理列表
     */
    @GET("ms-mobile/pm/manage/listPmCardData")
    Flowable<ResultWrapper<PmManageResponse>> getListPmCardData(@Query("fuzzySearchForPDA") String fuzzySearchForPda, @Query("pmStatus") String pmStatus, @Query("startIndex") int startIndex, @Query("pageSize") int pageSize,@Query("currentUserId") String currentUserId);
    /**
     *  PM管理列表
     */
    @GET("ms-mobile/pm/manage/listPmCardData")
    Flowable<ResultWrapper<PmManageResponse>> getListPmCardData(@Query("fuzzySearchForPDA") String fuzzySearchForPda,@Query("currentUserId") String currentUserId);
    /**
     *  PM管理列表详情
     */
    @GET("ms-mobile/pm/manage/getPmCardInfo")
    Flowable<ResultWrapper<PmManageCardInfo>> getManagerCardInfo(@Query("cardId") String cardId);
    /**
     *  获取PM报告列表
     */
    @GET("ms-mobile/pm/report/listPmReport")
    Flowable<ResultWrapper<PmReportIndexResponse>> getlistPmReportData(@Query("fuzzySearchForPDA") String fuzzSearchForPDA,@Query("billStatus") String billStatus,@Query("startIndex") int startIndex,@Query("pageSize") int pageSize,@Query("currentUserId") String currentUserId);
    @GET("ms-mobile/pm/report/listPmReport")
    Flowable<ResultWrapper<PmReportIndexResponse>> getlistPmReportDataYet(@Query("billStatus") String billStatus,@Query("currentUserId") String currentUserId);
    @GET("ms-mobile/pm/report/listPmReport")
    Flowable<ResultWrapper<PmReportIndexResponse>> getlistPmReportDataDone(@Query("fuzzySearchForPDA") String fuzzSearchForPDA,@Query("billStatuses") String billStatuses,@Query("startIndex") int startIndex,@Query("pageSize") int pageSize,@Query("currentUserId") String currentUserId);
    /**
     *  获取PM报告列表
     */
    @GET("ms-mobile/pm/report/listPmReport")
    Flowable<ResultWrapper<PmReportIndexResponse>> getlistPmReportData(@Query("fuzzySearchForPDA") String fuzzSearchForPDA,@Query("currentUserId") String currentUserId);
    /**
     *  获取PM报告详情
     */
    @GET("ms-mobile/pm/report/getPmReport")
    Flowable<ResultWrapper<PmReportDetailResponse>> getPmReport(@Query("id") String id);


    /**
     *  获取检测类型  检测机构
     */
    @GET("ms-mobile/dictdata/list")
    Flowable<ResultWrapper<DictDataResponse>> getDictData(@Query("dictTypeValue") String dictTypeValue);

    /**
     *  设备详情接口
     */
    @GET("ms-mobile/base/assets/card/select")
    Flowable<ResultWrapper<CardResponse>> getCardData(@Query("qrCode") String id);

    /**
     *  获取新增设备详情接口
     */
    @GET("ms-mobile/pm/report/getPmProject")
    Flowable<ResultWrapper<PmProjectResponse>> getPmProject(@Query("id") String id);

    /**  仅保存：/pm/report/savePmReport， 提交：/pm/report/submitPmReport
     *  提交新增PM报告列表   ?{currentUserId}
     */
    @POST("ms-mobile/pm/report/savePmReport")
    Flowable<ResultWrapper<SubmitResponse>> savePmReport(@Body PmReportDetailSubmit body,@Query("currentUserId") String currentUserId);
    @POST("ms-mobile/pm/report/submitPmReport")
    Flowable<ResultWrapper<SubmitResponse>> submitPmReport(@Body PmReportDetailSubmit body, @Query("currentUserId") String currentUserId);

    /**
     *  删除PM报告
     * @param  ids:报告单ID   currentUserId:登陆人的auid
     * @return
     */
    @GET("ms-mobile/pm/report/deletePmReport")
    Flowable<ResultWrapper<SubmitResponse>> deletePmReport(@Query("ids") String ids,@Query("currentUserId") String currentUserId);

    /**
     * 上传签名图片
     */
    @Multipart
    @POST("ms-mobile/picture/upload")
    Flowable<ResultWrapper<UploadImgResponse>> uploadSignName(@PartMap Map<String, RequestBody> multipartParams);

    /**
     * PM报告签字
     */
    @GET("ms-mobile/pm/report/signPmReports")
    Flowable<ResultWrapper<SubmitResponse>> sighPmReports(@Query("ids") String ids,@Query("currentUserId") String currentUserId,@Query("thumbnailId") String thumbnailId,@Query("pictureId") String pictureId);
}
