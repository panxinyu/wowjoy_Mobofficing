package cn.wowjoy.office.data.local;

import com.google.gson.Gson;

/**
 * Created by Sherily on 2017/9/30.
 * Description: serialize with gson
 */

public class GsonSerializer<T> implements EntitySerializer<T> {

    private Gson mGson;

    public GsonSerializer(Gson gson) {
        mGson = gson;
    }

    @Override
    public String serialize(T t) {
        return mGson.toJson(t);
    }

    @Override
    public T deserialize(String string, Class<T> tClass) {
        return mGson.fromJson(string, tClass);
    }
}
