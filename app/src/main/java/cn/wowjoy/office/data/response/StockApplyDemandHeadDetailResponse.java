package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Administrator on 2018/4/18.
 */

public class StockApplyDemandHeadDetailResponse implements Serializable{
    private String headCode;

    private String demandFrom;

    private String  status;

    private String storage;

    private String storageView;

    private String commitDept;

    private String commitDeptView;

    private String mainDepartmentCode;

    private String totalCanOutNumber; //总未出库数量

    @SerializedName("totalHasOutNumber")
    private String totalOutNumber; //总出库数量

    @SerializedName("detailList")
    private List<ApplyDemandHeadDetailItem> mList;

    private DecimalFormat fnum  =   new  DecimalFormat("##0.00");

    public String getTotalOutNumber() {
        if(null == totalOutNumber){
            return "0.00";
        }
        return totalOutNumber;
    }

    public void setTotalOutNumber(String totalOutNumber) {
        this.totalOutNumber = totalOutNumber;
    }

    public String getCommitDeptView() {
        return commitDeptView;
    }

    public void setCommitDeptView(String commitDeptView) {
        this.commitDeptView = commitDeptView;
    }

    public String getStorageView() {
        return storageView;
    }

    public void setStorageView(String storageView) {
        this.storageView = storageView;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getDemandFrom() {
        return demandFrom;
    }

    public void setDemandFrom(String demandFrom) {
        this.demandFrom = demandFrom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getCommitDept() {
        return commitDept;
    }

    public void setCommitDept(String commitDept) {
        this.commitDept = commitDept;
    }

    public String getMainDepartmentCode() {
        return mainDepartmentCode;
    }

    public void setMainDepartmentCode(String mainDepartmentCode) {
        this.mainDepartmentCode = mainDepartmentCode;
    }

    public String getTotalCanOutNumber() {
    //    int v = (int) Float.parseFloat(totalCanOutNumber);
        return twoFloat(totalCanOutNumber);
    }

    public void setTotalCanOutNumber(String totalCanOutNumber) {
        this.totalCanOutNumber = totalCanOutNumber;
    }

    public List<ApplyDemandHeadDetailItem> getList() {
        return mList;
    }

    public void setList(List<ApplyDemandHeadDetailItem> list) {
        mList = list;
    }

    public String twoFloat(String string){
        float v = Float.parseFloat(string);
        return fnum.format(v);
    }
}
