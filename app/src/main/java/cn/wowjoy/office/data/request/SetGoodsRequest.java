package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/11/21.
 * Description:
 */

public class SetGoodsRequest {
    @SerializedName("goods")
    private SetGoodsJsonRequest goods;

    private String file;

    public SetGoodsRequest() {
    }

    public SetGoodsJsonRequest getGoods() {
        return goods;
    }

    public void setGoods(SetGoodsJsonRequest goods) {
        this.goods = goods;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
