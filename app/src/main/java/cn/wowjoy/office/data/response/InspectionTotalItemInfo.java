package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Administrator on 2017/11/17.
 */

public class InspectionTotalItemInfo implements Serializable{
    /**
     * 任务ID
     */
    @SerializedName("id")
    private String id;
    /**
     * 巡检任务单据编码
     */
    @SerializedName("billNo")
    private String billNo;
    /**
     * 巡检任务单据名称
     */
    @SerializedName("billName")
    private String billName;
    /**
     * 单据名称
     */
    @SerializedName("billStatus")
    private String billStatus;
    /**
     * 业务状态
     */
    @SerializedName("taskStatus")
    private String taskStatus;
    /**
     * 创建时间
     */
    @SerializedName("createDatetime")
    private String createDatetime;
    /**
     * 任务开始时间
     */
    @SerializedName("taskStartDatetime")
    private String taskStartDatetime;
    /**
     * 任务结束时间
     */
    @SerializedName("taskEndDatetime")
    private String taskEndDatetime;
    /**
     * 关联巡检记录ID
     */
    @SerializedName("inspectRecordId")
    private String inspectRecordId;
    /**
     * 使用科室
     */
    @SerializedName("useDepartmentName")
    private String useDepartmentName;   //使用科室

    @SerializedName("inspectRecordBillStatus")
    private String inspectRecordBillStatus; //巡检记录单据状态

    private boolean isDone =false;

    private boolean isTime = false;

    public boolean isTime() {
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String end = taskEndDatetime + " 23:59:59";
        try {
            long now = System.currentTimeMillis();
            long startTime = df1.parse(taskStartDatetime).getTime();
            long endTime = df2.parse(end).getTime();
            isTime = (now>= startTime && now <= endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isTime;
    }

    public void setTime(boolean time) {
        isTime = time;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public String getTaskStartDatetime() {
        return taskStartDatetime;
    }

    public void setTaskStartDatetime(String taskStartDatetime) {
        this.taskStartDatetime = taskStartDatetime;
    }

    public String getInspectRecordBillStatus() {
        return inspectRecordBillStatus;
    }

    public void setInspectRecordBillStatus(String inspectRecordBillStatus) {
        this.inspectRecordBillStatus = inspectRecordBillStatus;
    }

    public InspectionTotalItemInfo() {
        //     ResultList
    }

    public InspectionTotalItemInfo(String billNo, String billName, String createDatetime, String taskStartDatetime, String taskEndDatetime, String useDepartmentName) {
        this.billNo = billNo;
        this.billName = billName;
        this.createDatetime = createDatetime;
        this.taskStartDatetime = taskStartDatetime;
        this.taskEndDatetime = taskEndDatetime;
        this.useDepartmentName = useDepartmentName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillName() {
//        String str = taskStartDatetime.replaceAll("/","");
//        String substring = str.substring(str.length() - 4, str.length());
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getTaskStratDatetime() {
        return taskStartDatetime;
    }

    public void setTaskStratDatetime(String taskStratDatetime) {
        this.taskStartDatetime = taskStratDatetime;
    }

    public String getTaskEndDatetime() {
        return taskEndDatetime;
    }

    public void setTaskEndDatetime(String taskEndDatetime) {
        this.taskEndDatetime = taskEndDatetime;
    }

    public String getInspectRecordId() {
        return inspectRecordId;
    }

    public void setInspectRecordId(String inspectRecordId) {
        this.inspectRecordId = inspectRecordId;
    }

    public String getUseDepartmentName() {
        return useDepartmentName;
    }

    public void setUseDepartmentName(String useDepartmentName) {
        this.useDepartmentName = useDepartmentName;
    }
}
