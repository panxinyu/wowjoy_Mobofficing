package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2018/1/8.
 * Description:
 */

public class StockDetailListResponse {
    @SerializedName("stockList")
    private List<MaterialSearchResponse> stockDetailResponseList;

    @SerializedName("goods")
    private GoodsDetailResponse goodsDetailResponse;

    @SerializedName("totalCount")
    private String totalCount;


    public List<MaterialSearchResponse> getStockDetailResponseList() {
        return stockDetailResponseList;
    }

    public void setStockDetailResponseList(List<MaterialSearchResponse> stockDetailResponseList) {
        this.stockDetailResponseList = stockDetailResponseList;
    }

    public GoodsDetailResponse getGoodsDetailResponse() {
        return goodsDetailResponse;
    }

    public void setGoodsDetailResponse(GoodsDetailResponse goodsDetailResponse) {
        this.goodsDetailResponse = goodsDetailResponse;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }
}
