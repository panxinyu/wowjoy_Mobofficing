package cn.wowjoy.office.data.event;

public class CountEvent {
    private int count;
    private String status;

    public CountEvent(String status) {
        this.status = status;
    }

    public CountEvent(int count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
