package cn.wowjoy.office.data.response;

import java.io.Serializable;


public class UpdateInfo implements Serializable {

    /**
     * forced : 0
     * updateContent : 新版本，新惊喜
     * apkUrl :
     * pluginUrl : https://gitee.com/inwowjoy/doctor_workstation/raw/master/wowjoyDoc.jar
     * versionCode : 1
     * versionName : 1.0.0
     */

    private int forced;
    private String updateContent;
    private String apkUrl;
    private String pluginUrl;
    private int versionCode;
    private String versionName;

    public int getForced() {
        return forced;
    }

    public void setForced(int forced) {
        this.forced = forced;
    }

    public String getUpdateContent() {
        return updateContent;
    }

    public void setUpdateContent(String updateContent) {
        this.updateContent = updateContent;
    }

    public String getApkUrl() {
        return apkUrl;
    }

    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }

    public String getPluginUrl() {
        return pluginUrl;
    }

    public void setPluginUrl(String pluginUrl) {
        this.pluginUrl = pluginUrl;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    @Override
    public String toString() {
        return "UpdateInfo{" +
                "forced=" + forced +
                ", updateContent='" + updateContent + '\'' +
                ", apkUrl='" + apkUrl + '\'' +
                ", pluginUrl='" + pluginUrl + '\'' +
                ", versionCode=" + versionCode +
                ", versionName='" + versionName + '\'' +
                '}';
    }
}
