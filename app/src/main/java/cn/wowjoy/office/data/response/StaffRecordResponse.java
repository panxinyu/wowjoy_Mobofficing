package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import cn.wowjoy.office.utils.TextUtil;

/**
 * Created by Sherily on 2018/1/17.
 * Description:
 */

public class StaffRecordResponse implements Serializable {
    @SerializedName("detailId")
    private String detailId;
    @SerializedName("detailCode")
    private String detailCode;
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("amount")
    private String stockCount;
    @SerializedName("price")
    private String price;
    @SerializedName("batchNumber")
    private String batchNumber;
    @SerializedName("effectiveDate")
    private String effectiveDate;
    @SerializedName("creator")
    private String creator;
    @SerializedName("createTime")
    private String time;
    @SerializedName("modifier")
    private String modifier;
    @SerializedName("gmtModified")
    private String gmtModified;
    @SerializedName("isDeleted")
    private String isDeleted;

    @SerializedName("number")
    private String staffNumber;
    @SerializedName("creatorView")
    private String staffName;



    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(String staffNumber) {
        this.staffNumber = staffNumber;
    }

    public String getStockCount() {
        return stockCount;
    }

    public void setStockCount(String stockCount) {
        this.stockCount = stockCount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String formatName(){
        if (!TextUtils.isEmpty(staffName)){
            if (!TextUtils.isEmpty(staffNumber))
                return "姓名："+staffName+"（"+staffNumber+"）";
            else
                return "姓名："+staffName;
        }
        return "姓名：暂无";
    }
    private String formatString(String sb){
        String s = "0";
        if (sb.length() > 0)
            s = sb.toString().trim();
        BigDecimal bd = new BigDecimal(s);
        DecimalFormat df = new DecimalFormat("#############0.00");//小数点点不够两位补0，例如："0" --> 0.00（个位数补成0因为传入的是0则会显示成：.00，所以各位也补0；）
        String xs = df.format(bd.setScale(2, BigDecimal.ROUND_DOWN));
        return xs;
    }
    public String formatStock(){
        if (!TextUtils.isEmpty(stockCount))
            return "库存："+formatString(stockCount);
        return "库存：暂无";
    }

    public String formatTime(){
        if (!TextUtils.isEmpty(time))
            return "时间："+time;
        return "时间：暂无";
    }
}
