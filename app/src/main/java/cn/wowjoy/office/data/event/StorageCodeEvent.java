package cn.wowjoy.office.data.event;

public class StorageCodeEvent {
    private String storageCode;

    public StorageCodeEvent(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }
}
