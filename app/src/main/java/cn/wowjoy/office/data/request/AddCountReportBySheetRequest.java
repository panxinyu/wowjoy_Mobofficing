package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2018/1/25.
 * Description:
 */

public class AddCountReportBySheetRequest {
    @SerializedName("detailCode")
    private String detailCode;
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("ammount")
    private String amount;
    @SerializedName("price")
    private String price;
    @SerializedName("batchNumber")
    private String batchNumber;
    @SerializedName("effectiveDate")
    private String effectiveDate;
    @SerializedName("use")
    private String use;

    public AddCountReportBySheetRequest(String detailCode, String headCode, String goodsCode, String amount, String price, String batchNumber, String effectiveDate, String use) {
        this.detailCode = detailCode;
        this.headCode = headCode;
        this.goodsCode = goodsCode;
        this.amount = amount;
        this.price = price;
        this.batchNumber = batchNumber;
        this.effectiveDate = effectiveDate;
        this.use = use;
    }
}
