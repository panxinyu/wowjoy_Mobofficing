package cn.wowjoy.office.data.response;

/**
 * Created by Administrator on 2018/1/22.
 */

public class AssetsCardQueryCondition {
    private String produceNo;       //出场编码

    private String categoryName;    //资产类别名称

    private String categoryId;     // 资产类别ID

    private String cardName;      // 资产名称

    private String cardId;   // 资产ID

    private String cardNo;  //资产编码

    private String assetsSpec; //规格型号

    private String cardVersionId;

    public String getCardVersionId() {
        return cardVersionId;
    }

    public void setCardVersionId(String cardVersionId) {
        this.cardVersionId = cardVersionId;
    }

    public AssetsCardQueryCondition(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getProduceNo() {
        return produceNo;
    }

    public void setProduceNo(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getAssetsSpec() {
        return assetsSpec;
    }

    public void setAssetsSpec(String assetsSpec) {
        this.assetsSpec = assetsSpec;
    }

    @Override
    public String toString() {
        return "AssetsCardQueryCondition{" +
                "produceNo='" + produceNo + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", cardName='" + cardName + '\'' +
                ", cardId='" + cardId + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", assetsSpec='" + assetsSpec + '\'' +
                ", cardVersionId='" + cardVersionId + '\'' +
                '}';
    }
//----------------------------------- 病人返回信息 --------------------------
    private String patientName;
    private String patientId;
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
}
