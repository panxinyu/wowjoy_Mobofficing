package cn.wowjoy.office.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import cn.wowjoy.office.data.room.dao.UserDao;
import cn.wowjoy.office.data.room.entity.UserInfo;
import cn.wowjoy.office.pm.db.PmListDao;
import cn.wowjoy.office.pm.db.PmListDetailDao;
import cn.wowjoy.office.pm.db.PmReportListDetailEntity;
import cn.wowjoy.office.pm.db.PmReportListEntity;

/**
 * Created by Sherily on 2017/12/20.
 * Description:
 */
@Database(entities = {UserInfo.class, PmReportListEntity.class, PmReportListDetailEntity.class}, version = 2,exportSchema = false)
public abstract class UserDataBase extends RoomDatabase{
    public abstract UserDao userDao();
    public abstract PmListDao pmListDao();
    public abstract PmListDetailDao pmListDetailDao();
}
