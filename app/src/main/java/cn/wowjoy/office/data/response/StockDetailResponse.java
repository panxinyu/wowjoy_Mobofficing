package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Sherily on 2018/1/5.
 * Description:
 */

public class StockDetailResponse implements Serializable{
    private String indate;
    private String batchNumber;
    private String stock;

    public String getIndate() {
        return indate;
    }

    public void setIndate(String indate) {
        this.indate = indate;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }


    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
