package cn.wowjoy.office.data.response;

import java.io.Serializable;
import java.util.List;

import cn.wowjoy.office.R;


public class PatrolInfo implements Serializable {

    private String bednum;
    private String name;
    private String sex;
    private String status;
    private String time;
    private List<String> tags;

    public PatrolInfo(String bednum, String name, String sex, String status, String time, List<String> tags) {
        this.bednum = bednum;
        this.name = name;
        this.sex = sex;
        this.status = status;
        this.time = time;
        this.tags = tags;
    }

    public String getBednum() {
        return bednum;
    }

    public void setBednum(String bednum) {
        this.bednum = bednum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public int getTextColor() {
        if (status.contains("手术"))
            return 0xFFF76956;
        else if (status.contains("检查"))
            return 0xFF61BC5B;
        else if (status.contains("请假"))
            return 0xFFFA9754;
        else
            return 0xFF5294EA;
    }

    public int getTextBg() {
        if (status.contains("手术"))
            return R.drawable.corners_bg_full_tag1;
        else if (status.contains("检查"))
            return R.drawable.corners_bg_full_tag2;
        else if (status.contains("请假"))
            return R.drawable.corners_bg_full_tag3;
        else
            return R.drawable.corners_bg_full_tag0;
    }
}
