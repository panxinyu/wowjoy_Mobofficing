package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2017/11/14.
 */

public class StaffRequest {

    // 管理域ID
    @SerializedName("midi")
    private String midi;

    //名称和员工工号
    @SerializedName("queryByNameAndNumber")
    private String queryByNameAndNumber;

    public StaffRequest(String midi, String queryByNameAndNumber) {
        this.midi = midi;
        this.queryByNameAndNumber = queryByNameAndNumber;
    }

    public String getMidi() {
        return midi;
    }

    public void setMidi(String midi) {
        this.midi = midi;
    }

    public String getQueryByNameAndNumber() {
        return queryByNameAndNumber;
    }

    public void setQueryByNameAndNumber(String queryByNameAndNumber) {
        this.queryByNameAndNumber = queryByNameAndNumber;
    }
}
