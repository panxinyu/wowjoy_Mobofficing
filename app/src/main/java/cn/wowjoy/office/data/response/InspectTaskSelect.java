package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2017/12/6.
 */

public class InspectTaskSelect {
    @SerializedName("single")
    private InspectTaskDtlVO single;
    @SerializedName("msg")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public InspectTaskDtlVO getSingle() {
        return single;
    }

    public void setSingle(InspectTaskDtlVO single) {
        this.single = single;
    }
}
