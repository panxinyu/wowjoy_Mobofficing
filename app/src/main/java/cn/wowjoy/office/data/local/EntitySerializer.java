package cn.wowjoy.office.data.local;

/**
 * Created by Sherily on 2017/9/30.
 * Description: serialize entity
 */

public interface EntitySerializer<T> {

    String serialize(T t);

    T deserialize(String string, Class<T> tClass);
}