package cn.wowjoy.office.data.local;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.util.List;

/**
 * Date: 2017/2/27.
 * Created by Sherily on 2017/9/30.
 * Description: green dao Converter use gson
 */

public class ListStringConverter implements PropertyConverter<List<String>, String> {

//    @Inject
    Gson mGson;

    public ListStringConverter() {
        //注入方式未测试，暂且手动创建
        mGson = new Gson();
    }

    @Override
    public List<String> convertToEntityProperty(String databaseValue) {
        return mGson.fromJson(databaseValue, new TypeToken<List<String>>(){}.getType());
    }

    @Override
    public String convertToDatabaseValue(List<String> entityProperty) {
        return mGson.toJson(entityProperty);
    }
}