package cn.wowjoy.office.data.response;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import cn.wowjoy.office.BR;

/**
 * Created by Sherily on 2017/9/11.
 */

public class AppInfo extends BaseObservable{
    private String name;
    private int icon;
    private boolean isShowBdge = false;
    public final int appId;

    private String keyWords;
    private String iaid;
    private String auid;
    private String pmsKey;

    public int getAppId() {
        return appId;
    }

    public String getIaid() {
        return iaid;
    }

    public void setIaid(String iaid) {
        this.iaid = iaid;
    }

    public String getAuid() {
        return auid;
    }

    public void setAuid(String auid) {
        this.auid = auid;
    }

    public String getPmsKey() {
        return pmsKey;
    }

    public void setPmsKey(String pmsKey) {
        this.pmsKey = pmsKey;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
        notifyPropertyChanged(BR.keyWords);
    }

    @Bindable
    public String getKeyWords() {
        return keyWords;
    }

    public AppInfo(String name, int icon, boolean isShowBdge, int appId) {
        this.name = name;
        this.icon = icon;
        this.isShowBdge = isShowBdge;
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public boolean isShowBdge() {
        return isShowBdge;
    }

    public void setShowBdge(boolean showBdge) {
        isShowBdge = showBdge;
    }
}
