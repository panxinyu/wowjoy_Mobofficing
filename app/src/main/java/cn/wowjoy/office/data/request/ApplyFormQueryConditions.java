package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ApplyFormQueryConditions implements Serializable{
    @SerializedName("storageCode")
    private String storageCode;
    @SerializedName("status")
    private String status;
    @SerializedName("startIndex")
    private int startIndex;
    @SerializedName("pageSize")
    private int pageSize;

    @SerializedName("haveDone")
    private String haveDone;

    public ApplyFormQueryConditions(String storageCode, String status, int startIndex, int pageSize) {
        this.storageCode = storageCode;
        this.status = status;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
    }

    public ApplyFormQueryConditions(String storageCode, int startIndex, int pageSize, String haveDone) {
        this.storageCode = storageCode;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
        this.haveDone = haveDone;
    }
}
