package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2017/11/14.
 * Description:
 */

public class GoodsResponse {
    @SerializedName("totalCount")
    private int totalCount;

    @SerializedName("resultList")
    private List<GoodsDetailResponse> resultList;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<GoodsDetailResponse> getResultList() {
        return resultList;
    }

    public void setResultList(List<GoodsDetailResponse> resultList) {
        this.resultList = resultList;
    }
}
