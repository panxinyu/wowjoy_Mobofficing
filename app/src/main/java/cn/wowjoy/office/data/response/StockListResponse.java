package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2018/1/8.
 * Description:
 */

public class StockListResponse {
    @SerializedName("data")
    private List<MaterialSearchResponse> stockList;

    @SerializedName("totalCount")
    private String totalCount;

    public List<MaterialSearchResponse> getStockList() {
        return stockList;
    }

    public void setStockList(List<MaterialSearchResponse> stockList) {
        this.stockList = stockList;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }
}
