package cn.wowjoy.office.data.request;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/4/16.
 */

public class GoodStockRequest implements Serializable{
    private String goodsCode;

    private String inStorageDetailBatchNumber;

    private String inStorageDetailEffectiveDate;

    private String inStorageDetailPrice;

    private String qrCode;


    private String batchNumber;
    private String price;
    private String effectiveDate;

    public String getEffectiveDate() {
        if(null  == effectiveDate){
            return "有效日期: 暂无";
        }
        return "有效日期: "+effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceC() {
        if(null == price){
            return "单价: 暂无";
        }
        return "单价: ￥"+price;
    }

    @Override
    public String toString() {
        return "GoodStockRequest{" +
                "goodsCode='" + goodsCode + '\'' +
                ", inStorageDetailBatchNumber='" + inStorageDetailBatchNumber + '\'' +
                ", inStorageDetailEffectiveDate='" + inStorageDetailEffectiveDate + '\'' +
                ", inStorageDetailPrice='" + inStorageDetailPrice + '\'' +
                ", qrCode='" + qrCode + '\'' +
                '}';
    }
    public String getBatchNumber() {
        if(null == batchNumber){
            return "生产编号: 暂无";
        }
        return "生产编号: "+batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }
    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getInStorageDetailBatchNumber() {
        return inStorageDetailBatchNumber;
    }

    public void setInStorageDetailBatchNumber(String inStorageDetailBatchNumber) {
        this.inStorageDetailBatchNumber = inStorageDetailBatchNumber;
    }

    public String getInStorageDetailEffecticeDate() {
        return inStorageDetailEffectiveDate;
    }

    public void setInStorageDetailEffecticeDate(String inStorageDetailEffecticeDate) {
        this.inStorageDetailEffectiveDate = inStorageDetailEffecticeDate;
    }

    public String getInStorageDetailPrice() {
        return inStorageDetailPrice;
    }

    public void setInStorageDetailPrice(String inStorageDetailPrice) {
        this.inStorageDetailPrice = inStorageDetailPrice;
    }

    public String getQrCode() {
        return qrCode;
    }
    public String getQrCodeC() {
        if(null == qrCode){
           return "二维码编号: 暂无";
        }
        return  "二维码编号: "+qrCode;
    }
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
