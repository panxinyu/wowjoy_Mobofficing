package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/4/16.
 */

public class StockOtherOutSubmitRequest {
    private String applyDepartmentCode;

    private String  demandUser;

    private String storageCode;

    private String remark;

    private String isSuccess;

    private String isConfirm;

    @SerializedName("patientNameView")
    private String patientName;

    @SerializedName("patientName")
    private String patientId;

    @SerializedName("outStorageDetails")
    private List<OutStorageDetail> outStorageList;

    private String sfzdjf;  //是否自动计费
    private String brblhm;  //病人病例号码

    public String getSfzdjf() {
        return sfzdjf;
    }

    public void setSfzdjf(String sfzdjf) {
        this.sfzdjf = sfzdjf;
    }

    public String getBrblhm() {
        return brblhm;
    }

    public void setBrblhm(String brblhm) {
        this.brblhm = brblhm;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getIsConfirm() {
        return isConfirm;
    }

    public void setIsConfirm(String isConfirm) {
        this.isConfirm = isConfirm;
    }

    public String getApplyDepartmentCode() {
        return applyDepartmentCode;
    }

    public void setApplyDepartmentCode(String applyDepartmentCode) {
        this.applyDepartmentCode = applyDepartmentCode;
    }

    public String getDemandUser() {
        return demandUser;
    }

    public void setDemandUser(String demandUser) {
        this.demandUser = demandUser;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<OutStorageDetail> getOutStorageList() {
        return outStorageList;
    }

    public void setOutStorageList(List<OutStorageDetail> outStorageList) {
        this.outStorageList = outStorageList;
    }
}
