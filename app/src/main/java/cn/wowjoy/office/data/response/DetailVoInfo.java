package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DetailVoInfo implements Serializable{
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("detailCode")
    private String detailCode;
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("price")
    private String price;
    @SerializedName("number")
    private String number;
    @SerializedName("sumMoney")
    private String sumMoney;
    @SerializedName("batchNumber")
    private String batchNumber;
    @SerializedName("effectiveDate")
    private String effectiveDate;
    @SerializedName("creator")
    private String creator;
    @SerializedName("gmtCreated")
    private String gmtCreated;
    @SerializedName("modifier")
    private String modifier;
    @SerializedName("gmtModified")
    private String gmtModified;
    @SerializedName("isDeleted")
    private String isDeleted;
    @SerializedName("goodsVo")
    private GoodsDetailResponse goodsVo;
    @SerializedName("canReturn")
    private String canReturn;
    @SerializedName("returnNumber")
    private String returnNumber;
    @SerializedName("qRVOList")
    private List<QRVoInfo> qrvolist;  //申领单的qr集合


    @SerializedName("unit")
    private String unit;
    @SerializedName("specifications")
    private String specifications;
    @SerializedName("sumAmont")
    private String sumAmont;
    @SerializedName("unitName")
    private String unitName;
    @SerializedName("usageCode")
    private String usageCode;
    @SerializedName("goodsName")
    private String goodsName;
    @SerializedName("flowNumber")
    private String flowNumber;
    @SerializedName("isQR")
    private String isQR;
    @SerializedName("demandNumber")
    private String demandNumber;
    @SerializedName("createDatetime")
    private String createDatetime;

    @SerializedName("qRList")
    private List<QRVoInfo> qRList;  //调拨单的qr集合

    public List<QRVoInfo> getqRList() {
        if(null == qRList ){
            qRList = new ArrayList<>();
        }
        return qRList;
    }

    public void setqRList(List<QRVoInfo> qRList) {
        this.qRList = qRList;
    }

    public String getGoodsCode2(){
        if (null == goodsVo){
            return goodsCode;
        } else {
            return goodsVo.getGoodsCode();
        }
    }
    public boolean isNeedQR(){
        if (null != goodsVo){
           return goodsVo.isNeedQR();
        } else {
            if (!TextUtils.isEmpty(isQR) && TextUtils.equals("y",isQR)){
                return true;
            }
            return false;
        }
    }
    public void addqRList(QRVoInfo qrVoInfo){
        if (null == qRList){
            qRList = new ArrayList<>();
        }
        qRList.add(qrVoInfo);
    }

    public void addQRVOList(QRVoInfo qrVoInfo){
        if (null == qrvolist){
            qrvolist = new ArrayList<>();
        }
        qrvolist.add(qrVoInfo);
    }


    public boolean isQRDone(){
        if (null != qRList && !qRList.isEmpty() && qRList.size() == Float.parseFloat(demandNumber))
            return true;
        return false;
    }

    public boolean isQRVODone(){
        if (null != qrvolist && !qrvolist.isEmpty() && qrvolist.size() == Float.parseFloat(number))
            return true;
        return false;
    }


    public String formatGoodsName(){
        if (null != goodsVo){
            return goodsVo.formatName();
        } else {
            if (!TextUtils.isEmpty(goodsName))
                return  goodsName;
            return "暂无";
        }

    }

    public String formatSpecifications(){
        if (null != goodsVo){
            return goodsVo.formatSpecifications2();
        } else {
            if (!TextUtils.isEmpty(specifications))
                return specifications;
            return "暂无";
        }
    }

    public String formatPrice2(){
        if (null != goodsVo){
            return goodsVo.formatPrice3();
        } else {
            return formatPrice3();
        }
    }
    public String formatPrice3(){
        if (!TextUtils.isEmpty(price))
            return "￥" + price;
        return "暂无";
    }

    public String formatUnit(){
        if (null != goodsVo){
            return goodsVo.formatUnit();
        } else {
            if (!TextUtils.isEmpty(unitName))
                return unitName;
            return "暂无";
        }
    }

    public String formatGoodsCode2(){
        if (null != goodsVo){
            return goodsVo.formatGoodsCode2();
        } else {
            if (!TextUtils.isEmpty(goodsCode))
                return goodsCode;
            return "暂无";
        }
    }

    public String formatNumer(){
        if (null != goodsVo){
            if (!TextUtils.isEmpty(number))
                return number;
            return "暂无";
        } else {
            if (!TextUtils.isEmpty(demandNumber))
                return demandNumber;
            return "暂无";
        }
    }
    public String formatSpecifications2(){
        if (null != goodsVo){
            return goodsVo.formatSpecifications();
        } else {
            if (!TextUtils.isEmpty(specifications))
                return "规格型号：" + specifications;
            return "规格型号：暂无";
        }
    }
    public String formatGoodsCode(){
        if (null != goodsVo){
            return goodsVo.formatGoodsCode();
        } else {
            if (!TextUtils.isEmpty(goodsCode))
                return "物资编码：" + goodsCode;
            return "物资编码：暂无";
        }
    }


    public String formatPrice(){
        if (null != goodsVo){
            return goodsVo.formatPrice2();
        } else {
            if (!TextUtils.isEmpty(price)){
                if (!TextUtils.isEmpty(unitName))
                    return "物品单价：￥"+price+"/"+unitName;
                else
                    return "物品单价：￥"+price;

            }
            return "物品单价：暂无";
        }
    }

    public String formatNumber(){
        if (!TextUtils.isEmpty(number))
            return "申领数量：" + number;
        else if (!TextUtils.isEmpty(demandNumber))
            return "申领数量：" + demandNumber;
        else
            return "申领数量：无";
    }

    public String getKeyNumber(){
        if (!TextUtils.isEmpty(number))
            return number;
        else
            return demandNumber;
    }
    private String formatString(String sb){
        String s = "0";
        if (sb.length() > 0)
            s = sb.toString().trim();
        BigDecimal bd = new BigDecimal(s);
        DecimalFormat df = new DecimalFormat("#############0.00");//小数点点不够两位补0，例如："0" --> 0.00（个位数补成0因为传入的是0则会显示成：.00，所以各位也补0；）
        String xs = df.format(bd.setScale(2, BigDecimal.ROUND_DOWN));
        return xs;
    }

    public String formatOutNumber(){
        if (isNeedQR()){
            if (null == goodsVo){
                if (null != qRList && !qRList.isEmpty()){
                    return "出库数量：" + formatString(String.valueOf(qRList.size()));

                }
                return "出库数量：0.00";

            } else {
                if (null != qrvolist && !qrvolist.isEmpty()){
                    return "出库数量：" + formatString(String.valueOf(qrvolist.size()));
                }
                return "出库数量：0.00";
            }
        } else {
            if (!TextUtils.isEmpty(number))
                return "出库数量：" + number;
            else if (!TextUtils.isEmpty(demandNumber))
                return "出库数量：" + demandNumber;
            else
                return "出库数量：无";
        }

    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getSumAmont() {
        return sumAmont;
    }

    public void setSumAmont(String sumAmont) {
        this.sumAmont = sumAmont;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUsageCode() {
        return usageCode;
    }

    public void setUsageCode(String usageCode) {
        this.usageCode = usageCode;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getFlowNumber() {
        return flowNumber;
    }

    public void setFlowNumber(String flowNumber) {
        this.flowNumber = flowNumber;
    }

    public String getIsQR() {
        return isQR;
    }

    public void setIsQR(String isQR) {
        this.isQR = isQR;
    }

    public String getDemandNumber() {
        return demandNumber;
    }

    public void setDemandNumber(String demandNumber) {
        this.demandNumber = demandNumber;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNumber() {
        if(null == number){
            return "暂无";
        }
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(String sumMoney) {
        this.sumMoney = sumMoney;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(String gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public GoodsDetailResponse getGoodsVo() {
        return goodsVo;
    }

    public void setGoodsVo(GoodsDetailResponse goodsVo) {
        this.goodsVo = goodsVo;
    }

    public String getCanReturn() {
        if(null == canReturn){
            return "暂无";
        }
        return canReturn;
    }

    public void setCanReturn(String canReturn) {
        this.canReturn = canReturn;
    }

    public String getReturnNumber() {
        return returnNumber;
    }

    public void setReturnNumber(String returnNumber) {
        this.returnNumber = returnNumber;
    }

    public List<QRVoInfo> getQrvolist() {
        if(null == qrvolist ){
            qrvolist = new ArrayList<>();
        }
        return qrvolist;
    }

    public void setQrvolist(List<QRVoInfo> qrvolist) {
        this.qrvolist = qrvolist;
    }
}
