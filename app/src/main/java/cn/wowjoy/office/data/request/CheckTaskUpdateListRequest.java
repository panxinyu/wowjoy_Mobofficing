package cn.wowjoy.office.data.request;

/**
 * Created by Administrator on 2018/1/24.
 */

public class CheckTaskUpdateListRequest {
    private String qrcode;

    private String billId;

    private String bookQuantity;

    private String inventoryQuantity;

    public CheckTaskUpdateListRequest(String qrcode, String billId, String bookQuantity, String inventoryQuantity) {
        this.qrcode = qrcode;
        this.billId = billId;
        this.bookQuantity = bookQuantity;
        this.inventoryQuantity = inventoryQuantity;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getBookQuantity() {
        return bookQuantity;
    }

    public void setBookQuantity(String bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    public String getInventoryQuantity() {
        return inventoryQuantity;
    }

    public void setInventoryQuantity(String inventoryQuantity) {
        this.inventoryQuantity = inventoryQuantity;
    }
}
