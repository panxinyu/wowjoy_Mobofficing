package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Sherily on 2018/1/17.
 * Description:
 */

public class StockRecordResponse {
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("storageCode")
    private String storageCode;
    @SerializedName("checker")
    private String checker;
    @SerializedName("checkDate")
    private String chechDate;
    @SerializedName("commitUserId")
    private String commitUserId;
    @SerializedName("commitTime")
    private String commitTime;
    @SerializedName("status")
    private String status;
    @SerializedName("isMakeBill")
    private String isMakeBill;
    @SerializedName("suggestion")
    private String suggestion;
    @SerializedName("creator")
    private String creator;
    @SerializedName("gmtCreated")
    private String gmtCreated;
    @SerializedName("modifier")
    private String modifier;
    @SerializedName("gmtModifier")
    private String gmtModifier;
    @SerializedName("isDeleted")
    private String isDeleted;
    @SerializedName("storageName")
    private String storageName;
    @SerializedName("checkerName")
    private String checkerName;
    @SerializedName("statusView")
    private String statusView;
    @SerializedName("instanceId")
    private String instanceId;
    @SerializedName("examineIdMap")
    private String examineIdMap;
    @SerializedName("goods")
    private GoodsDetailResponse goods;
    @SerializedName("profitMoney")
    private String profit;
    @SerializedName("profitNumber")
    private String profitNumber;
    @SerializedName("lossMoney")
    private String loss;
    @SerializedName("lossNumber")
    private String lossNumber;
    @SerializedName("profitLossMoney")
    private String total;
    @SerializedName("profitLossNumber")
    private String goodsCount;
    @SerializedName("goodsProfitLossMoney")
    private String goodsTotal;
    @SerializedName("goodsProfitLossNumber")
    private String count;
    @SerializedName("examineResult")
    private String checkFailMsg;

    private String money;

    @SerializedName("countReportDetailVoList")
    private List<RecordResponse> recordResponses;
    @SerializedName("checkerList")
    private List<CheckerResponse> checkerResponseList;
    private StringBuilder checkers;

    public StringBuilder initCheckers(){
        if (!checkerResponseList.isEmpty()){
            checkers = new StringBuilder();
            for (CheckerResponse response : checkerResponseList){
                if (checkerResponseList.indexOf(response) > 0){
                    checkers.append("、"+response.getName()+"（"+response.getNumber()+"）");
                } else {
                    checkers.append(response.getName()+"（"+response.getNumber()+"）");
                }
            }
        }
        return checkers;
    }

    public void setCheckers(CheckerResponse response) {
        if (null != checkers){
            checkers.append("、"+response.getName()+"（"+response.getNumber()+"）");
        }
    }

    public StringBuilder getCheckers() {
        return checkers;
    }

    public String getProfitNumber() {
        return profitNumber;
    }

    public void setProfitNumber(String profitNumber) {
        this.profitNumber = profitNumber;
    }

    public String getLossNumber() {
        return lossNumber;
    }

    public void setLossNumber(String lossNumber) {
        this.lossNumber = lossNumber;
    }

    private StringBuilder formatChecker(){
        if (null != checkerResponseList && !checkerResponseList.isEmpty()){
            StringBuilder stringBuilder = new StringBuilder();
            for(CheckerResponse response : checkerResponseList){
                if (checkerResponseList.indexOf(response) > 0){
                    stringBuilder.append("、" + response.formatNameNumber());
                } else {
                    stringBuilder.append(response.formatNameNumber());
                }
            }
          return stringBuilder;
        }
       return null;
    }

    public String getCheckFailMsg() {
        return checkFailMsg;
    }

    public void setCheckFailMsg(String checkFailMsg) {
        this.checkFailMsg = checkFailMsg;
    }

    private String formatString(String sb){
        if (!TextUtils.isEmpty(sb)){
            String s = "0";
            if (sb.length() > 0)
                s = sb.toString().trim();
            BigDecimal bd = new BigDecimal(s);
            DecimalFormat df = new DecimalFormat("#############0.00");//小数点点不够两位补0，例如："0" --> 0.00（个位数补成0因为传入的是0则会显示成：.00，所以各位也补0；）
            String xs = df.format(bd.setScale(2, BigDecimal.ROUND_DOWN));
            return xs;
        } else {
            return null;
        }

    }
    public String formatProfit(){
        if (!TextUtils.isEmpty(profit) && Double.parseDouble(profit) > 0){
            return "\u002B"+formatString(profit);
        }
        return formatString(profit);
    }
    public String formatLoss(){
        if (!TextUtils.isEmpty(loss) && Double.parseDouble(loss) > 0){
            return "\u002B"+formatString(loss);
        }
        return formatString(loss);
    }
    public String formatTotal(){
        if ( !TextUtils.isEmpty(total) && Double.parseDouble(total) > 0){
            return "\u002B"+formatString(total);
        }
        return formatString(total);
    }

    public String formatCount(){
        if ( !TextUtils.isEmpty(count) && Double.parseDouble(count) > 0){
            return "\u002B"+formatString(count);
        }
        return formatString(count);
    }

    public String formatGoodsTotal(){
        if ( !TextUtils.isEmpty(goodsTotal) && Double.parseDouble(goodsTotal) > 0){
            return "\u002B"+formatString(goodsTotal);
        }
        return formatString(goodsTotal);
    }

    public String formatGoodsCount(){
        if ( !TextUtils.isEmpty(goodsCount) && Double.parseDouble(goodsCount) > 0){
            return "\u002B"+formatString(goodsCount);
        }
        return formatString(goodsCount);
    }
    public String formatCheckDate(){
        if (!TextUtils.isEmpty(chechDate)){
            return chechDate;
        }
        return getCurrentTime();
    }

    private String getCurrentTime(){
        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);
        return str;
    }
    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getLoss() {
        return loss;
    }

    public void setLoss(String loss) {
        this.loss = loss;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<CheckerResponse> getCheckerResponseList() {
        return checkerResponseList;
    }

    public void setCheckerResponseList(List<CheckerResponse> checkerResponseList) {
        this.checkerResponseList = checkerResponseList;
    }

    public GoodsDetailResponse getGoods() {
        return goods;
    }

    public void setGoods(GoodsDetailResponse goods) {
        this.goods = goods;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public List<RecordResponse> getRecordResponses() {
        return recordResponses;
    }

    public void setRecordResponses(List<RecordResponse> recordResponses) {
        this.recordResponses = recordResponses;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getChechDate() {
        return chechDate;
    }

    public void setChechDate(String chechDate) {
        this.chechDate = chechDate;
    }

    public String getCommitUserId() {
        return commitUserId;
    }

    public void setCommitUserId(String commitUserId) {
        this.commitUserId = commitUserId;
    }

    public String getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(String commitTime) {
        this.commitTime = commitTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsMakeBill() {
        return isMakeBill;
    }

    public void setIsMakeBill(String isMakeBill) {
        this.isMakeBill = isMakeBill;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(String gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getGmtModifier() {
        return gmtModifier;
    }

    public void setGmtModifier(String gmtModifier) {
        this.gmtModifier = gmtModifier;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getCheckerName() {
        return checkerName;
    }

    public void setCheckerName(String checkerName) {
        this.checkerName = checkerName;
    }

    public String getStatusView() {
        return statusView;
    }

    public void setStatusView(String statusView) {
        this.statusView = statusView;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getExamineIdMap() {
        return examineIdMap;
    }

    public void setExamineIdMap(String examineIdMap) {
        this.examineIdMap = examineIdMap;
    }
}
