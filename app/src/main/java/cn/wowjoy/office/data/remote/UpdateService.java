package cn.wowjoy.office.data.remote;

import cn.wowjoy.office.data.response.UpdateInfo;
import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Administrator on 2018/6/14.
 */

public interface UpdateService {

    @GET("panxinyu/wowjoy_Mobofficing_host/raw/master/plugin/update.json")
    Observable<UpdateInfo> checkUpdate();
}
