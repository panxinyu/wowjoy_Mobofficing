package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2018/1/24.
 * Description:
 */

public class AddInventoryResponse {
    @SerializedName("code")
    private String code;
    @SerializedName("name")
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
