package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/11/3.
 */

public class MaterialInspectionInfo implements Serializable {
    private String name;
    private String createData;
    private String number;
    private String taskData;
    private String room;
    private int state;

    @Override
    public String toString() {
        return "MaterialInspectionInfo{" +
                "name='" + name + '\'' +
                ", createData='" + createData + '\'' +
                ", number='" + number + '\'' +
                ", taskData='" + taskData + '\'' +
                ", room='" + room + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public MaterialInspectionInfo(String name, String createData, String number, String taskData, String room, int state) {
        this.name = name;
        this.createData = createData;
        this.number = number;
        this.taskData = taskData;
        this.room = room;
        this.state = state;
    }

    public MaterialInspectionInfo(String name, String createData, String number, String taskData, String room) {
        this.name = name;
        this.createData = createData;
        this.number = number;
        this.taskData = taskData;
        this.room = room;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateData() {
        return createData;
    }

    public void setCreateData(String createData) {
        this.createData = createData;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTaskData() {
        return taskData;
    }

    public void setTaskData(String taskData) {
        this.taskData = taskData;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}
