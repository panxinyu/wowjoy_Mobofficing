package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sherily on 2018/1/25.
 * Description:
 */

public class CheckerResponse implements Serializable {
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("checker")
    private String checker;
    @SerializedName("isChecker")
    private String isChecker;
    @SerializedName("checkId")
    private String checkId;
    @SerializedName("status")
    private String status;
    @SerializedName("userName")
    private String name;
    @SerializedName("number")
    private String number;
    @SerializedName("mainDeptName")
    private String apartment;
    @SerializedName("positionName")
    private String positionName;
    @SerializedName("userId")
    private String userId;
    @SerializedName("mainDept")
    private String mainDept;
    @SerializedName("staffSex")
    private String staffSex;

    public CheckerResponse(String headCode, String checker, String isChecker, String checkId) {
        this.headCode = headCode;
        this.checker = checker;
        this.isChecker = isChecker;
        this.checkId = checkId;
    }

    public String getStaffSex() {
        return staffSex;
    }

    public void setStaffSex(String staffSex) {
        this.staffSex = staffSex;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMainDept() {
        return mainDept;
    }

    public void setMainDept(String mainDept) {
        this.mainDept = mainDept;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public CheckerResponse(String name, String number, String apartment) {
        this.name = name;
        this.number = number;
        this.apartment = apartment;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }
    public String formatDepartmentPosition(){
        if (!TextUtils.isEmpty(apartment)){
            if (!TextUtils.isEmpty(positionName))
                return apartment + positionName;
            return apartment;
        } else if (!TextUtils.isEmpty(positionName)){
            return positionName;
        } else {
            return "暂无";
        }

    }


    public String formatNameNumber(){
        if (!TextUtils.isEmpty(name)){
            if (!TextUtils.isEmpty(number))
                return name+"（"+number+"）";
            else
                return name;
        } else
            return "暂无";
    }

    public String formatShortName(){
        if (!TextUtils.isEmpty(name)){
            if (name.length() > 2){
                return name.substring(name.length() - 2);
            } else
                return name;
        }
        return "未知";
    }

    public String getName() {
        if(null != name && !"".equals(name)){
            return name;
        }
        return "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getIsChecker() {
        return isChecker;
    }

    public void setIsChecker(String isChecker) {
        this.isChecker = isChecker;
    }

    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
