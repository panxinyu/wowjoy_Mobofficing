package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ApplyHeadInfoResponse implements Serializable {
    @SerializedName("headCode")
    private String headCode;
    @SerializedName("applyDepartmentCode")
    private String applyDepartmentCode;
    @SerializedName("mainDepartmentCode")
    private String mainDepartmentCode;
    @SerializedName("applicantId")
    private String applicantId;
    @SerializedName("storageCode")
    private String code;
    @SerializedName("applyDate")
    private String applyDate;
    @SerializedName("status")
    private String status;
    @SerializedName("remark")
    private String remark;
    @SerializedName("creator")
    private String creator;
    @SerializedName("gmtCreated")
    private String gmtCreated;
    @SerializedName("modifier")
    private String modifier;
    @SerializedName("gmtModified")
    private String gmtModified;
    @SerializedName("isDeleted")
    private String isDeleted;
    @SerializedName("patientName")
    private String patientName;
    @SerializedName("applyDepartmentName")
    private String applyDepartmentName;
    @SerializedName("mainDepartmentName")
    private String mainDepartmentName;
    @SerializedName("applicantName")
    private String applicantName;
    @SerializedName("storageName")
    private String storageName;
    @SerializedName("storageType")
    private String storageType;
    @SerializedName("totalAmount")
    private String totalAmount;
    @SerializedName("statusView")
    private String statusView;
    @SerializedName("detailNumber")
    private String detailNumber;
    @SerializedName("detailVoList")
    private List<DetailVoInfo> detailVoInfos;
    @SerializedName("examineId")
    private String examineId;
    @SerializedName("comments")
    private String comments;
    @SerializedName("isConfirm")
    private String isConfirm;

    @SerializedName("totalNumber")
    private String totalCount;

    @SerializedName("allocationDetailList")
    private List<DetailVoInfo> allocationDetailList;

    @SerializedName("incId")
    private String incId;

    @SerializedName("id")
    private String id;

    @SerializedName("allocateType")
    private String allocateType;

    @SerializedName("inStorageCode")
    private String inStorageCode;

    @SerializedName("outStorageCode")
    private String outStorageCode;

    @SerializedName("billStatus")
    private String billStatus;

    @SerializedName("businessStatus")
    private String businessStatus;

    @SerializedName("createUserId")
    private String createUserId;

    @SerializedName("createDeptId")
    private String createDeptId;

    @SerializedName("createDatetime")
    private String createDatetime;

    @SerializedName("createUserName")
    private String createUserName;

    @SerializedName("businessUserName")
    private String businessUserName;

    @SerializedName("businessUserId")
    private String businessUserId;

    @SerializedName("billStatusName")
    private String billStatusName;

    @SerializedName("businessStatusName")
    private String businessStatusName;

    @SerializedName("companyName")
    private String companyName;

    @SerializedName("isInStorage")
    private String isInStorage;

    @SerializedName("failReason")
    private String failReason;

    @SerializedName("createDeptName")
    private String createDeptName;

    @SerializedName("inStorageName")
    private String inStorageName;

    @SerializedName("outStorageName")
    private String outStorageName;


    public String getBusinessUserId() {
        return businessUserId;
    }

    public void setBusinessUserId(String businessUserId) {
        this.businessUserId = businessUserId;
    }

    private boolean iaAllot;
    private boolean isDone;

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public boolean isIaAllot() {
        return iaAllot;
    }

    public void setIaAllot(boolean iaAllot) {
        this.iaAllot = iaAllot;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateDeptName() {
        return createDeptName;
    }

    public void setCreateDeptName(String createDeptName) {
        this.createDeptName = createDeptName;
    }

    public String getInStorageName() {
        return inStorageName;
    }

    public void setInStorageName(String inStorageName) {
        this.inStorageName = inStorageName;
    }

    public String getOutStorageName() {
        return outStorageName;
    }

    public void setOutStorageName(String outStorageName) {
        this.outStorageName = outStorageName;
    }

    public List<DetailVoInfo> getAllocationDetailList() {
        return allocationDetailList;
    }

    public void setAllocationDetailList(List<DetailVoInfo> allocationDetailList) {
        this.allocationDetailList = allocationDetailList;
    }

    public String getIncId() {
        return incId;
    }

    public void setIncId(String incId) {
        this.incId = incId;
    }

    public String getAllocateType() {
        return allocateType;
    }

    public void setAllocateType(String allocateType) {
        this.allocateType = allocateType;
    }

    public String getInStorageCode() {
        return inStorageCode;
    }

    public void setInStorageCode(String inStorageCode) {
        this.inStorageCode = inStorageCode;
    }

    public String getOutStorageCode() {
        return outStorageCode;
    }

    public void setOutStorageCode(String outStorageCode) {
        this.outStorageCode = outStorageCode;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(String createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getBusinessUserName() {
        return businessUserName;
    }

    public void setBusinessUserName(String businessUserName) {
        this.businessUserName = businessUserName;
    }

    public String getBillStatusName() {
        return billStatusName;
    }

    public void setBillStatusName(String billStatusName) {
        this.billStatusName = billStatusName;
    }

    public String getBusinessStatusName() {
        return businessStatusName;
    }

    public void setBusinessStatusName(String businessStatusName) {
        this.businessStatusName = businessStatusName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIsInStorage() {
        return isInStorage;
    }

    public void setIsInStorage(String isInStorage) {
        this.isInStorage = isInStorage;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String formatTotalCount(){
        String ss = "";
        if (iaAllot){
            if (isDone){
                ss = "总出库数量：";
            } else {
                ss = "总未出数量：";
            }
        } else {
            if (isDone){
                ss = "总出库数量：";
            } else {
                ss = "总申领数量：";
            }
        }
        if (!TextUtils.isEmpty(totalCount))
            return ss + totalCount;
        return ss + "无";

    }

    public String formatApplicantName(){
        if (null !=applicantId){
            if (!TextUtils.isEmpty(applicantName)){
                return "需求人：" + applicantName;
            }
            return "需求人：无";
        } else {
            if (!TextUtils.isEmpty(inStorageName))
                return "调入仓库：" + inStorageName;
            return "调入仓库：无";
        }

    }

    public String formatPatientName(){
        if (null != applicantId){
            if (!TextUtils.isEmpty(patientName)){
                return "患者姓名：" + patientName;
            }
             return "患者姓名：无";
        } else {
            if (!TextUtils.isEmpty(outStorageName)){
                return "调出仓库：" + outStorageName;
            }
            return "调出仓库：无";
        }

    }

    public String formatApplyDepartmentName(){
        if (null != applicantId){
            if (!TextUtils.isEmpty(applyDepartmentName)){
                return "需求科室：" + applyDepartmentName;
            }
            return "需求科室：无";
        } else {
            if (!TextUtils.isEmpty(businessUserId)){
                return "业务员：" + businessUserId;
            }
            return "业务员：无";
        }

    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = headCode;
    }

    public String getApplyDepartmentCode() {
        return applyDepartmentCode;
    }

    public void setApplyDepartmentCode(String applyDepartmentCode) {
        this.applyDepartmentCode = applyDepartmentCode;
    }

    public String getMainDepartmentCode() {
        return mainDepartmentCode;
    }

    public void setMainDepartmentCode(String mainDepartmentCode) {
        this.mainDepartmentCode = mainDepartmentCode;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(String gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getApplyDepartmentName() {
        return applyDepartmentName;
    }

    public void setApplyDepartmentName(String applyDepartmentName) {
        this.applyDepartmentName = applyDepartmentName;
    }

    public String getMainDepartmentName() {
        return mainDepartmentName;
    }

    public void setMainDepartmentName(String mainDepartmentName) {
        this.mainDepartmentName = mainDepartmentName;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getStatusView() {
        return statusView;
    }

    public void setStatusView(String statusView) {
        this.statusView = statusView;
    }

    public String getDetailNumber() {
        return detailNumber;
    }

    public void setDetailNumber(String detailNumber) {
        this.detailNumber = detailNumber;
    }

    public List<DetailVoInfo> getDetailVoInfos() {
        return detailVoInfos;
    }

    public void setDetailVoInfos(List<DetailVoInfo> detailVoInfos) {
        this.detailVoInfos = detailVoInfos;
    }

    public String getExamineId() {
        return examineId;
    }

    public void setExamineId(String examineId) {
        this.examineId = examineId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIsConfirm() {
        return isConfirm;
    }

    public void setIsConfirm(String isConfirm) {
        this.isConfirm = isConfirm;
    }
}
