package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2018/1/8.
 * Description:
 */

public class StockListRequest {
    @SerializedName("storageCodeSearch")
    private String storageCodeSearch;
    @SerializedName("useSearch")
    private String useSearch;
    @SerializedName("mixSearch")
    private String mixSearch;
    @SerializedName("startIndex")
    private int startIndex;
    @SerializedName("pageSize")
    private int pageSize;
    @SerializedName("typeListFuzzySearch")
    private List<String> typeListSearch;

    public StockListRequest(String storageCodeSearch, String useSearch, String mixSearch, int startIndex, int pageSize, List<String> typeListSearch) {
        this.storageCodeSearch = storageCodeSearch;
        this.useSearch = useSearch;
        this.mixSearch = mixSearch;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
        this.typeListSearch = typeListSearch;
    }

    public String getStorageCodeSearch() {
        return storageCodeSearch;
    }

    public void setStorageCodeSearch(String storageCodeSearch) {
        this.storageCodeSearch = storageCodeSearch;
    }

    public String getUseSearch() {
        return useSearch;
    }

    public void setUseSearch(String useSearch) {
        this.useSearch = useSearch;
    }

    public String getMixSearch() {
        return mixSearch;
    }

    public void setMixSearch(String mixSearch) {
        this.mixSearch = mixSearch;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<String> getTypeListSearch() {
        return typeListSearch;
    }

    public void setTypeListSearch(List<String> typeListSearch) {
        this.typeListSearch = typeListSearch;
    }
}
