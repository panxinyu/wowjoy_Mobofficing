package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2017/12/1.
 */

public class StaffListResponse {
    @SerializedName("staffList")
    private List<StaffResponse> staffResponses;

    public List<StaffResponse> getStaffResponses() {
        return staffResponses;
    }

    public void setStaffResponses(List<StaffResponse> staffResponses) {
        this.staffResponses = staffResponses;
    }
}
