package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/4/17.
 */

public class StockApplyDemandHeadListRequest {
    @SerializedName("storage")
    private String storageCode;

    private String allFinish;

    private String toBeOut;

    private int startIndex;

    private int pageSize;

    public StockApplyDemandHeadListRequest(String storageCode, String allFinish, String toBeOut, int startIndex, int pageSize) {
        this.storageCode = storageCode;
        this.allFinish = allFinish;
        this.toBeOut = toBeOut;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
    }

    public String getAllFinish() {
        return allFinish;
    }

    public void setAllFinish(String allFinish) {
        this.allFinish = allFinish;
    }

    public String getToBeOut() {
        return toBeOut;
    }

    public void setToBeOut(String toBeOut) {
        this.toBeOut = toBeOut;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }



}
