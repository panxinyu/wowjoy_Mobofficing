package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2018/1/24.
 * Description:
 */

public class AddInventoryRequest {
    @SerializedName("storageCode")
    private String storageCode;
    @SerializedName("use")
    private String use;

    public AddInventoryRequest(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }
}
