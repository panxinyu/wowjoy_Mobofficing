package cn.wowjoy.office.data.remote;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by Sherily on 2017/11/29.
 * Description:
 */

public class NullOnEmptyConverterFactory extends Converter.Factory {
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
        return new Converter<ResponseBody, Object>() {
            @Override
            public Object convert(ResponseBody body) throws IOException {
                if (body == null || body.contentLength() == 0) return null;
                return delegate.convert(body);
            }
        };
    }
//        //判断当前的类型是否是我们需要处理的类型
//        if (ResultEmpty.class.equals(type)) {
//            //是则创建一个Converter返回转换数据
//            return new Converter<ResponseBody, ResultEmpty>() {
//                @Override
//                public ResultEmpty convert(ResponseBody value) throws IOException {
//                    //这里直接返回null是因为我们不需要使用到响应体,本来也没有响应体.
//                    //返回的对象会存到response.body()里.
//                    return null;
//                }
//            };
//        }
//        return null;
//
//    }
}

