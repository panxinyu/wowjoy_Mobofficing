package cn.wowjoy.office.data.mock;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

//import cn.wowjoy.office.BR;

/**
 * Created by Sherily on 2017/11/2.
 */

public class PopuModel extends BaseObservable {
    @SerializedName("dictId")//状态编码
    private String id;
    @SerializedName("dictTypeValue")//分类编码
    private String dictTypeValue;
    @SerializedName("dictTypeName")//分类名称
    private String dictTypeName;
    @SerializedName("dictDataValue")//字典编码
    private String dictDataValue;
    @SerializedName("dictDataId")//字典编码
    private String dictDataId;
    @SerializedName("dictDataName")//状态
    private String dictDataName;
    @SerializedName("name")
    private String name;
    @SerializedName("code")
    private String code;
    @SerializedName("storageCode")
    private String storageCode;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDictDataId() {
        return dictDataId;
    }

    public void setDictDataId(String dictDataId) {
        this.dictDataId = dictDataId;
    }

    public String getDictTypeValue() {
        return dictTypeValue;
    }

    public void setDictTypeValue(String dictTypeValue) {
        this.dictTypeValue = dictTypeValue;
    }

    public String getDictTypeName() {
        return dictTypeName;
    }

    public void setDictTypeName(String dictTypeName) {
        this.dictTypeName = dictTypeName;
    }

    public String getDictDataValue() {
        return dictDataValue;
    }

    public void setDictDataValue(String dictDataValue) {
        this.dictDataValue = dictDataValue;
    }

    public String getDictDataName() {
        return dictDataName;
    }

    public void setDictDataName(String dictDataName) {
        this.dictDataName = dictDataName;
    }

    private boolean isSelected = false;
    private String status;

    public String getCode() {
        if (!TextUtils.isEmpty(storageCode) )
            setCode(storageCode);
        return code;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public PopuModel(String name) {
        this.name = name;
    }

    public PopuModel(String name, boolean isSelected) {
        this.name = name;
        this.isSelected = isSelected;
    }

    public PopuModel(String name, boolean isSelected, String status) {
        this.name = name;
        this.isSelected = isSelected;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        if (!TextUtils.isEmpty(dictDataName))
            setName(dictDataName);
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
//        notifyPropertyChanged(BR.isSelected);
    }
}
