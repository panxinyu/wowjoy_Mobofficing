package cn.wowjoy.office.data.request;

/**
 * Created by Administrator on 2017/11/17.
 */

public class InspectionDetailRequest {
    private String id;

    private String billId;

    private String taskDtlId;

    private String taskId;

    private String appearanceStatus;

    private String assetsStatus;

    private String remarks;

    private String isExist;

   private String inspectAddress;

   private String inspectUserId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getTaskDtlId() {
        return taskDtlId;
    }

    public void setTaskDtlId(String taskDtlId) {
        this.taskDtlId = taskDtlId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getAppearanceStatus() {
        return appearanceStatus;
    }

    public void setAppearanceStatus(String appearanceStatus) {
        this.appearanceStatus = appearanceStatus;
    }

    public String getAssetsStatus() {
        return assetsStatus;
    }

    public void setAssetsStatus(String assetsStatus) {
        this.assetsStatus = assetsStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsExist() {
        return isExist;
    }

    public void setIsExist(String isExist) {
        this.isExist = isExist;
    }

    public String getInspectAddress() {
        return inspectAddress;
    }

    public void setInspectAddress(String inspectAddress) {
        this.inspectAddress = inspectAddress;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }
}
