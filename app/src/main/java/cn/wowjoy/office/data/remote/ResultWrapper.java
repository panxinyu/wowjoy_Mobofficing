package cn.wowjoy.office.data.remote;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/10/9.
 * Description: remote data wrapper
 */

public class ResultWrapper<T> {

    public static final String CODE_SUCCESS = "0";

    @SerializedName("returnCode")
    private int returnCode;

    @SerializedName("returnMsg")
    private String returnMsg;

    @SerializedName("data")
    @Nullable
    private T data;

    /**
     * 判断请求数据是否成功
     */
    public boolean isSuccess() {
        return 0 == returnCode;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    @Nullable
    public T getData() {
        return data;
    }
}
