package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import org.w3c.dom.ProcessingInstruction;

import cn.wowjoy.office.utils.TextUtil;

/**
 * Created by Sherily on 2017/12/28.
 * Description:
 */

public class MaterialSearchResponse {
    @SerializedName("amount")
    private String amount;
    @SerializedName("batchNumber")
    private String batchNumber;
    @SerializedName("creator")
    private String creator;
    @SerializedName("effecitiveDate")
    private String effecitiveDate;
    @SerializedName("flowNumber")
    private String flowNumber;
    @SerializedName("gmtCreated")
    private String gmtCreated;
    @SerializedName("gmtModified")
    private String gmtModified;
    @SerializedName("goods")
    private GoodsDetailResponse goods;
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("incId")
    private String incId;
    @SerializedName("inData")
    private String inData;
    @SerializedName("isDelete")
    private String isDelete;
    @SerializedName("isDeleted")
    private String isDeleted;
    @SerializedName("isLocked")
    private String isLocked;
    @SerializedName("lineNumber")
    private String lineNumber;
    @SerializedName("modifier")
    private String modifier;
    @SerializedName("price")
    private String price;
    @SerializedName("returnNumber")
    private String returnNumber;
    @SerializedName("stockId")
    private String stockId;
    @SerializedName("storageCode")
    private String storageCode;
    @SerializedName("storageName")
    private String storageName;
    @SerializedName("toBein")
    private String toBein;
    @SerializedName("today")
    private String today;
    @SerializedName("userName")
    private String userName;
    @SerializedName("warnDays")
    private String warnDays;
    @SerializedName("warnStatusView")
    private String warnStatusView;

    public String formatTotal(){
        if (!TextUtils.isEmpty(amount)){
            if (!TextUtils.isEmpty(goods.getUnitView()))
                return amount+goods.getUnitView();
            else
                return amount;
        }

        return "无库存";
    }

    public String formatBatchNumber(){
        if (!TextUtils.isEmpty(batchNumber))
            return "生产批号："+batchNumber;
        return "生产批号：无";
    }

    public String formatInDate(){
        if (!TextUtils.isEmpty(effecitiveDate))
            return "有效期："+effecitiveDate;
        return "有效期：无";
    }
    public String formatStock(){
        if (!TextUtils.isEmpty(amount))
            return "库存："+amount;
        return "库存：无";
    }
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getEffecitiveDate() {
        return effecitiveDate;
    }

    public void setEffecitiveDate(String effecitiveDate) {
        this.effecitiveDate = effecitiveDate;
    }

    public String getFlowNumber() {
        return flowNumber;
    }

    public void setFlowNumber(String flowNumber) {
        this.flowNumber = flowNumber;
    }

    public String getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(String gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(String gmtModified) {
        this.gmtModified = gmtModified;
    }

    public GoodsDetailResponse getGoods() {
        return goods;
    }

    public void setGoods(GoodsDetailResponse goods) {
        this.goods = goods;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getIncId() {
        return incId;
    }

    public void setIncId(String incId) {
        this.incId = incId;
    }

    public String getInData() {
        return inData;
    }

    public void setInData(String inData) {
        this.inData = inData;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(String isLocked) {
        this.isLocked = isLocked;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getReturnNumber() {
        return returnNumber;
    }

    public void setReturnNumber(String returnNumber) {
        this.returnNumber = returnNumber;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getStorageCode() {
        return storageCode;
    }

    public void setStorageCode(String storageCode) {
        this.storageCode = storageCode;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getToBein() {
        return toBein;
    }

    public void setToBein(String toBein) {
        this.toBein = toBein;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWarnDays() {
        return warnDays;
    }

    public void setWarnDays(String warnDays) {
        this.warnDays = warnDays;
    }

    public String getWarnStatusView() {
        return warnStatusView;
    }

    public void setWarnStatusView(String warnStatusView) {
        this.warnStatusView = warnStatusView;
    }

    private String name;

    private String type;

    private String stock;

    public MaterialSearchResponse(String name, String type, String stock) {
        this.name = name;
        this.type = type;
        this.stock = stock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
