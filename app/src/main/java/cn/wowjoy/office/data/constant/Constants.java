package cn.wowjoy.office.data.constant;


import android.os.Environment;

import okhttp3.MediaType;

/**
 * Created by Sherily on 2017/11/15.
 * Description:
 */

public interface Constants {
    //最大的图片选择数
    String MAX_SELECT_COUNT = "max_select_count";
    //是否单选
    String IS_SINGLE = "is_single";
    //初始位置
    String POSITION = "position";

    //初始位置
    String IS_CONFIRM = "is_confirm";

    int RESULT_CODE = 0x00000012;
    int EIDT_CODE = 0x00000015;
    int TAKEpHOTO_CODE = 0x00000013;
    int REFRESH_CODE = 0x00000014;

    int RESULT_OK = 101;
    int RESULT_FAIL = 102;

    int REQUEST_CODE = 103;
    int REQUEST_CODE_ADD = 104;
    int REQUEST_CODE_EDIT = 105;
    int REQUEST_CODE_PATIENT = 106;

    //其他
    int OTHER_CODE_APPLY = 200;
    //申领
    int RECEIPTS_CODE_APPLY = 201;
    //申请
    int RECEIPTS_CODE_APPLY_FOR = 202;
    //调拨
    int RECEIPTS_CODE_ALLOT = 203;


    String BAR_ADD = "SYSTEM_BAR_READ";
    String BAR_KEY = "BAR_VALUE";
    String RFID_ADD = "SYSTEM_RFID_READ";
    String RFID_KEY = "RFID_VALUE";
    String WOW_ADD = "SYSTEM_WOW_READ";
    String WOW_KEY = "com.motorolasolutions.emdk.datawedge.data_string";
    String WOW_NEW_ADD = "com.android.receive_scan_action";
    String WOW_NEW_KEY = "data";

    String GOODS_CODE = "GOODS_CODE";

    String JSON_CONTENT_TYPE = "application/json; charset=UTF-8";

    MediaType MEDIA_TYPE_JSON = MediaType.parse(JSON_CONTENT_TYPE);

    String EMPTY_JSON = "{}";
    String PERSONAL_INFO = "personal_info";
    String IS_NEED_REFRESH = "is_need_refresh";

    String UPLOAD_NORMAL_URL = "https://test-gateway.rubikstack.com/zuul/ceph/v1/buckets/objects";

    //------------ PM相关 ---------------------------//
    String PM_SEARCH = "PM_SEARCH";
    int REPORT_SEARCH = 101;
    int MANAGER_SEARCH = 100;

    //PM报告详情 状态
    String PM_REPORT = "PM_REPORT";
    int ADD_REPORT = 1;
    int EDIT_REPORT = 2;
    int LOOK_REPORT = 3;


    String BASE_PATH_PIC = Environment.getExternalStorageDirectory().getPath() + "/wowjoy/mob/pic/";
}