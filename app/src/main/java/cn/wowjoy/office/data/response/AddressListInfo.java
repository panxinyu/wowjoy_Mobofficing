package cn.wowjoy.office.data.response;

/**
 * Created by Administrator on 2017/10/18.
 */

public class AddressListInfo  {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressListInfo(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AddressListInfo{" +
                "name='" + name + '\'' +
                '}';
    }
}
