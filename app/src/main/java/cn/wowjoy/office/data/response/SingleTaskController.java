package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2017/11/20.
 */

public class SingleTaskController implements Serializable{
    /**
     * 巡检任务ID
     */
    @SerializedName("id")
    private String  id;
    /**
     * 巡检任务编码
     */
    @SerializedName("billNo")
    private String billNo;
    /**
     * 巡检任务名称
     */
    @SerializedName("billName")
    private String billName;
    /**
     * 巡检人
     */
    @SerializedName("inspectUserName")
    private String inspectUserName;
    /**
     * 使用科室
     */
    @SerializedName("useDepartmentName")
    private String useDepartmentName;
    /**
     * 使用科室
     */
    @SerializedName("useDepartmentId")
    private String useDepartmentId;
    /**
     * 巡检时间
     */
    @SerializedName("inspectDatetime")
    private String inspectDatetime;
    /**
     * 巡检任务明细集合
     */
    @SerializedName("dtlListPDA")
    private List<InspectTaskDtlVO> dtlListPDA;

    /**
     *
     */
    @SerializedName("inspectRecordId")
    private String inspectRecordId;

    @SerializedName("totalCount")
    private int totalCount;

    private String isDone; //当前单子是否全部完成

    private String  contentNote;  //巡检内容/问题
    private String  improvement; //改进措施
    private String  summary;     //巡检总结

    public String getContentNote() {
        return contentNote;
    }

    public void setContentNote(String contentNote) {
        this.contentNote = contentNote;
    }

    public String getImprovement() {
        return improvement;
    }

    public void setImprovement(String improvement) {
        this.improvement = improvement;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getIsDone() {
        return isDone;
    }

    public void setIsDone(String isDone) {
        this.isDone = isDone;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getInspectRecordId() {
        return inspectRecordId;
    }

    public void setInspectRecordId(String inspectRecordId) {
        this.inspectRecordId = inspectRecordId;
    }

    public String getUseDepartmentId() {
        return useDepartmentId;
    }

    public void setUseDepartmentId(String useDepartmentId) {
        this.useDepartmentId = useDepartmentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillName() {
        return billName;
    }

    public void setBillName(String billName) {
        this.billName = billName;
    }

    public String getInspectUserName() {
        return inspectUserName;
    }

    public void setInspectUserName(String inspectUserName) {
        this.inspectUserName = inspectUserName;
    }

    public String getInspectDatetime() {
        return inspectDatetime;
    }

    public void setInspectDatetime(String inspectDatetime) {
        this.inspectDatetime = inspectDatetime;
    }

    public String getUseDepartmentName() {
        return useDepartmentName;
    }

    public void setUseDepartmentName(String useDepartmentName) {
        this.useDepartmentName = useDepartmentName;
    }

    public String getInspecDatetime() {
        return inspectDatetime;
    }

    public void setInspecDatetime(String inspecDatetime) {
        this.inspectDatetime = inspecDatetime;
    }

    public List<InspectTaskDtlVO> getDtlListPDA() {
        return dtlListPDA;
    }

    public void setDtlListPDA(List<InspectTaskDtlVO> dtlListPDA) {
        this.dtlListPDA = dtlListPDA;
    }

    @Override
    public String toString() {
        return "SingleTaskController{" +
                "id='" + id + '\'' +
                ", billNo='" + billNo + '\'' +
                ", billName='" + billName + '\'' +
                ", inspectUserName='" + inspectUserName + '\'' +
                ", useDepartmentName='" + useDepartmentName + '\'' +
                ", inspectDatetime='" + inspectDatetime + '\'' +
                ", dtlListPDA=" + dtlListPDA +
                '}';
    }
}
