package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/11/17.
 */

public class InspectTaskDtlVO implements Serializable{

    /**
     * 设备编码
     */
    private String cardNo;
    /**
     * 卡片名称
     */
     private String cardName;
    /**
     * 巡检任务明细ID
     */
     private String id;
    /**
     * 巡检任务ID
     */
    private String billId;
    /**
     *  巡检任务ID
     */
    private String recordId;
    /**
     * 外观状态
     */
    private String appearanceStatus;

    private String labelStatus;  //标签齐全
    private String functionStatus; //功能检查
    private String cleanStatus; //设备清洁

    /**
     * 设备状态  y正常  n停用  m维修
     */
    private String assetsStatus;
    /**
     * 是否存在   y/n
     */
    private String isExist;

    private String inspectRecordId;

    private String inspectRecordDtlId;

    private String pictureId;  //原图ID
    private String thumbnailId; //缩率图Id

    private String produceNo;

    private String remarks;

    private String taskDtlId;

    private String brand;

    private String assetsSpec;

    private String inspectDatetime;
    private String localPath;
    /**
     *  大图附件URL
     */
    @SerializedName("pictureIdUrl")
    private String pictureIdUrl;

    /**
     * 查看URL
     */
    @SerializedName("assetsPictureIdUrl")
    private String assetsPictureIdUrl;
    private String assetsPictureId;

    /**
     * 小图ID
     */
    @SerializedName("thumbnailIdUrl")
    private String thumbnailIdUrl;
    /**
     * 二维码
     */
    @SerializedName("qrCode")
    private String qrCode;

    public String getAssetsPictureId() {
        return assetsPictureId;
    }

    public void setAssetsPictureId(String assetsPictureId) {
        this.assetsPictureId = assetsPictureId;
    }

    public String getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(String thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    public String getLabelStatus() {
        return labelStatus;
    }

    public void setLabelStatus(String labelStatus) {
        this.labelStatus = labelStatus;
    }

    public String getFunctionStatus() {
        return functionStatus;
    }

    public void setFunctionStatus(String functionStatus) {
        this.functionStatus = functionStatus;
    }

    public String getCleanStatus() {
        return cleanStatus;
    }

    public void setCleanStatus(String cleanStatus) {
        this.cleanStatus = cleanStatus;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getThumbnailIdUrl() {
        return thumbnailIdUrl;
    }

    public void setThumbnailIdUrl(String thumbnailIdUrl) {
        this.thumbnailIdUrl = thumbnailIdUrl;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getPictureIdUrl() {
        return pictureIdUrl;
    }

    public void setPictureIdUrl(String pictureIdUrl) {
        this.pictureIdUrl = pictureIdUrl;
    }

    public String getAssetsPictureIdUrl() {
        return assetsPictureIdUrl;
    }

    public void setAssetsPictureIdUrl(String assetsPictureIdUrl) {
        this.assetsPictureIdUrl = assetsPictureIdUrl;
    }
    public String getInspectDatetime() {
        return inspectDatetime;
    }

    public void setInspectDatetime(String inspectDatetime) {
        this.inspectDatetime = inspectDatetime;
    }

    public String getInspectRecordId() {
        return inspectRecordId;
    }

    public void setInspectRecordId(String inspectRecordId) {
        this.inspectRecordId = inspectRecordId;
    }

    public String getInspectRecordDtlId() {
        return inspectRecordDtlId;
    }

    public void setInspectRecordDtlId(String inspectRecordDtlId) {
        this.inspectRecordDtlId = inspectRecordDtlId;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public String getProduceNo() {
        return produceNo;
    }

    public void setProduceNo(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTaskDtlId() {
        return taskDtlId;
    }

    public void setTaskDtlId(String taskDtlId) {
        this.taskDtlId = taskDtlId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getAssetsSpec() {
        return assetsSpec;
    }

    public void setAssetsSpec(String assetsSpec) {
        this.assetsSpec = assetsSpec;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getAppearanceStatus() {
        return appearanceStatus;
    }

    public void setAppearanceStatus(String appearanceStatus) {
        this.appearanceStatus = appearanceStatus;
    }

    public String getAssetsStatus() {
        return assetsStatus;
    }

    public void setAssetsStatus(String assetsStatus) {
        this.assetsStatus = assetsStatus;
    }

    public String getIsExist() {
        return isExist;
    }

    public void setIsExist(String isExist) {
        this.isExist = isExist;
    }
}
