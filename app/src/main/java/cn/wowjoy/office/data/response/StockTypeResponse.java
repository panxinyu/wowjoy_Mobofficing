package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2018/1/5.
 * Description:
 */

public class StockTypeResponse {
    @SerializedName("leafCount")
    private String leafCount;
    @SerializedName("tree")
    private List<Type> trees;

    public String getLeafCount() {
        return leafCount;
    }

    public void setLeafCount(String leafCount) {
        this.leafCount = leafCount;
    }

    public List<Type> getTrees() {
        return trees;
    }

    public void setTrees(List<Type> trees) {
        this.trees = trees;
    }
}
