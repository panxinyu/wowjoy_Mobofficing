package cn.wowjoy.office.data.response;

/**
 * Created by Administrator on 2018/1/15.
 */

public class InventoryTotalItemInfo {
    private String id;

    private String billNo;

    private String assetsUseName;

    private String inventoryStartDatetime;

    private String inventoryEndDatetime;

    private String departmentName;

    private String createDatetime;

    private String inventoryStatus;

    private String inventoryStatusName;

    private boolean isDone;

    public boolean isDone() {
        if (null != inventoryStatus || !"".equals(inventoryStatus)) {
            return "24-2".equals(inventoryStatus);
        }
        return false;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getAssetsUseName() {
        return assetsUseName;
    }

    public void setAssetsUseName(String assetsUseName) {
        this.assetsUseName = assetsUseName;
    }

    public String getInventoryStartDatetime() {
        return inventoryStartDatetime;
    }

    public void setInventoryStartDatetime(String inventoryStartDatetime) {
        this.inventoryStartDatetime = inventoryStartDatetime;
    }

    public String getInventoryEndDatetime() {
        return inventoryEndDatetime;
    }

    public void setInventoryEndDatetime(String inventoryEndDatetime) {
        this.inventoryEndDatetime = inventoryEndDatetime;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }

    public void setCreateDatetime(String createDatetime) {
        this.createDatetime = createDatetime;
    }

    public String getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(String inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public String getInventoryStatusName() {
        return inventoryStatusName;
    }

    public void setInventoryStatusName(String inventoryStatusName) {
        this.inventoryStatusName = inventoryStatusName;
    }

    public String getCheckDate() {
        return inventoryStartDatetime + " --- " + inventoryEndDatetime;
    }
}
