package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/4/17.
 */

public class StockApplyDemandHeadListResponse {
    private int totalCount;

    @SerializedName("list")
    private List<StockApplyDemandHeadListSingle> mList;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<StockApplyDemandHeadListSingle> getList() {
        return mList;
    }

    public void setList(List<StockApplyDemandHeadListSingle> list) {
        mList = list;
    }
}
