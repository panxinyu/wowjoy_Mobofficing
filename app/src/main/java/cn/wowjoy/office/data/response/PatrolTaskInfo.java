package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/11/6.
 */

public class PatrolTaskInfo implements Serializable{
    private String id;
    private String time;
    private String device;
    private String  state;

    @Override
    public String toString() {
        return "PatrolTaskInfo{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", device='" + device + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    public PatrolTaskInfo(String id, String time, String device, String state) {
        this.id = id;
        this.time = time;
        this.device = device;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
