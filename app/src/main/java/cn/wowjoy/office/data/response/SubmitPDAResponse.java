package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2017/11/22.
 */

public class SubmitPDAResponse {
    @SerializedName("auid")
    private String auid;

    public String getAuid() {
        return auid;
    }

    public void setAuid(String auid) {
        this.auid = auid;
    }
}
