package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherily on 2017/11/14.
 * Description:
 */

public class GoodsRequest {

    //模糊查询，n:非停用，y:停用，不传全查
    @SerializedName("isStopSearch")
    private String isStopSearch;

    //模糊查询
    @SerializedName("mixSerach")
    private String mixSerach;

    @SerializedName("startIndex")
    private int startIndex;

    @SerializedName("pageSize")
    private int pageSize;

    public GoodsRequest(String isStopSearch, String mixSerach, int startIndex, int pageSize) {
        this.isStopSearch = isStopSearch;
        this.mixSerach = mixSerach;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
    }

    public GoodsRequest(String mixSerach, int startIndex, int pageSize) {
        this.mixSerach = mixSerach;
        this.startIndex = startIndex;
        this.pageSize = pageSize;
    }


    public String getIsStopSearch() {
        return isStopSearch;
    }

    public void setIsStopSearch(String isStopSearch) {
        this.isStopSearch = isStopSearch;
    }

    public String getMixSerach() {
        return mixSerach;
    }

    public void setMixSerach(String mixSerach) {
        this.mixSerach = mixSerach;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
