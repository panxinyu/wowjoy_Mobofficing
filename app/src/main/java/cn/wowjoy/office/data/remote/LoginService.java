package cn.wowjoy.office.data.remote;


import cn.wowjoy.office.BuildConfig;
import cn.wowjoy.office.data.mock.MovieInfoModel;
import cn.wowjoy.office.data.response.LoginInfo;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LoginService {
//    String BASE_URL = PreferenceManager.getInstance().getLoginHost();
//
//    @FormUrlEncoded
//    @Headers("Authorization:Basic ZjNjZTI3MzI4NzBlNDJmOWJkZjgwYzMxOWNkYzYxYTA6Yjk2Mjk3OTU3MzBjNGZjNDk1NjNhMzdiMzlmZTFmYmU=")
////    @Headers("Authorization:Basic Y2xpOnNlYw==")
//    @POST("oauth/token")
//    Observable<LoginInfo> login(@Field("grant_type") String type, @Field("username") String username, @Field("password") String password);

    /**
     * {
     * "rating": {
     * "max": 10,
     * "average": 7.4,
     * "stars": "40",
     * "min": 0
     * },
     * "reviews_count": 295,
     * "wish_count": 15213,
     * "douban_site": "",
     * "year": "2009",
     * "images": {
     * "small": "https://img1.doubanio.com/view/movie_poster_cover/ipst/public/p494268647.jpg",
     * "large": "https://img1.doubanio.com/view/movie_poster_cover/lpst/public/p494268647.jpg",
     * "medium": "https://img1.doubanio.com/view/movie_poster_cover/spst/public/p494268647.jpg"
     * }
     *
     * @param movieId 电影ID  测试用：25937854
     * @return
     */
    @GET("/v2/movie/subject"+"/{id}")
    Observable<MovieInfoModel> getMovieInfo(@Path("id") String movieId);

    @FormUrlEncoded
    @Headers(BuildConfig.AUTHOR)
    @POST("oauth/token")
    Observable<LoginInfo> loginw(@Field("grant_type") String type, @Field("username") String username, @Field("password") String password,@Field("mdid") String mdid);


    @FormUrlEncoded
    @Headers("Authorization:Basic Y2xpOnNlYw==")
    @POST("oauth/token")
    Observable<LoginInfo> login(@Field("grant_type") String type, @Field("username") String username, @Field("password") String password);


}
