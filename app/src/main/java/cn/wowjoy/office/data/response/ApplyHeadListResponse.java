package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApplyHeadListResponse {
    @SerializedName("totalPage")
    private String totalPage;
    @SerializedName("startIndex")
    private String startIndex;
    @SerializedName("pageSize")
    private String pageSize;
    @SerializedName("totalCount")
    private int totalCount;
    @SerializedName("count")
    private int count;
    @SerializedName("list")
    private List<ApplyHeadInfoResponse> list;



    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;

    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<ApplyHeadInfoResponse> getList() {
        return list;
    }

    public void setList(List<ApplyHeadInfoResponse> list) {
        this.list = list;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(String startIndex) {
        this.startIndex = startIndex;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
