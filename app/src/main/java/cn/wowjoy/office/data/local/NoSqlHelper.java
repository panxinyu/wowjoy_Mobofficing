package cn.wowjoy.office.data.local;


import cn.wowjoy.office.data.local.entity.DaoSession;

/**
 * Date: 2017/2/27.
 * Created by Sherily on 2017/9/30.
 * Description: key-value use green dao
 */

public class NoSqlHelper {

    private DaoSession mDaoSession;
    private EntitySerializer mEntitySerializer;

    public NoSqlHelper(DaoSession daoSession, EntitySerializer serializer) {
        mDaoSession = daoSession;
        mEntitySerializer = serializer;
    }

    @SuppressWarnings("unchecked")
    public <T> long save(String key, T t) {
        String value = mEntitySerializer.serialize(t);
        long time = System.currentTimeMillis();
        GreenDaoNoSqlEntity entity = new GreenDaoNoSqlEntity(key, value, time);
        return mDaoSession.insertOrReplace(entity);
    }

    @SuppressWarnings("unchecked")
    public <T> T load(String key, Class<T> tClass) {
        GreenDaoNoSqlEntity entity = mDaoSession.getGreenDaoNoSqlEntityDao()
                .load(key);
        if (entity != null && entity.getValue() != null) {
            return (T) mEntitySerializer.deserialize(entity.getValue(), tClass);
        } else {
            return null;
        }
    }

    public void delete(String key) {
        mDaoSession.getGreenDaoNoSqlEntityDao()
                .deleteByKey(key);
    }
}
