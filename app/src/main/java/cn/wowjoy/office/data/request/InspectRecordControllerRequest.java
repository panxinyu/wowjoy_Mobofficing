package cn.wowjoy.office.data.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/11/23.
 */

public class InspectRecordControllerRequest implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("billId")
    private String billId;
    @SerializedName("taskDtlId")
    private String taskDtlId;
    @SerializedName("taskId")
    private String taskId;
    @SerializedName("appearanceStatus")
    private String appearanceStatus;
    @SerializedName("assetsStatus")
    private String assetsStatus;
    @SerializedName("remarks")
    private String remarks;
    @SerializedName("isExist")
    private String isExist;
    @SerializedName("inspectAddress")
    private String inspectAddress;
    @SerializedName("inspectUserId")
    private String inspectUserId;
    @SerializedName("noRemarksPic")
    private String noRemarksPic;
    private String qrCode; //二维码
    private String labelStatus;  //标签齐全
    private String functionStatus; //功能检查
    private String cleanStatus; //设备清洁
    //缩略图ID
    private String pictureId;
    private String thumbnailId;

    public String getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(String thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    public String getLabelStatus() {
        return labelStatus;
    }

    public void setLabelStatus(String labelStatus) {
        this.labelStatus = labelStatus;
    }

    public String getFunctionStatus() {
        return functionStatus;
    }

    public void setFunctionStatus(String functionStatus) {
        this.functionStatus = functionStatus;
    }

    public String getCleanStatus() {
        return cleanStatus;
    }

    public void setCleanStatus(String cleanStatus) {
        this.cleanStatus = cleanStatus;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public String getNoRemarksPic() {
        return noRemarksPic;
    }

    public void setNoRemarksPic(String noRemarksPic) {
        this.noRemarksPic = noRemarksPic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getTaskDtlId() {
        return taskDtlId;
    }

    public void setTaskDtlId(String taskDtlId) {
        this.taskDtlId = taskDtlId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getAppearanceStatus() {
        return appearanceStatus;
    }

    public void setAppearanceStatus(String appearanceStatus) {
        this.appearanceStatus = appearanceStatus;
    }

    public String getAssetsStatus() {
        return assetsStatus;
    }

    public void setAssetsStatus(String assetsStatus) {
        this.assetsStatus = assetsStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getIsExist() {
        return isExist;
    }

    public void setIsExist(String isExist) {
        this.isExist = isExist;
    }

    public String getInspectAddress() {
        return inspectAddress;
    }

    public void setInspectAddress(String inspectAddress) {
        this.inspectAddress = inspectAddress;
    }

    public String getInspectUserId() {
        return inspectUserId;
    }

    public void setInspectUserId(String inspectUserId) {
        this.inspectUserId = inspectUserId;
    }
}
