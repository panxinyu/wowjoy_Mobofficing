package cn.wowjoy.office.data.response;

/**
 * Created by Sherily on 2017/9/7.
 */

public class BannerInfo {
    private String url;

    public String getUrl() {
        return url;
    }

    public BannerInfo(String url) {
        this.url = url;
    }
}
