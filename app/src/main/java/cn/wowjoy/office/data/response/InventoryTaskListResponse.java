package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/1/15.
 */

public class InventoryTaskListResponse  {

    @SerializedName("count")
    private int  count;


    @SerializedName("list")
    private List<InventoryTotalItemInfo> mInventoryTotalItemInfoLists;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<InventoryTotalItemInfo> getInventoryTotalItemInfoLists() {
        return mInventoryTotalItemInfoLists;
    }

    public void setInventoryTotalItemInfoLists(List<InventoryTotalItemInfo> inventoryTotalItemInfoLists) {
        this.mInventoryTotalItemInfoLists = inventoryTotalItemInfoLists;
    }
}
