package cn.wowjoy.office.data.remote;

/**
 * Created by Sherily on 2017/11/30.
 * Description:
 */

public class CreateInterceptorExceptioin extends Error {
    private int errorCode;
    private String retry_after;


    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getRetry_after() {
        return retry_after;
    }

    public void setRetry_after(String retry_after) {
        this.retry_after = retry_after;
    }

}
