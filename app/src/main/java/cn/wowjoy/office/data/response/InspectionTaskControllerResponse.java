package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2017/11/17.
 */

public class InspectionTaskControllerResponse {
    @SerializedName("single")
    private SingleTaskController single;

    public SingleTaskController getSingle() {
        return single;
    }

    public void setSingle(SingleTaskController single) {
        this.single = single;
    }
}
