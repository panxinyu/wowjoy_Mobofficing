package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sherily on 2017/12/29.
 * Description:
 */

public class Type {

    @SerializedName("code")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("usageCode")
    private String usageCode;
    @SerializedName("children")
    private List<Type> subType;

    private boolean selected;
    private int selectedCount;



    public String getUsageCode() {
        return usageCode;
    }

    public void setUsageCode(String usageCode) {
        this.usageCode = usageCode;
    }

    public Type(boolean selected) {
        this.selected = selected;
    }

    public Type(String name) {
        this.name = name;
    }

    public Type(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Type> getSubType() {
        return subType;
    }

    public void setSubType(List<Type> subType) {
        this.subType = subType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getSelectedCount() {
        if (selectedCount > 99){
            return 99;
        }
        return selectedCount;
    }

    public void setSelectedCount(int selectedCount) {
        this.selectedCount = selectedCount;
    }
}
