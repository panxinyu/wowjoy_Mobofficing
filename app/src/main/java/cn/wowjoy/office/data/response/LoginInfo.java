package cn.wowjoy.office.data.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sherily on 2017/9/28.
 */

public class LoginInfo implements Serializable{
    @SerializedName("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
