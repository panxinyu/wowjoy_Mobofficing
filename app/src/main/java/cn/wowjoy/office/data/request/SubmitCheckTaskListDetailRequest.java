package cn.wowjoy.office.data.request;

/**
 * Created by Administrator on 2018/1/16.
 */

public class SubmitCheckTaskListDetailRequest {
    private String taskId;


    private String id ;//编辑时候要传id      或者是 提交总任务的时候

    private String billId;

    private String cardVersionId;

    private String cardNo;

    private String remarks;

    private String inventoryQuantity;

    private String bookQuantity;

    private String produceNo;

    private String cardName;

    private String categoryId;

    private String assetsSpec;

    private String departmentId;

    private String isDeleted;

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getAssetsSpec() {
        return assetsSpec;
    }

    public void setAssetsSpec(String assetsSpec) {
        this.assetsSpec = assetsSpec;
    }

    public String getProduceNo() {
        return produceNo;
    }

    public void setProduceNo(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBookQuantity() {
        return bookQuantity;
    }

    public void setBookQuantity(String bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    public SubmitCheckTaskListDetailRequest() {
    }

    public SubmitCheckTaskListDetailRequest(String id, String taskId, String billId, String cardVersionId, String cardNo, String inventoryQuantity, String remarks) {
        this.id = id;
        this.billId = billId;
        this.cardVersionId = cardVersionId;
        this.cardNo = cardNo;
        this.inventoryQuantity = inventoryQuantity;
        this.remarks = remarks;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getCardVersionId() {
        return cardVersionId;
    }

    public void setCardVersionId(String cardVersionId) {
        this.cardVersionId = cardVersionId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getInventoryQuantity() {
        return inventoryQuantity;
    }

    public void setInventoryQuantity(String inventoryQuantity) {
        this.inventoryQuantity = inventoryQuantity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


    public SubmitCheckTaskListDetailRequest(String taskId) {
        this.taskId = taskId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
