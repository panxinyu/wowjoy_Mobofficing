package cn.wowjoy.office.data.response;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/1/23.
 */

public class InventoryTaskDtlVO implements Serializable{
    private String id;

    private String billID;

    private String cardName;

    private String cardNo;

    private String produceNo;

    private String originalValue;

    private String inventoryResult;

    private String remarks;

    private String inventoryStatus;

    private String inventoryDatetime;

    private String inventoryUserId;

    private String inventoryUserName;

    private String cardVersionId;

    private String departmentName;

    private String departmentId;

    private String orderType;


    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getInventoryUserName() {
        return inventoryUserName;
    }

    public void setInventoryUserName(String inventoryUserName) {
        this.inventoryUserName = inventoryUserName;
    }

    public String getCardVersionId() {
        return cardVersionId;
    }

    public void setCardVersionId(String cardVersionId) {
        this.cardVersionId = cardVersionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getProduceNo() {
        return produceNo;
    }

    public void setProduceNo(String produceNo) {
        this.produceNo = produceNo;
    }

    public String getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }

    public String getInventoryResult() {
        return inventoryResult;
    }

    public void setInventoryResult(String inventoryResult) {
        this.inventoryResult = inventoryResult;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(String inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public String getInventoryDatetime() {
        return inventoryDatetime;
    }

    public void setInventoryDatetime(String inventoryDatetime) {
        this.inventoryDatetime = inventoryDatetime;
    }

    public String getInventoryUserId() {
        return inventoryUserId;
    }

    public void setInventoryUserId(String inventoryUserId) {
        this.inventoryUserId = inventoryUserId;
    }
}
