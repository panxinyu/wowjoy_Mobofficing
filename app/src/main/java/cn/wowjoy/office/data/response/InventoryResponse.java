package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import cn.wowjoy.office.utils.TextUtil;

/**
 * Created by Sherily on 2018/1/12.
 * Description:
 */

public class InventoryResponse {
    @SerializedName("headCode")
    private String inventorynumber;
    @SerializedName("checkDate")
    private String time;
    @SerializedName("commitTime")
    private String createTime;
    @SerializedName("status")
    private String status;
    @SerializedName("statusView")
    private String statusView;

    public String getStatusView() {
        return statusView;
    }

    public void setStatusView(String statusView) {
        this.statusView = statusView;
    }

    public InventoryResponse(String inventorynumber, String time, String createTime, String status) {
        this.inventorynumber = inventorynumber;
        this.time = time;
        this.createTime = createTime;
        this.status = status;
    }

    public String getInventorynumber() {
        return inventorynumber;
    }

    public void setInventorynumber(String inventorynumber) {
        this.inventorynumber = inventorynumber;
    }

    public String formatInventorynumber(){
        if (!TextUtils.isEmpty(inventorynumber))
            return "盘点单号："+inventorynumber;
        return "盘点单号：暂无";
    }

    public String formatTime(){
        if (!TextUtils.isEmpty(time))
            return "盘点时间："+time;
        return "盘点时间：暂无";
    }

    public String formatCreateTime(){
        if (!TextUtils.isEmpty(createTime))
            return "盘点时间："+createTime;
        return "盘点时间：暂无";
    }
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
