package cn.wowjoy.office.data.response;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sherily on 2017/11/17.
 * Description:
 */

public class GoodsDetailResponse implements Serializable {
    @SerializedName("goodsCode")
    private String goodsCode;
    @SerializedName("name")
    private String name;
    @SerializedName("goodsName")
    private String goodsName;
    @SerializedName("unit")
    private String unit;
    @SerializedName("typeCode")
    private String typeCode;
    @SerializedName("typeView")
    private String typeView;
    @SerializedName("effectiveDate")
    private String effectiveDate;
    @SerializedName("batchNumber")
    private String batchNumber;

    /**
     * y/n
     */
    @SerializedName("isStop")
    private String isStop;

    /**
     * 分类
     */
    @SerializedName("typeCodeView")
    private String typeCodeView;

    /**
     * 计量单位
     */
    @SerializedName("unitView")
    private String unitView;

    /**
     * 规格型号
     */
    @SerializedName("specifications")
    private String specifications;

    @SerializedName("price")
    private String price;

    @SerializedName("pictureId")
    private String pictureId;

    /**
     * 用途
     */
    @SerializedName("usageCodeView")
    private String usageCodeView;

    @SerializedName("usageCode")
    private String usageCode;

    @SerializedName("storageList")
    private List<StorageInfo> storageList;


    /**
     * 仓库
     */
    @SerializedName("storages")
    private String storages;

    /**
     * 厂商
     */
    @SerializedName("manufacturers")
    private String manufacturers;

    /**
     * 品牌
     */
    @SerializedName("brand")
    private String brand;

    /**
     * 条形码
     */
    @SerializedName("barCode")
    private String barCode;

    /**
     * 供应科室
     */
    @SerializedName("departmentName")
    private String departmentName;

    /**
     * 是否需要批号
     */
    @SerializedName("isBatchNumber")
    private String isBatchNumber;

    /**
     * 图片
     */
    @SerializedName("pictureUrl")
    private String pictureUrl;

    /**
     * 缩略图
     */
    @SerializedName("pictureUrl2")
    private String pictureUrl2;

    /**
     * 是否需要有效期
     */
    @SerializedName("isEffectiveDate")
    private String isEffectiveDate;

    @SerializedName("isExpensive")
    private String isExpensive;

    @SerializedName("thumbnailId")
    private String thumbnailId;

    @SerializedName("minApplyCount")
    private String minApplyCount;

    @SerializedName("isMultipleApply")
    private String isMultipleApply;

    @SerializedName("safetyInventory")
    private String safetyInventory;

    @SerializedName("maxInventory")
    private String maxInventory;

    @SerializedName("creator")
    private String creator;

    @SerializedName("modifier")
    private String modifier;

    @SerializedName("departmentId")
    private String departmentId;

    @SerializedName("minPurchaseCount")
    private String minPurchaseCount;

    @SerializedName("baseCode")
    private String baseCode;

    @SerializedName("wubiCode")
    private String wubiCode;

    @SerializedName("alias")
    private String alias;

    @SerializedName("pinyinCode")
    private String pinyinCode;

    @SerializedName("canCharged")
    private String canCharged;


    @SerializedName("isHighPower")
    private String isHighPower;

    @SerializedName("medicalInsuranceName")
    private String medicalInsuranceName;

    @SerializedName("packageUnit")
    private String packageUnit;
    @SerializedName("unitConvert")
    private String unitConvert;
    @SerializedName("remark")
    private String remark;
    @SerializedName("nature")
    private String nature;
//    @SerializedName("financeAssortment")
//    private String financeAssortment;
    @SerializedName("isQR")
    private String isQR;
    @SerializedName("data")
    private String data;
    @SerializedName("postfix")
    private String postfix;
    @SerializedName("packageUnitView")
    private String packageUnitView;
    @SerializedName("natureView")
    private String natureView;
    @SerializedName("pictureForBase64")
    private String pictureForBase64;
    @SerializedName("departmentList")
    private List<DepartMentInfo> departmentList;
    @SerializedName("pictureList")
    private List<PictureInfo> pictureList;

    public boolean isNeedQR(){
        if (!TextUtils.isEmpty(isQR) && TextUtils.equals("y",isQR)){
            return true;
        }
        return false;
    }

    public String formatGoodsCode(){
        if (!TextUtils.isEmpty(goodsCode))
            return "物资编码：" + goodsCode;
        return "物资编码：暂无";
    }

    public String formatPrice2(){
        if (!TextUtils.isEmpty(price)){
            if (!TextUtils.isEmpty(unitView))
                return "物品单价：￥"+price+"/"+unitView;
            else
                return "物品单价：￥"+price;

        }
        return "物品单价：暂无";
    }

    public String formatSpecifications(){
        if (!TextUtils.isEmpty(specifications))
            return "规格型号：" + specifications;
        return "规格型号：暂无";
    }

    public String formatName(){
        if (!TextUtils.isEmpty(name))
            return name;
        return "暂无";
    }

    public String formatSpecifications2(){
        if (!TextUtils.isEmpty(specifications))
            return specifications;
        return "暂无";
    }

    public String formatPrice3(){
        if (!TextUtils.isEmpty(price))
            return "￥" + price;
        return "暂无";
    }

    public String formatUnit(){
        if (!TextUtils.isEmpty(unitView)){
            return unitView;
        }
        return "暂无";
    }

    public String formatGoodsCode2(){
        if (!TextUtils.isEmpty(goodsCode))
            return goodsCode;
        return "暂无";
    }

    public String getUsageCode() {
        return usageCode;
    }

    public void setUsageCode(String usageCode) {
        this.usageCode = usageCode;
    }

    public List<StorageInfo> getStorageList() {
        return storageList;
    }

    public void setStorageList(List<StorageInfo> storageList) {
        this.storageList = storageList;
    }

    public String getIsExpensive() {
        return isExpensive;
    }

    public void setIsExpensive(String isExpensive) {
        this.isExpensive = isExpensive;
    }

    public String getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(String thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    public String getMinApplyCount() {
        return minApplyCount;
    }

    public void setMinApplyCount(String minApplyCount) {
        this.minApplyCount = minApplyCount;
    }

    public String getIsMultipleApply() {
        return isMultipleApply;
    }

    public void setIsMultipleApply(String isMultipleApply) {
        this.isMultipleApply = isMultipleApply;
    }

    public String getSafetyInventory() {
        return safetyInventory;
    }

    public void setSafetyInventory(String safetyInventory) {
        this.safetyInventory = safetyInventory;
    }

    public String getMaxInventory() {
        return maxInventory;
    }

    public void setMaxInventory(String maxInventory) {
        this.maxInventory = maxInventory;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getMinPurchaseCount() {
        return minPurchaseCount;
    }

    public void setMinPurchaseCount(String minPurchaseCount) {
        this.minPurchaseCount = minPurchaseCount;
    }

    public String getBaseCode() {
        return baseCode;
    }

    public void setBaseCode(String baseCode) {
        this.baseCode = baseCode;
    }

    public String getWubiCode() {
        return wubiCode;
    }

    public void setWubiCode(String wubiCode) {
        this.wubiCode = wubiCode;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPinyinCode() {
        return pinyinCode;
    }

    public void setPinyinCode(String pinyinCode) {
        this.pinyinCode = pinyinCode;
    }

    public String getCanCharged() {
        return canCharged;
    }

    public void setCanCharged(String canCharged) {
        this.canCharged = canCharged;
    }

    public String getIsHighPower() {
        return isHighPower;
    }

    public void setIsHighPower(String isHighPower) {
        this.isHighPower = isHighPower;
    }

    public String getMedicalInsuranceName() {
        return medicalInsuranceName;
    }

    public void setMedicalInsuranceName(String medicalInsuranceName) {
        this.medicalInsuranceName = medicalInsuranceName;
    }

    public String getPackageUnit() {
        return packageUnit;
    }

    public void setPackageUnit(String packageUnit) {
        this.packageUnit = packageUnit;
    }

    public String getUnitConvert() {
        return unitConvert;
    }

    public void setUnitConvert(String unitConvert) {
        this.unitConvert = unitConvert;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

//    public String getFinanceAssortment() {
//        return financeAssortment;
//    }
//
//    public void setFinanceAssortment(String financeAssortment) {
//        this.financeAssortment = financeAssortment;
//    }

    public String getIsQR() {
        return isQR;
    }

    public void setIsQR(String isQR) {
        this.isQR = isQR;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPostfix() {
        return postfix;
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    public String getPackageUnitView() {
        return packageUnitView;
    }

    public void setPackageUnitView(String packageUnitView) {
        this.packageUnitView = packageUnitView;
    }

    public String getNatureView() {
        return natureView;
    }

    public void setNatureView(String natureView) {
        this.natureView = natureView;
    }

    public String getPictureForBase64() {
        return pictureForBase64;
    }

    public void setPictureForBase64(String pictureForBase64) {
        this.pictureForBase64 = pictureForBase64;
    }

    public List<DepartMentInfo> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(List<DepartMentInfo> departmentList) {
        this.departmentList = departmentList;
    }

    public List<PictureInfo> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<PictureInfo> pictureList) {
        this.pictureList = pictureList;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeView() {
        return typeView;
    }

    public void setTypeView(String typeView) {
        this.typeView = typeView;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    private String stockTotal;
    private List<StockDetailResponse> stockDetailResponseList;

    public String getStockTotal() {
        return stockTotal;
    }

    public void setStockTotal(String stockTotal) {
        this.stockTotal = stockTotal;
    }

    public List<StockDetailResponse> getStockDetailResponseList() {
        return stockDetailResponseList;
    }

    public void setStockDetailResponseList(List<StockDetailResponse> stockDetailResponseList) {
        this.stockDetailResponseList = stockDetailResponseList;
    }

    public String getPictureUrl2() {
        return pictureUrl2;
    }

    public void setPictureUrl2(String pictureUrl2) {
        this.pictureUrl2 = pictureUrl2;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsStop() {
        return isStop;
    }

    public void setIsStop(String isStop) {
        this.isStop = isStop;
    }

    public String getTypeCodeView() {
        return typeCodeView;
    }

    public void setTypeCodeView(String typeCodeView) {
        this.typeCodeView = typeCodeView;
    }

    public String getUnitView() {
        return unitView;
    }

    public void setUnitView(String unitView) {
        this.unitView = unitView;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public String getUsageCodeView() {
        return usageCodeView;
    }

    public void setUsageCodeView(String usageCodeView) {
        this.usageCodeView = usageCodeView;
    }

    public String getStorages() {
        return storages;
    }

    public void setStorages(String storages) {
        this.storages = storages;
    }

    public String getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(String manufacturers) {
        this.manufacturers = manufacturers;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getIsBatchNumber() {
        return isBatchNumber;
    }

    public void setIsBatchNumber(String isBatchNumber) {
        this.isBatchNumber = isBatchNumber;
    }

    public String getIsEffectiveDate() {
        return isEffectiveDate;
    }

    public void setIsEffectiveDate(String isEffectiveDate) {
        this.isEffectiveDate = isEffectiveDate;
    }
    public String formatType(){
        return "物资分类："+typeCodeView;
    }

    public String isNeedDate(){
        if ("y".equals(isEffectiveDate)){
            return "是";
        } else{
            return "否";
        }
    }
    public String isNeedBatch(){
        if ("y".equals(isBatchNumber)){
            return "是";
        } else{
            return "否";
        }
    }

    public String isStopStr(){
        if ("y".equals(isStop)){
            return "是";
        } else{
            return "否";
        }
    }
    public boolean stopStatus(){
        if ("y".equals(isStop)){
            return true;
        } else{
            return false;
        }
    }

    public String status(){
        if (stopStatus()){
            return "已停用";
        } else {
            return "已启用";
        }
    }

    public String handleText(){
        if (stopStatus()){
            return "启用";
        } else {
            return "停用";
        }
    }
    public String formatPrice(){
        if (!TextUtils.isEmpty(price)){
            if (!TextUtils.isEmpty(unitView))
                return "￥"+price+"/"+unitView;
            else
                return "￥"+price;

        }
        return "暂无单价";
    }


    public String formatBrand() {
        if (TextUtils.isEmpty(brand)){
            return "无";
        }
        return brand;
    }

    public String formatManufacturers(){
        if (TextUtils.isEmpty(manufacturers)){
            return "无";
        }
        return manufacturers;
    }

    public String formatBarCode(){
        if (TextUtils.isEmpty(barCode)){
            return "条形码：暂无";
        }
        return "条形码："+barCode;
    }
}
