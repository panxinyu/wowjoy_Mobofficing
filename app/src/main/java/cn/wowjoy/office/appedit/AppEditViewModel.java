package cn.wowjoy.office.appedit;

import android.databinding.ObservableBoolean;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.DragRVAdapter;
import cn.wowjoy.office.data.response.AppInfo;

/**
 * Created by Sherily on 2017/9/13.
 */

public class AppEditViewModel extends NewBaseViewModel{

    public final ObservableBoolean edit = new ObservableBoolean(false);

    public DragRVAdapter dragRVAdapter = new DragRVAdapter();
    public List<AppInfo> wdatas;

    public void setData(List<AppInfo> wdatas){
        this.wdatas = wdatas;
        dragRVAdapter.setAppInfos(wdatas);
    }


    public ItemTouchHelper getItemTouchHelper(){
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                    final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN |
                            ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                    final int swipeFlags = 0;
                    return makeMovementFlags(dragFlags, swipeFlags);
                } else {
                    final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                    final int swipeFlags = 0;
//                    final int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                    return makeMovementFlags(dragFlags, swipeFlags);
                }
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                //得到当拖拽的viewHolder的Position
                int fromPosition = viewHolder.getAdapterPosition();
                //拿到当前拖拽到的item的viewHolder
                int toPosition = target.getAdapterPosition();
                if (fromPosition < toPosition) {
                    for (int i = fromPosition ; i < toPosition ; i++) {
                        Collections.swap(wdatas, i, i + 1);
                    }
                } else {
                    for (int i = fromPosition ; i > toPosition ; i--) {
                        Collections.swap(wdatas, i, i - 1);
                    }
                }
                dragRVAdapter.notifyItemMoved(fromPosition, toPosition);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {


            }
            /**
             * 重写拖拽可用
             * @return
             */
            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }

            /**
             * 长按选中Item的时候开始调用
             *
             * @param viewHolder
             * @param actionState
             */
            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                    viewHolder.itemView.setBackgroundColor(Color.LTGRAY);
                }
                super.onSelectedChanged(viewHolder, actionState);
            }

            /**
             * 手指松开的时候还原
             * @param recyclerView
             * @param viewHolder
             */
            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
//                viewHolder.itemView.setBackgroundColor(0);
            }
        });
        return itemTouchHelper;
    }


    @Override
    public void onCreateViewModel() {

    }

    @Inject
    public AppEditViewModel(@NonNull NewMainApplication application) {
        super(application);
        dragRVAdapter.setmItemEventHandler(this);
    }

    public void changeStatus(){
        edit.set(!edit.get());
        dragRVAdapter.setEdit(edit.get());
    }

    public void delete(AppInfo appInfo){
        //请求接口
        int position = wdatas.indexOf(appInfo);
        wdatas.remove(appInfo);
       dragRVAdapter.notifyItemRemoved(position);
        Toast.makeText(getApplication(), "Delete"+appInfo.getName(), Toast.LENGTH_SHORT).show();

    }

    private List<AppInfo> createApps(){
        List<AppInfo> appInfos = new ArrayList<>();

        appInfos.add(new AppInfo("巡检管理",R.drawable.inspection_res_selector,false,1002));
        appInfos.add(new AppInfo("主数据",R.drawable.zhushuju_selector,false,1004));
        appInfos.add(new AppInfo("库存盘点", R.drawable.kucunpandian_selector,false,1001));
        appInfos.add(new AppInfo("库存详情",R.drawable.kucunxiangqing_selector,false,1003));
        return appInfos;

    }

    public void load(){
        setData(createApps());
    }


    public void handle(int adapterPosition) {
        AppInfo appInfo = wdatas.get(adapterPosition);
        Toast.makeText(getApplication(), "跳转至"+appInfo.getName()+"页面", Toast.LENGTH_SHORT).show();
    }
}
