package cn.wowjoy.office.appedit;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.customview.OnRecyclerItemClickListener;
import cn.wowjoy.office.common.decoration.GridSpacesItemDecoration;
import cn.wowjoy.office.databinding.ActivityAppEditBinding;
import cn.wowjoy.office.utils.PreferenceManager;

public class AppEditActivity extends NewBaseActivity<ActivityAppEditBinding,AppEditViewModel> {

    private ItemTouchHelper itemTouchHelper;

    @Inject
    PreferenceManager preferenceManager;



    @Override
    protected void init(Bundle savedInstanceState) {

        binding.setViewModel(viewModel);

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.dragRv.setLayoutManager(new GridLayoutManager(this,4));
        binding.dragRv.setAdapter(viewModel.dragRVAdapter);
        int space = getResources().getDimensionPixelSize(R.dimen.space_bootom);
        binding.dragRv.addItemDecoration(new GridSpacesItemDecoration(space, true));
        itemTouchHelper = viewModel.getItemTouchHelper();
        itemTouchHelper.attachToRecyclerView(binding.dragRv);
        binding.dragRv.addOnItemTouchListener(new OnRecyclerItemClickListener(binding.dragRv) {
            @Override
            public void onItemClick(RecyclerView.ViewHolder vh) {
                if (viewModel.edit.get())
                    viewModel.handle(vh.getAdapterPosition());
            }

            @Override
            public void onItemLongClick(RecyclerView.ViewHolder vh) {
               if (!viewModel.edit.get()){
                   viewModel.changeStatus();
               }
                itemTouchHelper.startDrag(vh);
            }
        });

        viewModel.load();



    }



    @Override
    protected int getLayoutId() {
        return R.layout.activity_app_edit;
    }

    @Override
    protected Class<AppEditViewModel> getViewModel() {
        return AppEditViewModel.class;
    }
}
