package cn.wowjoy.office.retrievepsw;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.base.BaseActivity;
import cn.wowjoy.office.base.BasePresenter;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.databinding.ActivityRetrievePswBinding;
import cn.wowjoy.office.utils.PreferenceManager;

public class RetrievePswActivity extends NewBaseActivity<ActivityRetrievePswBinding,RetrievePswViewModel> {


    @Inject
    PreferenceManager preferenceManager;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            binding.sendCode.setEnabled(!TextUtils.isEmpty(binding.telephone.getText()));
            binding.submit.setEnabled(!TextUtils.isEmpty(binding.telephone.getText()) && !TextUtils.isEmpty(binding.code.getText()) );
        }
    };




    @Override
    protected void init(Bundle savedInstanceState) {

        binding.setViemodel(viewModel);
        setSupportActionBar(binding.toolbar);
        String tel = getIntent().getStringExtra("Telphone");
        if (!TextUtils.isEmpty(tel)){
            binding.telephone.setText(tel);
        }

        binding.telephone.addTextChangedListener(textWatcher);
        binding.code.addTextChangedListener(textWatcher);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.login:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_retrieve_psw;
    }

    @Override
    protected Class<RetrievePswViewModel> getViewModel() {
        return RetrievePswViewModel.class;
    }
}
