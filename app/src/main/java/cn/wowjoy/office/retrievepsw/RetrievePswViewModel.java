package cn.wowjoy.office.retrievepsw;

import android.support.annotation.NonNull;
import android.widget.Toast;

import javax.inject.Inject;

import cn.wowjoy.office.base.BaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;

/**
 * Created by Sherily on 2017/9/29.
 */

public class RetrievePswViewModel extends NewBaseViewModel {
    @Inject
    public RetrievePswViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }

    public void sendCode(String tel){
        //send code
        Toast.makeText(getApplication().getApplicationContext(), "发送验证码到"+tel, Toast.LENGTH_SHORT).show();
    }

    public void submit(String tel,String code){
        //submit
        Toast.makeText(getApplication().getApplicationContext(), tel+"收到的验证码是"+code, Toast.LENGTH_SHORT).show();
    }
}
