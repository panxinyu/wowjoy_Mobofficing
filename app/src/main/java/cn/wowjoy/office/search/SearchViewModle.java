package cn.wowjoy.office.search;

import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.github.jdsjlzx.recyclerview.LRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.common.adapter.SearchAppInfoAdapter;
import cn.wowjoy.office.data.response.AppInfo;
import cn.wowjoy.office.utils.MockSearchUtil;

/**
 * Created by Sherily on 2017/9/30.
 */

public class SearchViewModle extends NewBaseViewModel {
    public SearchAppInfoAdapter searchAppInfoAdapter = new SearchAppInfoAdapter();
    public LRecyclerViewAdapter adapter = new LRecyclerViewAdapter(searchAppInfoAdapter);
    private List<AppInfo> datas;
    final MediatorLiveData<AppInfo> appInfoMediatorLiveData = new MediatorLiveData<>();

    public void setData(List<AppInfo> appInfos,String keyWord){
        datas = appInfos;
        searchAppInfoAdapter.setDatas(datas,keyWord);
        searchAppInfoAdapter.notifyDataSetChanged();
//        searchAppInfoAdapter.notifyItemMoved(0,datas.size() - 1);
    }


    @Override
    public void onCreateViewModel() {

    }

    @Inject
    public SearchViewModle(@NonNull NewMainApplication application) {
        super(application);
        searchAppInfoAdapter.setmEventHandleListener(this);
    }

    private List<AppInfo> createApps(){
        List<AppInfo> appInfos = new ArrayList<>();
        appInfos.add(new AppInfo("主数据",R.drawable.zhushuju_selector,false,1004));
        appInfos.add(new AppInfo("库存盘点", R.drawable.kucunpandian_selector,false,1001));
        appInfos.add(new AppInfo("库存详情",R.drawable.kucunxiangqing_selector,false,1003));

        appInfos.add(new AppInfo("巡检管理",R.drawable.inspection_res_selector,false,1002));
        appInfos.add(new AppInfo("设备盘点",R.drawable.material_res_selector,false,1011));
        appInfos.add(new AppInfo("出库管理",R.drawable.inspection_res_selector,false,1012));
        appInfos.add(new AppInfo("敬请期待",R.drawable.more_res_selector,false,1005));
        return appInfos;
    }

    public void handle(AppInfo appInfo){
       appInfoMediatorLiveData.setValue(appInfo);
    }

    public MediatorLiveData<AppInfo> getAppInfoMediatorLiveData() {
        return appInfoMediatorLiveData;
    }

    public void search(String keyWord){
        List<AppInfo> appInfos = MockSearchUtil.searchResult(keyWord,createApps());
        setData(appInfos,keyWord);

    }
}
