package cn.wowjoy.office.search;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;

import javax.inject.Inject;

import cn.wowjoy.office.R;
import cn.wowjoy.office.appedit.AppEditActivity;

import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.common.decoration.SimpleItemDecoration;
import cn.wowjoy.office.data.response.AppInfo;
import cn.wowjoy.office.databinding.ActivitySearchBinding;
import cn.wowjoy.office.materialinspection.xunjian.MaterialActivity;
import cn.wowjoy.office.materialmanage.materiallist.MaterialListActivity;
import cn.wowjoy.office.utils.PreferenceManager;


public class SearchActivity extends NewBaseActivity<ActivitySearchBinding,SearchViewModle> {


    @Inject
    PreferenceManager preferenceManager;

   Handler handler = new Handler();



    @Override
    protected void init(Bundle savedInstanceState) {

        handler.sendEmptyMessage(0);
        handler.post(new Runnable() {
            @Override
            public void run() {

            }
        });
        binding.setViewModle(viewModel);
        setSupportActionBar(binding.toolbar);

        addKeyboard();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setEmptyView(binding.emptyView.getRoot());
        binding.recyclerView.setAdapter(viewModel.adapter);
        binding.recyclerView.addItemDecoration(new SimpleItemDecoration(this,SimpleItemDecoration.VERTICAL_LIST));
        binding.recyclerView.setPullRefreshEnabled(false);
        binding.recyclerView.setLoadMoreEnabled(false);
        binding.searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (!TextUtils.isEmpty(v.getText().toString().trim())){
                    hideSoftInput();
                    viewModel.search(v.getText().toString().trim());
                }

                return true;
            }
        });
        binding.searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.delete.setVisibility(TextUtils.isEmpty(binding.searchEditText.getText())?  View.GONE : View.VISIBLE);

            }
        });
        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.searchEditText.setText("");
                binding.delete.setVisibility(View.GONE);
            }
        });
        viewModel.getAppInfoMediatorLiveData().observe(this, new Observer<AppInfo>() {
            @Override
            public void onChanged(@Nullable AppInfo appInfo) {
                handle(appInfo);
            }
        });
    }

    /**
     * 获取焦点，调起软键盘
     */
    private void addKeyboard() {
        binding.searchEditText.setFocusable(true);
        binding.searchEditText.setFocusableInTouchMode(true);
        binding.searchEditText.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }


    @Override
    public boolean onSupportNavigateUp() {
        hideSoftInput();
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search;
    }

    @Override
    protected Class<SearchViewModle> getViewModel() {
        return SearchViewModle.class;
    }
    private void handle(AppInfo appInfo){
        switch (appInfo.appId){
            case 1001:
                Toast.makeText(this,"跳转至"+appInfo.getName(),Toast.LENGTH_SHORT).show();
                break;
            case 1002:
//                Toast.makeText(searchActivity,"跳转至"+appInfo.getName(),Toast.LENGTH_SHORT).show();
                Intent materialActivity = new Intent(this, MaterialActivity.class);
                startActivity(materialActivity);
                break;
            case 1003:
                ARouter.getInstance().build("/materialmanage/stock/materialstock").navigation();
//                Toast.makeText(this,"跳转至"+appInfo.getName(),Toast.LENGTH_SHORT).show();
                break;
            case 1004:
                Intent intent1 = new Intent(this, MaterialListActivity.class);
               startActivity(intent1);
                break;
            case 1005:
                Intent intent = new Intent(this, AppEditActivity.class);
                startActivity(intent);
                break;
        }
    }
}
