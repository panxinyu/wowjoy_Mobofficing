package cn.wowjoy.office.main;


import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.shizhefei.view.indicator.IndicatorViewPager;

import java.util.ArrayList;
import java.util.List;

import cn.wowjoy.office.R;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseActivity;
import cn.wowjoy.office.baselivedata.appbase.NewBaseFragment;
import cn.wowjoy.office.common.adapter.TabIndicatorFragmentAdapter;
import cn.wowjoy.office.common.widget.CreateDialog;
import cn.wowjoy.office.common.widget.MDialog;
import cn.wowjoy.office.databinding.ActivityMainBinding;
import cn.wowjoy.office.homepage.HomeFragment;
import cn.wowjoy.office.main.date.AuidsPmsResponse;

public class MainActivity extends NewBaseActivity<ActivityMainBinding, MainViewModel> {

    private List<Integer> icons;
    private List<String> titles;
    private List<NewBaseFragment> fragments;
    private IndicatorViewPager indicatorViewPager;
    private TabIndicatorFragmentAdapter tabIndicatorFragmentAdapter;
    private MDialog waitDialog;


    @Override
    public void onBackPressed() {
        preferenceManager.clearToken();
        super.onBackPressed();

    }

    private void initData(){
        titles = new ArrayList<>();
        titles.add("首页");
//        titles.add("通讯录");
//        titles.add("审批");
//        titles.add("我的");

        icons = new ArrayList<>();
        icons.add(R.drawable.maintab_1_selector);
//        icons.add(R.drawable.contacts_selector);
//        icons.add(R.drawable.approval_selector);
//        icons.add(R.drawable.mine_selector);

        fragments = new ArrayList<>();
        fragments.add(HomeFragment.newInstance("",""));
//        fragments.add(HomeFragment.newInstance("",""));
//        fragments.add(HomeFragment.newInstance("",""));
//        fragments.add(HomeFragment.newInstance("",""));



    }

    @Override
    protected Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        binding.setViewmodel(viewModel);


        initData();
//        binding.tabIndicator.setOnTransitionListener(new OnTransitionTextListener().setColor(ContextCompat.getColor(this,R.color.appText), ContextCompat.getColor(this,R.color.appText10)));
        indicatorViewPager = new IndicatorViewPager(binding.tabIndicator,binding.vp);
        tabIndicatorFragmentAdapter = new TabIndicatorFragmentAdapter(getSupportFragmentManager());
        tabIndicatorFragmentAdapter.setData(icons,titles,fragments);
        indicatorViewPager.setAdapter(tabIndicatorFragmentAdapter);
//        binding.vp.setCanScroll(false);
//        binding.vp.setOffscreenPageLimit(4);

        
    }


    @Override
    protected void onResume() {
        super.onResume();
    }



}
