package cn.wowjoy.office.main.date;

import java.io.Serializable;

public class AuidsPmsResponse implements Serializable {

    private String iaid;
    private String auid;
    private String pmsKey;

    public String getIaid() {
        return iaid;
    }

    public void setIaid(String iaid) {
        this.iaid = iaid;
    }

    public String getAuid() {
        return auid;
    }

    public void setAuid(String auid) {
        this.auid = auid;
    }

    public String getPmsKey() {
        return pmsKey;
    }

    public void setPmsKey(String pmsKey) {
        this.pmsKey = pmsKey;
    }

    @Override
    public String toString() {
        return "AuidsPmsResponse{" +
                "iaid='" + iaid + '\'' +
                ", auid='" + auid + '\'' +
                ", pmsKey='" + pmsKey + '\'' +
                '}';
    }
}
