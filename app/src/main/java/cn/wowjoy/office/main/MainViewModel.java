package cn.wowjoy.office.main;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;

/**
 * Created by Sherily on 2017/8/29.
 * Description:
 */

public class MainViewModel extends NewBaseViewModel {



    @Inject
    public MainViewModel(@NonNull NewMainApplication application) {
        super(application);
    }

    @Override
    public void onCreateViewModel() {

    }


}
