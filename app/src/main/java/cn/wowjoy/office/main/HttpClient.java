package cn.wowjoy.office.main;

import cn.wowjoy.office.data.response.UpdateInfo;
import io.reactivex.Flowable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface HttpClient {
    static HttpClient getGateWayService() {
        return HttpUtils.getInstance().getGiteeServer(HttpClient.class);
    }

    @GET("panxinyu/wowjoy_Mobofficing_host/raw/master/plugin/update.json")
    Flowable<UpdateInfo> checkUpdate();


    static HttpClient getDownloadService(String url) {
        return HttpUtils.getInstance().getDownloadServer(HttpClient.class, url);
    }

    @Streaming
    @GET
    Flowable<ResponseBody> downloadapk(@Url String fileUrl);

}
