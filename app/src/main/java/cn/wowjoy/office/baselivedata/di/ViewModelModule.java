package cn.wowjoy.office.baselivedata.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import cn.wowjoy.office.appedit.AppEditViewModel;
import cn.wowjoy.office.applystock.ApplyStockViewModel;
import cn.wowjoy.office.applystock.detail.ApplyHeadDetailViewModel;
import cn.wowjoy.office.applystock.receipts.ReceiptsDetailViewModel;
import cn.wowjoy.office.applystock.receipts.item.ReceiptsDetailItemViewModel;
import cn.wowjoy.office.common.widget.album.preview.PhotoPreviewViewModel;
import cn.wowjoy.office.common.widget.album.selector.ImageSelectorViewModel;
import cn.wowjoy.office.homepage.HomeViewModel;
import cn.wowjoy.office.loginnew.LoginViewModel;
import cn.wowjoy.office.main.MainViewModel;
import cn.wowjoy.office.materialinspection.applyfor.StockApplyIndexViewModel;
import cn.wowjoy.office.materialinspection.applyfor.detail.StockApplyDetailViewModel;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceDetailViewModel;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceViewModel;
import cn.wowjoy.office.materialinspection.applyfor.produce.qrdetail.StockQrDetailViewModel;
import cn.wowjoy.office.materialinspection.check.CheckIndexViewModel;
import cn.wowjoy.office.materialinspection.check.detail.CheckDetailViewModel;
import cn.wowjoy.office.materialinspection.check.detail.add.CategoryViewModel;
import cn.wowjoy.office.materialinspection.check.detail.add.CheckAddViewModel;
import cn.wowjoy.office.materialinspection.check.manage.search.CheckSearchViewModel;
import cn.wowjoy.office.materialinspection.check.task.CheckTaskListViewModel;
import cn.wowjoy.office.materialinspection.check.task.abnormal.AbNormalViewModel;
import cn.wowjoy.office.materialinspection.check.task.count.CheckCountViewModel;
import cn.wowjoy.office.materialinspection.check.task.search.CheckTaskListSearchViewModel;
import cn.wowjoy.office.materialinspection.stockout.StockOutIndexViewModel;
import cn.wowjoy.office.materialinspection.stockout.hand.StockHandAddViewModel;
import cn.wowjoy.office.materialinspection.stockout.hand.StockHandWriteViewModel;
import cn.wowjoy.office.materialinspection.stockout.otherout.StockOtherOutViewModel;
import cn.wowjoy.office.materialinspection.stockout.otherout.detail.StockOutDetailViewModel;
import cn.wowjoy.office.materialinspection.stockout.otherout.edit.StockOutHandEditViewModel;
import cn.wowjoy.office.materialinspection.stockout.search.SelectOutViewModel;
import cn.wowjoy.office.materialinspection.stockout.search.SelectPatientViewModel;
import cn.wowjoy.office.materialinspection.xunjian.MaterialViewModel;
import cn.wowjoy.office.materialinspection.xunjian.detail.DeviceDetailViewModel;
import cn.wowjoy.office.materialinspection.xunjian.manage.search.DoneTaskSearchViewModel;
import cn.wowjoy.office.materialinspection.xunjian.task.EditAdviceViewModel;
import cn.wowjoy.office.materialinspection.xunjian.task.PatrolTaskViewModel;
import cn.wowjoy.office.materialmanage.add.AddReportViewModel;
import cn.wowjoy.office.materialmanage.addReport.AddStockViewModel;
import cn.wowjoy.office.materialmanage.inventory.MaterialInventoryViewModel;
import cn.wowjoy.office.materialmanage.inventoryRecord.InventoryRecordViewModel;
import cn.wowjoy.office.materialmanage.inventoryReport.InventoryReportViewModel;
import cn.wowjoy.office.materialmanage.materialdetail.MaterialDetialViewModel;
import cn.wowjoy.office.materialmanage.materiallist.MaterialListViewModel;
import cn.wowjoy.office.materialmanage.search.MaterialSearchViewModel;
import cn.wowjoy.office.materialmanage.stock.MaterialStockViewModel;
import cn.wowjoy.office.messagecenter.MessageCenterViewModel;
import cn.wowjoy.office.personal.PersonalViewModel;
import cn.wowjoy.office.personal.about.AboutViewModel;
import cn.wowjoy.office.personal.alter.AlterViewModel;
import cn.wowjoy.office.personal.exchange.MyCompanyViewModel;
import cn.wowjoy.office.personal.myinfo.MyInfoViewModel;
import cn.wowjoy.office.personal.myteam.MyTeamViewModel;
import cn.wowjoy.office.personal.setting.SettingViewModel;
import cn.wowjoy.office.pm.view.viewodel.CheckPmReportViewModel;
import cn.wowjoy.office.pm.view.viewodel.ElectSafeViewModel;
import cn.wowjoy.office.pm.view.viewodel.FunctionCheckViewModel;
import cn.wowjoy.office.pm.view.viewodel.LookCheckViewModel;
import cn.wowjoy.office.pm.view.viewodel.PmManagerViewModel;
import cn.wowjoy.office.pm.view.viewodel.PmReportDetailViewModel;
import cn.wowjoy.office.pm.view.viewodel.PmReportViewModel;
import cn.wowjoy.office.pm.view.viewodel.PmSearchViewModel;
import cn.wowjoy.office.pm.view.viewodel.RemarkViewModel;
import cn.wowjoy.office.pm.view.viewodel.SignNameViewModel;
import cn.wowjoy.office.qrcodescan.QrCodeScanViewModel;
import cn.wowjoy.office.retrievepsw.RetrievePswViewModel;
import cn.wowjoy.office.search.SearchViewModle;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Sherily on 2017/12/19.
 * Description:
 */
@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindsLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindsMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindsHomeViewModel(HomeViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PersonalViewModel.class)
    abstract ViewModel bindsPersonalViewModel(PersonalViewModel personalViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AboutViewModel.class)
    abstract ViewModel bindsAboutViewModel(AboutViewModel aboutViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AlterViewModel.class)
    abstract ViewModel bindsAlterViewModel(AlterViewModel alterViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MyInfoViewModel.class)
    abstract ViewModel bindsMyInfoViewModel(MyInfoViewModel alterViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MyTeamViewModel.class)
    abstract ViewModel bindsMyTeamViewModel(MyTeamViewModel alterViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(MyCompanyViewModel.class)
    abstract ViewModel bindsMyCompanyViewModel(MyCompanyViewModel myCompanyViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MessageCenterViewModel.class)
    abstract ViewModel bindsMessageCenterViewModel(MessageCenterViewModel messageCenterViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModle.class)
    abstract ViewModel bindsSearchViewModle(SearchViewModle searchViewModle);

    @Binds
    @IntoMap
    @ViewModelKey(AppEditViewModel.class)
    abstract ViewModel bindsAppEditViewModel(AppEditViewModel appEditViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(MaterialListViewModel.class)
    abstract ViewModel bindsMaterialListViewModel(MaterialListViewModel materialListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MaterialDetialViewModel.class)
    abstract ViewModel bindsMaterialDetialViewModel(MaterialDetialViewModel materialDetialViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ImageSelectorViewModel.class)
    abstract ViewModel bindsImageSelectorViewModel(ImageSelectorViewModel imageSelectorViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PhotoPreviewViewModel.class)
    abstract ViewModel bindsPhotoPreviewViewModel(PhotoPreviewViewModel photoPreviewViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(QrCodeScanViewModel.class)
    abstract ViewModel bindsQrCodeScanViewModel(QrCodeScanViewModel qrCodeScanViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(RetrievePswViewModel.class)
    abstract ViewModel bindsRetrievePswViewModel(RetrievePswViewModel retrievePswViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SettingViewModel.class)
    abstract ViewModel bindsSettingViewModell(SettingViewModel settingViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MaterialViewModel.class)
    abstract ViewModel bindsMaterialViewModel(MaterialViewModel settingViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(DoneTaskSearchViewModel.class)
    abstract ViewModel bindsDoneTaskSearchViewModel(DoneTaskSearchViewModel doneTaskSearchViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PatrolTaskViewModel.class)
    abstract ViewModel bindsPatrolTaskViewModel(PatrolTaskViewModel patrolTaskViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DeviceDetailViewModel.class)
    abstract ViewModel bindsDeviceDetailViewModel(DeviceDetailViewModel deviceDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MaterialInventoryViewModel.class)
    abstract ViewModel bindsMaterialInventoryViewModel(MaterialInventoryViewModel materialInventoryViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MaterialSearchViewModel.class)
    abstract ViewModel bindsMaterialSearchViewModel(MaterialSearchViewModel materialSearchViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MaterialStockViewModel.class)
    abstract ViewModel bindsMaterialStockViewModel(MaterialStockViewModel materialStockViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(InventoryRecordViewModel.class)
    abstract ViewModel bindsInventoryRecordViewModel(InventoryRecordViewModel inventoryRecordViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddStockViewModel.class)
    abstract ViewModel bindsAddStockViewModel(AddStockViewModel addStockViewModel);



    @Binds
    @IntoMap
    @ViewModelKey(CheckIndexViewModel.class)
    abstract ViewModel bindsCheckIndexViewModel(CheckIndexViewModel checkIndexViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckTaskListViewModel.class)
    abstract ViewModel bindsCheckTaskListViewModel(CheckTaskListViewModel checkTaskListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckSearchViewModel.class)
    abstract ViewModel bindsCheckSearchViewModel(CheckSearchViewModel checkSearchViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckCountViewModel.class)
    abstract ViewModel bindsCheckCountViewModel(CheckCountViewModel checkCountViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckDetailViewModel.class)
    abstract ViewModel bindsCheckDetailViewModel(CheckDetailViewModel checkDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckAddViewModel.class)
    abstract ViewModel bindsCheckAddViewModel(CheckAddViewModel checkAddViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AbNormalViewModel.class)
    abstract ViewModel bindsAbNormalViewModel(AbNormalViewModel abNormalViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(AddReportViewModel.class)
    abstract ViewModel bindsAddReportViewModel(AddReportViewModel addReportViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(InventoryReportViewModel.class)
    abstract ViewModel bindsInventoryReportViewModel(InventoryReportViewModel inventoryReportViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckTaskListSearchViewModel.class)
    abstract ViewModel bindsCheckTaskListSearchViewModel(CheckTaskListSearchViewModel checkTaskListSearchViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CategoryViewModel.class)
    abstract ViewModel bindsCategoryViewModel(CategoryViewModel categoryViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(StockOutIndexViewModel.class)
    abstract ViewModel bindsStockOutIndexViewModel(StockOutIndexViewModel stockOutIndexViewModel);



    @Binds
    @IntoMap
    @ViewModelKey(StockOtherOutViewModel.class)
    abstract ViewModel bindsStockOtherOutViewModel(StockOtherOutViewModel stockOtherOutViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(SelectOutViewModel.class)
    abstract ViewModel bindsSelectOutViewModel(SelectOutViewModel selectOutViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockHandAddViewModel.class)
    abstract ViewModel bindsStockHandAddViewModel(StockHandAddViewModel stockHandAddViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockHandWriteViewModel.class)
    abstract ViewModel bindsStockHandWriteViewModel(StockHandWriteViewModel stockHandWriteViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockOutDetailViewModel.class)
    abstract ViewModel bindsStockOutDetailViewModel(StockOutDetailViewModel stockOutDetailViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(StockOutHandEditViewModel.class)
    abstract ViewModel bindsStockOutHandEditViewModel(StockOutHandEditViewModel stockOutHandEditViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockApplyIndexViewModel.class)
    abstract ViewModel bindsStockApplyIndexViewModel(StockApplyIndexViewModel statisticViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockApplyDetailViewModel.class)
    abstract ViewModel bindsStockApplyDetailViewModel(StockApplyDetailViewModel stockApplyDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ApplyStockViewModel.class)
    abstract ViewModel bindsApplyStockViewModel(ApplyStockViewModel applyStockViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReceiptsDetailViewModel.class)
    abstract ViewModel bindsReceiptsDetailViewModel(ReceiptsDetailViewModel receiptsDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ApplyHeadDetailViewModel.class)
    abstract ViewModel bindsApplyHeadDetailViewModel(ApplyHeadDetailViewModel applyHeadDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockApplyProduceViewModel.class)
    abstract ViewModel bindsStockApplyProduceViewModel(StockApplyProduceViewModel stockApplyProduceViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockApplyProduceDetailViewModel.class)
    abstract ViewModel bindsStockApplyProduceDetailViewModel(StockApplyProduceDetailViewModel stockApplyProduceDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(StockQrDetailViewModel.class)
    abstract ViewModel bindsStockQrDetailViewModel(StockQrDetailViewModel stockQrDetailViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReceiptsDetailItemViewModel.class)
    abstract ViewModel bindsReceiptsDetailItemViewModel(ReceiptsDetailItemViewModel receiptsDetailItemViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SelectPatientViewModel.class)
    abstract ViewModel bindsSelectPatientViewModel(SelectPatientViewModel selectPatientViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(PmManagerViewModel.class)
    abstract ViewModel bindsPmManagerViewModel(PmManagerViewModel pmManagerViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(PmReportDetailViewModel.class)
    abstract ViewModel bindsPmReportDetailViewModel(PmReportDetailViewModel pmReportDetailViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(PmReportViewModel.class)
    abstract ViewModel bindsPmReportViewModel(PmReportViewModel pmReportViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(PmSearchViewModel.class)
    abstract ViewModel bindsPmSearchViewModel(PmSearchViewModel pmSearchViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(LookCheckViewModel.class)
    abstract ViewModel bindsLookCheckViewModel(LookCheckViewModel lookCheckViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(ElectSafeViewModel.class)
    abstract ViewModel bindsElectSafeViewModel(ElectSafeViewModel electSafeViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(FunctionCheckViewModel.class)
    abstract ViewModel bindsFunctionCheckViewModel(FunctionCheckViewModel functionCheckViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(SignNameViewModel.class)
    abstract ViewModel bindsSignNameViewModel(SignNameViewModel signNameViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CheckPmReportViewModel.class)
    abstract ViewModel bindsCheckPmReportViewModel(CheckPmReportViewModel checkPmReportViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(RemarkViewModel.class)
    abstract ViewModel bindsRemarkViewModel(RemarkViewModel remarkViewModel);
    @Binds
    @IntoMap
    @ViewModelKey(EditAdviceViewModel.class)
    abstract ViewModel bindsEditAdviceViewModel(EditAdviceViewModel editAdviceViewModel);
    @Binds
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);


}
