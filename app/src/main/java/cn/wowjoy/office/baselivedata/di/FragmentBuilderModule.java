package cn.wowjoy.office.baselivedata.di;

import cn.wowjoy.office.applystock.CompletedFragment;
import cn.wowjoy.office.applystock.StayOutStockFragment;
import cn.wowjoy.office.homepage.HomeFragment;
import cn.wowjoy.office.materialinspection.applyfor.index.StockDoneFragment;
import cn.wowjoy.office.materialinspection.applyfor.index.StockNoDoneFragment;
import cn.wowjoy.office.materialinspection.check.manage.done.CheckDoneFragment;
import cn.wowjoy.office.materialinspection.check.manage.nodone.CheckNoDoneFragment;
import cn.wowjoy.office.materialinspection.check.task.count.difference.DifferentFragment;
import cn.wowjoy.office.materialinspection.check.task.count.statistic.StatisticFragment;
import cn.wowjoy.office.materialinspection.xunjian.manage.done.DoneFragment;
import cn.wowjoy.office.materialinspection.xunjian.manage.nodone.NoDoneFragment;
import cn.wowjoy.office.materialmanage.add.AllFragment;
import cn.wowjoy.office.materialmanage.add.EnteredFragment;
import cn.wowjoy.office.materialmanage.add.NotEnteredFragment;
import cn.wowjoy.office.materialmanage.inventoryReport.RecordFragment;
import cn.wowjoy.office.materialmanage.inventoryReport.RecordLossesFragment;
import cn.wowjoy.office.materialmanage.inventoryReport.RecordProfitFragment;
import cn.wowjoy.office.pm.view.fragment.manager.MAllFragment;
import cn.wowjoy.office.pm.view.fragment.report.RAllFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sherily on 2017/12/18.
 * Description:对Fragment暴露dagger注入
 */
@Module
public abstract class FragmentBuilderModule {
    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();
    @ContributesAndroidInjector
    abstract NoDoneFragment contributeNoDoneFragment();
    @ContributesAndroidInjector
    abstract DoneFragment contributeDoneFragment();



    @ContributesAndroidInjector
    abstract DifferentFragment contributeDifferentFragment();
    @ContributesAndroidInjector
    abstract StatisticFragment contributeStatisticFragment();
    @ContributesAndroidInjector
    abstract CheckDoneFragment contributeCheckDoneFragment();
    @ContributesAndroidInjector
    abstract CheckNoDoneFragment contributeCheckNoDoneFragment();

    @ContributesAndroidInjector
    abstract EnteredFragment enteredFragment();
    @ContributesAndroidInjector
    abstract NotEnteredFragment notEnteredFragment();
    @ContributesAndroidInjector
    abstract AllFragment allFragment();
    @ContributesAndroidInjector
    abstract RecordFragment recordFragment();
    @ContributesAndroidInjector
    abstract RecordProfitFragment recordProfitFragment();
    @ContributesAndroidInjector
    abstract RecordLossesFragment recordLossesFragment();

    @ContributesAndroidInjector
    abstract StayOutStockFragment stayOutStockFragment();
    @ContributesAndroidInjector
    abstract CompletedFragment completedFragment();

    @ContributesAndroidInjector
    abstract StockNoDoneFragment stockNoDoneFragment();
    @ContributesAndroidInjector
    abstract StockDoneFragment stockDoneFragment();

    @ContributesAndroidInjector
    abstract MAllFragment mMAllFragment();
    @ContributesAndroidInjector
    abstract RAllFragment mRAllFragment();
}
