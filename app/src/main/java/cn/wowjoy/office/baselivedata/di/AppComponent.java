package cn.wowjoy.office.baselivedata.di;

import android.app.Application;

import javax.inject.Singleton;

import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Sherily on 2017/12/18.
 * Description:
 */
@Singleton
@Component(modules = { AndroidInjectionModule.class,AndroidSupportInjectionModule.class, ActivityBuilderModule.class,FragmentBuilderModule.class,NewAppModule.class/*, RestModule.class*//*, GreenDaoModule.class*/})
public interface AppComponent extends AndroidInjector<NewMainApplication> {

//       @Component.Builder
//       interface Builder {
//            @BindsInstance
//            AppComponent.Builder application(Application application);
//
//            AppComponent build();
//        }
//
//        AppComponent inject(NewMainApplication application);
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<NewMainApplication> {
    }

//        //向子组件曝露注入的全局对象
//        LoginService getLoginService();
//        ApiService getApiService();
//        PreferenceManager getPreferenceManager();
//
//        DaoSession daosession();
//        UserInfoDao userInfoDao();



}
