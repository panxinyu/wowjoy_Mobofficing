package cn.wowjoy.office.baselivedata.di;

import cn.wowjoy.office.appedit.AppEditActivity;
import cn.wowjoy.office.applystock.ApplyStockActivity;
import cn.wowjoy.office.applystock.detail.ApplyHeadDetailActivity;
import cn.wowjoy.office.applystock.detail.ApplyMaterialActivity;
import cn.wowjoy.office.applystock.receipts.ReceiptsDetailActivity;
import cn.wowjoy.office.applystock.receipts.item.ReceiptsDetailItemActivity;
import cn.wowjoy.office.common.widget.album.preview.PhotoPreviewActivity;
import cn.wowjoy.office.common.widget.album.selector.ImageSelectorActivity;
import cn.wowjoy.office.loginnew.LoginActivity;
import cn.wowjoy.office.main.MainActivity;
import cn.wowjoy.office.materialinspection.applyfor.StockApplyIndexActivity;
import cn.wowjoy.office.materialinspection.applyfor.detail.StockApplyDetailActivity;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceActivity;
import cn.wowjoy.office.materialinspection.applyfor.produce.StockApplyProduceDetailActivity;
import cn.wowjoy.office.materialinspection.applyfor.produce.qrdetail.StockQrDetailActivity;
import cn.wowjoy.office.materialinspection.check.CheckIndexActivity;
import cn.wowjoy.office.materialinspection.check.detail.CheckDetailActivity;
import cn.wowjoy.office.materialinspection.check.detail.add.CategoryActivity;
import cn.wowjoy.office.materialinspection.check.detail.add.CheckAddActivity;
import cn.wowjoy.office.materialinspection.check.manage.search.CheckSearchActivity;
import cn.wowjoy.office.materialinspection.check.task.CheckTaskListActivity;
import cn.wowjoy.office.materialinspection.check.task.abnormal.AbNormalActivity;
import cn.wowjoy.office.materialinspection.check.task.count.CheckCountActivity;
import cn.wowjoy.office.materialinspection.check.task.search.CheckTaskListSearchActivity;
import cn.wowjoy.office.materialinspection.stockout.StockOutIndexActivity;
import cn.wowjoy.office.materialinspection.stockout.hand.StockHandAddActivity;
import cn.wowjoy.office.materialinspection.stockout.hand.StockHandWriteActivity;
import cn.wowjoy.office.materialinspection.stockout.otherout.StockOtherOutActivity;
import cn.wowjoy.office.materialinspection.stockout.otherout.detail.StockOutDetailActivity;
import cn.wowjoy.office.materialinspection.stockout.otherout.edit.StockOutHandEditActivity;
import cn.wowjoy.office.materialinspection.stockout.search.SelectOutActivity;
import cn.wowjoy.office.materialinspection.stockout.search.SelectPatientActivity;
import cn.wowjoy.office.materialinspection.xunjian.MaterialActivity;
import cn.wowjoy.office.materialinspection.xunjian.detail.DeviceDetail2Activity;
import cn.wowjoy.office.materialinspection.xunjian.detail.DeviceDetailActivity;
import cn.wowjoy.office.materialinspection.xunjian.manage.search.DoneTaskSearchActivity;
import cn.wowjoy.office.materialinspection.xunjian.task.EditAdviceActivity;
import cn.wowjoy.office.materialinspection.xunjian.task.PatrolTaskActivity;
import cn.wowjoy.office.materialmanage.add.AddReportActivity;
import cn.wowjoy.office.materialmanage.addReport.AddStockActivity;
import cn.wowjoy.office.materialmanage.addReport.EditStockActivity;
import cn.wowjoy.office.materialmanage.addReport.StockRecordeActivity;
import cn.wowjoy.office.materialmanage.inventory.MaterialInventoryActivity;
import cn.wowjoy.office.materialmanage.inventoryRecord.InventoryRecordActivity;
import cn.wowjoy.office.materialmanage.inventoryReport.AddReport2Activity;
import cn.wowjoy.office.materialmanage.inventoryReport.StaffActivity;
import cn.wowjoy.office.materialmanage.materialdetail.MaterialDetailActivity;
import cn.wowjoy.office.materialmanage.materiallist.MaterialListActivity;
import cn.wowjoy.office.materialmanage.search.MaterialSearchActivity;
import cn.wowjoy.office.materialmanage.stock.MaterialStockActivity;
import cn.wowjoy.office.messagecenter.MessageCenterActivity;
import cn.wowjoy.office.personal.PersonalActivity;
import cn.wowjoy.office.personal.about.AboutActivity;
import cn.wowjoy.office.personal.alter.AlterActivity;
import cn.wowjoy.office.personal.exchange.MyCompanyActivity;
import cn.wowjoy.office.personal.myinfo.MyInfoActivity;
import cn.wowjoy.office.personal.myteam.MyTeamActivity;
import cn.wowjoy.office.personal.setting.SettingActivity;
import cn.wowjoy.office.pm.view.PmManagerActivity;
import cn.wowjoy.office.pm.view.PmReportActivity;
import cn.wowjoy.office.pm.view.PmReportDetailActivity;
import cn.wowjoy.office.pm.view.PmSearchActivity;
import cn.wowjoy.office.pm.view.SignNameActivity;
import cn.wowjoy.office.pm.view.detail.CheckPmReportActivity;
import cn.wowjoy.office.pm.view.detail.ElectSafeActivity;
import cn.wowjoy.office.pm.view.detail.FunctionCheckActivity;
import cn.wowjoy.office.pm.view.detail.LookCheckActivity;
import cn.wowjoy.office.pm.view.detail.RemarkActivity;
import cn.wowjoy.office.qrcodescan.QrCodeScanActivity;
import cn.wowjoy.office.retrievepsw.RetrievePswActivity;
import cn.wowjoy.office.search.SearchActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Sherily on 2017/12/18.
 * Description：对activity暴露dagger注入
 */
@Module
public abstract class ActivityBuilderModule {
    @ContributesAndroidInjector
    abstract LoginActivity loginActivity();

    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector
    abstract AboutActivity aboutActivity();

    @ContributesAndroidInjector
    abstract AlterActivity alterActivity();

    @ContributesAndroidInjector
    abstract PersonalActivity personalActivity();

    @ContributesAndroidInjector
    abstract MyInfoActivity myInfoActivity();

    @ContributesAndroidInjector
    abstract MyCompanyActivity myCompanyActivity();

    @ContributesAndroidInjector
    abstract MyTeamActivity myTeamActivity();

    @ContributesAndroidInjector
    abstract MessageCenterActivity messageCenterActivity();

    @ContributesAndroidInjector
    abstract SearchActivity searchActivity();

    @ContributesAndroidInjector
    abstract AppEditActivity appEditActivity();

    @ContributesAndroidInjector
    abstract MaterialListActivity materialListActivity();

    @ContributesAndroidInjector
    abstract MaterialDetailActivity materialDetailActivity();

    @ContributesAndroidInjector
    abstract ImageSelectorActivity imageSelectorActivity();

    @ContributesAndroidInjector
    abstract PhotoPreviewActivity photoPreviewActivity();

    @ContributesAndroidInjector
    abstract QrCodeScanActivity qrCodeScanActivity();

    @ContributesAndroidInjector
    abstract RetrievePswActivity retrievePswActivity();

    @ContributesAndroidInjector
    abstract SettingActivity settingActivity();

    @ContributesAndroidInjector
    abstract MaterialActivity mMaterialActivity();

    @ContributesAndroidInjector
    abstract DoneTaskSearchActivity mDoneTaskSearchActivity();

    @ContributesAndroidInjector
    abstract PatrolTaskActivity mPatrolTaskActivity();

    @ContributesAndroidInjector
    abstract DeviceDetailActivity mDeviceDetailActivity();

    @ContributesAndroidInjector
    abstract CheckSearchActivity mCheckSearchActivity();
    @ContributesAndroidInjector
    abstract CheckCountActivity mCheckCountActivity();

    @ContributesAndroidInjector
    abstract CheckIndexActivity mCheckIndexActivity();

    @ContributesAndroidInjector
    abstract CheckTaskListActivity mCheckTaskListActivity();

    @ContributesAndroidInjector
    abstract CheckDetailActivity mCheckDetailActivity();

    @ContributesAndroidInjector
    abstract CheckAddActivity mCheckAddActivity();

    @ContributesAndroidInjector
    abstract MaterialInventoryActivity materialInventoryActivity();

    @ContributesAndroidInjector
    abstract MaterialSearchActivity materialSearchActivity();

    @ContributesAndroidInjector
    abstract MaterialStockActivity materialStockActivity();

    @ContributesAndroidInjector
    abstract InventoryRecordActivity inventoryRecordActivity();

    @ContributesAndroidInjector
    abstract EditStockActivity editStockActivity();

    @ContributesAndroidInjector
    abstract AddStockActivity addStockActivity();

    @ContributesAndroidInjector
    abstract StockRecordeActivity stockRecordeActivity();

    @ContributesAndroidInjector
    abstract AddReportActivity  addReportActivity();

    @ContributesAndroidInjector
    abstract AddReport2Activity addReport2Activity();

    @ContributesAndroidInjector
    abstract StaffActivity staffActivity();

    @ContributesAndroidInjector
    abstract CheckTaskListSearchActivity checkTaskListSearchActivity();

    @ContributesAndroidInjector
    abstract AbNormalActivity mAbNormalActivity();

    @ContributesAndroidInjector
    abstract CategoryActivity mCategoryActivity();

    @ContributesAndroidInjector
    abstract StockOutIndexActivity mStockOutIndexActivity();

    @ContributesAndroidInjector
    abstract StockOtherOutActivity mStockOtherOutActivity();

    @ContributesAndroidInjector
    abstract SelectOutActivity mSelectOutActivity();

    @ContributesAndroidInjector
    abstract StockHandAddActivity mStockHandAddActivity();

    @ContributesAndroidInjector
    abstract StockHandWriteActivity mStockHandWriteActivity();

    @ContributesAndroidInjector
    abstract StockOutDetailActivity mStockOutDetailActivity();

    @ContributesAndroidInjector
    abstract ApplyStockActivity mApplyStockActivity();

    @ContributesAndroidInjector
    abstract ApplyHeadDetailActivity mApplyHeadDetailActivity();


    @ContributesAndroidInjector
    abstract StockOutHandEditActivity mStockOutHandEditActivity();


    @ContributesAndroidInjector
    abstract StockApplyIndexActivity mStockApplyIndexActivity();

    @ContributesAndroidInjector
    abstract ReceiptsDetailActivity mReceiptsDetailActivity();

    @ContributesAndroidInjector
    abstract StockApplyDetailActivity mStockApplyDetailActivity();

    @ContributesAndroidInjector
    abstract StockApplyProduceActivity mStockApplyProduceActivity();

    @ContributesAndroidInjector
    abstract StockApplyProduceDetailActivity mStockApplyProduceDetailActivity();

    @ContributesAndroidInjector
    abstract ApplyMaterialActivity mApplyMaterialActivity();

    @ContributesAndroidInjector
    abstract StockQrDetailActivity mStockQrDetailActivity();

    @ContributesAndroidInjector
    abstract ReceiptsDetailItemActivity mReceiptsDetailItemActivity();

    @ContributesAndroidInjector
    abstract SelectPatientActivity mSelectPatientActivity();

    @ContributesAndroidInjector
    abstract PmManagerActivity mPmManagerActivity();
    @ContributesAndroidInjector
    abstract PmReportActivity mPmReportActivity();
    @ContributesAndroidInjector
    abstract PmReportDetailActivity mPmReportDetailActivity();
    @ContributesAndroidInjector
    abstract PmSearchActivity mPmSearchActivity();
    @ContributesAndroidInjector
    abstract LookCheckActivity mLookCheckActivity();
    @ContributesAndroidInjector
    abstract ElectSafeActivity mElectSafeActivity();
    @ContributesAndroidInjector
    abstract FunctionCheckActivity mFunctionCheckActivity();
    @ContributesAndroidInjector
    abstract SignNameActivity mSignNameActivity();
    @ContributesAndroidInjector
    abstract CheckPmReportActivity mCheckPmReportActivity();
    @ContributesAndroidInjector
    abstract RemarkActivity mRemarkActivity();
    @ContributesAndroidInjector
    abstract DeviceDetail2Activity mDeviceDetail2Activity();
    @ContributesAndroidInjector
    abstract EditAdviceActivity mEditAdviceActivity();
}
