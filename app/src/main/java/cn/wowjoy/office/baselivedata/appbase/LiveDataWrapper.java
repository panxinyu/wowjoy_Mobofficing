package cn.wowjoy.office.baselivedata.appbase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static cn.wowjoy.office.baselivedata.appbase.Status.ERROR;
import static cn.wowjoy.office.baselivedata.appbase.Status.LOADING;
import static cn.wowjoy.office.baselivedata.appbase.Status.SUCCESS;

/**
 * Created by Sherily on 2017/12/19.
 * Description:
 */

public class LiveDataWrapper<T> {
    @NonNull
    public final Status status;
    @Nullable
    public final T data;
    @Nullable public final Throwable error;
    private LiveDataWrapper(@NonNull Status status, @Nullable T data, @Nullable Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static <T> LiveDataWrapper<T> success(@NonNull T data) {
        return new LiveDataWrapper<>(SUCCESS, data, null);
    }

    public static <T> LiveDataWrapper<T> error(Throwable error, @Nullable T data) {
        return new LiveDataWrapper<>(ERROR, data, error);
    }

    public static <T> LiveDataWrapper<T> loading(@Nullable T data) {
        return new LiveDataWrapper<>(LOADING, data, null);
    }
}
