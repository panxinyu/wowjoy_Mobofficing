package cn.wowjoy.office.baselivedata.di;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;

import javax.inject.Singleton;

import cn.wowjoy.office.BuildConfig;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.RestModule;
import cn.wowjoy.office.data.room.UserDataBase;
import cn.wowjoy.office.data.room.dao.UserDao;
import cn.wowjoy.office.pm.db.PmListDao;
import cn.wowjoy.office.pm.db.PmListDetailDao;
import cn.wowjoy.office.utils.PreferenceManager;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Sherily on 2017/12/18.
 * Description:
 */
@Module(includes = {ViewModelModule.class, RestModule.class/*,GreenDaoModule.class*/})
public class NewAppModule {
    @Provides
    @Singleton
    PreferenceManager providePreferenceManager(NewMainApplication application){
        PreferenceManager.init(application);
        return PreferenceManager.getInstance();
    }

    @Provides
    @Singleton
    UserDataBase provideUserDataBase(NewMainApplication application) {
        return Room.databaseBuilder(application, UserDataBase.class, BuildConfig.ROOM_DAO_DB_NAME).
                addMigrations(MIGRATION_1_2).build();
    }
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE PmListDao Add COLUMN test TEXT");
        }
    };
    @Provides
    @Singleton
    UserDao provideUserDao(UserDataBase userDataBase) {
        return userDataBase.userDao();
    }

    @Provides
    @Singleton
    PmListDao providePmListDao(UserDataBase userDataBase) {
        return userDataBase.pmListDao();
    }
    @Provides
    @Singleton
    PmListDetailDao providePmListDetailDao(UserDataBase userDataBase) {
        return userDataBase.pmListDetailDao();
    }
}
