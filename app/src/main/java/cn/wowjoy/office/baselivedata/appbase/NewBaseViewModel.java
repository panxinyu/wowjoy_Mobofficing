package cn.wowjoy.office.baselivedata.appbase;

import android.arch.lifecycle.AndroidViewModel;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Sherily on 2017/12/19.
 * Description:
 */

public abstract class NewBaseViewModel extends AndroidViewModel {
    public final int pageSize = 10;
    public int pageIndex = 0;
    public int localPageIndex = 1;
    private CompositeDisposable mCompositeDisposable;
    public ObservableBoolean isRefreshing = new ObservableBoolean(false);
    public ObservableBoolean isLoading = new ObservableBoolean(true);
    public ObservableBoolean isEmpty = new ObservableBoolean(false);
    public ObservableBoolean isError = new ObservableBoolean(false);

    public RecyclerView.LayoutManager layoutManager = null;


    public NewBaseViewModel(@NonNull NewMainApplication application) {
        super(application);
        onCreate();
    }
   public void isRefreshOrLoadMore(boolean isRefresh){
       if (isRefresh){
           pageIndex = 0;
       } else {
           ++pageIndex;
       }
   }
   //本地数据库的起始Index从1开始
    public void isRefreshOrLoadMoreLocal(boolean isRefresh){
        if (isRefresh){
            localPageIndex = 1;
        } else {
            ++localPageIndex;
        }
    }
    public void refreshComplete() {
        isRefreshing.set(true);
        isRefreshing.set(false);
    }

    public void setState(boolean isLoading, boolean isEmpty, boolean isError) {
        this.isLoading.set(isLoading);
        this.isEmpty.set(isEmpty);
        this.isError.set(isError);
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public ObservableBoolean getIsLoading() {
        return isLoading;
    }

    public ObservableBoolean getIsEmpty() {
        return isEmpty;
    }

    public ObservableBoolean getIsError() {
        return isError;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

    private void onCreate() {
        this.mCompositeDisposable = new CompositeDisposable();
        onCreateViewModel();
    }
    public abstract void onCreateViewModel();
    public void onDestroy() {
        if (null != mCompositeDisposable) {
            this.mCompositeDisposable.clear();
            this.mCompositeDisposable = null;
        }
    }

    public void loadData(boolean ref) {
    }

    protected void addDisposable(Disposable disposable) {
        if (null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }
}
