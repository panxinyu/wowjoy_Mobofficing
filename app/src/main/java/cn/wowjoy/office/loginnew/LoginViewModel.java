package cn.wowjoy.office.loginnew;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import android.support.annotation.NonNull;



import javax.inject.Inject;

import cn.wowjoy.office.BuildConfig;
import cn.wowjoy.office.baselivedata.appbase.LiveDataWrapper;
import cn.wowjoy.office.baselivedata.appbase.NewBaseViewModel;
import cn.wowjoy.office.baselivedata.appbase.NewMainApplication;
import cn.wowjoy.office.data.remote.ApiService;
import cn.wowjoy.office.data.remote.LoginService;
import cn.wowjoy.office.data.remote.ResultDataParse;
import cn.wowjoy.office.data.remote.RxSchedulerTransformer;
import cn.wowjoy.office.data.response.LoginInfo;
import cn.wowjoy.office.data.response.StaffListResponse;
import cn.wowjoy.office.data.response.TestResponse;
import cn.wowjoy.office.data.room.dao.UserDao;

import cn.wowjoy.office.utils.PreferenceManager;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sherily on 2017/12/19.
 * Description:
 */

public class LoginViewModel extends NewBaseViewModel {
    @Inject
    LoginService loginService;
    @Inject
    PreferenceManager preferenceManager;
    @Inject
    ApiService apiService;
    @Inject
    UserDao userDao;



    @Inject
    public LoginViewModel(@NonNull NewMainApplication application) {
        super(application);
    }


    @Override
    public void onCreateViewModel() {

    }
    public MediatorLiveData<LiveDataWrapper<StaffListResponse>> staff = new MediatorLiveData<>();
    public void getStaffInfo(){
        staff.setValue(LiveDataWrapper.loading(null));
        Disposable disposable= apiService.getStaffInfo("app")
                .flatMap(new ResultDataParse<StaffListResponse>())
                .compose(new RxSchedulerTransformer<StaffListResponse>())
                .subscribe(new Consumer<StaffListResponse>() {
                    @Override
                    public void accept(StaffListResponse staffResponse) throws Exception {
                        staff.setValue(LiveDataWrapper.success(staffResponse));

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        staff.setValue(LiveDataWrapper.error(throwable,null));

                    }
                });
        addDisposable(disposable);

    }
    public LiveData<LiveDataWrapper<TestResponse>> getVersion(){
//        waitDialog = CreateDialog.waitingDialog(materialListActivity);
        final MediatorLiveData<LiveDataWrapper<TestResponse>> test = new MediatorLiveData<>();
        test.setValue(LiveDataWrapper.loading(null));
        Disposable disposable = apiService.getVersion()
                .flatMap(new ResultDataParse<TestResponse>())
                .compose(new RxSchedulerTransformer<TestResponse>())
                .subscribe(new Consumer<TestResponse>() {
                    @Override
                    public void accept(TestResponse testResponse) throws Exception {
                        test.setValue(LiveDataWrapper.success(testResponse));
//                        waitDialog.dismiss();
//                        new MyToast(materialListActivity).showinfo(testResponse.getVersion());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        test.setValue(LiveDataWrapper.error(throwable,null));
//                        waitDialog.dismiss();
//                        materialListActivity.handleException(throwable,false);
                    }
                });
        addDisposable(disposable);
        return test;
    }



    public LiveData<LiveDataWrapper<LoginInfo>>  loginInw(String name, String pw){
        final MediatorLiveData<LiveDataWrapper<LoginInfo>> loginLiveData = new MediatorLiveData<>();
        Disposable disposable = loginService.loginw("password", name, pw, BuildConfig.MDID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<LoginInfo>() {
                    @Override
                    protected void onStart() {
                        loginLiveData.setValue(LiveDataWrapper.loading(null));

                    }

                    @Override
                    public void onNext(LoginInfo loginInfo) {
                        preferenceManager.setCurrentUserAccessToken(loginInfo.getAccessToken());

                        loginLiveData.setValue(LiveDataWrapper.success(loginInfo));
                    }

                    @Override
                    public void onError(Throwable e) {
//
                        //还要保存到数据库，保存数据信息
                        preferenceManager.clearToken();
                        loginLiveData.setValue(LiveDataWrapper.error(e,null));

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        addDisposable(disposable);
        return loginLiveData;
    }
    public void goToMain(){
//        if ( !TextUtils.isEmpty(preferenceManager.getCurrentUserAccessToken()) && null != loginActivity){
//            if (loginActivity != null){
//                loginActivity.restComponet();
//                Intent mIntent = new Intent(loginActivity, MainActivity.class);
//                loginActivity.startActivity(mIntent);
//                loginActivity.finish();
//            }
//        }
    }

}
