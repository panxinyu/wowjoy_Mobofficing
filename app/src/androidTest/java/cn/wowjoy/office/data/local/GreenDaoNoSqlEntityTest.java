package cn.wowjoy.office.data.local;

import org.greenrobot.greendao.test.AbstractDaoTestStringPk;

import cn.wowjoy.office.data.local.GreenDaoNoSqlEntity;
import cn.wowjoy.office.data.local.GreenDaoNoSqlEntityDao;

public class GreenDaoNoSqlEntityTest extends AbstractDaoTestStringPk<GreenDaoNoSqlEntityDao, GreenDaoNoSqlEntity> {

    public GreenDaoNoSqlEntityTest() {
        super(GreenDaoNoSqlEntityDao.class);
    }

    @Override
    protected GreenDaoNoSqlEntity createEntity(String key) {
        GreenDaoNoSqlEntity entity = new GreenDaoNoSqlEntity();
        entity.setKey(key);
        return entity;
    }

}
