package cn.wowjoy.office.data.local.entity;

import org.greenrobot.greendao.test.AbstractDaoTestLongPk;

import cn.wowjoy.office.data.local.entity.UserInfo;
import cn.wowjoy.office.data.local.entity.UserInfoDao;

public class UserInfoTest extends AbstractDaoTestLongPk<UserInfoDao, UserInfo> {

    public UserInfoTest() {
        super(UserInfoDao.class);
    }

    @Override
    protected UserInfo createEntity(Long key) {
        UserInfo entity = new UserInfo();
        entity.setId(key);
        return entity;
    }

}
